# README #

** This work is in progress **

Current version: 0.1

playMe is a gamification framework that integrates gamification elements (players, points, badges, levels, rewards, etc) into websites.
playMe currently consists of 3 distinct modules:
1. **Core** module implements the actual gamification logic. It exposes services to other modules via RESTful APIs.
2. **Admin** module is a UI to allow for system admins to configure the system properly and view/modify data. It also allows users with fewer privileges to view some information, e.g. statistics
3. **Web** module is a demo site that demonstrates how gamification elements can be integrated into an e-shop.


### Installation ###

* Build is performed via Maven.
* Each module is built and deployed independently. For either admin or web modules to work, core module must be deployed as well.
* For deployment we use Tomcat. Each module has itw own set of properties that must be placed under conf folder in Tomcat.
* Each module has its own DB schema, which must be created in MySQL before deployments (scripts available under each module folder).

Each module comes with its own Docker container. Additionally two more modules exist to setup MySQL and ActiveMQ.
The commands to initiate all modules are as follows:

* docker run --name mysql -p 3306:3306 -p 33060:33060 -v /src/main/docker:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=root -t playme/playme-mysql
* docker run -d -p 8161:8161 -p 61616:61616 -p 5672:5672 -p 61613:61613 -p 1883:1883 -p 61614:61614 -t playme/playme-activemq
* docker run -p 9000:8080 --name core -t playme/playme-core
* docker run -p 9001:8080 --name admin -t playme/playme-admin
* docker run -p 9002:8080 --name web -t playme/playme-web


### Application URLs ###

Assuming that you have not specified another set of IPs/ports:

1. [http://192.168.99.100:9001/playMe-admin/login.action](Link URL)
2. [http://192.168.99.100:9002/playMe-demo/home.action#/index](Link URL)