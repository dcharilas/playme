package com.playMe.demo.constant;

public class Constants {

    /* Model attributes */
    public static final String propertiesAttrName = "properties";

    /* Session attributes */
    public static final String userAttrName = "user";
    public static final String rewardAttrName = "currentReward";

    /* Grid default configuration */
    public static final int defaultStartPage = 1;
    public static final int defaultPageSize = 10;

}
