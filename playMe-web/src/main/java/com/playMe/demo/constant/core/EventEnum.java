package com.playMe.demo.constant.core;

/**
 * List of supported events
 * 
 * @author Charilas
 *
 */
public enum EventEnum {

    REDEEM_POINTS,
	REGISTER_USER,
	UPDATE_USER_PROFILE,
	FACEBOOK_LOGIN,
	FACEBOOK_LIKE,
	FACEBOOK_SHARE,
	TWITTER_LOGIN,
	TWITTER_LIKE,
	TWITTER_SHARE,
	GOOGLEPLUS_LOGIN,
	GOOGLEPLUS_LIKE,
	GOOGLEPLUS_SHARE,
	USER_ACTION_ASSIGN_POINTS,
	USER_ACTION_ASSIGN_BADGE;

    public String value() {
        return name();
    }

    public static EventEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

