package com.playMe.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.demo.constant.core.EventEnum;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.RewardDisplay;
import com.playMe.demo.model.UserRewardDisplay;
import com.playMe.demo.model.core.*;
import com.playMe.demo.service.ProfileService;
import com.playMe.demo.service.RewardService;
import com.playMe.demo.service.util.EventUtil;
import com.playMe.demo.util.RestUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service that handles reward=related requests
 *
 * @author Dimitris Charilas
 */
@Service("rewardService")
public class RewardSercviceImpl implements RewardService {

    private static final Logger logger = LoggerFactory.getLogger(RewardSercviceImpl.class);

    private final static String rewardURL = "/services/reward/get/active";
    private final static String rewardItemURL = "/services/reward/get/id";
    private final static String userRewardURL = "/services/reward/get/user";
    private final static String redeemURL = "/services/event/external/insert";
    private static final String userServiceAPIEndpoint = "/services/user/get";

    private ObjectMapper objectMapper;

    @Autowired
    private ProfileService profileService;

    @Value("#{appProperties['core.url.context']}")
    private String coreContext;

    @Value("#{appProperties['playme.demo.application.id']}")
    private String applicationId;


    public RewardSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve application rewards
     *
     * @return
     */
    public List<RewardDisplay> getRewards(DemoUser user) {
        logger.info("getRewards()");

        try {
            String targetURL = coreContext + rewardURL +"/" +applicationId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                RewardCore[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(RewardCore[].class));

                /*
                 * Construct list for display
                 */
                List<RewardDisplay> displayList = new ArrayList<>();
                for (RewardCore reward : Arrays.asList(responseItem)) {
                    displayList.add(new RewardDisplay(reward, reward.getPointsreq() <= user.getUserCore().getPoints()));
                }

                return displayList;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve rewards.", e);
            return new ArrayList<RewardDisplay>();
        }
    }



    /**
     * Calls core REST service to retrieve a specific reward
     *
     * @return
     */
    public RewardDisplay getReward(DemoUser user, int rewardId) {
        logger.info("getReward()");

        try {
            String targetURL = coreContext + rewardItemURL +"/" +rewardId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                RewardCore responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(RewardCore.class));

                if (responseItem != null) {
                    return new RewardDisplay(responseItem, responseItem.getPointsreq() <= user.getUserCore().getPoints());
                } else {
                    return null;
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve rewards.", e);
            return null;
        }
    }


    /**
     * Calls core REST service to retrieve user rewards
     *
     * @param user
     * @return
     */
    public List<UserRewardDisplay> getUserRewards(DemoUser user) {
        logger.info("getUserRewards()");

        try {
            String targetURL = coreContext + userRewardURL +"/" +applicationId +"/" +user.getUserCore().getId();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                UserrewardCore[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserrewardCore[].class));

                /*
                 * Construct list for display
                 */
                List<UserRewardDisplay> displayList = new ArrayList<>();
                for (UserrewardCore userreward : Arrays.asList(responseItem)) {
                    displayList.add(new UserRewardDisplay(userreward));
                }

                return displayList;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user rewards.", e);
            return new ArrayList<UserRewardDisplay>();
        }
    }



    /**
     * Calls core REST service to redeem points in order to get reward
     *
     * @param user
     * @param rewardId
     * @return
     */
    public DemoUser redeemReward(DemoUser user, int rewardId) {
        logger.info("getUserRewards()");

        try {
            String targetURL = coreContext + redeemURL;
            logger.debug("Target url: " + targetURL);

           /*
            * Construct the event that should be sent to core
            * Numeric Value 1 -> reward id
            */
            ExternalEventCore event = EventUtil.createEvent(user, applicationId, EventEnum.REDEEM_POINTS);
            event.setNumericvalue1(rewardId);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, event);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ExternalEventCore createdEvent = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ExternalEventCore.class));

                /*
                 * Verify that an identifier was generated (stored in Textvalue1)
                 */
                if (createdEvent.getId() > 0 && !StringUtils.isEmpty(createdEvent.getTextvalue1())) {
                    /*
                     * Check if the reward was actually assisgned
                     * Record is retrieved based on its unique identifier
                     */
/*                    UserrewardCore reward = getUserReward(createdEvent.getTextvalue1());
                    if (reward == null) {
                        throw new Exception("No reward assignment record found.");
                    }*/
                    /*
                     * Now it is time to load the user
                     */
                    UserCore retrievedUser = profileService.getUser(user.getUsername());
                    /*
                     * Fill DemoUser object with additional information
                     */
                    if (retrievedUser != null) {
                        user.setUserCore(retrievedUser);
                    }

                    return user;
                } else {
                    return null;
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user rewards.", e);
            return null;
        }
    }



    /**
     * Calls core REST service to retrieve a specific user reward
     *
     * @param identifier
     * @return
     */
    private UserrewardCore getUserReward(String identifier) {
        logger.info("getUserReward()");

        try {
            String targetURL = coreContext + userRewardURL +"/" +identifier;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                UserrewardCore responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserrewardCore.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user reward.", e);
            return null;
        }
    }


}
