package com.playMe.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.demo.constant.core.EventEnum;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.RewardDisplay;
import com.playMe.demo.model.UserRewardDisplay;
import com.playMe.demo.model.core.*;
import com.playMe.demo.service.ProductService;
import com.playMe.demo.service.ProfileService;
import com.playMe.demo.service.RewardService;
import com.playMe.demo.service.util.EventUtil;
import com.playMe.demo.util.RestUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service that handles product-related requests
 *
 * @author Dimitris Charilas
 */
@Service("productService")
public class ProductSercviceImpl implements ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductSercviceImpl.class);

    private final static String eventURL = "/services/event/external/insert";

    private ObjectMapper objectMapper;

    @Autowired
    private ProfileService profileService;

    @Value("#{appProperties['core.url.context']}")
    private String coreContext;

    @Value("#{appProperties['playme.demo.application.id']}")
    private String applicationId;


    public ProductSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to assign points to user
     *
     * @param user
     * @param orderAmount
     * @return
     */
    public DemoUser assignPoints(DemoUser user, int orderAmount) {
        logger.info("getUserRewards()");

        try {
            String targetURL = coreContext + eventURL;
            logger.debug("Target url: " + targetURL);

           /*
            * Construct the event that should be sent to core
            * Numeric Value 1 -> points
            */
            ExternalEventCore event = EventUtil.createEvent(user, applicationId, EventEnum.USER_ACTION_ASSIGN_POINTS);
            event.setNumericvalue1(orderAmount);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, event);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ExternalEventCore createdEvent = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ExternalEventCore.class));

                if (createdEvent.getId() > 0) {
                    /*
                     * Now it is time to load the user
                     */
                    UserCore retrievedUser = profileService.getUser(user.getUsername());
                    /*
                     * Fill DemoUser object with additional information
                     */
                    if (retrievedUser != null) {
                        user.setUserCore(retrievedUser);
                    }

                    return user;
                } else {
                    return null;
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not assign points to user.", e);
            return null;
        }
    }


}
