package com.playMe.demo.service.util;

import com.playMe.demo.constant.core.EventEnum;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.core.ApplicationCore;
import com.playMe.demo.model.core.EventDefinitionCore;
import com.playMe.demo.model.core.ExternalEventCore;
import com.playMe.demo.model.core.UserCore;

import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class EventUtil {


    /**
     * Creates basic configuration for a new core event
     *
     * @param user
     * @param applicationId
     * @param type
     * @return
     */
    public static ExternalEventCore createEvent(DemoUser user, String applicationId, EventEnum type) {

        ExternalEventCore event = new ExternalEventCore();
        ApplicationCore application = new ApplicationCore();
        application.setId(Integer.parseInt(applicationId));
        event.setApplication(application);
        EventDefinitionCore eventDefinition = new EventDefinitionCore();
        eventDefinition.setName(type.name());
        event.setEventdefinition(eventDefinition);
        UserCore userCore = new UserCore();
        userCore.setUsername(user.getUserCore().getUsername());
        event.setUser(userCore);
        event.setEventdate(new Date());

        return event;
    }
}
