package com.playMe.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.demo.constant.core.EventEnum;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.exception.DuplicateEntryException;
import com.playMe.demo.model.core.ApplicationCore;
import com.playMe.demo.model.core.EventDefinitionCore;
import com.playMe.demo.model.core.ExternalEventCore;
import com.playMe.demo.model.core.UserCore;
import com.playMe.demo.model.form.LoginInfo;
import com.playMe.demo.model.form.RegisterInfo;
import com.playMe.demo.service.LoginService;
import com.playMe.demo.service.util.EventUtil;
import com.playMe.demo.util.RestUtil;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.playMe.demo.db.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * Service that handles register and login actions
 *
 * @author Dimitris Charilas
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    private final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    private static final String userServiceAPIEndpoint = "/services/user/login";
    private final static String addUserURL = "/services/event/external/insert";

    private ObjectMapper objectMapper;

    @Autowired
    private Mapper dozerBeanMapper;

    @Autowired
    private UserRepository userRepository;

    @Value("#{appProperties['core.url.context']}")
    private String coreContext;

    @Value("#{appProperties['playme.demo.application.id']}")
    private String applicationId;



    public LoginServiceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Inserts new user to DB of web application and sends request
     * to core module so that user is inserted in the game.
     *
     * @param registerInfo
     * @return
     */
    public DemoUser registerUser(RegisterInfo registerInfo) {

        logger.info("Inserting user with username " +registerInfo.getUsername() +" and password " +registerInfo.getPassword());

        DemoUser user = null;

        try {
            /*
             * Check first if user already exists
             */
            user = userRepository.findByUsernameAndPassword(registerInfo.getUsername(), registerInfo.getPassword());
            if (user != null) {
                // user already exists
                throw new DuplicateEntryException();
            }
            /*
             * User does not exist, so register him
             */
            user = new DemoUser();
            user.setRegDate(new Date());
            user.setUsername(registerInfo.getUsername());
            user.setPassword(registerInfo.getPassword());
            user.setFirstname(registerInfo.getFirstname());
            user.setLastname(registerInfo.getLastname());
            user.setEmail(registerInfo.getEmail());
            user = userRepository.save(user);

            logger.info("Demo user id " +user.getId());

            /*
             * If user is inserted successfully we need to call the core module
             */
            if (user != null && user.getId() != 0) {
                logger.info("Inserted user. Id is " +user.getId());
                String targetURL = coreContext +addUserURL;
                logger.info("targetURL = " +targetURL);

                /*
                 * Construct the event that should be sent to core
                 * Text Value 1 -> username
                 * Text Value 2 -> firstname
                 * Text Value 3 -> lastname
                 * Text Value 4 -> email
                 */
                ExternalEventCore event = EventUtil.createEvent(user, applicationId, EventEnum.REGISTER_USER);
                event.setTextvalue1(user.getUsername());
                event.setTextvalue2(user.getFirstname());
                event.setTextvalue3(user.getLastname());
                event.setTextvalue4(user.getEmail());
                /*
                 * Get the service response
                 */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, event);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    ExternalEventCore createdEvent = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(ExternalEventCore.class));

                    if (createdEvent.getId() > 0) {
                        /*
                         * Now it is time to load the user
                         */
                        targetURL = coreContext +userServiceAPIEndpoint +"/" +applicationId +"/username/" +user.getUsername();
                        logger.info("targetURL = " +targetURL);
                        response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
                        /*
                         * Parse the response
                         */
                        UserCore retrievedUser = objectMapper.readValue(response.getBody(),
                                objectMapper.getTypeFactory().constructType(UserCore.class));

                        logger.info(retrievedUser.toString());
                        /*
                         * Fill DemoUser object with additional information
                         */
                        user.setUserCore(retrievedUser);

                        return user;
                    } else {
                        return null;
                    }
                } else {
                    throw new Exception();
                }
            } else {
                logger.info("User is not inserted");
            }

        } catch (Exception e) {
            logger.error("Could not insert user.", e);
            return null;
        }

        return null;
    }


    /**
     * Loads a user from DB
     *
     * @param loginInfo
     * @return
     */
    public DemoUser loginUser(LoginInfo loginInfo) {

        logger.info("Loading user with username " +loginInfo.getUsername() +" and password " +loginInfo.getPassword());

        DemoUser user = null;

        try {
            user = userRepository.findByUsernameAndPassword(loginInfo.getUsername(),loginInfo.getPassword());
            /*
             * If user exists check that he exists in core DB as well
             */
            if (user != null) {
                String targetURL = coreContext +userServiceAPIEndpoint +"/" +applicationId +"/username/" +user.getUsername();
                logger.info("targetURL = " +targetURL);
                /*
                 * Get the service response
                 */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                   /*
                    * Parse the response
                    */
                    UserCore retrievedUser = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(UserCore.class));

                    logger.info(retrievedUser.toString());

                    /*
                     * Fill DemoUser object with additional information
                     */
                    user.setUserCore(retrievedUser);

                    return user;
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.info("User is not found");
            return null;
        }

        return user;
    }
}
