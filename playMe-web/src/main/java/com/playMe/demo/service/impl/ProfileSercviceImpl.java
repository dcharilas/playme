package com.playMe.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.db.repository.UserRepository;
import com.playMe.demo.model.BadgeDisplay;
import com.playMe.demo.model.ResponseItem;
import com.playMe.demo.model.core.*;
import com.playMe.demo.service.LeaderboardService;
import com.playMe.demo.service.ProfileService;
import com.playMe.demo.util.RestUtil;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Service that handles actions related to user profile
 *
 * @author Dimitris Charilas
 */
@Service("profileService")
public class ProfileSercviceImpl implements ProfileService {

    private static final Logger logger = LoggerFactory.getLogger(ProfileSercviceImpl.class);

    private static final String userServiceAPIEndpoint = "/services/user/get";
    private final static String getUserLevelURL = "/services/user/level/get";
    private final static String getUserBadgesURL = "/services/user/badges/get";
    private final static String getApplicationBadgesURL = "/services/badge/get/active";
    private final static String getLevelHistoryURL = "/services/user/level/history/get";
    private final static String getPointsHistoryURL = "/services/user/points/history/get";

    private final static String getPointsForGraphURL = "/services/statistics/get/user/points/";
    private final static String getRedeemablePointsForGraphURL = "/services/statistics/get/user/redeempoints";

    private final static String saveUserURL = "/services/user/insert/partial";

    private ObjectMapper objectMapper;

    @Autowired
    private Mapper dozerBeanMapper;

    @Autowired
    private UserRepository userRepository;

    @Value("#{appProperties['core.url.context']}")
    private String coreContext;

    @Value("#{appProperties['playme.demo.application.id']}")
    private String applicationId;


    public ProfileSercviceImpl() {
        objectMapper = new ObjectMapper();
    }



    /**
     * Calls core REST service to load the user
     *
     * @param username
     * @return
     */
    public UserCore getUser(String username) {
        logger.info("getUser(): (" +username +")");

        try {
            String targetURL = coreContext +userServiceAPIEndpoint +"/" +applicationId +"/username/" +username;
            logger.info("targetURL = " + targetURL);
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                UserCore retrievedUser = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserCore.class)); 
                logger.info(retrievedUser.toString());

                return retrievedUser;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve level.", e);
            return null;
        }
    }


    /**
     * Calls core REST service to retrieve user level
     *
     * @param userId
     * @return
     */
    public LevelCore getLevel(long userId) {
        logger.info("getLevel(): (" +userId +")");

        try {
            String targetURL = coreContext + getUserLevelURL +"/" +applicationId +"/" +userId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                LevelCore responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(LevelCore.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve level.", e);
            return null;
        }
    }


    /**
     * Calls core REST service to retrieve user level history
     *
     * @param userId
     * @return
     */
    public ResponseItem getLevelHistory(long userId) {
        logger.info("getLevelHistory(): (" +userId +")");

        try {
            String targetURL = coreContext + getLevelHistoryURL +"/" +applicationId +"/" +userId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem<UserlevelprogressionCore> responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve level history.", e);
            return new ResponseItem(0, new ArrayList<UserlevelprogressionCore>());
        }
    }



    /**
     * Calls core REST service to retrieve user points history
     *
     * @param userId
     * @return
     */
    public ResponseItem getPointsHistory(long userId) {
        logger.info("getPointsHistory(): (" +userId +")");

        try {
            String targetURL = coreContext + getPointsHistoryURL +"/" +applicationId +"/" +userId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem<UserpointshistoryCore> responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve points history.", e);
            return new ResponseItem(0, new ArrayList<UserpointshistoryCore>());
        }
    }



    /**
     * Calls core REST service to retrieve user points history for the graph
     *
     * @param userId
     * @param totalPoints
     * @return
     */
    public List<StatisticsDateItem> getPointsPerDayStatistics(long userId, boolean totalPoints) {
        logger.info("getDateStatistics():" +userId +"," +totalPoints);

        String targetURL = coreContext;

        try {
           /*
            * Call the proper statistics method
            */
            if (totalPoints) {
                targetURL += getPointsForGraphURL +"/" +applicationId +"/" +userId;
            } else {
                targetURL += getRedeemablePointsForGraphURL +"/" +applicationId +"/" +userId;
            }

            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                StatisticsDateItem[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(StatisticsDateItem[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve statistics.", e);
            return null;
        }
    }



    /**
     * Calls core REST service to retrieve badges for display
     *
     * @param userId
     * @return
     */
    public List<BadgeDisplay> getBadges(long userId) {
        logger.info("getBadges(): (" +userId +")");

        try {
            /*
             * First get all application badges that are currently active
             */
            String targetURL = coreContext + getApplicationBadgesURL +"/" +applicationId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                BadgeCore[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(BadgeCore[].class));
                /*
                 * Now get user badges
                 */
                List<BadgeCore> userBadges = getUserBadges(userId);
                List<BadgeDisplay> finalList = constructBadgesForDisplay(userBadges, Arrays.asList(responseItem));

                return finalList;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve available badges.", e);
            return new ArrayList<BadgeDisplay>();
        }
    }



    /**
     * Calls core REST service to retrieve user badges
     *
     * @param userId
     * @return
     */
    private List<BadgeCore> getUserBadges(long userId) {
        logger.info("getUserBadgesURL(): (" +userId +")");

        try {
            String targetURL = coreContext + getUserBadgesURL +"/" +applicationId +"/" +userId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                BadgeCore[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(BadgeCore[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user badges.", e);
            return new ArrayList<BadgeCore>();
        }
    }


    /**
     * Accepts two lists of badges
     *    - all badges that are currently active in the application
     *    - the badges currently assigned to the user
     * Based on these two list the final list for display badges is constructed.
     *
     * @param userBadges
     * @param applicationBadges
     * @return
     */
    private List<BadgeDisplay> constructBadgesForDisplay(List<BadgeCore> userBadges, List<BadgeCore> applicationBadges) {
        List<BadgeDisplay> displayList = new ArrayList<>();

        /*
         * First set the isActivated flag, depending on whether the user has the badge or not
         */
        boolean isActivated;
        for (BadgeCore applBadge : applicationBadges) {
            isActivated = false;
            for (BadgeCore userBadge : userBadges) {
                if (userBadge.getId() == applBadge.getId()) {
                    isActivated = true;
                    break;
                }
            }
            displayList.add(new BadgeDisplay(applBadge, isActivated));
        }
        /*
         * Now check for the scenario where the user has a badge that is currently not active.
         * This should be added additionally
         */
        boolean alreadyAdded;
        for (BadgeCore userBadge : userBadges) {
            alreadyAdded = false;
            for (BadgeDisplay displayBadge : displayList) {
                if (userBadge.getId() == displayBadge.getId()) {
                    alreadyAdded = true;
                    break;
                }
            }
            if (!alreadyAdded) {
                displayList.add(new BadgeDisplay(userBadge, true));
            }
        }

        return displayList;
    }


    /**
     * Calls core REST service to ssve user information
     *
     * @param user
     * @return
     */
    public DemoUser saveUser(DemoUser user) {
        logger.info("saveUser(): (" +user.getUsername() +")");

        String targetURL = "";

        try {
            /*
             * First save the DemoUser object
             */
            userRepository.save(user);
            /*
             * Now update the core user as well
             */
            targetURL = coreContext + saveUserURL;
            logger.info("Target url: " + targetURL);
            /*
             * Get core user entity
             */
            UserCore userCore = dozerBeanMapper.map(user, UserCore.class);
            ApplicationCore applicationCore = new ApplicationCore();
            applicationCore.setId(Integer.parseInt(applicationId));
            userCore.setApplication(applicationCore);
            /*
             * Get the service response
             */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, userCore);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                UserCore retrievedUser = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserCore.class));

                logger.info(retrievedUser.toString());

                /*
                 * Fill DemoUser object with additional information
                 */
                user.setUserCore(retrievedUser);

                return user;
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add user.", e);
        }

        return user;
    }

}
