package com.playMe.demo.service;

import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.BadgeDisplay;
import com.playMe.demo.model.ResponseItem;
import com.playMe.demo.model.core.LevelCore;
import com.playMe.demo.model.core.StatisticsDateItem;
import com.playMe.demo.model.core.UserCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface ProfileService {

    UserCore getUser(String username);

    LevelCore getLevel(long userId);

    ResponseItem getLevelHistory(long userId);

    ResponseItem getPointsHistory(long userId);

    List<BadgeDisplay> getBadges(long userId);

    DemoUser saveUser(DemoUser user);

    List<StatisticsDateItem> getPointsPerDayStatistics(long userId, boolean totalPoints);
}
