package com.playMe.demo.service;

import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.RewardDisplay;
import com.playMe.demo.model.UserRewardDisplay;
import com.playMe.demo.model.core.RewardCore;
import com.playMe.demo.model.core.UserrewardCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface RewardService {

    List<RewardDisplay> getRewards(DemoUser user);

    List<UserRewardDisplay> getUserRewards(DemoUser user);

    RewardDisplay getReward(DemoUser user, int rewardId);

    DemoUser redeemReward(DemoUser user, int rewardId);
}
