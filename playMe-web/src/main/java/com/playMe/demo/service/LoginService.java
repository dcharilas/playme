package com.playMe.demo.service;

import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.form.LoginInfo;
import com.playMe.demo.model.form.RegisterInfo;

/**
 * @author Dimitris Charilas
 */
public interface LoginService {

    DemoUser registerUser(RegisterInfo registerInfo);

    DemoUser loginUser(LoginInfo loginInfo);
}
