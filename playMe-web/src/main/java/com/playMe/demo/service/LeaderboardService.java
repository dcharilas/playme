package com.playMe.demo.service;

import com.playMe.demo.model.ResponseItem;

/**
 * @author Dimitris Charilas
 */
public interface LeaderboardService {

    ResponseItem getLeaderboard(int page, int pagesize);
}
