package com.playMe.demo.service;

import com.playMe.demo.db.entity.DemoUser;

/**
 * @author Dimitris Charilas
 */
public interface ProductService {

    DemoUser assignPoints(DemoUser user, int orderAmount);

}
