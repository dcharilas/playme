package com.playMe.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.demo.model.ResponseItem;
import com.playMe.demo.model.core.LeaderboardItemCore;
import com.playMe.demo.service.LeaderboardService;
import com.playMe.demo.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Service that loads leaderboard
 *
 * @author Dimitris Charilas
 */
@Service("leaderboardService")
public class LeaderboardSercviceImpl implements LeaderboardService {

    private static final Logger logger = LoggerFactory.getLogger(LeaderboardSercviceImpl.class);

    private final static String searchURL = "/services/leaderboard/get";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreContext;

    @Value("#{appProperties['playme.demo.application.id']}")
    private String applicationId;


    public LeaderboardSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve leaderboard
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getLeaderboard(int page, int pagesize) {
        logger.info("getLeaderboard(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreContext + searchURL +"/" +applicationId +"/paging/" +page +"/" +pagesize;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem<LeaderboardItemCore> responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve leaderboard.", e);
            return new ResponseItem(0, new ArrayList<LeaderboardItemCore>());
        }
    }




}
