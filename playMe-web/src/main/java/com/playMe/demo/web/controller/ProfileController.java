package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.BadgeDisplay;
import com.playMe.demo.model.ResponseItem;
import com.playMe.demo.model.core.LevelCore;
import com.playMe.demo.model.core.StatisticsDateItem;
import com.playMe.demo.service.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the profile pages.
 *
 * @author Dimitris Charilas
 */
@Controller
@RequestMapping(value = "/profile")
public class ProfileController {

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    private ProfileService profileService;


    /**
     * Get method for profile page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getProfilePage() {
        logger.info("getProfilePage()");
        return "pages/profile";
    }

    /**
     * Get method for personal info page
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String getProfileInfoPage() {
        logger.info("getProfileInfoPage()");
        return "pages/personalInfo";
    }

    /**
     * Get method for personal info page
     */
    @RequestMapping(value = "/badges", method = RequestMethod.GET)
    public String getBadgesPage() {
        logger.info("getBadgesPage()");
        return "pages/badges";
    }


    /**
     * Get method for personal info page
     */
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String getHistoryPage() {
        logger.info("getHistoryPage()");
        return "pages/history";
    }


    /**
     * Get user from session
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public @ResponseBody DemoUser getUserObject(HttpServletRequest request) {
        return getUser(request);
    }


    /**
     * Get user level
     */
    @RequestMapping(value = "/level", method = RequestMethod.GET)
    public @ResponseBody LevelCore getLevel(HttpServletRequest request) {

        DemoUser user = getUser(request);
        if (user != null) {
            return profileService.getLevel(user.getUserCore().getId());
        }
        return null;
    }


    /**
     * Get user badges
     */
    @RequestMapping(value = "/badges/list", method = RequestMethod.GET)
    public @ResponseBody List<BadgeDisplay> getBadges(HttpServletRequest request) {

        DemoUser user = getUser(request);
        if (user != null) {
            return profileService.getBadges(user.getUserCore().getId());
        }
        return null;
    }


    /**
     * Get user level progression history
     */
    @RequestMapping(value = "/history/levels", method = RequestMethod.POST)
    public @ResponseBody ResponseItem getLevelHistory(HttpServletRequest request) {

        DemoUser user = getUser(request);
        if (user != null) {
            return profileService.getLevelHistory(user.getUserCore().getId());
        }
        return null;
    }


    /**
     * Get user points progression history
     */
    @RequestMapping(value = "/history/points", method = RequestMethod.POST)
    public @ResponseBody ResponseItem getPointsHistory(HttpServletRequest request) {

        DemoUser user = getUser(request);
        if (user != null) {
            return profileService.getPointsHistory(user.getUserCore().getId());
        }
        return null;
    }


    /**
     * Saves the personal information of current user
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity saveUserInfo(@RequestBody DemoUser user, HttpServletRequest request){
        logger.info("saveUserInfo():");
        DemoUser response = profileService.saveUser(user);
        if (response != null) {
            /*
             * Save user in session
             */
            HttpSession session = request.getSession();
            session.setAttribute(Constants.userAttrName, user);

            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns total points per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/points/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getPointsPerDay(HttpServletRequest request){
        DemoUser user = getUser(request);
        if (user != null && user.getUserCore() != null) {
            return profileService.getPointsPerDayStatistics(user.getUserCore().getId(),true);
        }
        return null;
    }


    /**
     * Returns redemable points per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/redeempoints/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getRedeemPointsPerDay(HttpServletRequest request){
        DemoUser user = getUser(request);
        if (user != null && user.getUserCore() != null) {
            return profileService.getPointsPerDayStatistics(user.getUserCore().getId(),false);
        }
        return null;
    }


    /**
     * Gets user object from session
     *
     * @param request
     * @return
     */
    private DemoUser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        DemoUser user = (DemoUser) session.getAttribute(Constants.userAttrName);
        return user;
    }

}
