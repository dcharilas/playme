package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.model.ResponseItem;
import com.playMe.demo.model.SearchOption;
import com.playMe.demo.service.LeaderboardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * Handles requests for the leaderboard page.
 *
 * @author Dimitris Charilas
 */
@Controller
@RequestMapping(value = "/leaderboard")
public class LeaderboardController {

	private static final Logger logger = LoggerFactory.getLogger(LeaderboardController.class);

    @Autowired
    private LeaderboardService leaderboardService;


    /**
     * Get method for leaderboard page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getLeaderboardPage(Model model) {
        logger.info("getLeaderboardPage()");
        return "pages/leaderboard";
    }


    /**
     * Returns the leaderboard
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getLeaderboard(@RequestBody SearchOption searchOption, HttpServletRequest request){
        logger.info("getLeaderboard(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        return leaderboardService.getLeaderboard(page,pagesize);
    }


    /**
     * Returns the leaderboard without pagination
     *
     * @return
     */
    @RequestMapping(value = "/load/single", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getLeaderboard(HttpServletRequest request){
        logger.info("getLeaderboard()");
        int page = Constants.defaultStartPage;
        int pagesize = Constants.defaultPageSize;
        return leaderboardService.getLeaderboard(page,pagesize);
    }

}
