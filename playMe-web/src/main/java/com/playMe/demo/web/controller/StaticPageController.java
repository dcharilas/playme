package com.playMe.demo.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for all static pages
 *
 * @author Dimitris Charilas
 */
@Controller
public class StaticPageController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    /**
     * Get method for contact page
     */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String getContactPage() {
        logger.info("getContactPage()");
        return "pages/contact";
    }

    /**
     * Get method for cart page
     */
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String getCartPage() {
        logger.info("getCartPage()");
        return "pages/cart";
    }


    /**
     * Get method for under construction page
     */
    @RequestMapping(value = "/underConstr", method = RequestMethod.GET)
    public String getUnderConstrPage() {
        logger.info("getUnderConstrPage()");
        return "pages/underConstr";
    }
}
