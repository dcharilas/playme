package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.RewardDisplay;
import com.playMe.demo.model.UserRewardDisplay;
import com.playMe.demo.model.core.UserrewardCore;
import com.playMe.demo.service.RewardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the reward pages.
 *
 * @author Dimitris Charilas
 */
@Controller
@RequestMapping(value = "/rewards")
public class RewardController {

    private static final Logger logger = LoggerFactory.getLogger(RewardController.class);

    @Autowired
    private RewardService rewardService;


    /**
     * Get method for rewards page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getRewardsPage() {
        logger.info("getRewardsPage()");
        return "pages/rewards";
    }

    /**
     * Get method for the rewards history page
     */
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String getRewardsHistoryPage() {
        logger.info("getRewardsHistoryPage()");
        return "pages/rewardsHistory";
    }


    /**
     * Get method for reward details page
     */
    @RequestMapping(value="item", method = RequestMethod.GET)
    public String getRewardPage(HttpServletRequest request) {
        logger.info("getRewardPage()");
        return "pages/reward";
    }


    /**
     * Load reward details from DB
     */
    @RequestMapping(value="item/load", method = RequestMethod.POST)
    public @ResponseBody RewardDisplay loadReward(HttpServletRequest request,
              @RequestBody int id) {
        logger.info("loadReward(): " + id);

        RewardDisplay reward = rewardService.getReward(getUser(request),id);
        /*
         * Save reward in session
         */
        HttpSession session = request.getSession();
        session.setAttribute(Constants.rewardAttrName, reward);

        return reward;
    }


    /**
     * Get reward from session
     */
    @RequestMapping(value = "/item/details", method = RequestMethod.GET)
    public @ResponseBody RewardDisplay getRewardObject(HttpServletRequest request) {
        logger.info("getRewardObject()");
        return getReward(request);
    }


    /**
     * Get rewards
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<RewardDisplay> getRewards(HttpServletRequest request) {
        logger.info("getRewards()");
        DemoUser user = getUser(request);
        if (user != null) {
            return rewardService.getRewards(user);
        }
        return null;
    }


    /**
     * Get user rewards
     */
    @RequestMapping(value = "/history/list", method = RequestMethod.GET)
    public @ResponseBody List<UserRewardDisplay> getUserRewards(HttpServletRequest request) {
        logger.info("getUserRewards()");
        DemoUser user = getUser(request);
        if (user != null) {
            return rewardService.getUserRewards(user);
        }
        return null;
    }


    /**
     * Redeem points to get reward
     */
    @RequestMapping(value="redeem", method = RequestMethod.POST)
    public @ResponseBody DemoUser redeemReward(HttpServletRequest request,
            @RequestBody int id) {
        logger.info("redeemReward(): "+id);
        DemoUser user = rewardService.redeemReward(getUser(request), id);
        if (user != null) {
            /*
             * Save user in session
             */
            HttpSession session = request.getSession();
            session.setAttribute(Constants.userAttrName, user);
            return user;
        }
        return null;
    }


    /**
     * Gets user object from session
     *
     * @param request
     * @return
     */
    private DemoUser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        DemoUser user = (DemoUser) session.getAttribute(Constants.userAttrName);
        return user;
    }

    /**
     * Gets reward object from session
     *
     * @param request
     * @return
     */
    private RewardDisplay getReward(HttpServletRequest request) {
        HttpSession session = request.getSession();
        RewardDisplay reward = (RewardDisplay) session.getAttribute(Constants.rewardAttrName);
        return reward;
    }

}
