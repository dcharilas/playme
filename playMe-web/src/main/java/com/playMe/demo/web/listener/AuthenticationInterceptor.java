package com.playMe.demo.web.listener;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.db.entity.DemoUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Interceptor for user authentication purposes
 *
 * @author Charilas
 *
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {

        logger.debug("AuthenticationInterceptor: Pre-handle");

        String path = request.getRequestURI().replace(request.getContextPath(),"");

        /*
         * Check if user exists in session
         */
        DemoUser user = getUser(request);
        if (user == null) {
            /*
             * If user is not logged in forbid restricted pages
             */
            if (path.contains("profile")) {
                response.sendRedirect(request.getContextPath() +"/home.action#login");
                return false;
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
           HttpServletResponse response, Object handler,
           ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request,
          HttpServletResponse response, Object handler, Exception ex) {


    }


    private DemoUser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        DemoUser user = (DemoUser) session.getAttribute(Constants.userAttrName);
        return user;
    }
}