package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.model.WebProperties;
import com.playMe.demo.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the home page.
 *
 * @author Dimitris Charilas
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;

    /**
     * Get method for home page
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView getHomePage(ModelAndView model) {
        logger.info("getHomePage()");
        /*
         * Fill properties
         */
        WebProperties props = new WebProperties();
        props.setCoreURL(coreURL);
        model.addObject(Constants.propertiesAttrName, JsonUtil.toJacksonJSON(props));
        model.setViewName("home");
        return model;
    }

    /**
     * Get method for index page
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String getIndexPage(Model model) {
        logger.info("getIndexPage()");
        return "pages/index";
    }

}
