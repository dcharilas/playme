package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Handles requests for the product page.
 *
 * @author Dimitris Charilas
 */
@Controller
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;


    /**
     * Get method for single product page
     */
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String getProductPage(/*@RequestParam("id") String id*/) {
        logger.info("getProductPage()");
        return "pages/product";
    }


    /**
     * Dummy method to place an order.
     * The actual functionality is to send the order cost to core so
     * that points are offered to user
     *
     * @return
     */
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity placeOrder(HttpServletRequest request, @RequestBody int total) {
        logger.info("placeOrder():" +total);
        DemoUser user = productService.assignPoints(getUser(request),total);
        if (user != null) {
            /*
             * Save user in session
             */
            HttpSession session = request.getSession();
            session.setAttribute(Constants.userAttrName, user);

            return new ResponseEntity(user, HttpStatus.OK);
        } else {
            logger.info("Order could not be placed");
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Gets user object from session
     *
     * @param request
     * @return
     */
    private DemoUser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        DemoUser user = (DemoUser) session.getAttribute(Constants.userAttrName);
        return user;
    }

}
