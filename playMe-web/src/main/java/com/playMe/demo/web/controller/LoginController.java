package com.playMe.demo.web.controller;

import com.playMe.demo.constant.Constants;
import com.playMe.demo.db.entity.DemoUser;
import com.playMe.demo.model.form.LoginInfo;
import com.playMe.demo.model.form.RegisterInfo;
import com.playMe.demo.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Handles requests for the login page.
 */
@Controller
public class LoginController  {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;


    @ModelAttribute
    public LoginInfo loginInfo() {
        LoginInfo loginForm = new LoginInfo();
        return loginForm;
    }

	/**
	 * Get method for login page
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(Model model) {
		logger.info("getLoginPage()");
		return "pages/login";
	}

    /**
     * Get method for register page
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterPage(Model model) {
        logger.info("getRegisterPage()");
        return "pages/register";
    }

    /**
     * Registers a new user
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @RequestMapping(value = "/register/save", method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {

        logger.info("registerUser(): " +registerInfo.getUsername() +"," +registerInfo.getPassword());

        DemoUser user;

        if (registerInfo.getUsername() != null && registerInfo.getPassword() != null) {
            /*
             * Perform the insert
             */
            user = loginService.registerUser(registerInfo);
            if (user != null) {
                logger.info("Register user with username " +user.getUsername()  +" and id " +user.getId());
                /*
                 * Save user in session
                 */
                HttpSession session = request.getSession();
                session.setAttribute(Constants.userAttrName, user);

                return new ResponseEntity(user, HttpStatus.OK);
            } else {
                logger.info("User could not be logged in");
                return new ResponseEntity(null, HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }



    /**
     * Login for existing users
     *
     * @param request
     * @param loginInfo
     * @return
     */
    @RequestMapping(value = "/login/authenticate", method = RequestMethod.POST)
    public ResponseEntity loginUser(@RequestBody LoginInfo loginInfo, HttpServletRequest request) {

        logger.info("loginUser(): " +loginInfo.getUsername() +"," +loginInfo.getPassword());

        DemoUser user;

        if (loginInfo.getUsername() != null && loginInfo.getPassword() != null) {
            /*
             * Perform the insert
             */
            user = loginService.loginUser(loginInfo);
            if (user != null) {
                logger.info("Logged in user with username " +loginInfo.getUsername()  +" and password " +loginInfo.getPassword());
                /*
                 * Save user in session
                 */
                HttpSession session = request.getSession();
                session.setAttribute(Constants.userAttrName, user);

                return new ResponseEntity(user, HttpStatus.OK);
            } else {
                logger.info("User could not be logged in");
                return new ResponseEntity(null, HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Remove user from session
     *
     * @param request
     */
    @RequestMapping(value = "login/invalidate", method = RequestMethod.GET)
    public void logoutUser(HttpServletRequest request, HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();
        DemoUser user = (DemoUser) session.getAttribute(Constants.userAttrName);
        session.setAttribute(Constants.userAttrName, null);
        //response.sendRedirect("home");
    }
}
