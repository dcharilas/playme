package com.playMe.demo.model.core;

import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class UserrewardCore implements java.io.Serializable {

	private long id;
	private RewardCore reward;
	private ApplicationCore application;
	private ExternalEventCore redeemevent;
	private ExternalEventCore deliverevent;
	private UserCore user;
	private Date redeemdate;
	private Date deliverdate;
    private String identifier;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RewardCore getReward() {
        return reward;
    }

    public void setReward(RewardCore reward) {
        this.reward = reward;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public ExternalEventCore getRedeemevent() {
        return redeemevent;
    }

    public void setRedeemevent(ExternalEventCore redeemevent) {
        this.redeemevent = redeemevent;
    }

    public ExternalEventCore getDeliverevent() {
        return deliverevent;
    }

    public void setDeliverevent(ExternalEventCore deliverevent) {
        this.deliverevent = deliverevent;
    }

    public UserCore getUser() {
        return user;
    }

    public void setUser(UserCore user) {
        this.user = user;
    }

    public Date getRedeemdate() {
        return redeemdate;
    }

    public void setRedeemdate(Date redeemdate) {
        this.redeemdate = redeemdate;
    }

    public Date getDeliverdate() {
        return deliverdate;
    }

    public void setDeliverdate(Date deliverdate) {
        this.deliverdate = deliverdate;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return "RewardCore [ id=" + id
                + ", identifier=" + identifier
                + (application != null ? application.toString() : "")
                + (reward != null ? reward.toString() : "")
                + (user != null ? user.toString() : "")
                + ", redeemdate=" + redeemdate
                + (redeemevent != null ? redeemevent.toString() : "")
                + ", deliverdate=" + deliverdate
                + (deliverevent != null ? deliverevent.toString() : "")
                + "]";
    }
}
