package com.playMe.demo.model.core;


import java.io.Serializable;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class UserlevelprogressionCore implements Serializable {


    private ProgressionKey id;
    private Date unlockdate;
    private ApplicationCore application;
    private LevelCore level;
    private UserCore user;



    public class ProgressionKey {

        private long userid;
        private int levelid;
        private int applicationid;


        public long getUserid() {
            return userid;
        }

        public void setUserid(long userid) {
            this.userid = userid;
        }

        public int getLevelid() {
            return levelid;
        }

        public void setLevelid(int levelid) {
            this.levelid = levelid;
        }

        public int getApplicationid() {
            return applicationid;
        }

        public void setApplicationid(int applicationid) {
            this.applicationid = applicationid;
        }
    }


    public ProgressionKey getId() {
        return id;
    }

    public void setId(ProgressionKey id) {
        this.id = id;
    }

    public Date getUnlockdate() {
        return unlockdate;
    }

    public void setUnlockdate(Date unlockdate) {
        this.unlockdate = unlockdate;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public LevelCore getLevel() {
        return level;
    }

    public void setLevel(LevelCore level) {
        this.level = level;
    }

    public UserCore getUser() {
        return user;
    }

    public void setUser(UserCore user) {
        this.user = user;
    }


    @Override
    public String toString() {
        return "UserlevelprogressionCore [user=" + (id!= null ? id.getUserid() : "") +
                ", application=" + (id != null ? id.getApplicationid() : "") +
                ", level=" + (id != null ? id.levelid : "") +
                ", unlockdate=" + unlockdate+ "]";
    }
}
