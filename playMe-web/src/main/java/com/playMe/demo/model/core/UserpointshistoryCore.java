package com.playMe.demo.model.core;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class UserpointshistoryCore implements Serializable {

    private long id;
    private int points;
    private Date updatedate;
    private UserCore user;
    private ApplicationCore application;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public UserCore getUser() {
        return user;
    }

    public void setUser(UserCore user) {
        this.user = user;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }


    @Override
    public String toString() {
        return "UserpointshistoryCore [ id=" + id +", points=" + points +
                ", updatedate=" + updatedate + "]";
    }
}
