package com.playMe.demo.model;

import com.playMe.demo.model.core.RewardCore;

/**
 * @author Dimitris Charilas
 */
public class RewardDisplay extends RewardCore {

    private boolean isActivated;


    public RewardDisplay(RewardCore reward, boolean isActivated) {
        this.setActiveimage(reward.getActiveimage());
        this.setInactiveimage(reward.getInactiveimage());
        this.setId(reward.getId());
        this.setDescriptionid(reward.getDescriptionid());
        this.setDescriptions(reward.getDescriptions());
        this.setName(reward.getName());
        this.setPointsreq(reward.getPointsreq());

        this.isActivated = isActivated;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }
}
