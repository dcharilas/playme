package com.playMe.demo.model.core;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class StatisticsDateItem<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private Date date;
    private T value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
