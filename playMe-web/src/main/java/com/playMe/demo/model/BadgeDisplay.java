package com.playMe.demo.model;

import com.playMe.demo.model.core.BadgeCore;

/**
 * @author Dimitris Charilas
 */
public class BadgeDisplay extends BadgeCore {

    private boolean isActivated;


    public BadgeDisplay (BadgeCore badge, boolean isActivated) {
        this.setActiveimage(badge.getActiveimage());
        this.setInactiveimage(badge.getInactiveimage());
        this.setId(badge.getId());
        this.setDescriptionid(badge.getDescriptionid());
        this.setDescriptions(badge.getDescriptions());
        this.setName(badge.getName());

        this.isActivated = isActivated;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }
}
