package com.playMe.demo.model.core;


/**
 * @author Dimitris Charilas
 */
public class LeaderboardItemCore {

    private String username;
    private byte[] avatar;
    private int points;
    private LevelCore level;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LevelCore getLevel() {
        return level;
    }

    public void setLevel(LevelCore level) {
        this.level = level;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
