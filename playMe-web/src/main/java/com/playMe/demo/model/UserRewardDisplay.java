package com.playMe.demo.model;

import com.playMe.demo.model.core.UserrewardCore;

import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class UserRewardDisplay extends RewardDisplay {


    private Date redeemDate;


    public UserRewardDisplay(UserrewardCore userReward) {
        super(userReward.getReward(), true);
        this.redeemDate = userReward.getRedeemdate();
    }

    public Date getRedeemDate() {
        return redeemDate;
    }

    public void setRedeemDate(Date redeemDate) {
        this.redeemDate = redeemDate;
    }
}
