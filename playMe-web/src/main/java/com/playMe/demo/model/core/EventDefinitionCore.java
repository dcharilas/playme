package com.playMe.demo.model.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
public class EventDefinitionCore implements Serializable {

    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "EventDefinitionCore [id=" + id + ", name=" + name +", "
                + "]";
    }
}
