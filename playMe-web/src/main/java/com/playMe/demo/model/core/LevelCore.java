package com.playMe.demo.model.core;

import java.io.Serializable;
import java.util.List;

/**
 * Class representing the User Blob retrieved from core module
 *
 */
public class LevelCore implements Serializable {
	private static final long serialVersionUID = 1L;

    private int id;
    private int descriptionid;
    private byte[] image;
    private String name;
    private List<DisplayTextCore> descriptions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDescriptionid() {
        return descriptionid;
    }

    public void setDescriptionid(int descriptionid) {
        this.descriptionid = descriptionid;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DisplayTextCore> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DisplayTextCore> descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        return "LevelCore [id=" + id + ", name=" + name + ", descriptionid=" + descriptionid+ "]";
    }
}

