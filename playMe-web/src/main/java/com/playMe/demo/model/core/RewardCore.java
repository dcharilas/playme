package com.playMe.demo.model.core;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public class RewardCore implements java.io.Serializable {

	private int id;
    private ApplicationCore application;
    private String name;
	private byte[] activeimage;
	private byte[] inactiveimage;
	private int descriptionid;
	private Integer pointsreq;
	private Boolean active;
    private List<DisplayTextCore> descriptions;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getActiveimage() {
        return activeimage;
    }

    public void setActiveimage(byte[] activeimage) {
        this.activeimage = activeimage;
    }

    public byte[] getInactiveimage() {
        return inactiveimage;
    }

    public void setInactiveimage(byte[] inactiveimage) {
        this.inactiveimage = inactiveimage;
    }

    public int getDescriptionid() {
        return descriptionid;
    }

    public void setDescriptionid(int descriptionid) {
        this.descriptionid = descriptionid;
    }

    public Integer getPointsreq() {
        return pointsreq;
    }

    public void setPointsreq(Integer pointsreq) {
        this.pointsreq = pointsreq;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<DisplayTextCore> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DisplayTextCore> descriptions) {
        this.descriptions = descriptions;
    }


    @Override
    public String toString() {
        return "RewardCore [active=" + active +", "
                + (application != null ? application.toString() : "")
                + ", pointsreq=" + pointsreq
                + ", name=" + name
                + "]";
    }
}
