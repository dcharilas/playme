package com.playMe.demo.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author Dimitris Charilas
 */
public class JsonUtil {

    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

    private final static String DATE_FORMAT = "dd/MM/yyyy";

    /**
     * Convert any given Java object to its corresponding JSON string format
     * using the Jackson mapper.
     *
     * @param <T> the input class type
     * @param object the instance of the Java object to convert to JSON
     *
     * @return the JSON representation of the input Java object
     */
    public static <T> String toJacksonJSON(T object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT));

        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String json = "";
        try {
            json = ow.writeValueAsString(object);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return json;
    }

}
