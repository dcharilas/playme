package com.playMe.demo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * @author Dimitris Charilas
 */
public class RestUtil {

    private final static Logger logger = LoggerFactory.getLogger(RestUtil.class);

    private static RestTemplate restTemplate;

    public static RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public static void setRestTemplate(RestTemplate restTemplate) {
        RestUtil.restTemplate = restTemplate;
    }

    /**
     * Calls a specified RESTful service
     *
     * @param targetURL
     * @return
     */
    public static ResponseEntity callRestUrl(String targetURL, HttpMethod httpMethod, Object form) {
        MultiValueMap<String, Object> body = null;
        /*
         * Construct headers
         */
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
        /*
         * Perform the call. If body exists then send it as JSON
         */
        HttpEntity requestEntity = form != null ? new HttpEntity(form,requestHeaders) : new HttpEntity(requestHeaders);
        ResponseEntity<byte[]> result = restTemplate.exchange(targetURL, httpMethod, requestEntity, byte[].class);
        return result;
    }
}
