package com.playMe.demo.db.repository;

import com.playMe.demo.db.entity.DemoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserRepository extends JpaRepository<DemoUser,Long> {

    DemoUser findByUsernameAndPassword(String username, String password);

}
