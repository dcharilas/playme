<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<jsp:include page="templates/head.jsp"></jsp:include>

<script>
    var PROPERTIES = ${properties};
    var contextPath = "${pageContext.request.contextPath}";
</script>

<body ng-app="playMe" ng-controller="MainCtrl">

<div id="Container1">
    <div id="Toolbar" ng-controller="HeaderCtrl">
        <jsp:include page="templates/header.jsp"></jsp:include>
    </div>

    <div id="Container2">
<%--        <div id="Banner">
            <div id="Identity">
                <a href="#"><img src="<spring:url value="/resources/images/logo.jpg" />" alt="levelUp" /></a>
            </div><!-- Identity -->
        </div><!-- Banner -->--%>
        <div id="Container3">

            <div ng-view id="Main">

            </div> <!-- Main -->
        </div><!-- Container3 -->
    </div><!-- Container2 -->
</div><!-- Container1 -->

<div id="Footer" ng-controller="FooterCtrl">
    <jsp:include page="templates/footer.jsp"></jsp:include>
</div><!-- Footer -->


</body>
</html>