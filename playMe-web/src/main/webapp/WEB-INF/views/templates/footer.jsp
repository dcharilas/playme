\<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="Footer">
    <div class="footer">
        <div class="container">
            <div class="footer-top-at">

                <div class="col-md-4 amet-sed">
                    <h4>MORE INFO</h4>
                    <ul class="nav-bottom">
                        <li><a href="#underConstr">How to order</a></li>
                        <li><a href="#underConstr">FAQ</a></li>
                        <li><a href="#contact">Location</a></li>
                        <li><a href="#underConstr">Shipping</a></li>
                        <li><a href="#underConstr">Membership</a></li>
                    </ul>
                </div>
                <div class="col-md-4 amet-sed ">
                    <h4>CONTACT US</h4>
                    <p>Contrary to popular belief</p>
                    <p>The standard chunk</p>
                    <p>office: +12 34 995 0792</p>
                    <ul class="social">
                        <li><a href="#underConstr"><i> </i></a></li>
                        <li><a href="#underConstr"><i class="twitter"> </i></a></li>
                        <li><a href="#underConstr"><i class="rss"> </i></a></li>
                        <li><a href="#underConstr"><i class="gmail"> </i></a></li>

                    </ul>
                </div>
                <div class="col-md-4 amet-sed">
                    <h4>Newsletter</h4>

                    <p>Sign Up to get all news update
                        and promo</p>

                    <form>
                        <input type="text" value="" onfocus="this.value='';"
                               onblur="if (this.value == '') {this.value ='';}">
                        <input type="submit" value="Sign up">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="footer-class">
            <p>© 2015 New store All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a>
            </p>
        </div>
    </div>
</div>
