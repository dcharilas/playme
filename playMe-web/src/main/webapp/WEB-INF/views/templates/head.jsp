<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
    <meta charset="utf-8">
    <title>levelUP Demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- CSS -->
    <link data-require="ng-table@*" data-semver="0.3.0" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ng-table/0.3.3/ng-table.min.css" />
    <link data-require="bootstrap-css@*" data-semver="3.3.4" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/flexslider/2.2/flexslider.css">

    <link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/style.css" />" media="screen, projection">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Jquery libraries -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/flexslider/2.2/jquery.flexslider-min.js"></script>

    <!-- Angular libraries -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.1/angular.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-route.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bower-angular-translate/2.6.1/angular-translate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bower-angular-translate-loader-static-files/2.6.1/angular-translate-loader-static-files.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ng-table/0.3.3/ng-table.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-messages.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-cookies.min.js"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.0/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.53/responsiveslides.min.js"></script>

    <!-- Other libs -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/simplecartjs/3.0.5/simplecart.min.js"></script>

    <!-- Graphs (D3) -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/liquidFillGauge.js" />"></script>

    <!-- Custom scripts -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/main.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/grid.js" />"></script>

    <!-- Services -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/services/LoginService.js" />"></script>

    <!-- Controllers -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/LoginCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/CommonCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/IndexCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/ProductCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/StaticPageCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/LeaderboardCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/ProfileCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/RewardCtrl.js" />"></script>

</head>