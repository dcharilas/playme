<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="header">
    <div class="header-top">
        <div class="container">
            <div id="tLang">
                <form method="get" action="">
                    <div>
                        <select id="LanguageSelection" ng-model="language" ng-change="changeLanguage(language.value)" ng-options="c.name for c in languages"></select>
                    </div>
                </form>
            </div><!-- tLang -->
            <div class="search">
                <form>
                    <input type="text" value="Search " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                    <input type="submit" value="Go">
                </form>
            </div>
            <div class="header-left">
                <ul>
                    <a ng-show="isLoggedIn()">{{ 'loginMsg.welcome' | translate}} {{$root.globals.currentUser.username}}</a>
                    <li><a href="#login" ng-hide="isLoggedIn()">{{ 'loginMsg.login' | translate}}</a></li>
                    <li><a href="#register" ng-hide="isLoggedIn()">{{ 'loginMsg.register' | translate}}</a></li>
                    <li><a href="#" ng-click="logout()" ng-show="isLoggedIn()">{{ 'loginMsg.logout' | translate}}</a></li>
                </ul>
                <div class="cart box_1">
                    <a href="#cart">
                        <h3> <div class="total">
                            <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                            <img src="<spring:url value="/resources/images/cart.png" />" alt=""/></h3>
                    </a>
                    <p><a href="javascript:;" class="simpleCart_empty">{{ 'loginMsg.emptyCart' | translate}}</a></p>

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="container">
        <div class="head-top">
            <div class="logo">
                <a href="index.html"><img src="<spring:url value="/resources/images/logo.png" />" alt=""></a>
            </div>

            <!-- Draw Menu -->
            <div id="Nav" class="navbar-collapse collapse navigacija navbar-ex1-collapse h_menu4" ng-controller="MenuCtrl">
                <ul id="menu-header" class="nav navbar-nav navbar-left skyblue">
                    <li ng-repeat="item in navLinks" class="dropdown {{item.cls}}">
                        <a href="{{item.href}}" class="dropdown-toggle {{item.color}}" data-toggle="dropdown" ng-show="showPage(item.requiresLogin)" translate>{{item.text}}</a>
                        <!-- Submenu-->
                        <ul role="menu" class="dropdown-menu" ng-show="item.subItems">
                            <li ng-repeat="subItem in item.subItems">
                                <span class="{{subItem.cls}} pull-left"></span>
                                <a href="{{subItem.href}}" class="{{item.color}}" translate>{{subItem.text}}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>

