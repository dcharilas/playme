<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!--content-->
<div class=" container">
    <div class=" register" id="registerContainer" ng-controller="RegisterCtrl">
        <h1>{{ 'loginMsg.register' | translate}}</h1>
        <form ng-submit="register()" role="form">
            <div class="col-md-3">
                <img class="masqot-login" src="<spring:url value="/resources/images/masqot-register.jpg" />" alt="">
            </div>
            <div class="col-md-4 register-top-grid">
                <h3>{{ 'loginMsg.personalInfo' | translate}}</h3>
                <div>
                    <span>{{ 'loginMsg.firstname' | translate}}</span>
                    <input type="text" ng-model="form.firstname" required>
                </div>
                <div>
                    <span>{{ 'loginMsg.lastname' | translate}}</span>
                    <input type="text" ng-model="form.lastname" required>
                </div>
                <div>
                    <span>{{ 'loginMsg.email' | translate}}</span>
                    <input type="text" ng-model="form.email" required>
                </div>
            </div>
            <div class="col-md-4 register-bottom-grid">
                <h3>{{ 'loginMsg.loginInfo' | translate}}</h3>
                <div>
                    <span>{{ 'loginMsg.username' | translate}}</span>
                    <input type="text" ng-model="form.username" required>
                </div>
                <div>
                    <span>{{ 'loginMsg.password' | translate}}</span>
                    <input type="password" ng-model="form.password" required>
                </div>
                <div>
                    <span>{{ 'loginMsg.confirmPassword' | translate}}</span>
                    <input type="password" required>
                </div>
                <input type="submit" value="submit">

            </div>
            <div class="clearfix"> </div>
        </form>
    </div>
</div>