<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container">
    <div class="content-menu register">

        <table>
            <tr>
                <div>
                    <a class="profile-text font-bold">{{ 'loginMsg.welcome' | translate}} {{user.username}} !</a>
                    <a class="profile-text"> {{ 'profileMsg.currentPoints1' | translate}}</a>
                    <a class="profile-points">{{user.userCore.points}}</a>
                    <a class="profile-text">{{ 'profileMsg.currentPoints2' | translate}}.</a>
                </div>
            </tr>
            <tr>
                <div>
                    <a class="profile-text"> {{ 'profileMsg.redeemPoints1' | translate}}</a>
                    <a class="profile-points">{{user.userCore.redeemablepoints}}</a>
                    <a class="profile-text">{{ 'profileMsg.redeemPoints2' | translate}}</a>
                </div>
            </tr>
            <tr>
                <div>
                    <a class="profile-text">{{ 'profileMsg.keepUp' | translate}}</a>
                </div>
            </tr>
            <tr>
                <li class="col-md-9 levelWrapper">
                    <div class="level-icon">
                        <img ng-src="data:image/jpeg;base64,{{level.image}}">
                    </div>
                    <div class="level-title"> {{level.name}}
                        <div class="level-info">
                            <div>{{level.description}}</div>
                        </div>
                    </div>
                </li>
            </tr>
        </table>

    </div>
</div>
