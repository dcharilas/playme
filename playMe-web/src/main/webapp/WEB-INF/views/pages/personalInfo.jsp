<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container">

<div class="vd_content-section clearfix">
<div class="row">
<div class="col-sm-12">
<div class="panel widget light-widget">
<div class="panel-heading no-title"></div>
<form class="form-horizontal" role="form" ng-submit="saveHandler()" novalidate>
<div class="panel-body">
<h2 class="mgbt-xs-20"><span class="font-semibold">{{ 'profileMsg.profileOf' | translate}} : {{user.username}}</span></h2>
<br/>

<div class="row">
    <div class="col-sm-3 mgbt-xs-20">
        <div class="form-group">
            <div class="col-xs-12">

                <div class="form-img text-center mgbt-xs-15">
                    <!-- If image does not exist show default one -->
                    <img class="profile-avatar" ng-src="{{user.avatar && user.avatar || 'resources/images/user.png'}}" id="photo-id"/>
                </div>

                <div id="browseBtn" class="form-img-action text-center mgbt-xs-20">
                    <a class='btn vd_btn  vd_bg-blue' href='javascript:;'>
                        <i class="fa fa-cloud-upload append-icon glyphicon glyphicon-cloud-upload"></i>
                        {{ 'profileMsg.upload' | translate}}
                        <input id="fileUploadButton" type="file" name="file_source" size="40" fileread="user.avatar"
                               onchange="angular.element(this).scope().uploadHandler(this)" />
                    </a>
                </div>

                <br/>

                <div>
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr>
                            <td>{{ 'profileMsg.memberSince' | translate}}</td>
                            <td>{{user.regDate | date:'MMM d, y'}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div id="completeness">
                    <svg id="fillgauge"></svg>
                    <span>{{ 'profileMsg.profileCompl' | translate}}</span>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-9" style="padding-left: 40px;">
    <h3 class="mgbt-xs-15">{{ 'profileMsg.accountSettings' | translate}}</h3>


    <input type="hidden" class="form-control" ng-model="user.id">


    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'loginMsg.email' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="email" class="form-control" placeholder="email@yourcompany.com" ng-model="user.email">
                </div>
                <!-- col-xs-12 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group hidden">
        <label class="col-sm-3 control-label">{{ 'loginMsg.username' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="username" ng-model="user.username">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">


                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'loginMsg.password' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="password" class="width-40 form-control" placeholder="password" ng-model="user.password">
                </div>
                <!-- col-xs-12 -->
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'loginMsg.confirmPassword' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="password" class="width-40 form-control">
                </div>
                <!-- col-xs-12 -->
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <hr/>
    <h3 class="mgbt-xs-15">{{ 'profileMsg.profileSettings' | translate}}</h3>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'loginMsg.firstname' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" ng-model="user.firstname">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'loginMsg.lastname' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" ng-model="user.lastname">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'profileMsg.gender' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9 radio-div">
                  <span class="vd_radio radio-info radio-butto">
                    <input type="radio" checked="" value="M" id="optionsRadios3" name="optionsRadios2" ng-model="user.gender">
                    <label for="optionsRadios3"> {{ 'profileMsg.male' | translate}} </label>
                  </span>
                  <span class="vd_radio  radio-info radio-butto">
                    <input type="radio" value="F" id="optionsRadios4" name="optionsRadios2" ng-model="user.gender">
                    <label for="optionsRadios4"> {{ 'profileMsg.female' | translate}} </label>
                  </span>
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <div class="form-group">
        <label class="col-sm-3 control-label date-label">{{ 'profileMsg.birthdate' | translate}}</label>

        <div class="col-sm-9 controls date-input">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9 input-group input-append date">
                    <input type="text" class="form-control" id="inputBirthDateModal" ng-model="user.birthdate"/>
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <!-- col-xs-12 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->


    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'profileMsg.phone' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" ng-model="user.phone">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->


    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'profileMsg.address' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" ng-model="user.address">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->


    <div class="form-group">
        <label class="col-sm-3 control-label">{{ 'profileMsg.postcode' | translate}}</label>

        <div class="col-sm-9 controls">
            <div class="row mgbt-xs-0">
                <div class="col-xs-9">
                    <input type="text" class="form-control" ng-model="user.postcode">
                </div>
                <!-- col-xs-9 -->
                <div class="col-xs-2">

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- col-sm-10 -->
    </div>
    <!-- form-group -->

    <hr/>


    </div>
    <!-- col-sm-12 -->
</div>
<!-- row -->

</div>
<!-- panel-body -->
<div class="pd-20">
    <button type="submit" class="btn vd_btn vd_bg-green col-md-offset-3">
        <span class="menu-icon glyphicon glyphicon-ok">
            <i class="fa fa-fw fa-check"></i>
        </span> {{ 'profileMsg.save' | translate}}
    </button>
</div>
</form>
</div>
<!-- Panel Widget -->
</div>
<!-- col-sm-12-->
</div>
<!-- row -->

</div>
<!-- .vd_content-section -->


</div>


<script language="JavaScript">
    var scope = $('#Main').scope();

    /**
     * Draw profile completeness gauge
     */
    scope.$watch("user", function(n, o){
        if (exists(scope.user)) {
            var config2 = liquidFillGaugeDefaultSettings();
            var gauge2= loadLiquidFillGauge("fillgauge", scope.user.userCore.completeness, config2);
            config2.circleColor = "#D4AB6A";
            config2.textColor = "#553300";
            config2.waveTextColor = "#805615";
            config2.waveColor = "#AA7D39";
            config2.circleThickness = 0.1;
            config2.circleFillGap = 0.2;
            config2.textVertPosition = 0.8;
            config2.waveAnimateTime = 2000;
            config2.waveHeight = 0.3;
            config2.waveCount = 1;
        }
    });
</script>