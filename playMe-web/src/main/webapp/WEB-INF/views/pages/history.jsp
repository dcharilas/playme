<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container">

    <div class="row" style="margin-top: 25px">
        <div class="col-sm-5 mgbt-xs-20 pull-left">

            <div id="gridArea">
                <table ng-table="tableParams" class="table">
                    <tr ng-repeat="history in $data">
                        <td data-title="'Update Date'" filter="{ 'Date': 'text' }">{{history.updatedate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
                        <td data-title="'Points'">
                            <span ng-class="{ 'plus': history.points >= 0,'minus': history.points < 0 }">{{history.points}}</span>
                        </td>
                    </tr>
                </table>
            </div>

        </div> <!-- column -->
        <div class="col-sm-5 mgbt-xs-20 pull-right">

            <div id="gridArea2">
                <table ng-table="tableParams2" class="table">
                    <tr ng-repeat="history in $data">
                        <td data-title="'Unlock Date'" filter="{ 'Date': 'text' }">{{history.unlockdate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
                        <td data-title="'Level'">{{history.level.name}}</td>
                    </tr>
                </table>
            </div>
        </div> <!-- column -->

   </div> <!-- row -->

    <div id="pointsChart" style="margin-left: 10%;"></div>

</div>


<script>

function addAxesAndLegend (svg, xAxis, yAxis, margin, chartWidth, chartHeight) {
    var legendWidth  = 230,
        legendHeight = 80;

    // clipping to make sure nothing appears behind legend
    svg.append('clipPath')
            .attr('id', 'axes-clip')
            .append('polygon')
            .attr('points', (-margin.left)                 + ',' + (-margin.top)                 + ' ' +
                    (chartWidth - legendWidth - 1) + ',' + (-margin.top)                 + ' ' +
                    (chartWidth - legendWidth - 1) + ',' + legendHeight                  + ' ' +
                    (chartWidth + margin.right)    + ',' + legendHeight                  + ' ' +
                    (chartWidth + margin.right)    + ',' + (chartHeight + margin.bottom) + ' ' +
                    (-margin.left)                 + ',' + (chartHeight + margin.bottom));

    var axes = svg.append('g')
            .attr('clip-path', 'url(#axes-clip)');

    axes.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + chartHeight + ')')
            .call(xAxis);

    axes.append('g')
            .attr('class', 'y axis')
            .call(yAxis)
            .append('text')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text('Points');

    var legend = svg.append('g')
            .attr('class', 'legend')
            .attr('transform', 'translate(' + (chartWidth - legendWidth) + ', 0)');

    legend.append('rect')
            .attr('class', 'legend-bg')
            .attr('width',  legendWidth)
            .attr('height', legendHeight);

    legend.append('rect')
            .attr('class', 'outer')
            .attr('width',  75)
            .attr('height', 20)
            .attr('x', 10)
            .attr('y', 10);

    legend.append('text')
            .attr('x', 115)
            .attr('y', 25)
            .text('Total');

    legend.append('rect')
            .attr('class', 'inner')
            .attr('width',  75)
            .attr('height', 20)
            .attr('x', 10)
            .attr('y', 40);

    legend.append('text')
            .attr('x', 115)
            .attr('y', 55)
            .text('Redeemable');
}

function drawPaths (svg, data, x, y) {

    var upperOuterArea = d3.svg.area()
            .interpolate('basis')
            .x (function (d) { return x(d.date) || 1; })
            .y0(function (d) { return y(d.total); })
            .y1(function (d) { return y(0); });

    var lowerInnerArea = d3.svg.area()
            .interpolate('basis')
            .x (function (d) { return x(d.date) || 1; })
            .y0(function (d) { return y(d.redeem); })
            .y1(function (d) { return y(0); });


    svg.datum(data);

    svg.append('path')
            .attr('class', 'area upper outer')
            .attr('d', upperOuterArea)
            .attr('clip-path', 'url(#rect-clip)');

    svg.append('path')
            .attr('class', 'area lower inner')
            .attr('d', lowerInnerArea)
            .attr('clip-path', 'url(#rect-clip)');

}

function addMarker (marker, svg, chartHeight, x) {
    var radius = 40,
            xPos = x(marker.date) - radius - 3,
            yPosStart = chartHeight - radius - 3,
            yPosEnd = (marker.type === 'Client' ? 80 : 160) + radius - 3;

    var markerG = svg.append('g')
            .attr('class', 'marker '+marker.type.toLowerCase())
            .attr('transform', 'translate(' + xPos + ', ' + yPosStart + ')')
            .attr('opacity', 0);

    markerG.transition()
            .duration(1000)
            .attr('transform', 'translate(' + xPos + ', ' + yPosEnd + ')')
            .attr('opacity', 1);

    markerG.append('path')
            .attr('d', 'M' + radius + ',' + (chartHeight-yPosStart) + 'L' + radius + ',' + (chartHeight-yPosStart))
            .transition()
            .duration(1000)
            .attr('d', 'M' + radius + ',' + (chartHeight-yPosEnd) + 'L' + radius + ',' + (radius*2));

    markerG.append('circle')
            .attr('class', 'marker-bg')
            .attr('cx', radius)
            .attr('cy', radius)
            .attr('r', radius);

    markerG.append('text')
            .attr('x', radius)
            .attr('y', radius*0.9)
            .text('New Level!');

    markerG.append('text')
            .attr('class', 'marker-level')
            .attr('x', radius)
            .attr('y', radius*1.5)
            .text(marker.level);
}

function startTransitions (svg, chartWidth, chartHeight, rectClip, markers, x) {
    rectClip.transition()
            .duration(1000*markers.length)
            .attr('width', chartWidth);

    markers.forEach(function (marker, i) {
        setTimeout(function () {
            addMarker(marker, svg, chartHeight, x);
        }, 1000 + 500*i);
    });
}

function makeChart (data, markers) {
    var svgWidth  = 960,
            svgHeight = 500,
            margin = { top: 20, right: 20, bottom: 40, left: 40 },
            chartWidth  = svgWidth  - margin.left - margin.right,
            chartHeight = svgHeight - margin.top  - margin.bottom;

    var x = d3.time.scale().range([0, chartWidth])
                    .domain(d3.extent(data, function (d) { return d.date; })),
            y = d3.scale.linear().range([chartHeight, 0])
                    .domain([0, 1.3 * d3.max(data, function (d) { return d.total; })]);

    var xAxis = d3.svg.axis().scale(x).orient('bottom')
.innerTickSize(-chartHeight).outerTickSize(0).tickPadding(10),
            yAxis = d3.svg.axis().scale(y).orient('left')
                    .innerTickSize(-chartWidth).outerTickSize(0).tickPadding(10);

    var svg = d3.select('#pointsChart').append('svg')
            .attr('width',  svgWidth)
            .attr('height', svgHeight)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // clipping to start chart hidden and slide it in later
    var rectClip = svg.append('clipPath')
            .attr('id', 'rect-clip')
            .append('rect')
            .attr('width', 0)
            .attr('height', chartHeight);

    addAxesAndLegend(svg, xAxis, yAxis, margin, chartWidth, chartHeight);
    drawPaths(svg, data, x, y);
    startTransitions(svg, chartWidth, chartHeight, rectClip, markers, x);
}


/**
 * Draw graph
 */
var scope = $('#Main').scope();
scope.$watch("graphData", function(n, o){
    var grapshdata = scope.graphData;
    var markerdata = scope.markerData;
    if (exists(grapshdata)) {
        makeChart(grapshdata, markerdata);
    }
});


</script>
