<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="product">
<div class="container">
<div class="col-md-3 product-price">

    <div class=" rsidebar span_1_of_left">
        <div class="of-left">
            <h3 class="cate">Categories</h3>
        </div>
        <ul class="menu">
            <li class="item1"><a href="#">Men </a>
                <ul class="cute">
                    <li class="subitem1"><a href="#product">Cute Kittens </a></li>
                    <li class="subitem2"><a href="#product">Strange Stuff </a></li>
                    <li class="subitem3"><a href="#product">Automatic Fails </a></li>
                </ul>
            </li>
            <li class="item2"><a href="#">Women </a>
                <ul class="cute">
                    <li class="subitem1"><a href="#product">Cute Kittens </a></li>
                    <li class="subitem2"><a href="#product">Strange Stuff </a></li>
                    <li class="subitem3"><a href="#product">Automatic Fails </a></li>
                </ul>
            </li>
            <li class="item3"><a href="#">Kids</a>
                <ul class="cute">
                    <li class="subitem1"><a href="#product">Cute Kittens </a></li>
                    <li class="subitem2"><a href="#product">Strange Stuff </a></li>
                    <li class="subitem3"><a href="#product">Automatic Fails</a></li>
                </ul>
            </li>
            <li class="item4"><a href="#">Accesories</a>
                <ul class="cute">
                    <li class="subitem1"><a href="#product">Cute Kittens </a></li>
                    <li class="subitem2"><a href="#product">Strange Stuff </a></li>
                    <li class="subitem3"><a href="#product">Automatic Fails</a></li>
                </ul>
            </li>

            <li class="item4"><a href="#">Shoes</a>
                <ul class="cute">
                    <li class="subitem1"><a href="#product">Cute Kittens </a></li>
                    <li class="subitem2"><a href="#product">Strange Stuff </a></li>
                    <li class="subitem3"><a href="#product">Automatic Fails </a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--initiate accordion-->
    <script type="text/javascript">
        $(function () {
            var menu_ul = $('.menu > li > ul'),
                    menu_a = $('.menu > li > a');
            menu_ul.hide();
            menu_a.click(function (e) {
                e.preventDefault();
                if (!$(this).hasClass('active')) {
                    menu_a.removeClass('active');
                    menu_ul.filter(':visible').slideUp('normal');
                    $(this).addClass('active').next().stop(true, true).slideDown('normal');
                } else {
                    $(this).removeClass('active');
                    $(this).next().stop(true, true).slideUp('normal');
                }
            });

        });
    </script>

    <div class="sellers">


    </div>
    <!---->
    <div class="product-bottom">
        <div class="of-left-in">
            <h3 class="best">Best Sellers</h3>
        </div>
        <div class="product-go">
            <div class=" fashion-grid">
                <a href="#"><img class="img-responsive " src="<spring:url value="/resources/images/p1.jpg" />" alt=""></a>

            </div>
            <div class=" fashion-grid1">
                <h6 class="best2"><a href="#">Lorem ipsum dolor sit
                    amet consectetuer </a></h6>

                <span class=" price-in1"> $40.00</span>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="product-go">
            <div class=" fashion-grid">
                <a href="#"><img class="img-responsive " src="<spring:url value="/resources/images/p2.jpg" />" alt=""></a>

            </div>
            <div class="fashion-grid1">
                <h6 class="best2"><a href="#">Lorem ipsum dolor sit
                    amet consectetuer </a></h6>

                <span class=" price-in1"> $40.00</span>
            </div>

            <div class="clearfix"></div>
        </div>

    </div>
</div>
<div class="col-md-9 product-price1">
    <div class="col-md-5 single-top">
        <div class="flexslider">
            <ul class="slides">
                <li data-thumb="<spring:url value="/resources/images/si.jpg" />">
                    <img src="<spring:url value="/resources/images/si.jpg" />"/>
                </li>
                <li data-thumb="<spring:url value="/resources/images/si1.jpg" />">
                    <img src="<spring:url value="/resources/images/si1.jpg" />"/>
                </li>
                <li data-thumb="<spring:url value="/resources/images/si2.jpg" />">
                    <img src="<spring:url value="/resources/images/si2.jpg" />"/>
                </li>
                <li data-thumb="<spring:url value="/resources/images/si.jpg" />">
                    <img src="<spring:url value="/resources/images/si.jpg" />"/>
                </li>
            </ul>
        </div>

        <script>
            // Can also be used with $(document).ready()
            jQuery(document).ready(function($){
                $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: "thumbnails"
                });
            });
        </script>
    </div>
    <div class="col-md-7 single-top-in simpleCart_shelfItem">
        <div class="single-para ">
            <h4>Evening Dress</h4>

            <div class="star-on">
                <ul class="star-footer">
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i> </i></a></li>
                </ul>
                <div class="review">
                    <a href="#"> 1 customer review </a>

                </div>
                <div class="clearfix"></div>
            </div>

            <h5 class="item_price">$ 95.00</h5>

            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                diam nonummy nibh euismod tincidunt ut laoreet dolore
                magna aliquam erat </p>

            <div class="available">
                <ul>
                    <li>Color
                        <select>
                            <option>Silver</option>
                            <option>Black</option>
                            <option>Dark Black</option>
                            <option>Red</option>
                        </select></li>
                    <li class="size-in">Size<select>
                        <option>Large</option>
                        <option>Medium</option>
                        <option>small</option>
                        <option>Large</option>
                        <option>small</option>
                    </select></li>
                    <div class="clearfix"></div>
                </ul>
            </div>
            <ul class="tag-men">
                <li><span>TAG</span>
                    <span class="women1">: Women,</span></li>
                <li><span>SKU</span>
                    <span class="women1">: CK09</span></li>
            </ul>
            <a href="#" class="add-cart item_add" ng-show="isLoggedIn()">{{ 'rewardMsg.addCart' | translate}}</a>

        </div>
    </div>
    <div class="clearfix"></div>
    <!---->
    <div class="cd-tabs">

    </div>

</div>

<div class="clearfix"></div>
</div>
</div>