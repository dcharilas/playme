<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="container">

    <div class="content-menu badgesWrapper column-xs-2 column-sm-3 height-xs-4">
        <div data-rel="scroll">
            <ul class="list-wrapper">

                <li ng-repeat="reward in rewards" class="col-md-3">
                    <div class="reward-icon">
                        <img ng-src="data:image/jpeg;base64,{{reward.activeimage}}" >
                    </div>
                    <div class="menu-text"> {{reward.name}} ({{reward.pointsreq}} {{ 'rewardMsg.points' | translate}})
                        <div class="menu-info">
                            <div>{{ 'rewardMsg.redeemedOn' | translate}} {{reward.redeemDate | date}}</div>
                            <%--<div>{{reward.description}}</div>--%>
                        </div>
                    </div>
                </li>

            </ul>
        </div>

    </div>
</div>