<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container">
    <div class="check">
        <h1>My Shopping Bag (2)</h1>
        <div class="col-md-9 cart-items">

            <script>$(document).ready(function(c) {
                $('.close1').on('click', function(c){
                    $('.cart-header').fadeOut('slow', function(c){
                        $('.cart-header').remove();
                    });
                });
            });
            </script>
            <div class="cart-header">
                <div class="close1"> </div>
                <div class="cart-sec simpleCart_shelfItem">
                    <div class="cart-item cyc">
                        <img src="<spring:url value="/resources/images/pic1.jpg" />" class="img-responsive" alt=""/>
                    </div>
                    <div class="cart-item-info">
                        <h3><a href="#underConstr">Mountain Hopper(XS R034)</a><span>Model No: 3578</span></h3>
                        <ul class="qty">
                            <li><p>Size : 5</p></li>
                            <li><p>Qty : 1</p></li>
                        </ul>

                        <div class="delivery">
                            <span>{{ 'cartMsg.delMessage' | translate}}</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <script>$(document).ready(function(c) {
                $('.close2').on('click', function(c){
                    $('.cart-header2').fadeOut('slow', function(c){
                        $('.cart-header2').remove();
                    });
                });
            });
            </script>
            <div class="cart-header2">
                <div class="close2"> </div>
                <div class="cart-sec simpleCart_shelfItem">
                    <div class="cart-item cyc">
                        <img src="<spring:url value="/resources/images/pic2.jpg" />" class="img-responsive" alt=""/>
                    </div>
                    <div class="cart-item-info">
                        <h3><a href="#underConstr">Mountain Hopper(XS R034)</a><span>Model No: 3578</span></h3>
                        <ul class="qty">
                            <li><p>Size : 5</p></li>
                            <li><p>Qty : 1</p></li>
                        </ul>
                        <div class="delivery">
                            <span>{{ 'cartMsg.delMessage' | translate}}</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <div class="col-md-3 cart-total">
            <a class="continue" href="#underConstr">{{ 'cartMsg.contBasket' | translate}}</a>
            <div class="price-details">
                <h3>{{ 'cartMsg.priceDetails' | translate}}</h3>
                <span>{{ 'cartMsg.total' | translate}}</span>
                <span class="total1">{{amount}}</span>
                <span>{{ 'cartMsg.discount' | translate}}</span>
                <span class="total1">---</span>
                <span>{{ 'cartMsg.delCharges' | translate}}</span>
                <span class="total1">{{delivery}}</span>
                <div class="clearfix"></div>
            </div>
            <ul class="total_price">
                <li class="last_price"> <h4>{{ 'cartMsg.total' | translate}}</h4></li>
                <li class="last_price"><span>{{total}}</span></li>
                <div class="clearfix"> </div>
            </ul>

            <div class="clearfix"></div>
            <a class="order" href="#" ng-click="placeOrder(total)" ng-show="isLoggedIn()">{{ 'cartMsg.placeOrder' | translate}}</a>

        </div>

        <div class="clearfix"> </div>
    </div>
</div>
