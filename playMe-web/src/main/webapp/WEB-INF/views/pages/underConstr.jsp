<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="container">
    <div class="account">
        <h1>{{ 'titleMsg.underConstr' | translate}}</h1>
        <img class="construction-img" src="<spring:url value="/resources/images/Under-construction.png"/>"/>
        <div class="clearfix"> </div>
    </div>
</div>