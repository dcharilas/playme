<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div id="banner" class="banner-image-1">
    <div class="container">
        <script>
            $(function () {
                $("#slider").responsiveSlides({
                    auto: true,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks",
                    pager: true,
                    after: function(){
                        if (exists(document.getElementById("banner"))) {
                            var active_rel = $("." + this.namespace + "1_on").attr("image");
                            document.getElementById("banner").className = "ng-scope " +active_rel;
                        }
                    }
                });
            });
        </script>
        <div id="top" class="callbacks_container">
            <ul class="rslides" id="slider">
                <li image="banner-image-1">
                    <div class="banner-text">
                        <h3>Lorem Ipsum is not simply dummy </h3>

                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                            classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a
                            Latin professor .</p>
                        <a href="#product">Learn More</a>
                    </div>
                    <%--<img src="<spring:url value="/resources/images/carousel_1.jpg" />"/>--%>
                </li>

                <li image="banner-image-2">
                    <div class="banner-text">
                        <h3>There are many variations </h3>

                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                            classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a
                            Latin professor .</p>
                        <a href="#product">Learn More</a>
                    </div>
                    <%--<img src="<spring:url value="/resources/images/carousel_2.jpg" />"/>--%>
                </li>

                <li image="banner-image-3">
                    <div class="banner-text">
                        <h3>Sed ut perspiciatis unde omnis</h3>

                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                            classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a
                            Latin professor .</p>
                        <a href="#product">Learn More</a>
                        <%--<img src="<spring:url value="/resources/images/carousel_3.png" />"/>--%>
                    </div>

                </li>
            </ul>
        </div>

    </div>
</div>

<!--content-->
<div class="content">
    <div class="container">
        <div class="content-top">
            <h1>NEW RELEASED</h1>

            <div class="grid-in">
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi.jpg" />" alt="">
                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>T-Shirt</span>
                            </h3>
                        </div>
                    </a>


                    <p><a href="#product">Contrary to popular</a></p>
                </div>
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi1.jpg" />" alt="">
                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>Shoe</span>
                            </h3>
                        </div>
                    </a>

                    <p><a href="#product">classical Latin</a></p>
                </div>
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi2.jpg" />" alt="">
                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>Bag</span>
                            </h3>
                        </div>
                    </a>

                    <p><a href="#product">undoubtable</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="grid-in">
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi3.jpg" />" alt="">

                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>Shirt</span>
                            </h3>
                        </div>
                    </a>

                    <p><a href="#product">suffered alteration</a></p>
                </div>
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi4.jpg" />" alt="">

                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>Bag</span>
                            </h3>
                        </div>
                    </a>

                    <p><a href="#product">Content here</a></p>
                </div>
                <div class="col-md-4 grid-top">
                    <a href="#product" class="b-link-stripe b-animate-go  thickbox">
                        <img class="img-responsive" src="<spring:url value="/resources/images/pi5.jpg" />" alt="">

                        <div class="b-wrapper">
                            <h3 class="b-animate b-from-left    b-delay03 ">
                                <span>Shoe</span>
                            </h3>
                        </div>
                    </a>

                    <p><a href="#product">readable content</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!----->

        <%--	<div class="content-bottom">
                <ul>
                    <li><a href="#"><img class="img-responsive" src="images/lo.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo1.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo2.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo3.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo4.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo5.png" alt=""></a></li>
                <div class="clearfix"> </div>
                </ul>
            </div>--%>
    </div>
</div>