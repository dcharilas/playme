<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="container">

    <div class="content-menu badgesWrapper column-xs-2 column-sm-3 height-xs-4">
        <div data-rel="scroll">
            <ul class="list-wrapper">

                <li ng-repeat="reward in rewards" class="col-md-4">
                    <div class="portfolio-group">
                        <a class="portfolio-item" ng-click="showReward(reward)">
                            <img ng-src="data:image/jpeg;base64,{{reward.activated && reward.activeimage || reward.inactiveimage}}" >
                            <h2><span class="spacer">{{reward.pointsreq}}</span></h2>
                            <div class="detail" ng-class="{'detail-noneligible': !reward.activated}">
                                <i>{{reward.name}}</i>
                                <p><b>{{reward.pointsreq}}</b> {{ 'rewardMsg.pointsReq' | translate}}<br>
                                   {{ 'rewardMsg.info' | translate}} : {{reward.description}}<br>
                                </p>
                                <br><br><br>
                                <p ng-show="{{reward.activated}}" class="reward-eligible">{{ 'rewardMsg.enoughPoints' | translate}}</p>
                                <p ng-show="{{!reward.activated}}" class="reward-noneligible">{{ 'rewardMsg.needPoints' | translate}}</p>
                            </div>
                        </a>
                    </div>
                </li>

            </ul>
        </div>

    </div>
</div>