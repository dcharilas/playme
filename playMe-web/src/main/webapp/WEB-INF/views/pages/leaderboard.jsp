<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container">
    <div class=" register">
        <div class="col-md-4">
            <img class="masqot-login" src="<spring:url value="/resources/images/masqot-champion.jpg" />" alt="">
        </div>
        <div id="gridArea" class="col-md-7">
            <table ng-table="tableParams" class="table">
                <tr ng-repeat="user in $data" ng-class="{ 'grid-emphasis': isCurrentUser(user.username) }">
                    <td width="30px">
                        <!-- If image does not exist show default one -->
                        <img class="avatar" ng-src="{{user.avatar && 'data:image/jpeg;base64,' +user.avatar || 'resources/images/user.png'}}"/>
                    </td>
                    <td data-title="'Username'">{{user.username}}</td>
                    <td data-title="'Points'">{{user.points}}</td>
                    <td data-title="'Level'">{{user.level.name}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
