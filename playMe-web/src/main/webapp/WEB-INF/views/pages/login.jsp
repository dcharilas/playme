<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="container">
    <div class="account">
        <h1>{{ 'loginMsg.login' | translate}}</h1>
        <div class="account-pass" id="loginContainer" ng-controller="LoginCtrl" >
            <div class="col-md-4">
                <img class="masqot-login" src="<spring:url value="/resources/images/masqot-login.jpg" />" alt="">
            </div>
            <div class="col-md-5 account-top">
                <form name="form" ng-submit="login()" role="form">
                    <div>
                        <span>{{ 'loginMsg.username' | translate}}</span>
                        <input type="text" ng-model="username" required>
                    </div>
                    <div>
                        <span >{{ 'loginMsg.password' | translate}}</span>
                        <input type="password" ng-model="password" required>
                    </div>
                    <input type="submit" value="Login">
                </form>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>