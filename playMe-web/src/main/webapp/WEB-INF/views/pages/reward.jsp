<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="product">
<div class="container">
<div class="col-md-3 product-price">

    <!--initiate accordion-->
    <script type="text/javascript">
        $(function () {
            var menu_ul = $('.menu > li > ul'),
                    menu_a = $('.menu > li > a');
            menu_ul.hide();
            menu_a.click(function (e) {
                e.preventDefault();
                if (!$(this).hasClass('active')) {
                    menu_a.removeClass('active');
                    menu_ul.filter(':visible').slideUp('normal');
                    $(this).addClass('active').next().stop(true, true).slideDown('normal');
                } else {
                    $(this).removeClass('active');
                    $(this).next().stop(true, true).slideUp('normal');
                }
            });

        });
    </script>

    <div class="sellers">


    </div>
    <!---->

</div>
<div class="col-md-9 product-price1">
    <div class="col-md-5 single-top">
        <div class="flexslider">
            <ul class="slides">
                <li <%--data-thumb="<spring:url value="/resources/images/si.jpg" />"--%>>
                    <img ng-src="data:image/jpeg;base64,{{ reward.activeimage }}"/>
                </li>
            </ul>
        </div>

        <script>
            // Can also be used with $(document).ready()
            jQuery(document).ready(function($){
                $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: "thumbnails"
                });
            });
        </script>
    </div>
    <div class="col-md-7">
        <div class="single-para ">
            <h4>{{reward.name}}</h4>
            <h5 class="item_price">{{reward.pointsreq}} {{ 'rewardMsg.points' | translate}}</h5>
            <p>{{reward.description}}</p>
            <a href="#rewards" ng-click="redeemReward(reward)" class="add-cart item_add" ng-class="{'ng-hide': !reward.activated}">{{ 'rewardMsg.redeem' | translate}}</a>
        </div>
    </div>
    <div class="clearfix"></div>
    <!---->
    <div class="cd-tabs">

    </div>

</div>

<div class="clearfix"></div>
</div>
</div>