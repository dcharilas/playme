<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="container">

    <div class="content-menu badgesWrapper column-xs-2 column-sm-3 height-xs-4">
        <div data-rel="scroll">
            <ul class="list-wrapper">

                <li ng-repeat="badge in badges" class="col-md-4">
                    <div class="menu-icon">
                       <img ng-class="{'hvr-bounce-in': badge.activated}"
                            ng-src="data:image/jpeg;base64,{{badge.activated && badge.activeimage || badge.inactiveimage}}">
                    </div>
                    <div class="menu-text"> {{badge.name}}
                        <div class="menu-info">
                            <div>{{badge.description}}</div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>

    </div>
</div>