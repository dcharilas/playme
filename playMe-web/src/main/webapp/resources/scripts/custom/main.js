var languages = ["en","fr"];

/**
 * Import module dependencies
 * @type {*}
 */
var myApp = angular.module('playMe',
    ['ngRoute', 'pascalprecht.translate', 'ngTable', 'ui.bootstrap', 'ngMessages', 'ngCookies']
);

myApp.config(['$translateProvider', function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'resources/messages/locale_',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
}]);


/**
 * The main controller
 *
 */
myApp.controller('MainCtrl', ['$scope', function($scope) {
    $scope.initialSidebarHeight = 580;

    $scope.$on('$routeChangeSuccess', function (event, currRoute, prevRoute) {

    });

}]);


myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/index', {
                templateUrl: 'index.action',
                controller: 'IndexCtrl'
            }).
            when('/login', {
                templateUrl: 'login.action',
                controller: 'LoginCtrl'
            }).
            when('/register', {
                templateUrl: 'register.action',
                controller: 'RegisterCtrl'
            }).
            when('/product', {
                templateUrl: 'product.action',
                controller: 'ProductCtrl'
            }).
            when('/contact', {
                templateUrl: 'contact.action',
                controller: 'ContactCtrl'
            }).
            when('/cart', {
                templateUrl: 'cart.action',
                controller: 'CartCtrl'
            }).
            when('/leaderboard', {
                templateUrl: 'leaderboard.action',
                controller: 'LeaderboardCtrl'
            }).
            when('/profile', {
                templateUrl: 'profile.action',
                controller: 'ProfileCtrl'
            }).
            when('/personalInfo', {
                templateUrl: 'profile/info.action',
                controller: 'ProfileInfoCtrl'
            }).
            when('/badges', {
                templateUrl: 'profile/badges.action',
                controller: 'BadgesCtrl'
            }).
            when('/history', {
                templateUrl: 'profile/history.action',
                controller: 'HistoryCtrl'
            }).
            when('/rewards', {
                templateUrl: 'rewards.action',
                controller: 'RewardCtrl'
            }).
            when('/reward', {
                templateUrl: 'rewards/item.action',
                controller: 'RewardItemCtrl'
            }).
            when('/myRewards', {
                templateUrl: 'rewards/history.action',
                controller: 'RewardHistoryCtrl'
            }).
            when('/underConstr', {
                templateUrl: 'underConstr.action',
                controller: 'UnderConstrCtrl'
            }).
            otherwise({
                redirectTo: '/index'
            });
    }]);


/**
 * Handle user login info in cookie
 */
myApp.run(['$rootScope', '$location', '$cookieStore', '$http', '$document', 'LoginService',
    function ($rootScope, $location, $cookieStore, $http, $document, LoginService) {

        var homeURL = contextPath +'/home.action';
        var d = new Date();
        var n = d.getTime();  //n in ms
        var minutesToLogout = 30*60*1000 //set end time to 30 min from now

        $rootScope.idleEndTime = n+minutesToLogout;

        /**
         * Keep user logged in after page refresh
         */
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        /**
         * Redirect to login page if not logged in
         */
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
/*            if (!$location.absUrl().endsWith("login.action") && !$rootScope.globals.currentUser) {
                location.href = contextPath + '/login.action';
            }*/
        });

        function logout() {
            LoginService.ClearCredentials();
            /*
             * Redirect to index page
             */
            location.href = homeURL;
        }

        /**
         * Check if user is idle too much time. If yes then logout.
         */
        function checkAndResetIdle() {
            var d = new Date();
            var n = d.getTime();  //n in ms

            if (n>$rootScope.idleEndTime) {
                $document.find('body').off('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart'); //un-monitor events
                logout();
            } else {
                $rootScope.idleEndTime = n+minutesToLogout;
            }
        }
        $document.find('body').on('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart', checkAndResetIdle); //monitor events
    }]);



jQuery(document).ready(function($){
	var tabItems = $('.cd-tabs-navigation a'),
		tabContentWrapper = $('.cd-tabs-content');

	tabItems.on('click', function(event){
		event.preventDefault();
		var selectedItem = $(this);
		if( !selectedItem.hasClass('selected') ) {
			var selectedTab = selectedItem.data('content'),
				selectedContent = tabContentWrapper.find('li[data-content="'+selectedTab+'"]'),
				slectedContentHeight = selectedContent.innerHeight();
			
			tabItems.removeClass('selected');
			selectedItem.addClass('selected');
			selectedContent.addClass('selected').siblings('li').removeClass('selected');
			//animate tabContentWrapper height when content changes 
			tabContentWrapper.animate({
				'height': slectedContentHeight
			}, 200);
		}
	});

	//hide the .cd-tabs::after element when tabbed navigation has scrolled to the end (mobile version)
	checkScrolling($('.cd-tabs nav'));
	$(window).on('resize', function(){
		checkScrolling($('.cd-tabs nav'));
		tabContentWrapper.css('height', 'auto');
	});
	$('.cd-tabs nav').on('scroll', function(){ 
		checkScrolling($(this));
	});

	function checkScrolling(tabs){
		var totalTabWidth = parseInt(tabs.children('.cd-tabs-navigation').width()),
		 	tabsViewport = parseInt(tabs.width());
		if( tabs.scrollLeft() >= totalTabWidth - tabsViewport) {
			tabs.parent('.cd-tabs').addClass('is-ended');
		} else {
			tabs.parent('.cd-tabs').removeClass('is-ended');
		}
	}
});



/* ------------------
 * Prototype overrides
 * ----------------- */

Array.prototype.contains = function(elem) {
    for (var i in this) {
        if (this[i] == elem) return true;
    }
    return false;
}

Array.prototype.clone = function() {
    return this.slice(0);
};

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


/* ------------------
 * Utility functions
 * ----------------- */

function trim(value) {
    value = value.replace(/^\s+/,'');
    value = value.replace(/\s+$/,'');
    return value;
}

function exists(val){
    return (val!=null && val!="" && val!=undefined);
}


function clone(elem){
    return jQuery.extend(true, {}, elem);
}

function isEmpty(obj) {
    return !exists(obj) || Object.keys(obj).length === 0;
}


function fixImageForUpload(data) {
    return data.replace(/data:image\/jpeg;base64,/g, '')
        .replace(/data:image\/png;base64,/g, '')
        .replace(/data:image\/jpg;base64,/g, '');
}


/* ------------------------
 * Date utility functions
 * ------------------------ */

function getDateTimeDisplay($filter, date) {
    return $filter('date')(date, 'dd/MM/yyyy hh:mm:ss');
}

function getDateDisplay($filter, date) {
    return $filter('date')(date, 'dd/MM/yyyy');
}

function getDateTimeFromString(datestring) {
    //TODO this will not work with current format
    return new Date(datestring);
}

function getDateFromString(datestring) {
    var parts = datestring.split("/");
    return new Date(Number(parts[2]), Number(parts[1]) - 1, Number(parts[0]));
}