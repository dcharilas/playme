myApp.controller('ProductCtrl', ['$scope', '$http', '$filter', '$translate', '$rootScope',
    function($scope, $http, $filter, $translate, $rootScope) {

        $scope.isLoggedIn = function() {
            return (exists($rootScope.globals) && exists($rootScope.globals.currentUser));
        }

    }]);



myApp.controller('CartCtrl', ['$scope', '$http', '$filter', '$translate', '$rootScope',
    function($scope, $http, $filter, $translate, $rootScope) {

        var homeURL = contextPath +'/home.action';
        $scope.orderURL = contextPath + '/order.action';

        $scope.amount = '150.00';
        $scope.delivery = '15.00';
        $scope.total = '165.00';

        $scope.isLoggedIn = function() {
            return (exists($rootScope.globals) && exists($rootScope.globals.currentUser));
        }

        /**
         * Send the total points of the order.
         */
        $scope.placeOrder = function(total) {
            $http.post($scope.orderURL, total)
                .success(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.orderSaved'));
                    angular.element('.simpleCart_empty').trigger('click');
                    /*
                     * Redirect to another page
                     */
                    //TODO redirect somewhere
                    location.href = homeURL;
                })
                .error(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.orderNotSaved'));
                });
        }
    }]);
