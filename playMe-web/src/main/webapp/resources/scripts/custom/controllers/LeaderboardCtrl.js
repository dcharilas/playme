myApp.controller('LeaderboardCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$rootScope',
    function($scope, $http, $filter, ngTableParams, $translate, $rootScope) {

        //$scope.searchURL = contextPath + '/leaderboard/load.action';
        $scope.searchURL = contextPath + '/leaderboard/load/single.action';

        $scope.tableParams = initializeSimpleGrid($scope, $http, $filter, ngTableParams, $translate, $scope.searchURL, $scope.tableParams);


        $scope.isCurrentUser = function(username) {
            if (exists($rootScope.globals) && exists($rootScope.globals.currentUser)) {
                return $rootScope.globals.currentUser.username == username;
            }
            return false;
        }

    }]);
