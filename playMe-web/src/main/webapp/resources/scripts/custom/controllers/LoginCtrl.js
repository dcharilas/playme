myApp.controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$cookieStore', '$translate', 'LoginService',
    function ($scope, $rootScope, $location, $cookieStore, $translate, LoginService) {

        var afterLoginURL = contextPath +'/home.action#/profile';
        /**
         *  Reset user information
         */
        LoginService.ClearCredentials();

        /**
         * Login button handler
         */
        $scope.login = function () {
            $scope.dataLoading = true;
            /*
             * Call login service
             */
            LoginService.Login($scope.username, $scope.password, function (response) {
                if (response.success) {
                    LoginService.SetCredentials($scope.username, $scope.password, response);
                    /*
                     * Redirect to profile page
                     */
                    location.href = afterLoginURL;
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                    bootbox.alert($translate.instant('notificationMsg.userNotFound'));
                }
            });
        };

}]);




myApp.controller('RegisterCtrl', ['$scope', '$rootScope', '$location', '$cookieStore', 'LoginService',
    function ($scope, $rootScope, $location, $cookieStore, LoginService) {

        /**
         * Register button handler
         */
        $scope.register = function () {
            $scope.dataLoading = true;
            /*
             * Call login service
             */
            LoginService.Register($scope.form, function (response) {
                if (response.success) {
                    bootbox.alert($translate.instant('notificationMsg.userRegistered'));
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                    bootbox.alert($translate.instant('notificationMsg.userNotRegistered'));
                }
            });
        };

    }]);