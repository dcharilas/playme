myApp.controller('MenuCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    $rootScope.navLinks = [
        {text: "navigationMsg.home", href: "#index", cls:"", color:"color6", pageId:"page_index", requiresLogin: false},
        {text: "navigationMsg.profile", href: "#profile", cls:"", color:"color3", pageId:"page_profile", requiresLogin: true, subItems:[
            {text: "navigationMsg.personalInfo", href: "#personalInfo", cls:"glyphicon glyphicon-user", pageId:"page_profileInfo"},
            {text: "navigationMsg.badges", href: "#badges", cls:"glyphicon glyphicon-star", pageId:"page_badges"},
            {text: "navigationMsg.history", href: "#history", cls:"glyphicon glyphicon-calendar", pageId:"page_history"}
        ]},
        {text: "navigationMsg.rewards", href: "#rewards", cls:"", color:"color4", pageId:"page_rewards", requiresLogin: true, subItems:[
            {text: "navigationMsg.myRewards", href: "#myRewards", cls:"glyphicon glyphicon-gift", pageId:"page_myRewards"}
        ]},
        {text: "navigationMsg.leaderboard", href: "#leaderboard", cls:"", color:"color1", pageId:"page_leader", requiresLogin: false},
        {text: "navigationMsg.contact", href: "#contact", cls:"", color:"color2", pageId:"page_contact", requiresLogin: false}
    ];
    $scope.navLinks = $rootScope.navLinks;


    /**
     * Determine whether user is allowed to view this page
     *
     * @param pageId
     */
    $scope.showPage = function(requiresLogin) {
        if (!requiresLogin) {
            return true;
        } else {
            return (exists($rootScope.globals) && exists($rootScope.globals.currentUser));
        }
    }
    $rootScope.showPage = $scope.showPage;

}]);


/**
* The Header Controller
*/
myApp.controller('HeaderCtrl', ['$scope', '$translate', 'LoginService', '$rootScope', '$http',
    function($scope, $translate, LoginService, $rootScope, $http) {

    var homeURL = contextPath +'/home.action';
    var logoutURL = contextPath +'/login/invalidate.action';

    $scope.constants = {
  		logout_btn : "Log out",
  		loginText : "You are logged in as:"
  	};

    $scope.isLoggedIn = function() {
        return (exists($rootScope.globals) && exists($rootScope.globals.currentUser));
    }

    $scope.languages = [
	    {name:'en', value:'en'},
	    {name:'fr', value:'fr'}
  	];
  	$scope.language = $scope.languages[0];


  	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
	};

    /**
     * Logout link handler
     */
    $scope.logout = function() {
        LoginService.ClearCredentials();
        /*
         * Invalidate the session and redirect to login page
         */
        $http.get(logoutURL)
            .success(function (data) {
                location.href = homeURL;
            })
            .error(function(){
            })
    }

}]);

/**
* The Footer Controller
*/
myApp.controller('FooterCtrl', ['$scope', function($scope) {
    $scope.constants = {
    	aboutLink : "About"
    };
    $scope.aboutlink = {url: "GEN_About.html"}
}]);