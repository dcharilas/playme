myApp.controller('RewardCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getRewardsURL = contextPath + '/rewards/list.action';
        $scope.getRewardURL = contextPath + '/home.action#/reward';
        $scope.getRewardObjectURL = contextPath + '/rewards/item/load.action';
        /**
         * Load user data
         */
        $http.get($scope.getUserObjectURL)
            .success(function (data) {
                $scope.user = data;
            });

        /**
         * Load application rewards
         */
        $http.get($scope.getRewardsURL)
            .success(function (data) {
                $scope.rewards = data;
                /*
                 * Set description according to locale
                 */
                if (exists($scope.rewards)) {
                    var locale = $translate.use();
                    for (var b=0; b<data.length; b++) {
                        if (exists(data[b].descriptions)) {
                            for (var i=0; i<data[b].descriptions.length; i++) {
                                if (exists(data[b].descriptions[i]) && data[b].descriptions[i].id.locale == locale) {
                                    $scope.rewards[b].description = data[b].descriptions[i].value;
                                    break;
                                }
                            }
                        }
                    }
                }
            });


        $scope.showReward = function(reward) {
            /**
             * Load reward data
             */
            $http.post($scope.getRewardObjectURL, reward.id)
                .success(function (data) {
                    $scope.reward = data;
                    /*
                     * Redirect to reward page
                     */
                    location.href = $scope.getRewardURL;
                });
        }
    }]);



myApp.controller('RewardHistoryCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getUserRewardsURL = contextPath + '/rewards/history/list.action';

        /**
         * Load user rewards
         */
        $http.get($scope.getUserRewardsURL)
            .success(function (data) {
                $scope.rewards = data;
                /*
                 * Set description according to locale
                 */
                if (exists($scope.rewards)) {
                    var locale = $translate.use();
                    for (var b=0; b<data.length; b++) {
                        if (exists(data[b].descriptions)) {
                            for (var i=0; i<data[b].descriptions.length; i++) {
                                if (exists(data[b].descriptions[i]) && data[b].descriptions[i].id.locale == locale) {
                                    $scope.rewards[b].description = data[b].descriptions[i].value;
                                    break;
                                }
                            }
                        }
                    }
                }
            });


    }]);


myApp.controller('RewardItemCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getRewardURL = contextPath + '/rewards/item/details.action';
        $scope.redeemRewardURL = contextPath + '/rewards/redeem.action';

        /**
         * Load user data
         */
        $http.get($scope.getRewardURL)
            .success(function (data) {
                $scope.reward = data;

                if (exists($scope.reward)) {
                    var locale = $translate.use();
                    if (exists(data.descriptions)) {
                        for (var i = 0; i < data.descriptions.length; i++) {
                            if (exists(data.descriptions[i]) && data.descriptions[i].id.locale == locale) {
                                $scope.reward.description = data.descriptions[i].value;
                                break;
                            }
                        }
                    }

                }
            });


        /**
         * Sends request to redeem reward
         */
        $scope.redeemReward = function(reward) {
            $http.post($scope.redeemRewardURL, reward.id)
                .success(function (data) {
                    if (exists(data)) {
                        $scope.user = data;
                        /*bootbox.alert(reward.pointsreq +' ' +$translate.instant('notificationMsg.redeemSuccess1') +
                            reward.name +' ' +$translate.instant('notificationMsg.redeemSuccess2'));*/
                        bootbox.alert($translate.instant('notificationMsg.redeemRequest'));

                    } else {
                        bootbox.alert($translate.instant('notificationMsg.redeemFail'));
                    }
                })
                .error(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.redeemFail'));
                });

        }

    }]);

