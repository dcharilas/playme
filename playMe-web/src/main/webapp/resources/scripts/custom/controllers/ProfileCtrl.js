myApp.controller('ProfileInfoCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getUserObjectURL = contextPath + '/profile/user.action';
        $scope.saveUserURL = contextPath + '/profile/save.action';

        /**
         * Load user data
         */
        $http.get($scope.getUserObjectURL)
            .success(function (data) {
                $scope.user = data;
                /*
                 * Fix date format
                 */
                $scope.user.birthdate = getDateDisplay($filter, $scope.user.birthdate);
            });


        /**
         * Configure the datepicker fields
         */
        $('#inputBirthDateModal').datepicker({
            format: 'dd/mm/yyyy',
            startDate: '01/01/1940',
            endDate: '31/12/2001'
        });


        $scope.uploadHandler = function ($this) {
            //$("#upload-file-info").html($(this).val());
            $('#photo-id').attr('ng-src',$scope.user.avatar);
        };


        /**
         * Handler for save button
         */
        $scope.saveHandler = function(){
            /*
             * Fix dates
             */
            var cloneForm = jQuery.extend(true, {}, $scope.user);
            if (exists($scope.user.birthdate)) {
                cloneForm.birthdate = getDateFromString($scope.user.birthdate);
            }
            cloneForm.userCore = null;
            /*
             * Call the service
             */
            $http.post($scope.saveUserURL, cloneForm)
                .success(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.userSaved'));
                })
                .error(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.userNotSaved'));
                });
        }

    }]);


myApp.controller('ProfileCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getUserObjectURL = contextPath + '/profile/user.action';
        $scope.getLevelURL = contextPath + '/profile/level.action';

        /**
         * Load user data
         */
        $http.get($scope.getUserObjectURL)
            .success(function (data) {
                $scope.user = data;
                /*
                 * Fix date format
                 */
                $scope.user.birthdate = getDateDisplay($filter, $scope.user.birthdate);
            });


        /**
         * Load user level
         */
        $http.get($scope.getLevelURL)
            .success(function (data) {
                $scope.level = data;
                /*
                 * Set description according to locale
                 */
                if (exists($scope.level)) {
                    var locale = $translate.use();
                    if (exists(data.descriptions)) {
                        for (var i=0; i<data.descriptions.length; i++) {
                            if (exists(data.descriptions[i]) && data.descriptions[i].id.locale == locale) {
                                $scope.level.description = data.descriptions[i].value;
                                break;
                            }
                        }
                    }
                }
            });

    }]);



myApp.controller('BadgesCtrl', ['$scope', '$http', '$filter', '$translate',
    function($scope, $http, $filter, $translate) {

        $scope.getBadgesURL = contextPath + '/profile/badges/list.action';


        /**
         * Load user badges
         */
        $http.get($scope.getBadgesURL)
            .success(function (data) {
                $scope.badges = data;
                /*
                 * Set description according to locale
                 */
                if (exists($scope.badges)) {
                    var locale = $translate.use();
                    for (var b=0; b<data.length; b++) {
                        if (exists(data[b].descriptions)) {
                            for (var i=0; i<data[b].descriptions.length; i++) {
                                if (exists(data[b].descriptions[i]) && data[b].descriptions[i].id.locale == locale) {
                                    $scope.badges[b].description = data[b].descriptions[i].value;
                                    break;
                                }
                            }
                        }
                    }
                }
            });


    }]);



myApp.controller('HistoryCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate',
    function($scope, $http, $filter, ngTableParams, $translate) {

        $scope.getPointsHistoryURL = contextPath + '/profile/history/points.action';
        $scope.getLevelHistoryURL = contextPath + '/profile/history/levels.action';
        $scope.getPointsPerDayStatisticsURL = contextPath + '/profile/points/day.action';
        $scope.getRedeemPointsPerDayStatisticsURL = contextPath + '/profile/redeempoints/day.action';

        $scope.tableParams = initializeSimpleGrid($scope, $http, $filter, ngTableParams, $translate, $scope.getPointsHistoryURL, $scope.tableParams);
        $scope.tableParams2 = initializeSimpleGrid($scope, $http, $filter, ngTableParams, $translate, $scope.getLevelHistoryURL, $scope.tableParams2);


        /**
         * Load points data
         */
        $http.get($scope.getPointsPerDayStatisticsURL)
            .success(function (data) {
                $scope.pointsData = data;
                /**
                 * Load redeemable points data
                 */
                $http.get($scope.getRedeemPointsPerDayStatisticsURL)
                    .success(function (data) {
                        $scope.redeemPointsData = data;
                        if (exists($scope.pointsData) && exists($scope.redeemPointsData)) {
                            $scope.transformData();
                        }
                    });
            });


        /**
         * Transform data so that they can be displayed in
         * the history graph
         */
        $scope.transformData = function() {
            var data = [];

            for (var i=0; i<$scope.redeemPointsData.length; i++) {
                var dataItem = {};
                dataItem.date = new Date($scope.redeemPointsData[i].date);
                dataItem.redeem = $scope.redeemPointsData[i].value;
                var foundTotal = false;
                for (var j=0; j<$scope.pointsData.length; j++) {
                    if ($scope.redeemPointsData[i].date == $scope.pointsData[j].date) {
                        dataItem.total = $scope.pointsData[j].value;
                        foundTotal = true;
                        break;
                    }
                }
                if (!foundTotal && i>0) {
                    dataItem.total = data[data.length-1].total;
                }

                data.push(dataItem);
            }

            /*
             * Transform level data for markers too
             */
            if (exists($scope.tableParams2.data)) {
                var markers = [];
                for (var i=0; i<$scope.tableParams2.data.length; i++) {
                    var level = $scope.tableParams2.data[i];
                    var markerItem = {};
                    markerItem.date = new Date(level.unlockdate);
                    markerItem.type = 'Client';
                    markerItem.level = level.level.name;
                    markers.push(markerItem);
                }
                $scope.markerData = markers;
            }

            $scope.graphData = data;
        }

    }]);

