var defaultGridPage = 1;
var defaultGridPageSize = 10;


/* -----------------------------------------
 * Grid initialisation and button handlers
 * -----------------------------------------*/

function initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate) {
    /*
     * Function to hide alerts section
     */
    $scope.hideAlerts = function() {
        // hide alerts
        if ($("#successAlert")) {
            $("#successAlert").hide();
        }
        if ($("#errorAlert")) {
            $("#errorAlert").hide();
        }
    }

    $scope.showSuccess = function() {
        $scope.hideAlerts();
        if ($("#successAlert")) {
            $("#successAlert").show();
        }
    }

    $scope.showError = function() {
        $scope.hideAlerts();
        if ($("#errorAlert")) {
            $("#errorAlert").show();
        }
    }

    /*
     * Grid configuration
     */
    $scope.tableParams = new ngTableParams({
        page: defaultGridPage,            // show first page
        count: defaultGridPageSize,       // count per page
        sorting: {
            id: 'asc'     // initial sorting
        }
    }, {
        total: 0, // length of data
        getData: function ($defer, params) {
            // hide alerts
            $scope.hideAlerts();

            /*
             * Load Grid data
             */
            var dataObj = {
                "page": params.$params.page,
                "pagesize": params.$params.count
            };
            /*
             * Add search form content
             */
            if (!isEmpty($scope.searchForm)) {
                dataObj.form = $scope.searchForm;
            }
            $http.post($scope.searchURL, dataObj)
                .success(function (data) {
                    // update table params
                    params.total(data.total);
                    // set new data
                    // use build-in angular filter
                    var filters = params.filter();
                    var tempDateFilter;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data.items, params.orderBy()) : data.items;

                    /*
                     * Apply filters if needed
                     */
                    if(filters) {
                        orderedData = $filter('filter')(orderedData, filters);
                        filters.Date = tempDateFilter;
                    }

                    $scope.data = orderedData;
                    $defer.resolve(orderedData);
                    //$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                    // set checkboxes
                    if (document.getElementById("select_all")) {
                        setGridCheckboxes($scope);
                    }
                });
        }
    });

}



function initializeSimpleGrid($scope, $http, $filter, ngTableParams, $translate, url, tableParams) {
    /*
     * Grid configuration
     */
    tableParams = new ngTableParams({
        page: 1,   // show first page
        total: 1,  // value less than count hide pagination
        count: defaultGridPageSize,  // count per page
        sorting: {
            id: 'asc'     // initial sorting
        }
    }, {
        counts: [], // hide page counts control
        getData: function ($defer, params) {
            /*
             * Add search form content
             */
            if (!isEmpty($scope.searchForm)) {
                dataObj.form = $scope.searchForm;
            }
            $http.post(url)
                .success(function (data) {
                    // set new data
                    // use build-in angular filter
                    var filters = params.filter();
                    var tempDateFilter;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data.items, params.orderBy()) : data.items;

                    /*
                     * Apply filters if needed
                     */
                    if(filters) {
                        if(filters.Date) {
                            orderedData = $filter('customDateFilter')(orderedData, filters.Date);
                            tempDateFilter = filters.Date;
                            delete filters.Date;
                        }
                        orderedData = $filter('filter')(orderedData, filters);
                        filters.Date = tempDateFilter;
                    }

                    $scope.data = orderedData;
                    $defer.resolve(orderedData);
                });
        }
    });

    return tableParams;

}