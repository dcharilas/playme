use playme;

-- ---
-- Table 'SocialIntegration'
--
-- ---

DROP TABLE IF EXISTS `SocialIntegration`;

CREATE TABLE `SocialIntegration` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `socialnetwork` VARCHAR(30) NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `username` INTEGER NULL DEFAULT NULL,
  `socialid` INTEGER NULL DEFAULT NULL,
  `integrationdate` DATETIME NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`)
);


-- ---
-- Table 'Mission'
--
-- ---

DROP TABLE IF EXISTS `Mission`;

CREATE TABLE `Mission` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `missionimage` BLOB NULL DEFAULT NULL,
  `descriptionid` INTEGER NULL DEFAULT NULL,
  `active` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
  -- FOREIGN KEY (`descriptionid`) REFERENCES `DisplayText` (`id`)
);

-- ---
-- Table 'MissionTask'
--
-- ---

DROP TABLE IF EXISTS `MissionTask`;

CREATE TABLE `MissionTask` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `missionid` INTEGER NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `taskimage` BLOB NULL DEFAULT NULL,
  `descriptionid` INTEGER NULL DEFAULT NULL,
  `sequence` INTEGER NULL DEFAULT NULL,
  `active` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`missionid`) REFERENCES `Mission` (`id`)
);



-- ---
-- Table 'TaskRequirementDefinition'
--
-- ---
DROP TABLE IF EXISTS `TaskRequirementDefinition`;

CREATE TABLE `TaskRequirementDefinition` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `taskid` INTEGER NULL DEFAULT NULL,
  `requirementname` VARCHAR(100) NULL DEFAULT NULL,
  `requirementvalue` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`taskid`) REFERENCES `MissionTask` (`id`)
);


-- ---
-- Table 'UserMissions'
--
-- ---

DROP TABLE IF EXISTS `UserMissions`;

CREATE TABLE `UserMissions` (
  `missionid` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `assigned` bit NULL DEFAULT NULL,
  `completed` bit NULL DEFAULT NULL,
  `currenttaskid` INTEGER NULL DEFAULT NULL,
  `nexttaskid` INTEGER NULL DEFAULT NULL,
  `assigndate` DATETIME NULL DEFAULT NULL,
  `completiondate` DATETIME NULL DEFAULT NULL,
  `deadline` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`missionid`, `userid`,`applicationid`),
  FOREIGN KEY (`missionid`) REFERENCES `Mission` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);
