use playme;
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'DisplayText'
-- 
-- ---

DROP TABLE IF EXISTS `DisplayText`;
        
CREATE TABLE `DisplayText` (
  `messageid` INTEGER NULL DEFAULT NULL,
  `locale` VARCHAR(2) NULL DEFAULT NULL,
  `value` VARCHAR(1000) NULL DEFAULT NULL,
  PRIMARY KEY (`messageid`,`locale`)
);


-- ---
-- Table 'Application'
-- 
-- ---

DROP TABLE IF EXISTS `Application`;
        
CREATE TABLE `Application` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(50) NOT NULL DEFAULT 'NULL',
  PRIMARY KEY (`id`)
);



-- ---
-- Table 'Configuration'
-- 
-- ---

DROP TABLE IF EXISTS `Configuration`;
        
CREATE TABLE `Configuration` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `property` VARCHAR(100) NULL DEFAULT NULL,
  `value` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);

-- ---
-- Table 'Level'
-- 
-- ---

DROP TABLE IF EXISTS `Level`;
        
CREATE TABLE `Level` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `image` BLOB NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `descriptionid` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
  -- FOREIGN KEY (`descriptionid`) REFERENCES `DisplayText` (`messageid`)
);
ALTER TABLE Level MODIFY COLUMN image MEDIUMBLOB NULL DEFAULT NULL;

-- ---
-- Table 'ApplicationLevels'
-- 
-- ---

DROP TABLE IF EXISTS `ApplicationLevels`;
        
CREATE TABLE `ApplicationLevels` (
  `applicationid` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `levelid` INTEGER NULL DEFAULT NULL,
  `sequence` INTEGER NULL DEFAULT NULL,
  `active` bit NULL DEFAULT NULL,
  `pointsrequired` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`applicationid`, `levelid`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`levelid`) REFERENCES `Level` (`id`)
);


-- ---
-- Table 'User'
-- 
-- ---

DROP TABLE IF EXISTS `User`;
        
CREATE TABLE `User` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `regDate` TIME NULL DEFAULT NULL,
  `firstname` VARCHAR(100) NULL DEFAULT NULL,
  `lastname` VARCHAR(100) NULL DEFAULT NULL,
  `username` VARCHAR(30) NULL DEFAULT NULL,
  `levelid` INTEGER NULL DEFAULT NULL,
  `points` INTEGER NULL DEFAULT NULL,
  `gender` VARCHAR(1) NULL DEFAULT NULL,
  `age` INTEGER NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `phone` VARCHAR(15) NULL DEFAULT NULL,
  `address` VARCHAR(150) NULL DEFAULT NULL,
  `postcode` VARCHAR(10) NULL DEFAULT NULL,
  `birthdate` DATE NULL DEFAULT NULL,
  `avatar` BLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`levelid`) REFERENCES `Level` (`id`)
);
ALTER TABLE User MODIFY id BIGINT AUTO_INCREMENT DEFAULT NULL;
ALTER TABLE User MODIFY regDate DATE NULL DEFAULT NULL;
ALTER TABLE User MODIFY COLUMN avatar MEDIUMBLOB NULL DEFAULT NULL;
ALTER TABLE playme.user ADD COLUMN `redeemablepoints` INTEGER NOT NULL DEFAULT 0;

-- ---
-- Table 'UserLevelProgression'
-- 
-- ---

DROP TABLE IF EXISTS `UserLevelProgression`;
        
CREATE TABLE `UserLevelProgression` (
  `userid` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `levelid` INTEGER NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `unlockdate` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`userid`, `levelid`, `applicationid`),
  FOREIGN KEY (`levelid`) REFERENCES `Level` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);


-- ---
-- Table 'Badge'
-- 
-- ---

DROP TABLE IF EXISTS `Badge`;
        
CREATE TABLE `Badge` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `activeimage` BLOB NULL DEFAULT NULL,
  `inactiveimage`BLOB NULL DEFAULT NULL,
  `descriptionid` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
  -- FOREIGN KEY (`descriptionid`) REFERENCES `DisplayText` (`id`)
);
ALTER TABLE Badge MODIFY COLUMN activeimage MEDIUMBLOB NULL DEFAULT NULL;
ALTER TABLE Badge MODIFY COLUMN inactiveimage MEDIUMBLOB NULL DEFAULT NULL;

-- ---
-- Table 'UserBadges'
-- 
-- ---

DROP TABLE IF EXISTS `UserBadges`;
        
CREATE TABLE `UserBadges` (
  `badgeid` INTEGER NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `unlockdate` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`userid`, `badgeid`, `applicationid`),
  FOREIGN KEY (`badgeid`) REFERENCES `Badge` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);

-- ---
-- Table 'ApplicationBadge'
-- 
-- ---

DROP TABLE IF EXISTS `ApplicationBadges`;
        
CREATE TABLE `ApplicationBadges` (
  `applicationid` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `badgeid` INTEGER NULL DEFAULT NULL,
  `active` bit NULL DEFAULT NULL,
  PRIMARY KEY (`applicationid`, `badgeid`),
  FOREIGN KEY (`badgeid`) REFERENCES `Badge` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);
ALTER TABLE applicationbadges ADD COLUMN `awardafterpoints` INTEGER NULL DEFAULT NULL;
ALTER TABLE applicationbadges ADD COLUMN `awardafterlevelseq` INTEGER NULL DEFAULT NULL;
ALTER TABLE applicationbadges ADD COLUMN `awardafterlogins` INTEGER NULL DEFAULT NULL;
ALTER TABLE applicationbadges ADD COLUMN `awardaftercompleteness` INTEGER NULL DEFAULT NULL;


-- ---
-- Table 'EventDefinition'
-- 
-- ---

DROP TABLE IF EXISTS `EventDefinition`;
        
CREATE TABLE `EventDefinition` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);


-- ---
-- Table 'ApplicationEvent'
-- 
-- ---

DROP TABLE IF EXISTS `EventAction`;
        
CREATE TABLE `EventAction` (
  `eventid` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `badgeaward` INTEGER NULL DEFAULT NULL,
  `pointsaward` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`eventid`, `applicationid`),
  FOREIGN KEY (`eventid`) REFERENCES `EventDefinition` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);


-- ---
-- Table 'ExternalEventHistory'
-- 
-- ---

DROP TABLE IF EXISTS `ExternalEventHistory`;
        
CREATE TABLE `ExternalEventHistory` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `eventid` INTEGER NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `eventdate` DATE NULL DEFAULT NULL,
  `comments` VARCHAR(200) NULL DEFAULT NULL,
  `numericvalue_1` INTEGER NULL DEFAULT 0,
  `numericvalue_2` INTEGER NULL DEFAULT 0,
  `textvalue_1` VARCHAR(100) NULL DEFAULT NULL,
  `textvalue_2` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`eventid`) REFERENCES `EventDefinition` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`)
);
ALTER TABLE externaleventhistory MODIFY COLUMN eventdate DATETIME;
ALTER TABLE externaleventhistory ADD COLUMN `numericvalue_3` INTEGER NULL DEFAULT 0;
ALTER TABLE externaleventhistory ADD COLUMN `numericvalue_4` INTEGER NULL DEFAULT 0;
ALTER TABLE externaleventhistory ADD COLUMN `textvalue_3` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE externaleventhistory ADD COLUMN `textvalue_4` VARCHAR(100) NULL DEFAULT NULL;

-- ---
-- Table 'EventHistory'
-- 
-- ---

DROP TABLE IF EXISTS `EventHistory`;
        
CREATE TABLE `EventHistory` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `eventid` INTEGER NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL, 
  `externaleventid` BIGINT NULL DEFAULT NULL,
  `eventdate` DATE NULL DEFAULT NULL,
  `pointsawarded` INTEGER NULL DEFAULT NULL,
  `badgeawarded` INTEGER NULL DEFAULT NULL,
  `levelawarded` INTEGER NULL DEFAULT NULL,
  `missionidcompleted` INTEGER NULL DEFAULT NULL,
  `taskidcompleted` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`eventid`) REFERENCES `EventDefinition` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`externaleventid`) REFERENCES `ExternalEventHistory` (`id`)
);
ALTER TABLE eventhistory MODIFY COLUMN eventdate DATETIME;
ALTER TABLE eventhistory ADD CONSTRAINT fk_event_Badge FOREIGN KEY (badgeawarded) REFERENCES `Badge` (`id`);
ALTER TABLE eventhistory ADD CONSTRAINT fk_event_Level FOREIGN KEY (levelawarded) REFERENCES `Level` (`id`);

-- ---
-- Table 'UserPointsHistory'
-- 
-- ---

DROP TABLE IF EXISTS `UserPointsHistory`;
        
CREATE TABLE `UserPointsHistory` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `points` INTEGER NULL DEFAULT NULL,
  `updatedate` DATETIME NULL DEFAULT NULL,
  `eventhistoryid` BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`eventhistoryid`) REFERENCES `EventHistory` (`id`)
);


-- ---
-- Table 'UserProfileWeights'
--
-- ---

DROP TABLE IF EXISTS `UserProfileWeights`;

CREATE TABLE `UserProfileWeights` (
  `fieldname` VARCHAR(100) NULL DEFAULT NULL,
  `weight` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (fieldname)
);
INSERT INTO UserProfileWeights VALUES ('ADDRESS','10');
INSERT INTO UserProfileWeights VALUES ('AVATAR','20');
INSERT INTO UserProfileWeights VALUES ('BIRTHDATE','10');
INSERT INTO UserProfileWeights VALUES ('EMAIL','15');
INSERT INTO UserProfileWeights VALUES ('FIRSTNAME','10');
INSERT INTO UserProfileWeights VALUES ('GENDER','10');
INSERT INTO UserProfileWeights VALUES ('LASTNAME','10');
INSERT INTO UserProfileWeights VALUES ('PHONE','15');
INSERT INTO UserProfileWeights VALUES ('POSTCODE','10');
INSERT INTO UserProfileWeights VALUES ('SOCIALPROFILE','30');


INSERT INTO eventdefinition VALUES (1, 'REGISTER_USER');
INSERT INTO eventdefinition VALUES (2, 'USER_ACTION_ASSIGN_POINTS');
INSERT INTO eventdefinition VALUES (3, 'USER_ACTION_ASSIGN_BADGE');
INSERT INTO eventdefinition VALUES (4, 'REDEEM_POINTS');


-- ---
-- Table 'Reward'
--
-- ---

DROP TABLE IF EXISTS `Reward`;

CREATE TABLE `Reward` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `applicationid` INTEGER NOT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `activeimage` MEDIUMBLOB NULL DEFAULT NULL,
  `inactiveimage`MEDIUMBLOB NULL DEFAULT NULL,
  `descriptionid` INTEGER NULL DEFAULT NULL,
  `pointsreq` INTEGER NULL DEFAULT NULL,
  `active` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
  -- FOREIGN KEY (`descriptionid`) REFERENCES `DisplayText` (`id`)
);



-- ---
-- Table 'UserRewards'
--
-- ---

DROP TABLE IF EXISTS `UserRewards`;

CREATE TABLE `UserRewards` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `rewardid` INTEGER NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `redeemdate` DATETIME NULL DEFAULT NULL,
  `redeemeventid` BIGINT NULL DEFAULT NULL,
  `deliverdate` DATETIME NULL DEFAULT NULL,
  `delivereventid` BIGINT NULL DEFAULT NULL,
  `identifier` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`rewardid`) REFERENCES `Reward` (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`),
  FOREIGN KEY (`redeemeventid`) REFERENCES `EventHistory` (`id`),
  FOREIGN KEY (`delivereventid`) REFERENCES `EventHistory` (`id`)
);


-- ---
-- Table 'UserLogin'
--
-- ---

DROP TABLE IF EXISTS `UserLogin`;

CREATE TABLE `UserLogin` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);


-- ---
-- Table 'Usermessagecodes'
--
-- ---

DROP TABLE IF EXISTS `Usermessagecodes`;

CREATE TABLE `Usermessagecodes` (
  `applicationid` INTEGER NULL DEFAULT NULL,
  `msgcode` VARCHAR(30) NULL DEFAULT NULL,
  `locale` VARCHAR(2) NULL DEFAULT NULL,
  `message` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`applicationid`,`msgcode`,`locale`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);


-- ---
-- Table 'Usermessages'
--
-- ---

DROP TABLE IF EXISTS `Usermessages`;

CREATE TABLE `Usermessages` (
  `id` BIGINT NULL AUTO_INCREMENT DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `createdon` DATETIME NULL DEFAULT NULL,
  `msgcode` VARCHAR(30) NULL DEFAULT NULL,
  `value_1` VARCHAR(30) NULL DEFAULT NULL,
  `value_2` VARCHAR(30) NULL DEFAULT NULL,
  `viewedon` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);


-- ---
-- Table 'Pastusermessages'
--
-- ---

DROP TABLE IF EXISTS `Pastusermessages`;

CREATE TABLE `Pastusermessages` (
  `id` BIGINT NULL DEFAULT NULL,
  `userid` BIGINT NULL DEFAULT NULL,
  `applicationid` INTEGER NULL DEFAULT NULL,
  `createdon` DATETIME NULL DEFAULT NULL,
  `msgcode` VARCHAR(30) NULL DEFAULT NULL,
  `value_1` VARCHAR(30) NULL DEFAULT NULL,
  `value_2` VARCHAR(30) NULL DEFAULT NULL,
  `viewedon` DATETIME NULL DEFAULT NULL,
  `archivedon` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userid`) REFERENCES `User` (`id`),
  FOREIGN KEY (`applicationid`) REFERENCES `Application` (`id`)
);



-- ---
-- Table Indices
-- ---

CREATE INDEX `index_level_id` ON `Level` (`id`);
CREATE INDEX `index_applicationlevel_level_id` ON `ApplicationLevels` (`levelid`);
CREATE INDEX `index_applicationlevel_application_id` ON `ApplicationLevels` (`applicationid`);
CREATE INDEX `index_applicationlevel_id` ON `ApplicationLevels` (`applicationid`,`levelid`);
CREATE INDEX `index_user_id` ON `User` (`id`);
CREATE INDEX `index_user_application_id` ON `User` (`applicationid`);
CREATE INDEX `index_user_id_application_id` ON `User` (`id`,`applicationid`);

CREATE INDEX `index_userlevelprogression_id` ON `UserLevelProgression` (`userid`,`applicationid`);
CREATE INDEX `index_badge_id` ON `Badge` (`id`);
CREATE INDEX `index_userbadges_badge_id` ON `UserBadges` (`badgeid`);
CREATE INDEX `index_userbadges_user_id` ON `UserBadges` (`userid`,`applicationid`);
CREATE INDEX `index_applicationbadge_application_id` ON `ApplicationBadges` (`applicationid`);

CREATE INDEX `index_eventaction_id` ON `EventAction` (`eventid`,`applicationid`);
CREATE INDEX `index_externalevent_id` ON `ExternalEventHistory` (`id`);
CREATE INDEX `index_externalevent_event_id` ON `ExternalEventHistory` (`eventid`);
CREATE INDEX `index_externalevent_user_id` ON `ExternalEventHistory` (`userid`,`applicationid`);
CREATE INDEX `index_externalevent_date` ON `ExternalEventHistory` (`eventdate`);

CREATE INDEX `index_internalevent_id` ON `EventHistory` (`id`);
CREATE INDEX `index_internalevent_event_id` ON `EventHistory` (`eventid`);
CREATE INDEX `index_internalevent_user_id` ON `EventHistory` (`userid`,`applicationid`);
CREATE INDEX `index_internalevent_externalevent_id` ON `EventHistory` (`externaleventid`);
CREATE INDEX `index_internalevent_date` ON `EventHistory` (`eventdate`);

CREATE INDEX `index_userpointshistory_user_id` ON `UserPointsHistory` (`userid`,`applicationid`);

CREATE INDEX `index_reward_id` ON `Reward` (`id`);
CREATE INDEX `index_userrewards_application_id` ON `UserRewards` (`applicationid`);
CREATE INDEX `index_userrewards_user_id` ON `UserRewards` (`userid`, `applicationid`);

CREATE INDEX `index_userlogins_user_id` ON `UserLogin` (`userid`, `applicationid`);

CREATE INDEX `index_usermessages_user_id` ON `Usermessages` (`userid`, `applicationid`);
CREATE INDEX `index_pastusermessages_user_id` ON `Pastusermessages` (`userid`, `applicationid`);



