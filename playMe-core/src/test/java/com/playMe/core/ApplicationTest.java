package com.playMe.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.db.entity.Application;
import com.playMe.core.service.ApplicationService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class ApplicationTest {
	
	private final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);

	@Autowired
	ApplicationService applicationService;
	
	private int applId = 0;
	
	
	@Test
    public void testApplicationInsert() {
    	logger.info("ApplicationTest: testApplicationInsert");
    	Application myApp = applicationService.insertApplication("Test Application");
    	if (myApp != null) {
    		applId = myApp.getId();
    		logger.info("Insert done. Retrieved name is " +myApp.getName());
    	} else {
    		logger.info("Insert failed");
    	}
    }

	
	@Test
    public void testApplicationSelect() {
    	logger.info("ApplicationTest: testApplicationSelect");
    	applId = 8;
        Application myApp = applicationService.getApplication(applId);
        if (myApp != null) {
        	logger.info("Found application with id " +myApp.getId() +" and name " +myApp.getName());
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
	
	@Test
    public void testApplicationDelete() {
    	logger.info("ApplicationTest: testApplicationDelete");
    	applId = 8;
        applicationService.deleteApplication(applId);
    }

}
