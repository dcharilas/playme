package com.playMe.core;

import java.util.List;

import com.playMe.core.model.ResponseItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.Userpointshistory;
import com.playMe.core.service.PointService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
public class PointsTest {
	
	private final Logger logger = LoggerFactory.getLogger(PointsTest.class);

	@Autowired
	PointService pointService;
	
	private long userId = 0;
	private int applId = 0;
	
	
	@Test
    public void testUserAwardPoints() {
    	logger.info("PointsTest: testUserAwardPoints");
    	userId = 2;
    	applId = 8;
    	int points = 10;
    	User user = pointService.awardUserWithPoints(points, userId, applId, null);
    	if (user != null) {
        	logger.info("User " +user.getId() +" has now " +user.getPoints() +" points.");
    	} else {
    		logger.info("Update failed");
    	}	
    }
	

	@Test
    public void testUserPointsHistorySelect() {
    	logger.info("PointsTest: testUserPointsHistorySelect");
    	userId = 2;
    	applId = 8;
    	ResponseItem<Userpointshistory> history = pointService.getUserPointsHistory(userId, applId);
    	if (history != null) {
        	logger.info("Found " +history.getItems().size() +" records in points history.");
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
}
