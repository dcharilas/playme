package com.playMe.core;

import com.playMe.core.constant.EventEnum;
import com.playMe.core.db.entity.Eventdefinition;
import com.playMe.core.db.entity.Eventhistory;
import com.playMe.core.db.entity.Externaleventhistory;
import com.playMe.core.dto.Event;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.ProcessEventService;
import com.playMe.core.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class ProcessEventTest {
	
	private final Logger logger = LoggerFactory.getLogger(ProcessEventTest.class);

	@Autowired
    ProcessEventService processEventService;

    @Autowired
    UserService userService;

    @Autowired
    ApplicationService applicationService;

	
	private long userId = 0;
	private int applId = 0;
	private int eventId = 0;


    @Test
    public void testExternalEventProcess() {
        logger.info("EventTest: testExternalEventProcess");

        eventId = 1;
        applId = 8;
        userId = 2;

        Externaleventhistory event = new Externaleventhistory();
        Eventdefinition def = new Eventdefinition();
        def.setId(eventId);
        def.setName(EventEnum.USER_ACTION_ASSIGN_POINTS.name());
        event.setEventdefinition(def);
        event.setUser(userService.getUserProfile(userId, applId));
        event.setApplication(applicationService.getApplication(applId));
        event.setEventdate(new Date());
        event.setNumericvalue1(15);
        Event eventObj = new Event(event);

        List<Eventhistory> internalEvents = processEventService.processEvent(eventObj);
        if (internalEvents != null) {
            logger.info("Process done.");
        } else {
            logger.info("Process failed");
        }
    }
	
}
