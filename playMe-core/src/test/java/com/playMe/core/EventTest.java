package com.playMe.core;

import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.Eventdefinition;
import com.playMe.core.db.entity.Externaleventhistory;
import com.playMe.core.db.entity.User;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.constant.EventEnum;
import com.playMe.core.service.EventService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class EventTest {
	
	private final Logger logger = LoggerFactory.getLogger(EventTest.class);

	@Autowired
	EventService eventService;

    @Autowired
    UserService userService;

    @Autowired
    ApplicationService applicationService;

	
	private long userId = 0;
	private int applId = 0;
	private int eventId = 0;
	private int badgeId = 0;
	
	
	@Test
    public void testEventDefinitionInsert() {
    	logger.info("EventTest: testEventDefinitionInsert");
    	boolean insertsSuccess = true;
    	for (EventEnum e : EventEnum.values()) {
    		if (!eventService.insertEventDefinition(e.name())) {
    			insertsSuccess = false;
    			break;
    		}
    	}
    	if (insertsSuccess) {
    		logger.info("All inserts done.");
    	} else {
    		logger.info("Inserts failed");
    	}
    }
	
	
	
	@Test
    public void testEventActionInsert() {
    	logger.info("EventTest: testEventActionInsert");
    	eventId = 1;
    	applId = 8;
    	badgeId = 1;
    	boolean insertsSuccess = eventService.insertEventAction(eventId, applId, badgeId, 10);    	
    	if (insertsSuccess) {
    		logger.info("Insert done.");
    	} else {
    		logger.info("Insert failed");
    	}
    }
	
	
	
	@Test
    public void testExternalEventInsert() {
    	logger.info("EventTest: testExternalEventInsert");
    	eventId = 1;
    	applId = 8;
    	userId = 2;

        Externaleventhistory event = new Externaleventhistory();
        Application application = new Application();
        application.setId(applId);
        event.setApplication(application);
        Eventdefinition def = new Eventdefinition();
        def.setId(eventId);
        event.setEventdefinition(def);
        User user = new User();
        user.setId(userId);
        event.setUser(user);
        event.setComments("test 123");

    	event = eventService.registerEvent(event);
    	if (event != null) {
    		logger.info("Insert done.");
    	} else {
    		logger.info("Insert failed");
    	}
    }

}
