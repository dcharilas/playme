package com.playMe.core;

import java.util.List;

import com.playMe.core.db.entity.Applicationbadge;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.db.entity.Badge;
import com.playMe.core.service.BadgeService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class BadgeTest {
	
	private final Logger logger = LoggerFactory.getLogger(BadgeTest.class);

	@Autowired
	BadgeService badgeService;
	
	private long userId = 0;
	private int applId = 0;
	private int badgeId = 0;
	
	
	@Test
    public void testUserBadgesSelect() {
    	logger.info("BadgeTest: testUserBadgesSelect");
    	userId = 2;
    	applId = 8;
        List<Badge> badges = badgeService.getUserBadges(userId, applId);
        if (badges != null) {
        	logger.info("Found " +badges.size() + " badges for user with id " +userId);
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
	
	@Test
    public void testApplicationBadgesSelect() {
    	logger.info("BadgeTest: testApplicationBadgesSelect");
    	applId = 8;
        List<Applicationbadge> badges = badgeService.getApplicationBadges(applId, false);
        if (badges != null) {
        	logger.info("Found " +badges.size() + " badges for application with id " +applId);
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
	
	@Test
    public void testApplicationBadgeToggle() {
    	logger.info("BadgeTest: testApplicationBadgeToggle");
    	applId = 8;
    	badgeId = 2;
        Applicationbadge badge = badgeService.toggleBadge(applId,badgeId,false);
        if (badge != null) {
        	logger.info("Deactivated badge with id " +badgeId + " for application with id " +applId);
    	} else {
    		logger.info("Deactivation failed");
    	}
        badge = badgeService.toggleBadge(applId,badgeId,true);
        if (badge != null) {
        	logger.info("Activated badge with id " +badgeId + " for application with id " +applId);
    	} else {
    		logger.info("Activation failed");
    	}	
    }
	
	
}
