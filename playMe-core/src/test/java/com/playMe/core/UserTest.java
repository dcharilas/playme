package com.playMe.core;

import com.playMe.core.db.entity.Application;
import com.playMe.core.util.ImageUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.db.entity.User;
import com.playMe.core.service.UserService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class UserTest {
	
	private final Logger logger = LoggerFactory.getLogger(UserTest.class);

	@Autowired
	UserService userService;
	
	private long userId = 0;
	private int applId = 0;
	
	
	@Test
    public void testUserInsert() {
    	logger.info("UserTest: testUserInsert");
    	User myUser = userService.insertNewUser(18, "Test User");
    	if (myUser != null) {
    		userId = myUser.getId();
    		logger.info("Insert done. Retrieved id is " +userId);
    	} else {
    		logger.info("Insert failed");
    	}
    }
	
	
	@Test
    public void testUserSelect() {
    	logger.info("UserTest: testUserSelect");
    	userId = 2;
    	applId = 8;
    	User myUser = userService.getUserProfile(userId, applId);
    	if (myUser != null) {
        	logger.info("Found user with id " +myUser.getId() +" and username " +myUser.getUsername());
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
	
	@Test
    public void testUserUpdate() {
    	logger.info("UserTest: testUserDelete");
    	userId = 2;
    	applId = 8;
        User myUser = new User();
        myUser.setLastname("Wayne");
        myUser.setFirstname("Bruce");
        myUser.setEmail("batman@gmail.com");
        Application application = new Application();
        application.setId(applId);
        myUser.setApplication(application);
        userService.saveUserInfo(myUser);
    }


    @Test
    public void testProfileCompletenessPercentage() {
        logger.info("UserTest: testProfileCompletenessPercentage");
        User myUser = new User();
        myUser.setLastname("Wayne");
        myUser.setFirstname("Bruce");
        myUser.setEmail("batman@gmail.com");
        int percentage = userService.getProfileCompletenessPercentage(myUser);
        logger.info("percentage = " +percentage);
        // based on current configuration, percentage should be 25%
        assert(percentage == 25);
    }

}
