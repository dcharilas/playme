package com.playMe.core;

import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.Applicationlevel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.db.entity.Level;
import com.playMe.core.service.LevelService;

import java.util.List;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
public class LevelTest {
	
	private final Logger logger = LoggerFactory.getLogger(LevelTest.class);

	@Autowired
	LevelService levelService;
	
	private long userId = 0;
	private int applId = 0;
	
	
	
	@Test
    public void testUserLevelSelect() {
    	logger.info("LevelTest: testUserLevelSelect");
    	userId = 2;
    	applId = 8;
    	Level level = levelService.getUserLevel(userId, applId);
    	if (level != null) {
        	logger.info("User level is " +level.getId() +" (name = " +level.getName() +")");
    	} else {
    		logger.info("Select failed");
    	}	

    }


	@Test
    public void testApplicationLevelsSelect() {
    	logger.info("LevelTest: testApplicationLevelsSelect");
    	applId = 8;
    	List<Applicationlevel> levels = levelService.getApplicationLevels(applId);
    	if (levels != null) {
        	logger.info("Found " +levels.size() +levels);
    	} else {
    		logger.info("Select failed");
    	}
    }


    @Test
    public void testApplicationLevelInsert() {
        logger.info("LevelTest: testApplicationLevelsSelect");
        applId = 8;

        Level level = new Level();
        level.setName("Level1");
        level.setDescriptionid(1);

        Application application = new Application();
        application.setId(applId);

        Applicationlevel appLevel = new Applicationlevel();
        appLevel.setLevel(level);
        appLevel.setApplication(application);
        appLevel.setActive(true);

        appLevel = levelService.saveLevelInfo(appLevel);
        if (appLevel != null) {
            logger.info("New level id is " +appLevel.getLevel().getId() +" (name = " +appLevel.getLevel().getName() +")");
        } else {
            logger.info("Select failed");
        }
    }
}
