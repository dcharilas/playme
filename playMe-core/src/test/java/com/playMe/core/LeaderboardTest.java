package com.playMe.core;

import com.playMe.core.model.ResponseItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.playMe.core.service.LeaderboardService;


@ContextConfiguration(locations = {"classpath:test-spring-context.xml"}) 
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class) 
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class LeaderboardTest {
	
	private final Logger logger = LoggerFactory.getLogger(LeaderboardTest.class);

	@Autowired
	LeaderboardService leaderboardService;

	private int applId = 0;
	
	
	@Test
    public void testGetLeaderBoard() {
    	logger.info("BadgeTest: testGetLeaderBoard");
    	applId = 8;
        ResponseItem users = leaderboardService.getLeaderBoard(applId,0,10);
        if (users != null) {
        	logger.info("Found " +users.getItems().size() + " users in leaderboard of application " +applId);
    	} else {
    		logger.info("Select failed");
    	}	
    }
	
}
