package com.playMe.core.model;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class StatisticsItem<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private T value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
