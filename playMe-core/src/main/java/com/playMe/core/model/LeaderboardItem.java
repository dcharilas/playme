package com.playMe.core.model;

import com.playMe.core.db.entity.Level;
import org.dozer.Mapping;

/**
 * @author Dimitris Charilas
 */
public class LeaderboardItem {

    @Mapping("username")
    private String username;

    @Mapping("avatar")
    private byte[] avatar;

    @Mapping("points")
    private int points;

    @Mapping("level")
    private Level level;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
