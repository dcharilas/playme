package com.playMe.core.util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class DateUtil {

    /**
     * Calculate age from birthdate
     *
     * @param dateOfBirth
     * @return
     */
    public static int getAge(Date dateOfBirth) {
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }


    /**
     * Returns date is number to be used as part of an identifier
     *
     * @return
     */
    public static String getDateForIdentifier() {
        Calendar today = Calendar.getInstance();
        return (today.get(Calendar.DAY_OF_MONTH)+"" +today.get(Calendar.MONTH)+""
                +today.get(Calendar.YEAR));
    }
}
