package com.playMe.core.util;

import java.text.DecimalFormat;

/**
 * @author Charilas
 */
public class NumberUtil {


    /**
     * Rounds a number with the giver precision
     *
     * @param value
     * @param precision
     * @return
     */
    public static double round(double value, int precision) {
        int baseNum = 10;
        for (int i=0; i<precision-1; i++) {
            baseNum *= baseNum;
        }
        return Math.round(value*baseNum)/baseNum;
    }

    /**
     * Rounds a number
     *
     * @param value
     * @return
     */
    public static String round(double value) {
        DecimalFormat myFormat = new DecimalFormat("0.000");
        return myFormat.format(value);
    }
}
