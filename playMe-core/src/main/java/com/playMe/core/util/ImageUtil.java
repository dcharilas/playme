package com.playMe.core.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author Charilas
 */
public class ImageUtil {

    private final static Logger logger = LoggerFactory.getLogger(ImageUtil.class);

    private static final int IMG_WIDTH = 55;
    private static final int IMG_HEIGHT = 55;


    /**
     * Reads an image from a given URI
     *
     * @param is
     * @param fileName
     * @return
     * @throws IOException
     */
    public static byte[] loadImage(InputStream is, String fileName) throws IOException {
        /*
         * Read the image
         */
        BufferedImage originalImage = ImageIO.read(is);
        /*
         * Resize
         */
        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        BufferedImage resizedImage = resizeImage(originalImage, type);
        /*
         * Create the temp file
         */
        File tempFile = File.createTempFile("result", FilenameUtils.getExtension(fileName));
        ImageIO.write(resizedImage, FilenameUtils.getExtension(fileName), tempFile);
        return FileUtils.readFileToByteArray(tempFile);
    }


    /**
     * Resizes an image
     *
     * @param originalImage
     * @param type
     * @return
     */
    public static BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        return resizedImage;
    }


    /**
     * Resizes an image that is stored in byte array
     *
     * @param fileData
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public static byte[] resizeImage(byte[] fileData, int width, int height) throws IOException {
        if (fileData == null) {
            return fileData;
        }

        logger.info("Resizing image");

        ByteArrayInputStream in = new ByteArrayInputStream(fileData);
        BufferedImage img = ImageIO.read(in);
        if (height == 0) {
            height = (width * img.getHeight()) / img.getWidth();
        }
        if (width == 0) {
            width = (height * img.getWidth()) / img.getHeight();
        }
        Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ImageIO.write(imageBuff, "jpg", buffer);
        return buffer.toByteArray();
    }


    /**
     * Wrapper for previous method, does not provide dimensions
     *
     * @param fileData
     * @return
     * @throws IOException
     */
    public static byte[] resizeImage(byte[] fileData) throws IOException {
        return resizeImage(fileData,IMG_WIDTH,IMG_HEIGHT);
    }


}
