package com.playMe.core.util;

import java.util.Random;

/**
 * @author Dimitris Charilas
 */
public class IdentifierUtil {

    private static int rewardIdentifierLength = 20;


    private static Random rand;

    /**
     * Creates a unique identifier that will be used to identify a reward assignment
     *
     * @param appId
     * @param userId
     * @return
     */
    public static String getRewardIdentifier(int appId, long userId) {
        String identifier = "REW" +appId +"_" +userId +"_" +DateUtil.getDateForIdentifier() +"_";
        int remainingDigits = rewardIdentifierLength - identifier.length();
        int randomPart = randInt((int) Math.pow(10, remainingDigits-1), (int) Math.pow(10, remainingDigits)-1);
        identifier += +randomPart;
        return identifier;
    }


    /**
     * Generates a random integer
     *
     * @param min
     * @param max
     * @return
     */
    private static int randInt(int min, int max) {
        if (rand == null) {
            rand = new Random();
        }
        return rand.nextInt((max - min) + 1) + min;
    }
}
