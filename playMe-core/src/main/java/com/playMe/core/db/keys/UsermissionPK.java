package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the usermissions database table.
 * 
 */
@Embeddable
public class UsermissionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int missionid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private long userid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int applicationid;

	public UsermissionPK() {
	}
	public int getMissionid() {
		return this.missionid;
	}
	public void setMissionid(int missionid) {
		this.missionid = missionid;
	}
	public long getUserid() {
		return this.userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UsermissionPK)) {
			return false;
		}
		UsermissionPK castOther = (UsermissionPK)other;
		return 
			(this.missionid == castOther.missionid)
			&& this.userid == castOther.userid
			&& (this.applicationid == castOther.applicationid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.missionid;
		hash = hash * prime + (int) this.userid;
		hash = hash * prime + this.applicationid;
		
		return hash;
	}
}