package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the userbadges database table.
 * 
 */
@Embeddable
public class UserbadgePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private long userid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int badgeid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int applicationid;

	public UserbadgePK() {
	}
	public long getUserid() {
		return this.userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public int getBadgeid() {
		return this.badgeid;
	}
	public void setBadgeid(int badgeid) {
		this.badgeid = badgeid;
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserbadgePK)) {
			return false;
		}
		UserbadgePK castOther = (UserbadgePK)other;
		return 
			(this.userid == castOther.userid)
			&& (this.badgeid == castOther.badgeid)
			&& (this.applicationid == castOther.applicationid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + (int) this.userid;
		hash = hash * prime + this.badgeid;
		hash = hash * prime + this.applicationid;
		
		return hash;
	}
}