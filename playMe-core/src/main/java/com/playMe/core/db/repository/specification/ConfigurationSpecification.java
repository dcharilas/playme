package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Configuration entity
 *
 * @author Dimitris Charilas
 */
public class ConfigurationSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Configuration> createSpecFromSearchForm(final Configuration form) {
        return new Specification<Configuration>() {

            @Override
            public Predicate toPredicate(Root<Configuration> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }
                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Configuration, Application> applicationJoin = root.join(Configuration_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                return finalPredicate;
             }
        };
    }

}
