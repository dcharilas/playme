package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.playMe.core.db.keys.EventactionPK;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the eventaction database table.
 * 
 */
@Entity
@Table(name="eventaction", schema="", catalog="playme")
@NamedQuery(name="Eventaction.findAll", query="SELECT e FROM Eventaction e")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Eventaction implements Serializable {
	private static final long serialVersionUID = 1L;

    @JsonIgnore
	@EmbeddedId
	private EventactionPK id;

	private int pointsaward;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
	private Application application;

	//bi-directional many-to-one association to Badge
	@ManyToOne
	@JoinColumn(name="badgeaward")
	private Badge badge;

	//bi-directional many-to-one association to Eventdefinition
	@ManyToOne
	@JoinColumn(name="eventid", nullable=false, insertable=false, updatable=false)
	private Eventdefinition eventdefinition;

	public Eventaction() {
	}

	public EventactionPK getId() {
		return this.id;
	}

	public void setId(EventactionPK id) {
		this.id = id;
	}

	public int getPointsaward() {
		return this.pointsaward;
	}

	public void setPointsaward(int pointsaward) {
		this.pointsaward = pointsaward;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Badge getBadge() {
		return this.badge;
	}

	public void setBadge(Badge badge) {
		this.badge = badge;
	}

	public Eventdefinition getEventdefinition() {
		return this.eventdefinition;
	}

	public void setEventdefinition(Eventdefinition eventdefinition) {
		this.eventdefinition = eventdefinition;
	}

}