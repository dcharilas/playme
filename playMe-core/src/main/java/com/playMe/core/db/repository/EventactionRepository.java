package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Eventaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface EventactionRepository extends JpaRepository<Eventaction,Integer>, JpaSpecificationExecutor<Eventaction> {

    List<Eventaction> findByApplication_Id(int appId);

    Eventaction findByEventdefinition_IdAndApplication_Id(int eventId,int appId);
}
