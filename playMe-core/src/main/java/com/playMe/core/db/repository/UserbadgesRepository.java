package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Badge;
import com.playMe.core.db.entity.Userbadge;
import com.playMe.core.db.keys.UserbadgePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserbadgesRepository extends JpaRepository<Userbadge,UserbadgePK> {

    @Query("SELECT b FROM Badge b, Userbadge ub WHERE b.id = ub.badge.id AND ub.application.id = :appId AND ub.user.id = :userId")
    List<Badge> findUserBadges(@Param("userId") long userId, @Param("appId") int appId);
}
