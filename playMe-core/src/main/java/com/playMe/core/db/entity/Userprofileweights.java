package com.playMe.core.db.entity;

import javax.persistence.*;

/**
 * The persistent class for the userprofileweights database table.
 *
 */
@Entity
@Table(name = "userprofileweights", schema="", catalog="playme")
public class Userprofileweights {

    @Id
    @Column(name = "fieldname", length=100)
    private String fieldname;

    private int weight;

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

}
