package com.playMe.core.db.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the eventhistory database table.
 *
 */
@Entity
@Table(name="eventhistory", schema="", catalog="playme")
@NamedQuery(name="Eventhistory.findAll", query="SELECT e FROM Eventhistory e")
public class Eventhistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private long id;

    @ManyToOne
    @JoinColumn(name="badgeawarded")
	private Badge badge;

    @Temporal(TemporalType.TIMESTAMP)
    private Date eventdate;

    @ManyToOne
    @JoinColumn(name="levelawarded")
	private Level level;

	private int missionidcompleted;

	private int pointsawarded;

	private int taskidcompleted;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Eventdefinition
	@ManyToOne
	@JoinColumn(name="eventid")
	private Eventdefinition eventdefinition;

	//bi-directional many-to-one association to Externaleventhistory
	@ManyToOne
	@JoinColumn(name="externaleventid")
	private Externaleventhistory externaleventhistory;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid")
	private User user;

	//bi-directional many-to-one association to Userpointshistory
    @JsonIgnore
	@OneToMany(mappedBy="eventhistory")
	private List<Userpointshistory> userpointshistories;

    /*
     * Custom fields to allow date searching
     */
    @Transient
    private Date searchDateStart;

    @Transient
    private Date searchDateEnd;

	public Eventhistory() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Date getEventdate() {
		return this.eventdate;
	}

	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}

	public int getMissionidcompleted() {
		return this.missionidcompleted;
	}

	public void setMissionidcompleted(int missionidcompleted) {
		this.missionidcompleted = missionidcompleted;
	}

	public int getPointsawarded() {
		return this.pointsawarded;
	}

	public void setPointsawarded(int pointsawarded) {
		this.pointsawarded = pointsawarded;
	}

	public int getTaskidcompleted() {
		return this.taskidcompleted;
	}

	public void setTaskidcompleted(int taskidcompleted) {
		this.taskidcompleted = taskidcompleted;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Eventdefinition getEventdefinition() {
		return this.eventdefinition;
	}

	public void setEventdefinition(Eventdefinition eventdefinition) {
		this.eventdefinition = eventdefinition;
	}

	public Externaleventhistory getExternaleventhistory() {
		return this.externaleventhistory;
	}

	public void setExternaleventhistory(Externaleventhistory externaleventhistory) {
		this.externaleventhistory = externaleventhistory;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Userpointshistory> getUserpointshistories() {
		return this.userpointshistories;
	}

	public void setUserpointshistories(List<Userpointshistory> userpointshistories) {
		this.userpointshistories = userpointshistories;
	}

	public Userpointshistory addUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().add(userpointshistory);
		userpointshistory.setEventhistory(this);

		return userpointshistory;
	}

	public Userpointshistory removeUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().remove(userpointshistory);
		userpointshistory.setEventhistory(null);

		return userpointshistory;
	}


    public Date getSearchDateStart() {
        return searchDateStart;
    }

    public void setSearchDateStart(Date searchDateStart) {
        this.searchDateStart = searchDateStart;
    }

    public Date getSearchDateEnd() {
        return searchDateEnd;
    }

    public void setSearchDateEnd(Date searchDateEnd) {
        this.searchDateEnd = searchDateEnd;
    }
}