package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Eventaction entity
 *
 * @author Dimitris Charilas
 */
public class EventActionSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Eventaction> createSpecFromSearchForm(final Eventaction form) {
        return new Specification<Eventaction>() {

            @Override
            public Predicate toPredicate(Root<Eventaction> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Eventaction, Application> applicationJoin = root.join(Eventaction_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getEventdefinition() != null && form.getEventdefinition().getId() > 0) {
                    Join<Eventaction, Eventdefinition> definitionJoin = root.join(Eventaction_.eventdefinition);
                    finalPredicate.getExpressions().add(cb.equal(definitionJoin.<Integer>get(Eventdefinition_.id), form.getEventdefinition().getId()));
                }

                return finalPredicate;
             }
        };
    }

}
