package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Eventhistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface EventhistoryRepository extends JpaRepository<Eventhistory,Integer>, JpaSpecificationExecutor<Eventhistory> {

    @Query(value = "SELECT DISTINCT(DATE(e.eventdate)) AS thedate,\n" +
            "  CASE\n" +
            "     WHEN e.pointsawarded IS NOT NULL AND e.pointsawarded > 0 THEN 'POINTS'\n" +
            "     WHEN e.badgeawarded IS NOT NULL THEN 'BADGE'\n" +
            "     WHEN e.levelawarded IS NOT NULL THEN 'LEVEL'\n" +
            "     ELSE 'UNKNOWN'\n" +
            "  END AS type, COUNT(*)\n" +
            "FROM eventhistory e\n" +
            "WHERE DATE(e.eventdate) >= DATE_SUB(CURDATE(), INTERVAL :days DAY)\n" +
            "GROUP BY type, thedate\n" +
            "ORDER BY thedate ASC;", nativeQuery = true)
    List<Object[]> getEventsPerDay(@Param("days") int days);


    @Query(value = "SELECT DISTINCT(DATE(e.eventdate)) AS thedate,\n" +
            "  CASE\n" +
            "     WHEN e.pointsawarded IS NOT NULL AND e.pointsawarded > 0 THEN 'POINTS'\n" +
            "     WHEN e.badgeawarded IS NOT NULL THEN 'BADGE'\n" +
            "     WHEN e.levelawarded IS NOT NULL THEN 'LEVEL'\n" +
            "     ELSE 'UNKNOWN'\n" +
            "  END AS type, COUNT(*)\n" +
            "FROM eventhistory e\n" +
            "WHERE DATE(e.eventdate) >= DATE_SUB(CURDATE(), INTERVAL :days DAY)\n" +
            "AND applicationid = :applId\n" +
            "GROUP BY type, thedate\n" +
            "ORDER BY thedate ASC;", nativeQuery = true)
    List<Object[]> getApplicationEventsPerDay(@Param("days") int days, @Param("applId") int applId);
}
