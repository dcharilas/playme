package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Badge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface BadgeRepository extends JpaRepository<Badge,Integer> {

    @Query("SELECT ab FROM Badge b, Applicationbadge ab WHERE b.id = ab.badge.id AND ab.application.id = :appId AND b.name = :name")
    List<Badge> findApplicationBadge(@Param("appId") int appId, @Param("name") String name);

    @Query("SELECT b FROM Badge b, Applicationbadge ab WHERE b.id = ab.badge.id AND ab.application.id = :appId AND ab.active = true")
    List<Badge> findApplicationActiveBadges(@Param("appId") int appId);

}
