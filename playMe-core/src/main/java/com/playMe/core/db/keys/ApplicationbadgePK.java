package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the applicationbadge database table.
 * 
 */
@Embeddable
public class ApplicationbadgePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int applicationid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int badgeid;

	public ApplicationbadgePK() {
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}
	public int getBadgeid() {
		return this.badgeid;
	}
	public void setBadgeid(int badgeid) {
		this.badgeid = badgeid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ApplicationbadgePK)) {
			return false;
		}
		ApplicationbadgePK castOther = (ApplicationbadgePK)other;
		return 
			(this.applicationid == castOther.applicationid)
			&& (this.badgeid == castOther.badgeid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.applicationid;
		hash = hash * prime + this.badgeid;
		
		return hash;
	}
}