package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Eventdefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface EventdefinitionRepository extends JpaRepository<Eventdefinition,Integer>, JpaSpecificationExecutor<Eventdefinition> {

    Eventdefinition findByName(String name);

}
