package com.playMe.core.db.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the userpointshistory database table.
 * 
 */
@Entity
@Table(name="userpointshistory", schema="", catalog="playme")
@NamedQuery(name="Userpointshistory.findAll", query="SELECT u FROM Userpointshistory u")
public class Userpointshistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private long id;

	private int points;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedate;

	//bi-directional many-to-one association to User
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="userid")
	private User user;

	//bi-directional many-to-one association to Application
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Eventhistory
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="eventhistoryid")
	private Eventhistory eventhistory;

	public Userpointshistory() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPoints() {
		return this.points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Eventhistory getEventhistory() {
		return this.eventhistory;
	}

	public void setEventhistory(Eventhistory eventhistory) {
		this.eventhistory = eventhistory;
	}

}