package com.playMe.core.db.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the eventdefinition database table.
 * 
 */
@Entity
@Table(name="eventdefinition", schema="", catalog="playme")
@NamedQuery(name="Eventdefinition.findAll", query="SELECT e FROM Eventdefinition e")
public class Eventdefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(length=100)
	private String name;

	//bi-directional many-to-one association to Eventaction
    @JsonIgnore
	@OneToMany(mappedBy="eventdefinition")
	private List<Eventaction> eventactions;

	//bi-directional many-to-one association to Eventhistory
    @JsonIgnore
	@OneToMany(mappedBy="eventdefinition")
	private List<Eventhistory> eventhistories;

	//bi-directional many-to-one association to Externaleventhistory
    @JsonIgnore
	@OneToMany(mappedBy="eventdefinition")
	private List<Externaleventhistory> externaleventhistories;

	public Eventdefinition() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Eventaction> getEventactions() {
		return this.eventactions;
	}

	public void setEventactions(List<Eventaction> eventactions) {
		this.eventactions = eventactions;
	}

	public Eventaction addEventaction(Eventaction eventaction) {
		getEventactions().add(eventaction);
		eventaction.setEventdefinition(this);

		return eventaction;
	}

	public Eventaction removeEventaction(Eventaction eventaction) {
		getEventactions().remove(eventaction);
		eventaction.setEventdefinition(null);

		return eventaction;
	}

	public List<Eventhistory> getEventhistories() {
		return this.eventhistories;
	}

	public void setEventhistories(List<Eventhistory> eventhistories) {
		this.eventhistories = eventhistories;
	}

	public Eventhistory addEventhistory(Eventhistory eventhistory) {
		getEventhistories().add(eventhistory);
		eventhistory.setEventdefinition(this);

		return eventhistory;
	}

	public Eventhistory removeEventhistory(Eventhistory eventhistory) {
		getEventhistories().remove(eventhistory);
		eventhistory.setEventdefinition(null);

		return eventhistory;
	}

	public List<Externaleventhistory> getExternaleventhistories() {
		return this.externaleventhistories;
	}

	public void setExternaleventhistories(List<Externaleventhistory> externaleventhistories) {
		this.externaleventhistories = externaleventhistories;
	}

	public Externaleventhistory addExternaleventhistory(Externaleventhistory externaleventhistory) {
		getExternaleventhistories().add(externaleventhistory);
		externaleventhistory.setEventdefinition(this);

		return externaleventhistory;
	}

	public Externaleventhistory removeExternaleventhistory(Externaleventhistory externaleventhistory) {
		getExternaleventhistories().remove(externaleventhistory);
		externaleventhistory.setEventdefinition(null);

		return externaleventhistory;
	}

}