package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Userlogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserloginRepository extends JpaRepository<Userlogin,Integer> {

    @Query(value = "SELECT IF(COUNT(1) > 0, 1, 0) AS has_consec\n" +
            "FROM (\n" +
            "    SELECT *\n" +
            "    FROM (\n" +
            "        SELECT IF(b.date IS NULL, @val\\:=@val+1, @val) AS consec_set\n" +
            "        FROM UserLogin a\n" +
            "        CROSS JOIN (SELECT @val\\:=0) var_init\n" +
            "        LEFT JOIN UserLogin b ON\n" +
            "            a.userid = b.userid AND\n" +
            "            a.date = b.date + INTERVAL 1 DAY\n" +
            "        WHERE a.userid = :userId\n" +
            "        AND a.applicationid = :applId\n" +
            "        AND DATE(a.date) >= DATE_SUB(CURDATE(), INTERVAL (1 + :consecutiveDays) DAY)\n" +
            "    ) a\n" +
            "    GROUP BY a.consec_set\n" +
            "    HAVING COUNT(1) >= :consecutiveDays\n" +
            ") a", nativeQuery = true)
    int checkConsecutiveLogins(int applId, long userId, int consecutiveDays);
}
