package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Applicationbadge;
import com.playMe.core.db.entity.Badge;
import com.playMe.core.db.keys.ApplicationbadgePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface ApplicationbadgesRepository extends JpaRepository<Applicationbadge,ApplicationbadgePK>, JpaSpecificationExecutor<Applicationbadge> {

    List<Applicationbadge> findByApplication_Id(int appId);

    List<Applicationbadge> findByBadge_Id(int badgeId);

    @Query("SELECT ab FROM Badge b, Applicationbadge ab WHERE b.id = ab.badge.id AND ab.application.id = :appId")
    List<Applicationbadge> findApplicationBadges(@Param("appId") int appId);

    @Query("SELECT ab FROM Badge b, Applicationbadge ab WHERE b.id = ab.badge.id AND ab.application.id = :appId AND ab.active = true")
    List<Applicationbadge> findActiveApplicationBadges(@Param("appId") int appId);

    Applicationbadge findByApplication_IdAndBadge_Id(int appId, int badgeId);

    @Modifying
    @Query("UPDATE Applicationbadge ab SET ab.active = :active WHERE ab.badge.id = :badgeId AND ab.application.id = :appId")
    void toggleApplicationBadge(@Param("appId") int appId, @Param("badgeId") int badgeId, @Param("active") boolean active);

}
