package com.playMe.core.db.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Entity
@Table(name="pastusermessages", schema="", catalog="playme")
@NamedQuery(name="Pastusermessages.findAll", query="SELECT d FROM Pastusermessages d")
public class Pastusermessages {

    @Id
    @Column(name = "id")
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;

    @Column(name="msgcode", length=30)
    private String msgcode;

    @Column(name="value_1", length=30)
    private String value1;

    @Column(name="value_2", length=30)
    private String value2;

    @Temporal(TemporalType.TIMESTAMP)
    private Date viewedon;

    @Temporal(TemporalType.TIMESTAMP)
    private Date archivedon;

    //bi-directional many-to-one association to Application
    @ManyToOne
    @JoinColumn(name="applicationid")
    private Application application;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="userid")
    private User user;


    @Transient
    private List<Usermessagecodes> messages;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMsgcode() {
        return msgcode;
    }

    public void setMsgcode(String msgcode) {
        this.msgcode = msgcode;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public Date getViewedon() {
        return viewedon;
    }

    public void setViewedon(Date viewedon) {
        this.viewedon = viewedon;
    }

    public Date getArchivedon() {
        return archivedon;
    }

    public void setArchivedon(Date archivedon) {
        this.archivedon = archivedon;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Usermessagecodes> getMessages() {
        return messages;
    }

    public void setMessages(List<Usermessagecodes> messages) {
        this.messages = messages;
    }
}
