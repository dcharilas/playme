package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration,Integer>, JpaSpecificationExecutor<Configuration> {


}
