package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Userreward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserrewardsRepository extends JpaRepository<Userreward,Long> {

    List<Userreward> findByUser_IdAndApplicationId(long userId, int applId);

    Userreward findByIdentifier(String identifier);


    /* Statistics */

    @Query("SELECT a.name, count(*)\n" +
            "FROM Userreward u, Application a\n" +
            "WHERE u.application.id = a.id\n" +
            "group by u.application.id")
    List<Object[]> countRedeemedRewardsPerApplication();


    @Query("SELECT r.name, count(*)\n" +
            "FROM Userreward u, Reward r\n" +
            "WHERE u.application.id = :applId\n" +
            "AND u.reward.id = r.id\n" +
            "group by u.reward.id")
    List<Object[]> countRedeemedRewardsPerType(@Param("applId") int applId);


    @Query(value = "SELECT DATE(u.redeemdate) as date, 'REDEEM_POINTS', SUM(r.pointsreq) as points\n" +
            "FROM  userrewards u, reward r\n" +
            "WHERE u.applicationid = :applId\n" +
            "AND u.rewardid = r.id\n" +
            "AND DATE(u.redeemdate) >= DATE_SUB(CURDATE(), INTERVAL :days DAY)\n" +
            "GROUP BY DATE(u.redeemdate)\n" +
            "ORDER BY u.redeemdate ASC", nativeQuery = true)
    List<Object[]> countRedeemedPointsPerDay(@Param("days") int days, @Param("applId") int applId);

}
