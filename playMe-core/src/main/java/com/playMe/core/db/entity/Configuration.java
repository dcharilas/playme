package com.playMe.core.db.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the configuration database table.
 *
 */
@Entity
@Table(name = "configuration", schema="", catalog="playme")
@NamedQuery(name="Configuration.findAll", query="SELECT c FROM Configuration c")
public class Configuration implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique=true, nullable=false)
    private int id;

    //bi-directional many-to-one association to Application
    @ManyToOne
    @JoinColumn(name="applicationid")
    private Application application;

    @Basic
    @Column(name = "property")
    private String property;

    @Basic
    @Column(name = "value")
    private String value;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
