package com.playMe.core.db.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the socialintegration database table.
 * 
 */
@Entity
@Table(name="socialintegration", schema="", catalog="playme")
@NamedQuery(name="Socialintegration.findAll", query="SELECT s FROM Socialintegration s")
public class Socialintegration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date integrationdate;

	private int socialid;

	@Column(length=30)
	private String socialnetwork;

	private int username;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid")
	private User user;

	public Socialintegration() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getIntegrationdate() {
		return this.integrationdate;
	}

	public void setIntegrationdate(Date integrationdate) {
		this.integrationdate = integrationdate;
	}

	public int getSocialid() {
		return this.socialid;
	}

	public void setSocialid(int socialid) {
		this.socialid = socialid;
	}

	public String getSocialnetwork() {
		return this.socialnetwork;
	}

	public void setSocialnetwork(String socialnetwork) {
		this.socialnetwork = socialnetwork;
	}

	public int getUsername() {
		return this.username;
	}

	public void setUsername(int username) {
		this.username = username;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}