package com.playMe.core.db.entity;


import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user", schema="", catalog="playme")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private long id;

	@Column(length=150)
	private String address;

	private int age;

	@Lob
	@Column(columnDefinition="mediumblob")
	private byte[] avatar;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@Column(length=100)
	private String email;

	@Column(length=100)
	private String firstname;

	@Column(length=1)
	private String gender;

	@Column(length=100)
	private String lastname;

	@Column(length=15)
	private String phone;

	private int points;

    private int redeemablepoints;

	@Column(length=10)
	private String postcode;

	private Date regDate;

	@Column(length=30)
	private String username;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Level
	@ManyToOne
	@JoinColumn(name="levelid")
	private Level level;

	//bi-directional many-to-one association to Userbadge
    @JsonIgnore
	@OneToMany(mappedBy="user")
	private List<Userbadge> userbadges;
	
	//bi-directional many-to-one association to Userlevelprogression
    @JsonIgnore
	@OneToMany(mappedBy="user")
	private List<Userlevelprogression> userlevelprogressions;
	
	//bi-directional many-to-one association to Socialintegration
    @JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy="user")
	private List<Socialintegration> socialintegrations;
	
	//bi-directional many-to-one association to Usermission
    @JsonIgnore
	@OneToMany(mappedBy="user")
	private List<Usermission> usermissions;
	
	//bi-directional many-to-one association to Userpointshistory
    @JsonIgnore
	@OneToMany(mappedBy="user")
	private List<Userpointshistory> userpointshistories;

    @Transient
    private int completeness;

	public User() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public byte[] getAvatar() {
		return this.avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getPoints() {
		return this.points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

    public int getRedeemablepoints() {
        return redeemablepoints;
    }

    public void setRedeemablepoints(int redeemablepoints) {
        this.redeemablepoints = redeemablepoints;
    }

    public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Level getLevel() {
		return this.level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public List<Userbadge> getUserbadges() {
		return this.userbadges;
	}

	public void setUserbadges(List<Userbadge> userbadges) {
		this.userbadges = userbadges;
	}

    public int getCompleteness() {
        return completeness;
    }

    public void setCompleteness(int completeness) {
        this.completeness = completeness;
    }

    public Userbadge addUserbadge(Userbadge userbadge) {
		getUserbadges().add(userbadge);
		userbadge.setUser(this);

		return userbadge;
	}

	public Userbadge removeUserbadge(Userbadge userbadge) {
		getUserbadges().remove(userbadge);
		userbadge.setUser(null);

		return userbadge;
	}
	

	public List<Userlevelprogression> getUserlevelprogressions() {
		return this.userlevelprogressions;
	}

	public void setUserlevelprogressions(List<Userlevelprogression> userlevelprogressions) {
		this.userlevelprogressions = userlevelprogressions;
	}

	public Userlevelprogression addUserlevelprogression(Userlevelprogression userlevelprogression) {
		getUserlevelprogressions().add(userlevelprogression);
		userlevelprogression.setUser(this);

		return userlevelprogression;
	}

	public Userlevelprogression removeUserlevelprogression(Userlevelprogression userlevelprogression) {
		getUserlevelprogressions().remove(userlevelprogression);
		userlevelprogression.setUser(null);

		return userlevelprogression;
	}
	
	public List<Socialintegration> getSocialintegrations() {
		return this.socialintegrations;
	}

	public void setSocialintegrations(List<Socialintegration> socialintegrations) {
		this.socialintegrations = socialintegrations;
	}

	public Socialintegration addSocialintegration(Socialintegration socialintegration) {
		getSocialintegrations().add(socialintegration);
		socialintegration.setUser(this);

		return socialintegration;
	}

	public Socialintegration removeSocialintegration(Socialintegration socialintegration) {
		getSocialintegrations().remove(socialintegration);
		socialintegration.setUser(null);

		return socialintegration;
	}
	
	public List<Usermission> getUsermissions() {
		return this.usermissions;
	}

	public void setUsermissions(List<Usermission> usermissions) {
		this.usermissions = usermissions;
	}

	public Usermission addUsermission(Usermission usermission) {
		getUsermissions().add(usermission);
		usermission.setUser(this);

		return usermission;
	}

	public Usermission removeUsermission(Usermission usermission) {
		getUsermissions().remove(usermission);
		usermission.setUser(null);

		return usermission;
	}

	public List<Userpointshistory> getUserpointshistories() {
		return this.userpointshistories;
	}

	public void setUserpointshistories(List<Userpointshistory> userpointshistories) {
		this.userpointshistories = userpointshistories;
	}

	public Userpointshistory addUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().add(userpointshistory);
		userpointshistory.setUser(this);

		return userpointshistory;
	}

	public Userpointshistory removeUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().remove(userpointshistory);
		userpointshistory.setUser(null);

		return userpointshistory;
	}

}