package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.Usermessagecodes;
import com.playMe.core.db.entity.Usermessagecodes_;
import com.playMe.core.db.keys.UsermessagecodesPK;
import com.playMe.core.db.keys.UsermessagecodesPK_;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Usermessagecodes entity
 *
 * @author Dimitris Charilas
 */
public class UserMessageCodeSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Usermessagecodes> createSpecFromSearchForm(final Usermessagecodes form) {
        return new Specification<Usermessagecodes>() {

            @Override
            public Predicate toPredicate(Root<Usermessagecodes> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getId() != null) {
                    Join<Usermessagecodes, UsermessagecodesPK> keyJoin = root.join(Usermessagecodes_.id);
                    if (!StringUtils.isEmpty(form.getId().getMsgcode())) {
                        finalPredicate.getExpressions().add(cb.like(keyJoin.<String>get(UsermessagecodesPK_.msgcode), JpaUtil.likePattern(form.getId().getMsgcode())));
                    }
                    if (!StringUtils.isEmpty(form.getId().getLocale())) {
                        finalPredicate.getExpressions().add(cb.like(keyJoin.<String>get(UsermessagecodesPK_.locale), JpaUtil.likePattern(form.getId().getLocale())));
                    }
                    if (form.getId().getApplicationid() > 0) {
                        finalPredicate.getExpressions().add(cb.equal(keyJoin.<Integer>get(UsermessagecodesPK_.applicationid), form.getId().getApplicationid()));
                    }
                }

                return finalPredicate;
             }
        };
    }

}
