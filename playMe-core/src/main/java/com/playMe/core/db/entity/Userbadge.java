package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.playMe.core.db.keys.UserbadgePK;

import java.util.Date;


/**
 * The persistent class for the userbadges database table.
 * 
 */
@Entity
@Table(name="userbadges", schema="", catalog="playme")
@NamedQuery(name="Userbadge.findAll", query="SELECT u FROM Userbadge u")
public class Userbadge implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserbadgePK id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date unlockdate;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
	private Application application;

	//bi-directional many-to-one association to Badge
	@ManyToOne
	@JoinColumn(name="badgeid", nullable=false, insertable=false, updatable=false)
	private Badge badge;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid", nullable=false, insertable=false, updatable=false)
	private User user;

	public Userbadge() {
	}

	public UserbadgePK getId() {
		return this.id;
	}

	public void setId(UserbadgePK id) {
		this.id = id;
	}

	public Date getUnlockdate() {
		return this.unlockdate;
	}

	public void setUnlockdate(Date unlockdate) {
		this.unlockdate = unlockdate;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Badge getBadge() {
		return this.badge;
	}

	public void setBadge(Badge badge) {
		this.badge = badge;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}