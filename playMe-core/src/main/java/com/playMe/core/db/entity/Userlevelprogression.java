package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.playMe.core.db.keys.UserlevelprogressionPK;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Date;


/**
 * The persistent class for the userlevelprogression database table.
 * 
 */
@Entity
@Table(name="userlevelprogression", schema="", catalog="playme")
@NamedQuery(name="Userlevelprogression.findAll", query="SELECT u FROM Userlevelprogression u")
public class Userlevelprogression implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserlevelprogressionPK id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date unlockdate;

	//bi-directional many-to-one association to Application
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
	private Application application;

	//bi-directional many-to-one association to Level
	@ManyToOne
	@JoinColumn(name="levelid", nullable=false, insertable=false, updatable=false)
	private Level level;

	//bi-directional many-to-one association to User
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="userid", nullable=false, insertable=false, updatable=false)
	private User user;

	public Userlevelprogression() {
	}

	public UserlevelprogressionPK getId() {
		return this.id;
	}

	public void setId(UserlevelprogressionPK id) {
		this.id = id;
	}

	public Date getUnlockdate() {
		return this.unlockdate;
	}

	public void setUnlockdate(Date unlockdate) {
		this.unlockdate = unlockdate;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Level getLevel() {
		return this.level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}