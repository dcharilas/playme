package com.playMe.core.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Blob;
import java.util.List;


/**
 * The persistent class for the level database table.
 * 
 */
@Entity
@Table(name="level", schema="", catalog="playme")
@NamedQuery(name="Level.findAll", query="SELECT l FROM Level l")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Level implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	private int descriptionid;

	@Lob
	@Column(columnDefinition="mediumblob")
	private byte[] image;

	@Column(length=100)
	private String name;

	//bi-directional many-to-one association to User
    @JsonIgnore
	@OneToMany(mappedBy="level")
	private List<User> users;

    @Transient
    private List<Displaytext> descriptions;

    public Level() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDescriptionid() {
		return this.descriptionid;
	}

	public void setDescriptionid(int descriptionid) {
		this.descriptionid = descriptionid;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

    public List<Displaytext> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Displaytext> descriptions) {
        this.descriptions = descriptions;
    }

    public User addUser(User user) {
		getUsers().add(user);
		user.setLevel(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setLevel(null);

		return user;
	}

}