package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the eventaction database table.
 * 
 */
@Embeddable
public class EventactionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int eventid;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int applicationid;

	public EventactionPK() {
	}
	public int getEventid() {
		return this.eventid;
	}
	public void setEventid(int eventid) {
		this.eventid = eventid;
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EventactionPK)) {
			return false;
		}
		EventactionPK castOther = (EventactionPK)other;
		return 
			(this.eventid == castOther.eventid)
			&& (this.applicationid == castOther.applicationid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.eventid;
		hash = hash * prime + this.applicationid;
		
		return hash;
	}
}