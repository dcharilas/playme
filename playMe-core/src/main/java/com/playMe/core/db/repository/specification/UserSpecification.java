package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.Application_;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.User_;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for User entity
 *
 * @author Dimitris Charilas
 */
public class UserSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<User> createSpecFromSearchForm(final User form) {
        return new Specification<User>() {

            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (!StringUtils.isEmpty(form.getUsername())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(User_.username), JpaUtil.likePattern(form.getUsername())));
                }
                if (!StringUtils.isEmpty(form.getFirstname())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(User_.firstname), JpaUtil.likePattern(form.getFirstname())));
                }
                if (!StringUtils.isEmpty(form.getLastname())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(User_.lastname), JpaUtil.likePattern(form.getLastname())));
                }
                if (!StringUtils.isEmpty(form.getGender())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(User_.gender), JpaUtil.likePattern(form.getGender())));
                }
                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<User, Application> applicationJoin = root.join(User_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }

                return finalPredicate;
             }
        };
    }

}
