package com.playMe.core.db.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the taskrequirementdefinition database table.
 * 
 */
@Entity
@Table(name="taskrequirementdefinition", schema="", catalog="playme")
@NamedQuery(name="Taskrequirementdefinition.findAll", query="SELECT t FROM Taskrequirementdefinition t")
public class Taskrequirementdefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(length=100)
	private String requirementname;

	@Column(length=100)
	private String requirementvalue;

	//bi-directional many-to-one association to Missiontask
	@ManyToOne
	@JoinColumn(name="taskid")
	private Missiontask missiontask;

	public Taskrequirementdefinition() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRequirementname() {
		return this.requirementname;
	}

	public void setRequirementname(String requirementname) {
		this.requirementname = requirementname;
	}

	public String getRequirementvalue() {
		return this.requirementvalue;
	}

	public void setRequirementvalue(String requirementvalue) {
		this.requirementvalue = requirementvalue;
	}

	public Missiontask getMissiontask() {
		return this.missiontask;
	}

	public void setMissiontask(Missiontask missiontask) {
		this.missiontask = missiontask;
	}

}