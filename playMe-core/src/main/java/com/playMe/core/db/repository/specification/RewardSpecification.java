package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Reward entity
 *
 * @author Dimitris Charilas
 */
public class RewardSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Reward> createSpecFromSearchForm(final Reward form) {
        return new Specification<Reward>() {

            @Override
            public Predicate toPredicate(Root<Reward> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Reward, Application> applicationJoin = root.join(Reward_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }

                if (form.getActive() != null) {
                    finalPredicate.getExpressions().add(cb.equal(root.<Boolean>get(Reward_.active), form.getActive()));
                }

                return finalPredicate;
             }
        };
    }

}
