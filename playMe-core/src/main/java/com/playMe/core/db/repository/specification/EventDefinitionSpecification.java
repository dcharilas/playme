package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Eventdefinition entity
 *
 * @author Dimitris Charilas
 */
public class EventDefinitionSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Eventdefinition> createSpecFromSearchForm(final Eventdefinition form) {
        return new Specification<Eventdefinition>() {

            @Override
            public Predicate toPredicate(Root<Eventdefinition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (!StringUtils.isEmpty(form.getName())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(Eventdefinition_.name), JpaUtil.likePattern(form.getName())));
                }
                return finalPredicate;
             }
        };
    }

}
