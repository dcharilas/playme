package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

/**
 * Predicate specifications for Eventhistory entity
 *
 * @author Dimitris Charilas
 */
public class InternalEventSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Eventhistory> createSpecFromSearchForm(final Eventhistory form) {
        return new Specification<Eventhistory>() {

            @Override
            public Predicate toPredicate(Root<Eventhistory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Eventhistory, Application> applicationJoin = root.join(Eventhistory_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getUser() != null && !StringUtils.isEmpty(form.getUser().getUsername())) {
                    Join<Eventhistory, User> userJoin = root.join(Eventhistory_.user);
                    finalPredicate.getExpressions().add(cb.like(userJoin.<String>get(User_.username), JpaUtil.likePattern(form.getUser().getUsername())));
                }
                if (form.getEventdefinition() != null && form.getEventdefinition().getId() > 0) {
                    Join<Eventhistory, Eventdefinition> definitionJoin = root.join(Eventhistory_.eventdefinition);
                    finalPredicate.getExpressions().add(cb.equal(definitionJoin.<Integer>get(Eventdefinition_.id), form.getEventdefinition().getId()));
                }
                if (form.getExternaleventhistory() != null && form.getExternaleventhistory().getId() > 0) {
                    Join<Eventhistory, Externaleventhistory> externalEventJoin = root.join(Eventhistory_.externaleventhistory);
                    finalPredicate.getExpressions().add(cb.equal(externalEventJoin.<Long>get(Externaleventhistory_.id), form.getExternaleventhistory().getId()));
                }
                if (form.getSearchDateStart() != null) {
                    finalPredicate.getExpressions().add(cb.greaterThanOrEqualTo(root.<Date>get(Eventhistory_.eventdate), form.getSearchDateStart()));
                }
                if (form.getSearchDateEnd() != null) {
                    finalPredicate.getExpressions().add(cb.lessThanOrEqualTo(root.<Date>get(Eventhistory_.eventdate), form.getSearchDateEnd()));
                }
                return finalPredicate;
             }
        };
    }

}
