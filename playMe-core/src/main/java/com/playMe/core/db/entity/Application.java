package com.playMe.core.db.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the application database table.
 * 
 */
@Entity
@Table(name="application", schema="", catalog="playme")
@NamedQuery(name="Application.findAll", query="SELECT a FROM Application a")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false, length=50)
	private String name;

	//bi-directional many-to-one association to Applicationbadge
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Applicationbadge> applicationbadges;

	//bi-directional many-to-one association to Applicationlevel
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Applicationlevel> applicationlevels;

	//bi-directional many-to-one association to Eventaction
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Eventaction> eventactions;

	//bi-directional many-to-one association to Eventhistory
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Eventhistory> eventhistories;

	//bi-directional many-to-one association to Externaleventhistory
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Externaleventhistory> externaleventhistories;

	//bi-directional many-to-one association to User
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<User> users;

	//bi-directional many-to-one association to Userbadge
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Userbadge> userbadges;

	//bi-directional many-to-one association to Userlevelprogression
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Userlevelprogression> userlevelprogressions;

	//bi-directional many-to-one association to Mission
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Mission> missions;

	//bi-directional many-to-one association to Missiontask
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Missiontask> missiontasks;

	//bi-directional many-to-one association to Socialintegration
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Socialintegration> socialintegrations;

	//bi-directional many-to-one association to Usermission
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Usermission> usermissions;

	//bi-directional many-to-one association to Userpointshistory
    @JsonIgnore
	@OneToMany(mappedBy="application")
	private List<Userpointshistory> userpointshistories;

    //bi-directional many-to-one association to Configuration
    @JsonIgnore
    @OneToMany(mappedBy="application")
    private List<Configuration> configurations;


	public Application() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Applicationbadge> getApplicationbadges() {
		return this.applicationbadges;
	}

	public void setApplicationbadges(List<Applicationbadge> applicationbadges) {
		this.applicationbadges = applicationbadges;
	}

	public Applicationbadge addApplicationbadge(Applicationbadge applicationbadge) {
		getApplicationbadges().add(applicationbadge);
		applicationbadge.setApplication(this);

		return applicationbadge;
	}

	public Applicationbadge removeApplicationbadge(Applicationbadge applicationbadge) {
		getApplicationbadges().remove(applicationbadge);
		applicationbadge.setApplication(null);

		return applicationbadge;
	}

	public List<Applicationlevel> getApplicationlevels() {
		return this.applicationlevels;
	}

	public void setApplicationlevels(List<Applicationlevel> applicationlevels) {
		this.applicationlevels = applicationlevels;
	}

	public Applicationlevel addApplicationlevel(Applicationlevel applicationlevel) {
		getApplicationlevels().add(applicationlevel);
		applicationlevel.setApplication(this);

		return applicationlevel;
	}

	public Applicationlevel removeApplicationlevel(Applicationlevel applicationlevel) {
		getApplicationlevels().remove(applicationlevel);
		applicationlevel.setApplication(null);

		return applicationlevel;
	}

	public List<Eventaction> getEventactions() {
		return this.eventactions;
	}

	public void setEventactions(List<Eventaction> eventactions) {
		this.eventactions = eventactions;
	}

	public Eventaction addEventaction(Eventaction eventaction) {
		getEventactions().add(eventaction);
		eventaction.setApplication(this);

		return eventaction;
	}

	public Eventaction removeEventaction(Eventaction eventaction) {
		getEventactions().remove(eventaction);
		eventaction.setApplication(null);

		return eventaction;
	}

	public List<Eventhistory> getEventhistories() {
		return this.eventhistories;
	}

	public void setEventhistories(List<Eventhistory> eventhistories) {
		this.eventhistories = eventhistories;
	}

	public Eventhistory addEventhistory(Eventhistory eventhistory) {
		getEventhistories().add(eventhistory);
		eventhistory.setApplication(this);

		return eventhistory;
	}

	public Eventhistory removeEventhistory(Eventhistory eventhistory) {
		getEventhistories().remove(eventhistory);
		eventhistory.setApplication(null);

		return eventhistory;
	}

	public List<Externaleventhistory> getExternaleventhistories() {
		return this.externaleventhistories;
	}

	public void setExternaleventhistories(List<Externaleventhistory> externaleventhistories) {
		this.externaleventhistories = externaleventhistories;
	}

	public Externaleventhistory addExternaleventhistory(Externaleventhistory externaleventhistory) {
		getExternaleventhistories().add(externaleventhistory);
		externaleventhistory.setApplication(this);

		return externaleventhistory;
	}

	public Externaleventhistory removeExternaleventhistory(Externaleventhistory externaleventhistory) {
		getExternaleventhistories().remove(externaleventhistory);
		externaleventhistory.setApplication(null);

		return externaleventhistory;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setApplication(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setApplication(null);

		return user;
	}

	public List<Userbadge> getUserbadges() {
		return this.userbadges;
	}

	public void setUserbadges(List<Userbadge> userbadges) {
		this.userbadges = userbadges;
	}

	public Userbadge addUserbadge(Userbadge userbadge) {
		getUserbadges().add(userbadge);
		userbadge.setApplication(this);

		return userbadge;
	}

	public Userbadge removeUserbadge(Userbadge userbadge) {
		getUserbadges().remove(userbadge);
		userbadge.setApplication(null);

		return userbadge;
	}

	public List<Userlevelprogression> getUserlevelprogressions() {
		return this.userlevelprogressions;
	}

	public void setUserlevelprogressions(List<Userlevelprogression> userlevelprogressions) {
		this.userlevelprogressions = userlevelprogressions;
	}

	public Userlevelprogression addUserlevelprogression(Userlevelprogression userlevelprogression) {
		getUserlevelprogressions().add(userlevelprogression);
		userlevelprogression.setApplication(this);

		return userlevelprogression;
	}

	public Userlevelprogression removeUserlevelprogression(Userlevelprogression userlevelprogression) {
		getUserlevelprogressions().remove(userlevelprogression);
		userlevelprogression.setApplication(null);

		return userlevelprogression;
	}

	public List<Mission> getMissions() {
		return this.missions;
	}

	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}

	public Mission addMission(Mission mission) {
		getMissions().add(mission);
		mission.setApplication(this);

		return mission;
	}

	public Mission removeMission(Mission mission) {
		getMissions().remove(mission);
		mission.setApplication(null);

		return mission;
	}

	public List<Missiontask> getMissiontasks() {
		return this.missiontasks;
	}

	public void setMissiontasks(List<Missiontask> missiontasks) {
		this.missiontasks = missiontasks;
	}

	public Missiontask addMissiontask(Missiontask missiontask) {
		getMissiontasks().add(missiontask);
		missiontask.setApplication(this);

		return missiontask;
	}

	public Missiontask removeMissiontask(Missiontask missiontask) {
		getMissiontasks().remove(missiontask);
		missiontask.setApplication(null);

		return missiontask;
	}

	public List<Socialintegration> getSocialintegrations() {
		return this.socialintegrations;
	}

	public void setSocialintegrations(List<Socialintegration> socialintegrations) {
		this.socialintegrations = socialintegrations;
	}

	public Socialintegration addSocialintegration(Socialintegration socialintegration) {
		getSocialintegrations().add(socialintegration);
		socialintegration.setApplication(this);

		return socialintegration;
	}

	public Socialintegration removeSocialintegration(Socialintegration socialintegration) {
		getSocialintegrations().remove(socialintegration);
		socialintegration.setApplication(null);

		return socialintegration;
	}

	public List<Usermission> getUsermissions() {
		return this.usermissions;
	}

	public void setUsermissions(List<Usermission> usermissions) {
		this.usermissions = usermissions;
	}

	public Usermission addUsermission(Usermission usermission) {
		getUsermissions().add(usermission);
		usermission.setApplication(this);

		return usermission;
	}

	public Usermission removeUsermission(Usermission usermission) {
		getUsermissions().remove(usermission);
		usermission.setApplication(null);

		return usermission;
	}

	public List<Userpointshistory> getUserpointshistories() {
		return this.userpointshistories;
	}

	public void setUserpointshistories(List<Userpointshistory> userpointshistories) {
		this.userpointshistories = userpointshistories;
	}

	public Userpointshistory addUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().add(userpointshistory);
		userpointshistory.setApplication(this);

		return userpointshistory;
	}

	public Userpointshistory removeUserpointshistory(Userpointshistory userpointshistory) {
		getUserpointshistories().remove(userpointshistory);
		userpointshistory.setApplication(null);

		return userpointshistory;
	}


    public List<Configuration> getConfigurations() {
        return this.configurations;
    }

    public void setConfigurations(List<Configuration> configurations) {
        this.configurations = configurations;
    }

    public Configuration addConfiguration(Configuration configuration) {
        getConfigurations().add(configuration);
        configuration.setApplication(this);

        return configuration;
    }

    public Configuration removeConfiguration(Configuration configuration) {
        getConfigurations().remove(configuration);
        configuration.setApplication(null);

        return configuration;
    }

}