package com.playMe.core.db.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dimitris Charilas
 */
public class JpaUtil {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * likePattern
     *
     * @param searchTerm
     * @return returns %searchTerm% string for like expression
     */
    public static String likePattern(final String searchTerm) {
        StringBuilder pattern = new StringBuilder();
        pattern.append("%"+ searchTerm);
        pattern.append("%");
        return pattern.toString();
    }
}
