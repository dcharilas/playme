package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for ApplicationLevel entity
 *
 * @author Dimitris Charilas
 */
public class ApplicationLevelSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Applicationlevel> createSpecFromSearchForm(final Applicationlevel form) {
        return new Specification<Applicationlevel>() {

            @Override
            public Predicate toPredicate(Root<Applicationlevel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getLevel() != null && !StringUtils.isEmpty(form.getLevel().getName())) {
                    Join<Applicationlevel, Level> levelJoin = root.join(Applicationlevel_.level);
                    finalPredicate.getExpressions().add(cb.like(levelJoin.<String>get(Level_.name), JpaUtil.likePattern(form.getLevel().getName())));
                }
                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Applicationlevel, Application> applicationJoin = root.join(Applicationlevel_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getActive() != null) {
                    finalPredicate.getExpressions().add(cb.equal(root.<Boolean>get(Applicationlevel_.active), form.getActive()));
                }

                return finalPredicate;
             }
        };
    }

}
