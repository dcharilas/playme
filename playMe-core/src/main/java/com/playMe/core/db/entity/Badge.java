package com.playMe.core.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the badge database table.
 * 
 */
@Entity
@Table(name="badge", schema="", catalog="playme")
@NamedQuery(name="Badge.findAll", query="SELECT b FROM Badge b")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Badge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Lob
	@Column(columnDefinition="mediumblob")
	private byte[] activeimage;

	private int descriptionid;

	@Lob
	@Column(columnDefinition="mediumblob")
	private byte[] inactiveimage;

	@Column(length=100)
	private String name;

	//bi-directional many-to-one association to Applicationbadge
    @JsonIgnore
	@OneToMany(mappedBy="badge")
	private List<Applicationbadge> applicationbadges;

	//bi-directional many-to-one association to Userbadge
    @JsonIgnore
	@OneToMany(mappedBy="badge")
	private List<Userbadge> userbadges;

    @Transient
    private List<Displaytext> descriptions;

	public Badge() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getActiveimage() {
		return this.activeimage;
	}

	public void setActiveimage(byte[] activeimage) {
		this.activeimage = activeimage;
	}

	public int getDescriptionid() {
		return this.descriptionid;
	}

	public void setDescriptionid(int descriptionid) {
		this.descriptionid = descriptionid;
	}

	public byte[] getInactiveimage() {
		return this.inactiveimage;
	}

	public void setInactiveimage(byte[] inactiveimage) {
		this.inactiveimage = inactiveimage;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Applicationbadge> getApplicationbadges() {
		return this.applicationbadges;
	}

	public void setApplicationbadges(List<Applicationbadge> applicationbadges) {
		this.applicationbadges = applicationbadges;
	}

	public Applicationbadge addApplicationbadge(Applicationbadge applicationbadge) {
		getApplicationbadges().add(applicationbadge);
		applicationbadge.setBadge(this);

		return applicationbadge;
	}

	public Applicationbadge removeApplicationbadge(Applicationbadge applicationbadge) {
		getApplicationbadges().remove(applicationbadge);
		applicationbadge.setBadge(null);

		return applicationbadge;
	}

	public List<Userbadge> getUserbadges() {
		return this.userbadges;
	}

	public void setUserbadges(List<Userbadge> userbadges) {
		this.userbadges = userbadges;
	}

    public List<Displaytext> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Displaytext> descriptions) {
        this.descriptions = descriptions;
    }

    public Userbadge addUserbadge(Userbadge userbadge) {
		getUserbadges().add(userbadge);
		userbadge.setBadge(this);

		return userbadge;
	}

	public Userbadge removeUserbadge(Userbadge userbadge) {
		getUserbadges().remove(userbadge);
		userbadge.setBadge(null);

		return userbadge;
	}

}