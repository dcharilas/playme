package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.playMe.core.db.keys.UsermissionPK;

import java.util.Date;


/**
 * The persistent class for the usermissions database table.
 * 
 */
@Entity
@Table(name="usermissions", schema="", catalog="playme")
@NamedQuery(name="Usermission.findAll", query="SELECT u FROM Usermission u")
public class Usermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UsermissionPK id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date assigndate;

	@Column(columnDefinition="bit")
	private String assigned;

	@Column(columnDefinition="bit")
	private String completed;

	@Temporal(TemporalType.TIMESTAMP)
	private Date completiondate;

	private int currenttaskid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date deadline;

	private int nexttaskid;

	//bi-directional many-to-one association to Mission
	@ManyToOne
	@JoinColumn(name="missionid", nullable=false, insertable=false, updatable=false)
	private Mission mission;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid", nullable=false, insertable=false, updatable=false)
	private User user;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
	private Application application;

	public Usermission() {
	}

	public UsermissionPK getId() {
		return this.id;
	}

	public void setId(UsermissionPK id) {
		this.id = id;
	}

	public Date getAssigndate() {
		return this.assigndate;
	}

	public void setAssigndate(Date assigndate) {
		this.assigndate = assigndate;
	}

	public String getAssigned() {
		return this.assigned;
	}

	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	public String getCompleted() {
		return this.completed;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}

	public Date getCompletiondate() {
		return this.completiondate;
	}

	public void setCompletiondate(Date completiondate) {
		this.completiondate = completiondate;
	}

	public int getCurrenttaskid() {
		return this.currenttaskid;
	}

	public void setCurrenttaskid(int currenttaskid) {
		this.currenttaskid = currenttaskid;
	}

	public Date getDeadline() {
		return this.deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public int getNexttaskid() {
		return this.nexttaskid;
	}

	public void setNexttaskid(int nexttaskid) {
		this.nexttaskid = nexttaskid;
	}

	public Mission getMission() {
		return this.mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

}