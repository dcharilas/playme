package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Userprofileweights;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserprofileweightsRepository extends JpaRepository<Userprofileweights,String> {


}
