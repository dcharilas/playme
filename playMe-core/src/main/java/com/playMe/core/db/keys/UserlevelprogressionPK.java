package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the userlevelprogression database table.
 * 
 */
@Embeddable
public class UserlevelprogressionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false)
	private long userid;

	@Column(unique=true, nullable=false)
	private int levelid;

	@Column(unique=true, nullable=false)
	private int applicationid;

	public UserlevelprogressionPK() {
	}
	public long getUserid() {
		return this.userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public int getLevelid() {
		return this.levelid;
	}
	public void setLevelid(int levelid) {
		this.levelid = levelid;
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserlevelprogressionPK)) {
			return false;
		}
		UserlevelprogressionPK castOther = (UserlevelprogressionPK)other;
		return 
			(this.userid == castOther.userid)
			&& (this.levelid == castOther.levelid)
			&& (this.applicationid == castOther.applicationid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + (int) this.userid;
		hash = hash * prime + this.levelid;
		hash = hash * prime + this.applicationid;
		
		return hash;
	}
}