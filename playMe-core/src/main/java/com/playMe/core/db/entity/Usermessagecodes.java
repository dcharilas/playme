package com.playMe.core.db.entity;

import com.playMe.core.db.keys.UsermessagecodesPK;

import javax.persistence.*;

/**
 * @author Dimitris Charilas
 */
@Entity
@Table(name="usermessagecodes", schema="", catalog="playme")
@NamedQuery(name="Usermessagecodes.findAll", query="SELECT d FROM Usermessagecodes d")
public class Usermessagecodes {

    @EmbeddedId
    private UsermessagecodesPK id;

    //bi-directional many-to-one association to Application
    @ManyToOne
    @JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
    private Application application;

    @Column(length=300)
    private String message;


    public UsermessagecodesPK getId() {
        return id;
    }

    public void setId(UsermessagecodesPK id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
