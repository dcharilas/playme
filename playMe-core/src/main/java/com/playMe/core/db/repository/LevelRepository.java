package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface LevelRepository extends JpaRepository<Level,Integer> {

    @Query("SELECT al FROM Level l, Applicationlevel al WHERE l.id = al.level.id AND al.application.id = :appId order by sequence asc")
    List<Level> findApplicationLevels(@Param("appId") int appId);

    @Query("SELECT al FROM Level l, Applicationlevel al WHERE l.id = al.level.id AND al.application.id = :appId AND l.name = :name order by sequence asc")
    List<Level> findApplicationLevel(@Param("appId") int appId, @Param("name") String name);

}
