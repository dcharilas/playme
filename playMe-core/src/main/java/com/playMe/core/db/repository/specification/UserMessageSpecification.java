package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Usermessages entity
 *
 * @author Dimitris Charilas
 */
public class UserMessageSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Usermessages> createSpecFromSearchForm(final Usermessages form) {
        return new Specification<Usermessages>() {

            @Override
            public Predicate toPredicate(Root<Usermessages> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Usermessages, Application> applicationJoin = root.join(Usermessages_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getUser() != null && form.getUser().getId() > 0) {
                    Join<Usermessages, User> userJoin = root.join(Usermessages_.user);
                    finalPredicate.getExpressions().add(cb.equal(userJoin.<Long>get(User_.id), form.getUser().getId()));
                }

                return finalPredicate;
             }
        };
    }

}
