package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the applicationlevels database table.
 * 
 */
@Embeddable
public class ApplicationlevelPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int applicationid;

	@Column(unique=true, nullable=false)
	private int levelid;

	public ApplicationlevelPK() {
	}
	public int getApplicationid() {
		return this.applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}
	public int getLevelid() {
		return this.levelid;
	}
	public void setLevelid(int levelid) {
		this.levelid = levelid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ApplicationlevelPK)) {
			return false;
		}
		ApplicationlevelPK castOther = (ApplicationlevelPK)other;
		return 
			(this.applicationid == castOther.applicationid)
			&& (this.levelid == castOther.levelid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.applicationid;
		hash = hash * prime + this.levelid;
		
		return hash;
	}
}