package com.playMe.core.db.repository;

import com.playMe.core.db.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long>,JpaSpecificationExecutor<User> {

    User findByIdAndApplication_Id(long id, int appId);

    User findByUsernameAndApplication_Id(String username, int appId);

    List<User> findByApplication_Id(int appId, Pageable pageable);

    /* Statistics */

    @Query("SELECT u.gender, count(*) \n" +
            "FROM User u \n" +
            "group by u.gender")
    List<Object[]> countUsersPerGender();

    @Query("SELECT u.gender, count(*) \n" +
            "FROM User u \n" +
            "WHERE u.application.id = :applId\n" +
            "group by u.gender")
    List<Object[]> countUsersPerGender(@Param("applId") int applId);

    @Query("SELECT a.name, count(*)\n" +
            "FROM User u, Application a\n" +
            "WHERE u.application.id = a.id\n" +
            "group by u.application.id")
    List<Object[]> countUsersPerApplication();

    @Query("SELECT al.sequence, count(*)\n" +
            "FROM User u, Applicationlevel al\n" +
            "WHERE u.application.id = al.application.id\n" +
            "GROUP BY al.sequence")
    List<Object[]> countUsersPerLevelSequence();


    @Query(value = "SELECT gu.date, 'USERS' as code, (@runtot \\:= gu.users + @runtot) AS rt\n" +
            "FROM (SELECT @runtot\\:=0) c,\n" +
            "(SELECT distinct date(u.regDate) AS date, COUNT(*) AS users\n" +
            "FROM user u\n" +
            "WHERE u.applicationid = :applId\n" +
            "AND DATE(u.regDate) >= DATE_SUB(CURDATE(), INTERVAL :days DAY)\n" +
            "AND u.regDate IS NOT NULL\n" +
            "GROUP BY u.regDate\n" +
            "ORDER BY u.regDate ASC) gu", nativeQuery = true)
    List<Object[]> countUsersPerDayForAplication(@Param("days") int days, @Param("applId") int applId);


    @Query(value = "SELECT\n" +
            "  CASE\n" +
            "\t  WHEN age = 0 THEN 'N/A'\n" +
            "    WHEN age >= 10 AND age <= 20 THEN '10-20'\n" +
            "    WHEN age >=21 AND age <=30 THEN '21-30'\n" +
            "    WHEN age >=31 AND age <=40 THEN '31-40'\n" +
            "    WHEN age >=41 AND age <= 50 THEN '41-50'\n" +
            "    WHEN age >=51 AND age <=60 THEN '51-60'\n" +
            "    WHEN age >=61 THEN '61+'\n" +
            "  END AS ageband, COUNT(*)\n" +
            "FROM user\n" +
            "GROUP BY ageband", nativeQuery = true)
    List<Object[]> countUsersPerAgeGroup();

    @Query(value = "SELECT\n" +
            "  CASE\n" +
            "\t  WHEN age = 0 THEN 'N/A'\n" +
            "    WHEN age >= 10 AND age <= 20 THEN '10-20'\n" +
            "    WHEN age >=21 AND age <=30 THEN '21-30'\n" +
            "    WHEN age >=31 AND age <=40 THEN '31-40'\n" +
            "    WHEN age >=41 AND age <= 50 THEN '41-50'\n" +
            "    WHEN age >=51 AND age <=60 THEN '51-60'\n" +
            "    WHEN age >=61 THEN '61+'\n" +
            "  END AS ageband, COUNT(*)\n" +
            "FROM user\n" +
            "WHERE applicationid  = :applId\n" +
            "GROUP BY ageband", nativeQuery = true)
    List<Object[]> countUsersPerAgeGroup(@Param("applId") int applId);


}
