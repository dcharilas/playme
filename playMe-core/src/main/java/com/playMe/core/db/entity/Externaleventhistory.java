package com.playMe.core.db.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the externaleventhistory database table.
 * 
 */
@Entity
@Table(name="externaleventhistory", schema="", catalog="playme")
@NamedQuery(name="Externaleventhistory.findAll", query="SELECT e FROM Externaleventhistory e")
@XmlRootElement
public class Externaleventhistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private long id;

	@Column(length=200)
	private String comments;

    @Temporal(TemporalType.TIMESTAMP)
    private Date eventdate;

	@Column(name="numericvalue_1")
	private int numericvalue1;

	@Column(name="numericvalue_2")
	private int numericvalue2;

    @Column(name="numericvalue_3")
    private int numericvalue3;

    @Column(name="numericvalue_4")
    private int numericvalue4;

	@Column(name="textvalue_1", length=100)
	private String textvalue1;

	@Column(name="textvalue_2", length=100)
	private String textvalue2;

    @Column(name="textvalue_3", length=100)
    private String textvalue3;

    @Column(name="textvalue_4", length=100)
    private String textvalue4;

	//bi-directional many-to-one association to Eventhistory
    @JsonIgnore
	@OneToMany(mappedBy="externaleventhistory")
	private List<Eventhistory> eventhistories;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Eventdefinition
	@ManyToOne
	@JoinColumn(name="eventid")
	private Eventdefinition eventdefinition;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid")
	private User user;

    /*
     * Custom fields to allow date searching
     */
    @Transient
    private Date searchDateStart;

    @Transient
    private Date searchDateEnd;


	public Externaleventhistory() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getEventdate() {
		return this.eventdate;
	}

	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}

	public int getNumericvalue1() {
		return this.numericvalue1;
	}

	public void setNumericvalue1(int numericvalue1) {
		this.numericvalue1 = numericvalue1;
	}

	public int getNumericvalue2() {
		return this.numericvalue2;
	}

	public void setNumericvalue2(int numericvalue2) {
		this.numericvalue2 = numericvalue2;
	}

	public String getTextvalue1() {
		return this.textvalue1;
	}

	public void setTextvalue1(String textvalue1) {
		this.textvalue1 = textvalue1;
	}

	public String getTextvalue2() {
		return this.textvalue2;
	}

	public void setTextvalue2(String textvalue2) {
		this.textvalue2 = textvalue2;
	}

    public int getNumericvalue3() {
        return numericvalue3;
    }

    public void setNumericvalue3(int numericvalue3) {
        this.numericvalue3 = numericvalue3;
    }

    public int getNumericvalue4() {
        return numericvalue4;
    }

    public void setNumericvalue4(int numericvalue4) {
        this.numericvalue4 = numericvalue4;
    }

    public String getTextvalue3() {
        return textvalue3;
    }

    public void setTextvalue3(String textvalue3) {
        this.textvalue3 = textvalue3;
    }

    public String getTextvalue4() {
        return textvalue4;
    }

    public void setTextvalue4(String textvalue4) {
        this.textvalue4 = textvalue4;
    }

    public List<Eventhistory> getEventhistories() {
		return this.eventhistories;
	}

	public void setEventhistories(List<Eventhistory> eventhistories) {
		this.eventhistories = eventhistories;
	}

	public Eventhistory addEventhistory(Eventhistory eventhistory) {
		getEventhistories().add(eventhistory);
		eventhistory.setExternaleventhistory(this);

		return eventhistory;
	}

	public Eventhistory removeEventhistory(Eventhistory eventhistory) {
		getEventhistories().remove(eventhistory);
		eventhistory.setExternaleventhistory(null);

		return eventhistory;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Eventdefinition getEventdefinition() {
		return this.eventdefinition;
	}

	public void setEventdefinition(Eventdefinition eventdefinition) {
		this.eventdefinition = eventdefinition;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public Date getSearchDateStart() {
        return searchDateStart;
    }

    public void setSearchDateStart(Date searchDateStart) {
        this.searchDateStart = searchDateStart;
    }

    public Date getSearchDateEnd() {
        return searchDateEnd;
    }

    public void setSearchDateEnd(Date searchDateEnd) {
        this.searchDateEnd = searchDateEnd;
    }
}