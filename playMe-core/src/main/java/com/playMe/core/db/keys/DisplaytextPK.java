package com.playMe.core.db.keys;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the displaytext database table.
 * 
 */
@Embeddable
public class DisplaytextPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

    @Column(insertable=false, updatable=false, unique=true, nullable=false)
    private int messageid;

    @Column(length=2, insertable=false, updatable=false, unique=true, nullable=false)
    private String locale;

    public DisplaytextPK() {
    }

    public DisplaytextPK(int messageid, String locale) {
        this.messageid = messageid;
        this.locale = locale;
    }

    public int getMessageid() {
        return this.messageid;
    }

    public void setMessageid(int messageid) {
        this.messageid = messageid;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof DisplaytextPK))
            return false;
        DisplaytextPK castOther = (DisplaytextPK) other;

        return (this.getMessageid() == castOther.getMessageid())
                && ((this.getLocale() == castOther.getLocale()) || (this
                .getLocale() != null && castOther.getLocale() != null && this
                .getLocale().equals(castOther.getLocale())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getMessageid();
        result = 37 * result
                + (getLocale() == null ? 0 : this.getLocale().hashCode());
        return result;
    }
}