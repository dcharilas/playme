package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for ApplicationBadge entity
 *
 * @author Dimitris Charilas
 */
public class ApplicationBadgeSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Applicationbadge> createSpecFromSearchForm(final Applicationbadge form) {
        return new Specification<Applicationbadge>() {

            @Override
            public Predicate toPredicate(Root<Applicationbadge> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getBadge() != null && !StringUtils.isEmpty(form.getBadge().getName())) {
                    Join<Applicationbadge, Badge> badgeJoin = root.join(Applicationbadge_.badge);
                    finalPredicate.getExpressions().add(cb.like(badgeJoin.<String>get(Badge_.name), JpaUtil.likePattern(form.getBadge().getName())));
                }
                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Applicationbadge, Application> applicationJoin = root.join(Applicationbadge_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getActive() != null) {
                    finalPredicate.getExpressions().add(cb.equal(root.<Boolean>get(Applicationbadge_.active), form.getActive()));
                }

                return finalPredicate;
             }
        };
    }

}
