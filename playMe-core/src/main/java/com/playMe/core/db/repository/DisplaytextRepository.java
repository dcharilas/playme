package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Displaytext;
import com.playMe.core.db.keys.DisplaytextPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface DisplaytextRepository extends JpaRepository<Displaytext, DisplaytextPK>, JpaSpecificationExecutor<Displaytext> {

    List<Displaytext> findById_Messageid(@Param("messageId") int messageId);

    Displaytext findById_MessageidAndId_Locale(@Param("messageId") int messageId, @Param("locale") String locale);

    @Query("SELECT MAX(t.id.messageid)+1 from Displaytext t")
    int getNextMessageId();

}
