package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Applicationlevel;
import com.playMe.core.db.keys.ApplicationlevelPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface ApplicationlevelsRepository extends JpaRepository<Applicationlevel,ApplicationlevelPK>, JpaSpecificationExecutor<Applicationlevel> {

    List<Applicationlevel> findByApplication_Id(int appId);

    List<Applicationlevel> findByLevel_Id(int levelId);

    @Query("SELECT b FROM Applicationlevel b WHERE b.application.id = :appId order by sequence asc")
    List<Applicationlevel> findApplicationLevelsSequence(@Param("appId") int appId);

    @Query("SELECT sequence FROM Applicationlevel b WHERE b.application.id = :appId AND b.level.id = :levelId")
    int findApplicationLevelSequence(@Param("appId") int appId, @Param("levelId") int levelId);

    Applicationlevel findByApplication_IdAndLevel_Id(int appId, int levelId);

    @Modifying
    @Query("UPDATE Applicationlevel al SET al.active = :active WHERE al.level.id = :levelId AND al.application.id = :appId")
    void toggleApplicationLevel(@Param("appId") int appId, @Param("levelId") int levelId, @Param("active") boolean active);

    Applicationlevel findByApplication_IdAndSequence(int appId, int sequence);
}
