package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Externaleventhistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface ExternaleventhistoryRepository extends JpaRepository<Externaleventhistory,Long>, JpaSpecificationExecutor<Externaleventhistory> {

    @Query("SELECT DISTINCT(DATE(e.eventdate)), d.name, COUNT(*)\n" +
            "FROM Externaleventhistory e, Eventdefinition d\n" +
            "WHERE e.eventdefinition.id = d.id\n" +
            "AND DATE(e.eventdate) BETWEEN :startDate AND :endDate\n" +
            "GROUP BY e.eventdefinition.id, DATE(e.eventdate)\n" +
            "ORDER BY DATE(e.eventdate) ASC")
    List<Object[]> getExternalEventsPerDay(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("SELECT DISTINCT(DATE(e.eventdate)), d.name, COUNT(*)\n" +
            "FROM Externaleventhistory e, Eventdefinition d\n" +
            "WHERE e.eventdefinition.id = d.id\n" +
            "AND DATE(e.eventdate) BETWEEN :startDate AND :endDate\n" +
            "AND applicationid = :applId\n" +
            "GROUP BY e.eventdefinition.id, DATE(e.eventdate)\n" +
            "ORDER BY DATE(e.eventdate) ASC")
    List<Object[]> getApplicationEventsPerDay(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("applId") int applId);
}
