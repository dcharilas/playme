package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

/**
 * Predicate specifications for Externaleventhistory entity
 *
 * @author Dimitris Charilas
 */
public class ExternalEventSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Externaleventhistory> createSpecFromSearchForm(final Externaleventhistory form) {
        return new Specification<Externaleventhistory>() {

            @Override
            public Predicate toPredicate(Root<Externaleventhistory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (form.getApplication() != null && form.getApplication().getId() > 0) {
                    Join<Externaleventhistory, Application> applicationJoin = root.join(Externaleventhistory_.application);
                    finalPredicate.getExpressions().add(cb.equal(applicationJoin.<Integer>get(Application_.id), form.getApplication().getId()));
                }
                if (form.getUser() != null && !StringUtils.isEmpty(form.getUser().getUsername())) {
                    Join<Externaleventhistory, User> userJoin = root.join(Externaleventhistory_.user);
                    finalPredicate.getExpressions().add(cb.like(userJoin.<String>get(User_.username), JpaUtil.likePattern(form.getUser().getUsername())));
                }
                if (form.getEventdefinition() != null && form.getEventdefinition().getId() > 0) {
                    Join<Externaleventhistory, Eventdefinition> definitionJoin = root.join(Externaleventhistory_.eventdefinition);
                    finalPredicate.getExpressions().add(cb.equal(definitionJoin.<Integer>get(Eventdefinition_.id), form.getEventdefinition().getId()));
                }
                if (form.getSearchDateStart() != null) {
                    finalPredicate.getExpressions().add(cb.greaterThanOrEqualTo(root.<Date>get(Externaleventhistory_.eventdate), form.getSearchDateStart()));
                }
                if (form.getSearchDateEnd() != null) {
                    finalPredicate.getExpressions().add(cb.lessThanOrEqualTo(root.<Date>get(Externaleventhistory_.eventdate), form.getSearchDateEnd()));
                }
                return finalPredicate;
             }
        };
    }

}
