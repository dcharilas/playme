package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Userlevelprogression;
import com.playMe.core.db.keys.UserlevelprogressionPK;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserlevelprogressionRepository extends JpaRepository<Userlevelprogression,UserlevelprogressionPK> {

    List<Userlevelprogression> findByUser_IdAndApplication_Id(long userId, int applId, Pageable paging);
}
