package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Pastusermessages;
import com.playMe.core.db.entity.Usermessages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface PastusermessagesRepository extends JpaRepository<Pastusermessages,Long> {


}
