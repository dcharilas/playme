package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.Application_;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Predicate specifications for Application entity
 *
 * @author Dimitris Charilas
 */
public class ApplicationSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Application> createSpecFromSearchForm(final Application form) {
        return new Specification<Application>() {

            @Override
            public Predicate toPredicate(Root<Application> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (!StringUtils.isEmpty(form.getName())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(Application_.name), JpaUtil.likePattern(form.getName())));
                }
                return finalPredicate;
             }
        };
    }

}
