package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Usermessagecodes;
import com.playMe.core.db.keys.UsermessagecodesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface UsermessagecodesRepository extends JpaRepository<Usermessagecodes,UsermessagecodesPK>, JpaSpecificationExecutor<Usermessagecodes> {

    List<Usermessagecodes> findById_ApplicationidAndId_Msgcode(@Param("applId") int applId, @Param("msgcode") String msgcode);

    Usermessagecodes findById_ApplicationidAndId_MsgcodeAndId_Locale(@Param("applId") int applId, @Param("msgcode") String msgcode, @Param("locale") String locale);

}
