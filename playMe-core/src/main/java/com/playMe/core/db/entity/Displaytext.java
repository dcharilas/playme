package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.playMe.core.db.keys.DisplaytextPK;


/**
 * The persistent class for the displaytext database table.
 * 
 */
@Entity
@Table(name="displaytext", schema="", catalog="playme")
@NamedQuery(name="Displaytext.findAll", query="SELECT d FROM Displaytext d")
public class Displaytext implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DisplaytextPK id;

	@Column(length=1000)
	private String value;

	public Displaytext() {
	}

	public DisplaytextPK getId() {
		return this.id;
	}

	public void setId(DisplaytextPK id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}