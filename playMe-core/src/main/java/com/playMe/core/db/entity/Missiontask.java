package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the missiontask database table.
 * 
 */
@Entity
@Table(name="missiontask", schema="", catalog="playme")
@NamedQuery(name="Missiontask.findAll", query="SELECT m FROM Missiontask m")
public class Missiontask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(columnDefinition="bit")
	private String active;

	private int descriptionid;

	@Column(length=100)
	private String name;

	private int sequence;

	@Lob
	@Column(columnDefinition="blob")
	private Object taskimage;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Mission
	@ManyToOne
	@JoinColumn(name="missionid")
	private Mission mission;

	//bi-directional many-to-one association to Taskrequirementdefinition
	@OneToMany(mappedBy="missiontask")
	private List<Taskrequirementdefinition> taskrequirementdefinitions;

	public Missiontask() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getDescriptionid() {
		return this.descriptionid;
	}

	public void setDescriptionid(int descriptionid) {
		this.descriptionid = descriptionid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Object getTaskimage() {
		return this.taskimage;
	}

	public void setTaskimage(Object taskimage) {
		this.taskimage = taskimage;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Mission getMission() {
		return this.mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	public List<Taskrequirementdefinition> getTaskrequirementdefinitions() {
		return this.taskrequirementdefinitions;
	}

	public void setTaskrequirementdefinitions(List<Taskrequirementdefinition> taskrequirementdefinitions) {
		this.taskrequirementdefinitions = taskrequirementdefinitions;
	}

	public Taskrequirementdefinition addTaskrequirementdefinition(Taskrequirementdefinition taskrequirementdefinition) {
		getTaskrequirementdefinitions().add(taskrequirementdefinition);
		taskrequirementdefinition.setMissiontask(this);

		return taskrequirementdefinition;
	}

	public Taskrequirementdefinition removeTaskrequirementdefinition(Taskrequirementdefinition taskrequirementdefinition) {
		getTaskrequirementdefinitions().remove(taskrequirementdefinition);
		taskrequirementdefinition.setMissiontask(null);

		return taskrequirementdefinition;
	}

}