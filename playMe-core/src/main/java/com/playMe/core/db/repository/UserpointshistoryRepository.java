package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Userpointshistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserpointshistoryRepository extends JpaRepository<Userpointshistory,Long> {

    List<Userpointshistory> findByUser_IdAndApplication_Id(long userId, int appId, Pageable paging );

    @Query(value = "SELECT adjstat.date, 'USER_POINTS', MAX(adjstat.rt) as value FROM\n" +
            "(SELECT DATE(stat.date) as date, (@runtot \\:= stat.points + @runtot) AS rt\n" +
            "FROM (SELECT @runtot\\:=0) c,\n" +
            "(SELECT DATE(u.updatedate) as date,\n" +
            "  CASE\n" +
            "     WHEN u.points > 0 THEN u.points\n" +
            "     ELSE 0\n" +
            "  END AS points\n" +
            "FROM  userpointshistory u\n" +
            "WHERE u.applicationid = :applId\n" +
            "AND u.userid = :userId\n" +
            "ORDER BY u.updatedate ASC) as stat) as adjstat\n" +
            "GROUP BY DATE(adjstat.date)", nativeQuery = true)
    List<Object[]> getPointsHistory(@Param("applId") int applId, @Param("userId") long userId);


    @Query(value = "SELECT DATE(stat.date) as date, 'REDEEM_POINTS', (@runtot \\:= stat.points + @runtot) AS rt\n" +
            "FROM (SELECT @runtot\\:=0) c,\n" +
            "(SELECT DATE(u.updatedate) as date, SUM(u.points) as points\n" +
            "FROM  userpointshistory u\n" +
            "WHERE u.applicationid = :applId\n" +
            "AND u.userid = :userId\n" +
            "GROUP BY DATE(u.updatedate)\n" +
            "ORDER BY u.updatedate ASC) AS stat\n" +
            "GROUP BY DATE(stat.date)", nativeQuery = true)
    List<Object[]> getRedeemablePointsHistory(@Param("applId") int applId, @Param("userId") long userId);

}
