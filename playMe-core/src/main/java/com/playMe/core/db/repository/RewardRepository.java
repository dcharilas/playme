package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Reward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface RewardRepository extends JpaRepository<Reward,Integer>, JpaSpecificationExecutor<Reward> {

    List<Reward> findByApplicationId(int applId);

    @Query("SELECT r FROM Reward r WHERE r.application.id = :appId AND r.active = true ORDER BY r.pointsreq ASC")
    List<Reward> findApplicationActiveRewards(@Param("appId") int appId);
}
