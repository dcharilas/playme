package com.playMe.core.db.repository.specification;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.keys.DisplaytextPK;
import com.playMe.core.db.keys.DisplaytextPK_;
import com.playMe.core.db.util.JpaUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Predicate specifications for Displaytext entity
 *
 * @author Dimitris Charilas
 */
public class DisplayTextSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Specification<Displaytext> createSpecFromSearchForm(final Displaytext form) {
        return new Specification<Displaytext>() {

            @Override
            public Predicate toPredicate(Root<Displaytext> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate finalPredicate = cb.conjunction();
                if (form == null) {
                    return finalPredicate;
                }

                if (!StringUtils.isEmpty(form.getValue())) {
                    finalPredicate.getExpressions().add(cb.like(root.<String>get(Displaytext_.value), JpaUtil.likePattern(form.getValue())));
                }
                if (form.getId() != null && !StringUtils.isEmpty(form.getId().getLocale())) {
                    Join<Displaytext, DisplaytextPK> keyJoin = root.join(Displaytext_.id);
                    finalPredicate.getExpressions().add(cb.like(keyJoin.<String>get(DisplaytextPK_.locale), JpaUtil.likePattern(form.getId().getLocale())));
                }

                return finalPredicate;
             }
        };
    }

}
