package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Usermessages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
@Repository
public interface UsermessagesRepository extends JpaRepository<Usermessages,Long>, JpaSpecificationExecutor<Usermessages> {

    @Query(value = "SELECT * FROM usermessages\n" +
            "WHERE viewedon IS NOT NULL\n" +
            "AND viewedon < DATE_SUB(NOW(), INTERVAL 24 HOUR)" +
            "LIMIT :batchsize", nativeQuery = true)
    List<Usermessages> getReadMessages(@Param("batchsize") int batchsize);
}
