package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the mission database table.
 * 
 */
@Entity
@Table(name="mission", schema="", catalog="playme")
@NamedQuery(name="Mission.findAll", query="SELECT m FROM Mission m")
public class Mission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(columnDefinition="bit")
	private String active;

	private int descriptionid;

	@Lob
	@Column(columnDefinition="blob")
	private Object missionimage;

	@Column(length=100)
	private String name;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid")
	private Application application;

	//bi-directional many-to-one association to Missiontask
	@OneToMany(mappedBy="mission")
	private List<Missiontask> missiontasks;

	//bi-directional many-to-one association to Usermission
	@OneToMany(mappedBy="mission")
	private List<Usermission> usermissions;

	public Mission() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getDescriptionid() {
		return this.descriptionid;
	}

	public void setDescriptionid(int descriptionid) {
		this.descriptionid = descriptionid;
	}

	public Object getMissionimage() {
		return this.missionimage;
	}

	public void setMissionimage(Object missionimage) {
		this.missionimage = missionimage;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<Missiontask> getMissiontasks() {
		return this.missiontasks;
	}

	public void setMissiontasks(List<Missiontask> missiontasks) {
		this.missiontasks = missiontasks;
	}

	public Missiontask addMissiontask(Missiontask missiontask) {
		getMissiontasks().add(missiontask);
		missiontask.setMission(this);

		return missiontask;
	}

	public Missiontask removeMissiontask(Missiontask missiontask) {
		getMissiontasks().remove(missiontask);
		missiontask.setMission(null);

		return missiontask;
	}

	public List<Usermission> getUsermissions() {
		return this.usermissions;
	}

	public void setUsermissions(List<Usermission> usermissions) {
		this.usermissions = usermissions;
	}

	public Usermission addUsermission(Usermission usermission) {
		getUsermissions().add(usermission);
		usermission.setMission(this);

		return usermission;
	}

	public Usermission removeUsermission(Usermission usermission) {
		getUsermissions().remove(usermission);
		usermission.setMission(null);

		return usermission;
	}

}