package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.playMe.core.db.keys.ApplicationbadgePK;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the applicationbadge database table.
 * 
 */
@Entity
@Table(name="applicationbadges", schema="", catalog="playme")
@NamedQuery(name="Applicationbadge.findAll", query="SELECT a FROM Applicationbadge a")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Applicationbadge implements Serializable {
	private static final long serialVersionUID = 1L;

    @JsonIgnore
	@EmbeddedId
	private ApplicationbadgePK id;

	@Column(columnDefinition="bit")
	private Boolean active;

	//bi-directional many-to-one association to Application
	@ManyToOne
	@JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
	private Application application;

	//bi-directional many-to-one association to Badge
	@ManyToOne
	@JoinColumn(name="badgeid", nullable=false, insertable=false, updatable=false)
	private Badge badge;

    private Integer awardafterpoints;

    private Integer awardafterlevelseq;

    private Integer awardafterlogins;

    private Integer awardaftercompleteness;


    public Applicationbadge() {
	}

	public ApplicationbadgePK getId() {
		return this.id;
	}

	public void setId(ApplicationbadgePK id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Badge getBadge() {
		return this.badge;
	}

	public void setBadge(Badge badge) {
		this.badge = badge;
	}

    public Integer getAwardafterpoints() {
        return awardafterpoints;
    }

    public void setAwardafterpoints(Integer awardafterpoints) {
        this.awardafterpoints = awardafterpoints;
    }

    public Integer getAwardafterlevelseq() {
        return awardafterlevelseq;
    }

    public void setAwardafterlevelseq(Integer awardafterlevelseq) {
        this.awardafterlevelseq = awardafterlevelseq;
    }

    public Integer getAwardafterlogins() {
        return awardafterlogins;
    }

    public void setAwardafterlogins(Integer awardafterlogins) {
        this.awardafterlogins = awardafterlogins;
    }

    public Integer getAwardaftercompleteness() {
        return awardaftercompleteness;
    }

    public void setAwardaftercompleteness(Integer awardaftercompleteness) {
        this.awardaftercompleteness = awardaftercompleteness;
    }
}