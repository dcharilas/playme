package com.playMe.core.db.keys;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
@Embeddable
public class UsermessagecodesPK implements Serializable {

    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    private int applicationid;

    @Column(insertable=false, updatable=false, unique=true, nullable=false)
    private String msgcode;

    @Column(insertable=false, updatable=false, unique=true, nullable=false)
    private String locale;

    @Column(name = "applicationid")
    @Id
    public int getApplicationid() {
        return applicationid;
    }

    public void setApplicationid(int applicationid) {
        this.applicationid = applicationid;
    }

    @Id
    public String getMsgcode() {
        return msgcode;
    }

    public void setMsgcode(String msgcode) {
        this.msgcode = msgcode;
    }

    @Id
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsermessagecodesPK that = (UsermessagecodesPK) o;

        if (applicationid != that.applicationid) return false;
        if (locale != null ? !locale.equals(that.locale) : that.locale != null) return false;
        if (msgcode != null ? !msgcode.equals(that.msgcode) : that.msgcode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = applicationid;
        result = 31 * result + (msgcode != null ? msgcode.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        return result;
    }
}
