package com.playMe.core.db.repository;

import com.playMe.core.db.entity.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface ApplicationRepository extends JpaRepository<Application,Integer>,JpaSpecificationExecutor<Application> {

    List<Application> findByName(String appName);
}
