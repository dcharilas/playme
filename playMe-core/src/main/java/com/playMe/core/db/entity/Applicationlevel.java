package com.playMe.core.db.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.playMe.core.db.keys.ApplicationlevelPK;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the applicationlevels database table.
 * 
 */
@Entity
@Table(name="applicationlevels", schema="", catalog="playme")
@NamedQuery(name="Applicationlevel.findAll", query="SELECT a FROM Applicationlevel a")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Applicationlevel implements Serializable {
	private static final long serialVersionUID = 1L;

    @JsonIgnore
	@EmbeddedId
	private ApplicationlevelPK id;

	@Column(columnDefinition="bit")
	private Boolean active;

	private int pointsrequired;

	private int sequence;

	//bi-directional many-to-one association to Application
	@ManyToOne
    @JoinColumn(name="applicationid", nullable=false, insertable=false, updatable=false)
    private Application application;

    @ManyToOne
    @JoinColumn(name="levelid", nullable=false, insertable=false, updatable=false)
    private Level level;


	public Applicationlevel() {
	}

	public ApplicationlevelPK getId() {
		return this.id;
	}

	public void setId(ApplicationlevelPK id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getPointsrequired() {
		return this.pointsrequired;
	}

	public void setPointsrequired(int pointsrequired) {
		this.pointsrequired = pointsrequired;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
}