package com.playMe.core.dto;

import com.playMe.core.db.entity.Externaleventhistory;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * This is the equivalent of externaleventhistory class. This object is used in order
 * to push and poll event messages to the queue
 *
 * @author Dimitris Charilas
 */
@XmlRootElement
public class Event implements Serializable {

    private long id;
    private int eventId;
    private String comments;
    private Date eventdate;
    private int numericvalue1;
    private int numericvalue2;
    private int numericvalue3;
    private int numericvalue4;
    private String textvalue1;
    private String textvalue2;
    private String textvalue3;
    private String textvalue4;
    private int applicationId;
    private long userId;


    public Event(){}

    /**
     * Constructor
     *
     * @param externalEvent
     */
    public Event(Externaleventhistory externalEvent) {
        this.id = externalEvent.getId();
        this.eventId = externalEvent.getEventdefinition() != null ? externalEvent.getEventdefinition().getId() : 0;
        this.comments = externalEvent.getComments();
        this.eventdate = externalEvent.getEventdate();
        this.numericvalue1 = externalEvent.getNumericvalue1();
        this.numericvalue2 = externalEvent.getNumericvalue2();
        this.numericvalue3 = externalEvent.getNumericvalue3();
        this.numericvalue4 = externalEvent.getNumericvalue4();
        this.textvalue1 = externalEvent.getTextvalue1();
        this.textvalue2 = externalEvent.getTextvalue2();
        this.textvalue3 = externalEvent.getTextvalue3();
        this.textvalue4 = externalEvent.getTextvalue4();
        this.applicationId = externalEvent.getApplication() != null ? externalEvent.getApplication().getId() : 0;
        this.userId = externalEvent.getUser() != null ? externalEvent.getUser().getId() : 0;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getEventdate() {
        return eventdate;
    }

    public void setEventdate(Date eventdate) {
        this.eventdate = eventdate;
    }

    public int getNumericvalue1() {
        return numericvalue1;
    }

    public void setNumericvalue1(int numericvalue1) {
        this.numericvalue1 = numericvalue1;
    }

    public int getNumericvalue2() {
        return numericvalue2;
    }

    public void setNumericvalue2(int numericvalue2) {
        this.numericvalue2 = numericvalue2;
    }

    public String getTextvalue1() {
        return textvalue1;
    }

    public void setTextvalue1(String textvalue1) {
        this.textvalue1 = textvalue1;
    }

    public String getTextvalue2() {
        return textvalue2;
    }

    public void setTextvalue2(String textvalue2) {
        this.textvalue2 = textvalue2;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getNumericvalue3() {
        return numericvalue3;
    }

    public void setNumericvalue3(int numericvalue3) {
        this.numericvalue3 = numericvalue3;
    }

    public int getNumericvalue4() {
        return numericvalue4;
    }

    public void setNumericvalue4(int numericvalue4) {
        this.numericvalue4 = numericvalue4;
    }

    public String getTextvalue3() {
        return textvalue3;
    }

    public void setTextvalue3(String textvalue3) {
        this.textvalue3 = textvalue3;
    }

    public String getTextvalue4() {
        return textvalue4;
    }

    public void setTextvalue4(String textvalue4) {
        this.textvalue4 = textvalue4;
    }
}
