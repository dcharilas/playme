package com.playMe.core.jms;

import com.playMe.core.db.entity.Externaleventhistory;
import com.playMe.core.dto.Event;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Class used to add external events to queue so that they are processed later
 *
 * @author Charilas
 */
@Component
public class EventMessageProducer {

    private static final Logger logger = Logger.getLogger(EventMessageProducer.class);

    private static final String eventField = "event";

    @Autowired
    private JmsMessageProducer jmsMessageProducer;


    private Marshaller marshaller;
    private JAXBContext jaxbContext;


    public EventMessageProducer() throws JAXBException {
		/*
		 * JAXB init
		 */
        logger.info("EventMessageProducer: Creating JAXB marshaller");
        jaxbContext = JAXBContext.newInstance(Event.class);
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    }


    /**
     * Pushes an event to the queue
     *
     * @param event
     */
    public void pushEventToQueue(Externaleventhistory event) throws JAXBException {

        Map map = new HashMap();
        /*
         * Marshal the object into xml string
         */
        StringWriter sw = new StringWriter();
        Event jmsEvent = new Event(event);
        marshaller.marshal(jmsEvent, sw);
        map.put(eventField, sw.toString());
        /*
         * Send the message
         */
        jmsMessageProducer.send(map);
    }

}