package com.playMe.core.jms;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Retrieves a message from the JMS queue
 *
 * @author Charilas
 *
 */
@Component
public class JmsMessageProducer {

    private static final Logger logger = Logger.getLogger(JmsMessageProducer.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public void send(final Map map) {
        logger.debug("Sending message...");
        try {
            jmsTemplate.convertAndSend(map);
        } catch (Exception e) {
            logger.error("Communication with JMS queue failed.", e);
        }
    }

}