package com.playMe.core.jms;

import com.playMe.core.db.entity.Eventhistory;
import com.playMe.core.dto.Event;
import com.playMe.core.service.ProcessEventService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.List;

/**
 * Reads an event from the JMS queue
 *
 * @author Charilas
 *
 */
public class EventMessageConsumer implements MessageListener {

    private static final Logger logger = Logger.getLogger(EventMessageConsumer.class);

    private static final String eventField = "event";

    @Autowired
    private ProcessEventService processEventService;

    private Unmarshaller unmarshaller;
    private JAXBContext jaxbContext;



    public EventMessageConsumer() throws JAXBException {
        /*
		 * JAXB init
		 */
        logger.info("EventMessageConsumer: Creating JAXB unmarshaller");
        jaxbContext = JAXBContext.newInstance(Event.class);
        unmarshaller = jaxbContext.createUnmarshaller();
    }

    @Transactional
    public void onMessage(final Message message) {
        if (message instanceof MapMessage) {
            final MapMessage mapMessage = (MapMessage) message;
            try {
                /*
                 * Read the message and convert it to event object
                 */
                String textMessage = mapMessage.getString(eventField);
                StringReader reader = new StringReader(textMessage);
                Event event = (Event) unmarshaller.unmarshal(reader);
                logger.info("Retrieved message :" + textMessage);
                /*
                 * Now call the proper service to process the event
                 */
                List<Eventhistory> createdEvents = processEventService.processEvent(event);
                if (createdEvents == null/* || createdEvents.size() == 0*/) {
                    throw new Exception("Processing of external event failed.");
                }
            } catch (Exception e) {
                logger.error("Could not read event from queue", e);
                throw new RuntimeException(e);
            }
        }
    }

}