package com.playMe.core.job;


import com.playMe.core.service.UserMessageService;
import com.playMe.core.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * This class defines spring tasks that update the data stored in DB
 *
 * @author Charilas
 */
@Component
@EnableScheduling
public class DataUpdateTask {

    private static final Logger logger = Logger.getLogger(DataUpdateTask.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserMessageService userMessageService;


    /**
     * Updates the age of all users according to their birth date
     */
    @Scheduled(cron="${cron.playme.age.recalculation}")
    public void recalculateAge() {
        logger.info("Running DataUpdateTask.recalculateAge ...");
        int users = userService.updateUserAge();
        logger.info("Updated age of " +users +" users.");
    }


    /**
     * Finds user messages that have been read more than
     * 24 hours ago and moves them from usermessages to pastusermessages
     */
    @Scheduled(cron="${cron.playme.usermessages.move}")
    public void moveUserMessages() {
        logger.info("Running DataUpdateTask.moveUserMessages ...");
        int messages = userMessageService.moveUserMessages();
        logger.info("Moved " +messages +" messages.");
    }
}
