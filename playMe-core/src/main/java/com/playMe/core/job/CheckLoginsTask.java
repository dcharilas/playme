package com.playMe.core.job;


import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.Applicationbadge;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.BadgeService;
import com.playMe.core.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * This class defines spring tasks that update the data stored in DB
 *
 * @author Charilas
 */
@Component
@EnableScheduling
public class CheckLoginsTask {

    private static final Logger logger = Logger.getLogger(CheckLoginsTask.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private BadgeService badgeService;

    /**
     * Checks all users whether they have consecutive logins. If yes,
     * then proper badges are awarded.
     */
    @Scheduled(cron="${cron.playme.user.logins}")
    public void checkLogins() {
        logger.info("Running CheckLoginsTask.checkLogins ...");

        int awardedBadges = 0;
        /*
         * First get a list of all applications
         */
        List<Application> applications = applicationService.getAllApplications();
        if (applications == null) {
            return;
        }

        for (Application application : applications) {
            /*
             * Load list of badges that are related to consecutive logins for
             * this application
             */
            List<Applicationbadge> badges = badgeService.getApplicationBadgesForConsequtiveLogins(application.getId());
            if (badges != null && badges.size() > 0) {
                for (Applicationbadge badge : badges) {
                    /*
                     * Find which application users are qualified to be awarded this badge
                     */
                    List<Long> users = userService.checkConsecutiveLogins(application.getId(), badge.getAwardafterlogins().intValue());
                    if (users != null && users.size() > 0) {
                        /*
                         * Award each user with the badge
                         */
                        for (Long userId : users) {
                            badgeService.awardUserWithBadge(userId,application.getId(), badge.getBadge().getId());
                            awardedBadges++;
                        }

                    }
                }
            }
        }

        logger.info("Finished CheckLoginsTask.checkLogins. Awarded " +awardedBadges +" badges.");
    }


}
