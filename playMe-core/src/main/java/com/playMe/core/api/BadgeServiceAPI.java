package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.Applicationbadge;
import com.playMe.core.db.entity.Badge;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.BadgeService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/badge")
public class BadgeServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(BadgeServiceAPI.class);

    @Autowired
    private BadgeService badgeService;

    private ObjectMapper objectMapper;


    public BadgeServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @POST
    @Path("/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getBadgesAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Applicationbadge badge = null;
        if (!StringUtils.isEmpty(body)) {
            badge = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Applicationbadge.class));
        }
        return badgeService.getBadges(page, pagesize, field, dir, badge);
    }


    @GET
    @Path("/get/{applId}")
    @Produces("application/json")
    public List<Applicationbadge> getBadgesAPI(
            @PathParam(value = "applId") int applId) {

        return badgeService.getApplicationBadges(applId,false);
    }


    @GET
    @Path("/get/active/{applId}")
    @Produces("application/json")
    public List<Badge> getApplicationActiveBadgesAPI(
            @PathParam(value = "applId") int applId) {

        return badgeService.getApplicationActiveBadges(applId);
    }



    @PUT
    @Path("/insert/")
    @Produces("application/json")
    public Response updateBadgeAPI(@RequestBody String body) throws IOException {

        Applicationbadge badge = null;
        if (!StringUtils.isEmpty(body)) {
            badge = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Applicationbadge.class));
        }
        Applicationbadge badgeResponse = badgeService.saveBadgeInfo(badge);

        if (badgeResponse != null) {
            return Response.ok(badgeResponse).build();
        } else {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/delete/{badgeId}/{applId}")
    @Produces("application/json")
    public boolean deleteBadgeAPI(
            @PathParam(value = "badgeId") int badgeId,
            @PathParam(value = "applId") int applId) {

        return badgeService.deleteBadge(badgeId,applId);
    }
}
