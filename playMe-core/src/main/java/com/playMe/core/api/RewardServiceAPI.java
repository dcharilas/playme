package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.Reward;
import com.playMe.core.db.entity.Userreward;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.RewardService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/reward")
public class RewardServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(RewardServiceAPI.class);

    @Autowired
    private RewardService rewardService;

    private ObjectMapper objectMapper;


    public RewardServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @POST
    @Path("/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getRewardsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Reward reward = null;
        if (!StringUtils.isEmpty(body)) {
            reward = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Reward.class));
        }
        return rewardService.getRewards(page, pagesize, field, dir, reward);
    }


    @GET
    @Path("/get/{applId}")
    @Produces("application/json")
    public List<Reward> getRewardsAPI(
            @PathParam(value = "applId") int applId) {

        return rewardService.getApplicationActiveRewards(applId);
    }


    @GET
    @Path("/get/id/{rewardId}")
    @Produces("application/json")
    public Reward getRewardAPI(
            @PathParam(value = "rewardId") int rewardId) {

        return rewardService.getReward(rewardId);
    }


    @GET
    @Path("/get/active/{applId}")
    @Produces("application/json")
    public List<Reward> getApplicationActiveRewardsAPI(
            @PathParam(value = "applId") int applId) {

        return rewardService.getApplicationActiveRewards(applId);
    }


    @GET
    @Path("/get/user/{applId}/{userId}")
    @Produces("application/json")
    public List<Userreward> getUserRewardsAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") int userId) {

        return rewardService.getUserRewards(userId, applId);
    }


    @PUT
    @Path("/insert/")
    @Produces("application/json")
    public Response updateRewardAPI(@RequestBody String body) throws IOException {

        Reward reward = null;
        if (!StringUtils.isEmpty(body)) {
            reward = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Reward.class));
        }
        Reward response = rewardService.saveRewardInfo(reward);

        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/delete/{rewardId}")
    @Produces("application/json")
    public boolean deleteRewardAPI(
            @PathParam(value = "rewardId") int rewardId) {

        return rewardService.deleteReward(rewardId);
    }


    @GET
    @Path("/get/user/{identifier}")
    @Produces("application/json")
    public Userreward getUserRewardAPI(
            @PathParam(value = "identifier") String identifier) {

        return rewardService.getUserReward(identifier);
    }

}
