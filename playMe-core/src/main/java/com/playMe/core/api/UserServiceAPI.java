package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.BadgeService;
import com.playMe.core.service.LevelService;
import com.playMe.core.service.PointService;
import com.playMe.core.service.UserService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;


/**
 * REST service for invoking services related to users
 *
 * @author Charilas
 *
 */
@Path("/user")
public class UserServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(UserServiceAPI.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PointService pointService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private BadgeService badgeService;

    ObjectMapper objectMapper;


    public UserServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @PUT
    @Path("/insert/{applId}/{username}")
    @Produces("application/json")
    public User insertUserAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "username") String username) {

        return userService.insertNewUser(applId, username);
    }


    @GET
    @Path("/get/{applId}/{userId}")
    @Produces("application/json")
    public User getUserAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return userService.getUserProfile(userId,applId);
    }

    @GET
    @Path("/get/{applId}/username/{username}")
    @Produces("application/json")
    public User getUserByUsernameAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "username") String username) {

        return userService.getUserProfile(username,applId);
    }


    @GET
    @Path("/login/{applId}/username/{username}")
    @Produces("application/json")
    public User loginUserAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "username") String username) {

        return userService.loginUser(username,applId);
    }


    @GET
    @Path("/get/{applId}")
    @Produces("application/json")
    public List<User> getApplicationUsersAPI(
            @PathParam(value = "applId") int applId) {

        return userService.getApplicationUsers(applId);
    }


    @POST
    @Path("/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getUsersAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        User user = null;
        if (!StringUtils.isEmpty(body)) {
            user = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(User.class));
        }
        return userService.getUsers(page, pagesize, field, dir, user);
    }



    @GET
    @Path("/completeness/get/{applId}/{userId}")
    @Produces("application/json")
    public String getProfileCompletenessAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        double percentage = userService.getProfileCompletenessPercentage(userId, applId);
        return String.valueOf(percentage);
    }


    @PUT
    @Path("/insert/")
    @Produces("application/json")
    public Response updateUserAPI(@RequestBody String body) throws IOException {

        User user = null;
        if (!StringUtils.isEmpty(body)) {
            user = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(User.class));
        }
        User userResponse = userService.saveUserInfo(user);

        if (userResponse != null) {
            return Response.ok(userResponse).build();
        } else {
            return Response.serverError().build();
        }
    }



    @PUT
    @Path("/insert/partial")
    @Produces("application/json")
    public Response updatePartialUserAPI(@RequestBody String body) throws IOException {

        User user = null;
        if (!StringUtils.isEmpty(body)) {
            user = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(User.class));
        }
        User userResponse = userService.updateSpecificUserInfo(user);

        if (userResponse != null) {
            return Response.ok(userResponse).build();
        } else {
            return Response.serverError().build();
        }
    }


/*    @GET
    @Path("/points/update/{applId}/{userId}/{awardPoints}")
    @Produces("application/json")
    public User awardPointsAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId,
            @PathParam(value = "awardPoints") int awardPoints) {

        return pointService.awardUserWithPoints(awardPoints, userId, applId, null);
    }*/


    @GET
    @Path("/points/history/get/{applId}/{userId}")
    @Produces("application/json")
    public ResponseItem getPointsHistoryAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return pointService.getUserPointsHistory(userId, applId);
    }


/*
    @GET
    @Path("/level/update/{applId}/{userId}")
    @Produces("application/json")
    public User updateUserLevelAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return levelService.increaseUserLevel(userId, applId);
    }
*/


    @GET
    @Path("/level/history/get/{applId}/{userId}")
    @Produces("application/json")
    public ResponseItem getLevelHistoryAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return levelService.getUserLevelProgression(userId, applId);
    }


/*    @GET
    @Path("/badges/update/{applId}/{userId}/{badgeId}")
    @Produces("application/json")
    public List<Badge> updateUserBadgesAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId,
            @PathParam(value = "badgeId") int badgeId) {

        return badgeService.awardUserWithBadge(userId, applId, badgeId);
    }*/


    @GET
    @Path("/badges/get/{applId}/{userId}")
    @Produces("application/json")
    public List<Badge> getUserBadgesAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return badgeService.getUserBadges(userId, applId);
    }



    @GET
    @Path("/level/get/{applId}/{userId}")
    @Produces("application/json")
    public Level getUserLevelAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "userId") long userId) {

        return levelService.getUserLevel(userId, applId);
    }

}
