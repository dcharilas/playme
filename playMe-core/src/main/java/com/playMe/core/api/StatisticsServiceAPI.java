package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.constant.StatisticsEnum;
import com.playMe.core.model.StatisticsDateItem;
import com.playMe.core.model.StatisticsItem;
import com.playMe.core.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import java.util.List;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/statistics")
public class StatisticsServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(StatisticsServiceAPI.class);

    @Autowired
    private StatisticsService statisticsService;

    ObjectMapper objectMapper;


    public StatisticsServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @GET
         @Path("/get/user/gender/{applId}")
         @Produces("application/json")
         public List<StatisticsItem> getApplicationGenderStatisticsAPI(@PathParam(value = "applId") int applId) {

        return statisticsService.getStatistics(StatisticsEnum.USER_GENDER, applId);
    }

    @GET
    @Path("/get/user/gender")
    @Produces("application/json")
    public List<StatisticsItem> getGenderStatisticsAPI() {

        return statisticsService.getStatistics(StatisticsEnum.USER_GENDER);
    }

    @GET
    @Path("/get/user/age/{applId}")
    @Produces("application/json")
    public List<StatisticsItem> getAgeStatisticsAPI(@PathParam(value = "applId") int applId) {

        return statisticsService.getStatistics(StatisticsEnum.USER_AGE, applId);
    }

    @GET
    @Path("/get/user/age")
    @Produces("application/json")
    public List<StatisticsItem> getApplicationAgeStatisticsAPI() {

        return statisticsService.getStatistics(StatisticsEnum.USER_AGE);
    }


    @GET
    @Path("/get/application/users")
    @Produces("application/json")
    public List<StatisticsItem> getApplicationUsersStatisticsAPI() {

        return statisticsService.getStatistics(StatisticsEnum.APPLICATION_USERS);
    }

    @GET
    @Path("/get/user/level/sequence")
    @Produces("application/json")
    public List<StatisticsItem> getUserLevelSequenceStatisticsAPI() {

        return statisticsService.getStatistics(StatisticsEnum.USERS_LEVEL_SEQUENCE);
    }


    @GET
    @Path("/get/externalevent/day/{days}")
    @Produces("application/json")
    public List<StatisticsDateItem> getExternalEventsStatisticsAPI(
            @PathParam(value = "days") int days) {

        return statisticsService.getDateStatistics(StatisticsEnum.EXTERNAL_EVENTS_PER_DAY, days);
    }

    @GET
    @Path("/get/event/day/{days}")
    @Produces("application/json")
    public List<StatisticsDateItem> getEventsStatisticsAPI(
            @PathParam(value = "days") int days) {

        return statisticsService.getDateStatistics(StatisticsEnum.INTERNAL_EVENTS_PER_DAY, days);
    }

    @GET
    @Path("/get/externalevent/day/{days}/{applId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getApplicationExternalEventsStatisticsAPI(
            @PathParam(value = "days") int days, @PathParam(value = "applId") int applId) {

        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_EXTERNAL_EVENTS_PER_DAY, days, applId);
    }

    @GET
    @Path("/get/event/day/{days}/{applId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getApplicationEventsStatisticsAPI(
            @PathParam(value = "days") int days, @PathParam(value = "applId") int applId) {

        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_INTERNAL_EVENTS_PER_DAY, days, applId);
    }

    @GET
    @Path("/get/application/users/day/{days}/{applId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getApplicationUsersPerDayStatisticsAPI(
            @PathParam(value = "days") int days, @PathParam(value = "applId") int applId) {

        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_USERS_PER_DAY, days, applId);
    }

    @GET
    @Path("/get/user/points/{applId}/{userId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getUserPointsPerDayAPI(
            @PathParam(value = "applId") int applId, @PathParam(value = "userId") long userId) {

        return statisticsService.getDateStatistics(StatisticsEnum.TOTAL_POINTS_PER_DAY, 0, applId, userId);
    }

    @GET
    @Path("/get/user/redeempoints/{applId}/{userId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getUserRedeemablePointsPerDayAPI(
            @PathParam(value = "applId") int applId, @PathParam(value = "userId") long userId) {

        return statisticsService.getDateStatistics(StatisticsEnum.REDEEMABLE_POINTS_PER_DAY, 0, applId, userId);
    }

    @GET
    @Path("/get/reward/application")
    @Produces("application/json")
    public List<StatisticsItem> getRedeemedRewardsStatisticsAPI() {

        return statisticsService.getStatistics(StatisticsEnum.REDEEMED_REWARDS);
    }

    @GET
    @Path("/get/reward/type/{applId}")
    @Produces("application/json")
    public List<StatisticsItem> getRedeemedRewardsPerTypeStatisticsAPI(
            @PathParam(value = "applId") int applId) {

        return statisticsService.getStatistics(StatisticsEnum.REDEEMED_REWARD_PER_TYPE, applId);
    }

    @GET
    @Path("/get/reward/points/day/{days}/{applId}")
    @Produces("application/json")
    public List<StatisticsDateItem> getRedeemedPointsPerDayStatisticsAPI(
            @PathParam(value = "days") int days, @PathParam(value = "applId") int applId) {

        return statisticsService.getDateStatistics(StatisticsEnum.REDEEMED_POINTS_PER_DAY, days, applId);
    }
}
