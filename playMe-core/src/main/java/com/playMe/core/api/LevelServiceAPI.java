package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.LevelService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/level")
public class LevelServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(LevelServiceAPI.class);


    @Autowired
    private LevelService levelService;


    ObjectMapper objectMapper;


    public LevelServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @GET
    @Path("/get/{applId}")
    @Produces("application/json")
    public List<Applicationlevel> getLevelsAPI(
            @PathParam(value = "applId") int applId) {

        return levelService.getApplicationLevels(applId);
    }


    @POST
    @Path("/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getLevelsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Applicationlevel level = null;
        if (!StringUtils.isEmpty(body)) {
            level = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Applicationlevel.class));
        }
        return levelService.getLevels(page, pagesize, field, dir, level);
    }


    @PUT
    @Path("/insert/")
    @Produces("application/json")
    public Response updateLevelAPI(@RequestBody String body) throws IOException {

        Applicationlevel level = null;
        if (!StringUtils.isEmpty(body)) {
            level = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Applicationlevel.class));
        }
        Applicationlevel levelResponse = levelService.saveLevelInfo(level);

        if (levelResponse != null) {
            return Response.ok(levelResponse).build();
        } else {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/delete/{levelId}/{applId}")
    @Produces("application/json")
    public boolean deleteLevelAPI(
            @PathParam(value = "levelId") int levelId,
            @PathParam(value = "applId") int applId) {

        return levelService.deleteLevel(levelId,applId);
    }
}
