package com.playMe.core.api;

import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.LeaderboardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;

/**
 * REST service for invoking services related to leaderboard
 * 
 * @author Charilas
 *
 */
@Path("/leaderboard")
public class LeaderboardServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(LeaderboardServiceAPI.class);

    @Autowired
    private LeaderboardService leaderboardService;



    @GET
    @Path("/get/{applId}/paging/{page}/{pagesize}")
    @Produces("application/json")
    public ResponseItem getLeaderboardAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize) {

        return leaderboardService.getLeaderBoard(applId, page, pagesize);
    }


}
