package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.*;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;


/**
 * REST service for invoking services related to users
 *
 * @author Charilas
 *
 */
@Path("/event")
public class EventServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(EventServiceAPI.class);

    @Autowired
    private EventService eventService;

    ObjectMapper objectMapper;

    public EventServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @GET
    @Path("/definition/get")
    @Produces("application/json")
    public ResponseItem getEventDefinitionsAPI() {

        return eventService.getEventDefinitions();
    }


    @GET
    @Path("/action/insert/{appId}/{eventId}/{badgeAward}/{pointsAward}")
    @Produces("application/json")
    public boolean insertEventActionAPI(
            @PathParam(value = "appId") int appId,
            @PathParam(value = "eventId") int eventId,
            @PathParam(value = "badgeAward") int badgeAward,
            @PathParam(value = "pointsAward") int pointsAward) {

        return eventService.insertEventAction(eventId,appId,badgeAward,pointsAward);
    }


    @GET
    @Path("/action/get")
    @Produces("application/json")
    public List<Eventaction> getEventActionsAPI() {

        return eventService.getEventActions();
    }


    @GET
    @Path("/action/get/{appId}")
    @Produces("application/json")
    public List<Eventaction> getEventActionsAPI(
            @PathParam(value = "appId") int appId) {

        return eventService.getApplicationEventActions(appId);
    }



    @POST
    @Path("/external/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getExternalEventsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Externaleventhistory event = null;
        if (!StringUtils.isEmpty(body)) {
            event = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Externaleventhistory.class));
        }
        return eventService.getExternalEvents(page, pagesize, field, dir, event);
    }


    @PUT
    @Path("/external/insert")
    @Produces("application/json")
    public Response registerEventAPI(@RequestBody String body) throws IOException {

        Externaleventhistory event = null;
        if (!StringUtils.isEmpty(body)) {
            event = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Externaleventhistory.class));
        }
        event = eventService.registerEvent(event);
        if (event != null) {
            return Response.ok(event).build();
        } else {
            return Response.serverError().build();
        }
    }


    @POST
    @Path("/internal/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getInternalEventsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Eventhistory event = null;
        if (!StringUtils.isEmpty(body)) {
            event = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Eventhistory.class));
        }
        return eventService.getInternalEvents(page, pagesize, field, dir, event);
    }
}
