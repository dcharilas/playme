package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ConfigurationService;
import com.playMe.core.service.UserMessageService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/configuration")
public class ConfigurationServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(ConfigurationServiceAPI.class);


    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private UserMessageService userMessageService;


    ObjectMapper objectMapper;


    public ConfigurationServiceAPI() {
        objectMapper = new ObjectMapper();
    }



    @GET
    @Path("/usermessage/get/codes")
    @Produces("application/json")
    public List<String> getUserMessageCodesAPI() {

        return userMessageService.getAllUserMessageCodes();
    }

    @GET
    @Path("/message/get/{messageId}")
    @Produces("application/json")
    public List<Displaytext> getDescriptionsAPI(
            @PathParam(value = "messageId") int messageId) {

        return configurationService.getDescriptions(messageId);
    }

    @GET
    @Path("/message/get/{messageId}/{locale}")
    @Produces("application/json")
    public Displaytext getDescriptionAPI(
            @PathParam(value = "messageId") int messageId,
            @PathParam(value = "locale") String locale) {

        return configurationService.getDescription(messageId,locale);
    }


    @POST
    @Path("/eventdef/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getEventDefinitionsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Eventdefinition definition = null;
        if (!StringUtils.isEmpty(body)) {
            definition = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Eventdefinition.class));
        }
        return configurationService.getEventDefinitions(page, pagesize, field, dir,definition);
    }


    @POST
    @Path("/setting/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getConfigurationAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Configuration configuration = null;
        if (!StringUtils.isEmpty(body)) {
            configuration = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Configuration.class));
        }
        return configurationService.getConfiguration(page, pagesize, field, dir, configuration);
    }


    @POST
    @Path("/message/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getDisplayTextsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Displaytext text = null;
        if (!StringUtils.isEmpty(body)) {
            text = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Displaytext.class));
        }
        return configurationService.getDisplayTexts(page, pagesize, field, dir, text);
    }


    @POST
    @Path("/usermessage/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getUserMessageCodesAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Usermessagecodes text = null;
        if (!StringUtils.isEmpty(body)) {
            text = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Usermessagecodes.class));
        }
        return configurationService.getUserMessages(page, pagesize, field, dir, text);
    }


    @POST
    @Path("/profileweight/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getProfileWeightsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Userprofileweights weight = null;
        if (!StringUtils.isEmpty(body)) {
            weight = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Userprofileweights.class));
        }
        return configurationService.getProfileWeights(page, pagesize, field, dir, weight);
    }


    @POST
    @Path("eventaction/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getEventActionsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Eventaction action = null;
        if (!StringUtils.isEmpty(body)) {
            action = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Eventaction.class));
        }
        return configurationService.getEventActions(page, pagesize, field, dir, action);
    }


    @PUT
    @Path("/eventdef/insert/{name}")
    @Produces("application/json")
    public Response insertEventDefinitionAPI(
            @PathParam(value = "name") String name) {

        Eventdefinition response = configurationService.insertEventDefinition(name);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @PUT
    @Path("setting/insert/")
    @Produces("application/json")
    public Response updateConfigurationAPI(@RequestBody String body) throws IOException {

        Configuration configuration = null;
        if (!StringUtils.isEmpty(body)) {
            configuration = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Configuration.class));
        }
        Configuration response = configurationService.insertConfiguration(configuration);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    } 

    @PUT
    @Path("message/insert/")
    @Produces("application/json")
    public Response updateMessageAPI(@RequestBody String body) throws IOException {

        Displaytext text = null;
        if (!StringUtils.isEmpty(body)) {
            text = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Displaytext.class));
        }
        Displaytext response = configurationService.insertMessage(text);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @PUT
    @Path("usermessage/insert/")
    @Produces("application/json")
    public Response updateUserMessageAPI(@RequestBody String body) throws IOException {

        Usermessagecodes text = null;
        if (!StringUtils.isEmpty(body)) {
            text = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Usermessagecodes.class));
        }
        Usermessagecodes response = configurationService.insertUserMessage(text);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }



    @PUT
    @Path("message/insert/bulk")
    @Produces("application/json")
    public Response updateMessagesAPI(@RequestBody String body) throws IOException {

        Displaytext[] texts = null;
        if (!StringUtils.isEmpty(body)) {
            texts = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Displaytext[].class));
        }
        List<Displaytext> response = configurationService.insertMessages(Arrays.asList(texts));
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @PUT
    @Path("eventaction/insert/")
    @Produces("application/json")
    public Response updateEventActionAPI(@RequestBody String body) throws IOException {

        Eventaction action = null;
        if (!StringUtils.isEmpty(body)) {
            action = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Eventaction.class));
        }
        Eventaction response = configurationService.insertEventAction(action);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @PUT
    @Path("profileweight/insert/")
    @Produces("application/json")
    public Response updateProfileWeightAPI(@RequestBody String body) throws IOException {

        Userprofileweights weight = null;
        if (!StringUtils.isEmpty(body)) {
            weight = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Userprofileweights.class));
        }
        Userprofileweights response = configurationService.insertProfileWeight(weight);
        if (response != null) {
            return Response.ok(response).build();
        } else {
            return Response.serverError().build();
        }
    }


    @DELETE
    @Path("/eventdef/delete/{id}")
    @Produces("application/json")
    public boolean deleteEventDefinitionAPI(
            @PathParam(value = "id") int id) {

        return configurationService.deleteEventDefinition(id);
    }


    @DELETE
    @Path("/eventaction/delete/{applId}/{eventId}")
    @Produces("application/json")
    public boolean deleteEventActionAPI(
            @PathParam(value = "applId") int applId,
            @PathParam(value = "eventId") int eventId) {

        return configurationService.deleteEventAction(applId,eventId);
    }


    @DELETE
    @Path("/message/delete/{messageId}/{locale}")
    @Produces("application/json")
    public boolean deleteDisplayTextAPI(
            @PathParam(value = "messageId") int messageId,
            @PathParam(value = "locale") String locale) {

        return configurationService.deleteDisplayText(messageId, locale);
    }


    @DELETE
    @Path("/profileweight/delete/{id}")
    @Produces("application/json")
    public boolean deleteProfileWeightAPI(
            @PathParam(value = "id") String id) {

        return configurationService.deleteProfileWeight(id);
    }



    @DELETE
    @Path("/setting/delete/{id}")
    @Produces("application/json")
    public boolean deleteConfigurationAPI(
            @PathParam(value = "id") int id) {

        return configurationService.deleteConfiguration(id);
    }


    @DELETE
    @Path("/usermessage/delete/{messageCode}/{applId}/{locale}")
    @Produces("application/json")
    public boolean deleteDisplayTextAPI(
            @PathParam(value = "messageCode") String messageCode,
            @PathParam(value = "applId") int applId,
            @PathParam(value = "locale") String locale) {

        return configurationService.deleteUserMessage(messageCode, applId, locale);
    }
}
