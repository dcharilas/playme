package com.playMe.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ApplicationService;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * REST service for invoking services related to applications
 * 
 * @author Charilas
 *
 */
@Path("/application")
public class ApplicationServiceAPI {

    private final Logger logger = LoggerFactory.getLogger(ApplicationServiceAPI.class);

    @Autowired
    private ApplicationService applicationService;

    ObjectMapper objectMapper;


    public ApplicationServiceAPI() {
        objectMapper = new ObjectMapper();
    }


    @PUT
    @Path("/insert/{applName}")
    @Produces("application/json")
    public Response insertApplicationAPI(
            @PathParam(value = "applName") String applName) {

        Application application = applicationService.insertApplication(applName);
        if (application != null) {
            return Response.ok(application).build();
        } else {
            return Response.serverError().build();
        }
    }


    @GET
    @Path("/get/{applId}")
    @Produces("application/json")
    public Application getApplicationAPI(
            @PathParam(value = "applId") int applId) {

        return applicationService.getApplication(applId);
    }


    @POST
    @Path("/get/paging/{page}/{pagesize}/sorting/{field}/{dir}")
    @Produces("application/json")
    public ResponseItem getApplicationsAPI(
            @PathParam(value = "page") int page,
            @PathParam(value = "pagesize") int pagesize,
            @PathParam(value = "field") String field,
            @PathParam(value = "dir") String dir,
            @RequestBody String body) throws JAXBException, IOException {

        Application application = null;
        if (!StringUtils.isEmpty(body)) {
            application = objectMapper.readValue(body, objectMapper.getTypeFactory().constructType(Application.class));
        }
        return applicationService.getApplications(page,pagesize,field,dir,application);
    }


    @DELETE
    @Path("/delete/{applId}")
    @Produces("application/json")
    public boolean deleteApplicationAPI(
            @PathParam(value = "applId") int applId) {

        return applicationService.deleteApplication(applId);
    }

}
