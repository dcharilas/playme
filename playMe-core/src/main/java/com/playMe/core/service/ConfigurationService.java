package com.playMe.core.service;

import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service that handles all actions related to configuration entities
 *
 * @author Dimitris Charilas
 */
public interface ConfigurationService {

    @Transactional
	List<Displaytext> getDescriptions(int messageId);

    @Transactional
    Displaytext getDescription(int messageId, String locale);

    @Transactional
    List<Usermessagecodes> getUserMessagesPerCode(String messageCode, int applId);

    @Transactional
    ResponseItem getEventDefinitions(int page, int pagezize, String field, String dir, Eventdefinition definition);

    @Transactional
    ResponseItem getEventActions(int page, int pagezize, String field, String dir, Eventaction action);

    @Transactional
    ResponseItem getDisplayTexts(int page, int pagezize, String field, String dir, Displaytext text);

    @Transactional
    ResponseItem getProfileWeights(int page, int pagezize, String field, String dir, Userprofileweights weight);

    @Transactional
    ResponseItem getConfiguration(int page, int pagezize, String field, String dir, Configuration configuration);

    @Transactional
    ResponseItem getUserMessages(int page, int pagezize, String field, String dir, Usermessagecodes text);



    @Transactional
    Eventdefinition insertEventDefinition(String name);

    @Transactional
    Eventaction insertEventAction(Eventaction action);

    @Transactional
    Displaytext insertMessage(Displaytext text);

    @Transactional
    List<Displaytext> insertMessages(List<Displaytext> texts);

    @Transactional
    Userprofileweights insertProfileWeight(Userprofileweights weight);

    @Transactional
    Configuration insertConfiguration(Configuration configuration);

    @Transactional
    Usermessagecodes insertUserMessage(Usermessagecodes text);



    @Transactional
    boolean deleteEventDefinition(int id);

    @Transactional
    boolean deleteEventAction(int applId, int eventId);

    @Transactional
    boolean deleteDisplayText(int messageId, String locale);

    @Transactional
    boolean deleteProfileWeight(String id);

    @Transactional
    boolean deleteConfiguration(int id);

    @Transactional
    boolean deleteUserMessage(String messageCode, int applId, String locale);

}
