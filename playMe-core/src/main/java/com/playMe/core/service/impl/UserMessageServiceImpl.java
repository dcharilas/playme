package com.playMe.core.service.impl;

import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.repository.PastusermessagesRepository;
import com.playMe.core.db.repository.UsermessagecodesRepository;
import com.playMe.core.db.repository.UsermessagesRepository;
import com.playMe.core.db.repository.specification.UserMessageSpecification;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ConfigurationService;
import com.playMe.core.service.UserMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Service that handles all requests related to Usermessages, Pastusermessages entities
 *
 * @author Dimitris Charilas
 */
@Service("userMessageService")
public class UserMessageServiceImpl implements UserMessageService {
	
	private final Logger logger = LoggerFactory.getLogger(UserMessageServiceImpl.class);

    private static int readBatchSize = 2;
	
	@Autowired
    private UsermessagesRepository usermessagesRepository;

    @Autowired
    private PastusermessagesRepository pastusermessagesRepository;

    @Autowired
    private UsermessagecodesRepository usermessagecodesRepository;

    @Autowired
    private ConfigurationService configurationService;


    /**
     * Returns a list of all possible user message codes
     *
     * @return
     */
    public List<String> getAllUserMessageCodes() {
        List<String> codes = new ArrayList<>();
        List<UserMessageCodeEnum> enumCodes = Arrays.asList(UserMessageCodeEnum.values());
        for (UserMessageCodeEnum enumCode : enumCodes) {
            codes.add(enumCode.value());
        }
        return codes;
    }

    /**
     * Save a new user message
     *
     * @param usermessages
     * @return
     */
    public Usermessages addNewUserMessage(Usermessages usermessages) {
        logger.info("UserService.createNewUserMessage: " +usermessages.getMsgcode());

        try {
            usermessages.setCreatedon(new Date());
            return usermessagesRepository.save(usermessages);
        } catch (Exception e) {
            logger.error("Could not save level info.", e);
            return null;
        }
    }


    /**
     * Creates a new Usermessages object that will be saved
     *
     * @param user
     * @param application
     * @param values
     * @param msgCode
     * @return
     */
    public Usermessages createNewUserMessage(User user, Application application, List<String> values, UserMessageCodeEnum msgCode) {
        Usermessages usermessage = new Usermessages();
        usermessage.setUser(user);
        usermessage.setApplication(application);
        usermessage.setCreatedon(new Date());
        usermessage.setMsgcode(msgCode.value());
        if (values != null && values.size() > 0) {
            usermessage.setValue1(values.get(0));
            if (values.size() > 1) {
                usermessage.setValue2(values.get(1));
            }
        }

        return addNewUserMessage(usermessage);
    }


    /**
     * Wrapper for the previous function in case user and application objects are not available
     *
     * @param userId
     * @param applId
     * @param values
     * @param msgCode
     * @return
     */
    public Usermessages createNewUserMessage(long userId, int applId, List<String> values, UserMessageCodeEnum msgCode) {
        User user = new User();
        user.setId(userId);
        Application application = new Application();
        application.setId(applId);
        return createNewUserMessage(user, application, values, msgCode);
    }


    /**
     * Return all unread messages for a specific user
     *
     * @param applId
     * @param userId
     * @return
     */
    public ResponseItem getUnreadUserMessages(int applId, long userId, int messagesNumber) {
        logger.info("UserService.getUnreadUserMessages: " +applId +"," +userId);

        try {
            /*
             * Construct search object
             */
            Usermessages usermessages = new Usermessages();
            Application application = new Application();
            application.setId(applId);
            usermessages.setApplication(application);
            User user = new User();
            user.setId(userId);
            usermessages.setUser(user);

            /*
             * Get the messages
             */
            Sort.Order pointsOrder = new Sort.Order(Sort.Direction.DESC, "createdon");
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(0, messagesNumber, sort);
            Page<Usermessages> resultsPage = usermessagesRepository.findAll(
                    new UserMessageSpecification().createSpecFromSearchForm(usermessages),paging);
            if (resultsPage != null) {
                /*
                 * This means that messages have been read. Mark them properly
                 */
                List<Usermessages> messagesToUpdate = new ArrayList<>();
                for (Usermessages readMessage : resultsPage.getContent()) {
                    if (readMessage.getViewedon() == null) {
                        readMessage.setViewedon(new Date());
                        messagesToUpdate.add(readMessage);
                    }
                }
                if (messagesToUpdate.size() > 0) {
                    usermessagesRepository.save(messagesToUpdate);
                }

                return new ResponseItem(resultsPage.getTotalElements(), addMessages(resultsPage.getContent()));
            } else {
                return new ResponseItem(0, new ArrayList<Usermessages>());
            }
        } catch (Exception e) {
            logger.error("Could not load users.", e);
            return null;
        }

    }


    /**
     * Marks a list of user messages as read
     *
     * @param usermessages
     * @return
     */
	private void markUserMessagesAsRead(List<Usermessages> usermessages) {
        logger.info("UserService.markUserMessagesAsRead: " +usermessages.size());

        List<Pastusermessages> pastusermessages = new ArrayList<>();
        Pastusermessages pastusermessage;

        /*
         * Convert all usermessages to pastusermessages
         */
        for (Usermessages usermessage : usermessages) {
            pastusermessage = getPastUserMessage(usermessage);
            pastusermessage.setArchivedon(new Date());
            pastusermessages.add(pastusermessage);
        }
        /*
         * Save all pastusermessages
         */
        pastusermessagesRepository.save(pastusermessages);
        /*
         * Finally delete usermessages
         */
        if (pastusermessages != null && pastusermessages.size() == usermessages.size()) {
            usermessagesRepository.delete(usermessages);
        }
    }



    /**
     * This method is used by a task. Its role is to
     * find user messages that have been read more than
     * 24 hours ago and move them from usermessages to pastusermessages
     */
    public int moveUserMessages() {
        /*
         * Read messages in batches
         */
        int totalMessages= 0;
        Sort.Order pointsOrder = new Sort.Order(Sort.Direction.ASC, "id");
        Sort sort = new Sort(pointsOrder);
        /*
         * Keep reading until there are no more messages.
         * There is no need for pagination since messages are deleted
         * after they are retrieved
         */
        List<Usermessages> resultsPage = usermessagesRepository.getReadMessages(readBatchSize);
        while (resultsPage != null && resultsPage.size() > 0) {
            markUserMessagesAsRead(resultsPage);
            totalMessages += resultsPage.size();
            resultsPage = usermessagesRepository.getReadMessages(readBatchSize);
        }

        return totalMessages;
    }



    /**
     * Converts a Usermessages to a Pastusermessages
     *
     * @param usermessage
     * @return
     */
    private Pastusermessages getPastUserMessage(Usermessages usermessage) {
        Pastusermessages pastusermessage = new Pastusermessages();
        pastusermessage.setCreatedon(usermessage.getCreatedon());
        pastusermessage.setMsgcode(usermessage.getMsgcode());
        pastusermessage.setValue1(usermessage.getValue1());
        pastusermessage.setValue2(usermessage.getValue2());
        pastusermessage.setApplication(usermessage.getApplication());
        pastusermessage.setUser(usermessage.getUser());
        pastusermessage.setViewedon(usermessage.getViewedon());
        pastusermessage.setId(usermessage.getId());
        return pastusermessage;
    }


    /**
     * Add actual message to user messages
     *
     * @param usermessages
     * @return
     */
    private List<Usermessages> addMessages(List<Usermessages> usermessages) {
        for (Usermessages usermessage : usermessages) {
            /*
             * Get the translations
             */
            List<Usermessagecodes> messages = configurationService.getUserMessagesPerCode(usermessage.getMsgcode(), usermessage.getApplication().getId());
            /*
             * Support variables
             * Currently two wildcards are supported
             */
            for (Usermessagecodes message : messages) {
                if (usermessage.getValue1() != null) {
                    message.setMessage(message.getMessage().replace("{1}",usermessage.getValue1()));
                }
                if (usermessage.getValue2() != null) {
                    message.setMessage(message.getMessage().replace("{2}",usermessage.getValue2()));
                }
            }

            usermessage.setMessages(messages);
        }
        return usermessages;
    }

}
