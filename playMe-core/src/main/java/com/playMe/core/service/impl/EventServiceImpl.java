package com.playMe.core.service.impl;

import com.playMe.core.constant.EventEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.keys.EventactionPK;
import com.playMe.core.db.repository.EventactionRepository;
import com.playMe.core.db.repository.EventdefinitionRepository;
import com.playMe.core.db.repository.EventhistoryRepository;
import com.playMe.core.db.repository.ExternaleventhistoryRepository;
import com.playMe.core.db.repository.specification.ExternalEventSpecification;
import com.playMe.core.db.repository.specification.InternalEventSpecification;
import com.playMe.core.jms.EventMessageProducer;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.*;
import com.playMe.core.util.IdentifierUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Service that handles all actions related to entities associated with events
 *
 * @author Dimitris Charilas
 */
@Service("eventService")
public class EventServiceImpl implements EventService {

	private final Logger logger = LoggerFactory.getLogger(EventServiceImpl.class);
	
	private final static int numberOfNumericParameters = 2;
	private final static int numberOfStringParameters = 2;


    @Autowired
    private ExternaleventhistoryRepository externaleventhistoryRepository;

    @Autowired
    private EventdefinitionRepository eventdefinitionRepository;

    @Autowired
    private EventactionRepository eventactionRepository;

    @Autowired
    private EventhistoryRepository eventhistoryRepository;

    @Autowired
    private PointService pointService;

    @Autowired
    private BadgeService badgeService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserService userService;

    @Autowired
    private EventMessageProducer eventPush;
	
	/**
	 * Insert the definition of a new event
	 * 
	 * @param name
	 * @return
	 */
	public boolean insertEventDefinition(String name) {
		logger.info("EventService.insertEventDefinition: " +name);
        try {
            Eventdefinition ed = new Eventdefinition();
            ed.setName(name);
            eventdefinitionRepository.save(ed);
            return true;
        } catch (Exception e) {
            logger.error("Could not insert event definition.", e);
            return false;
        }
	}


    /**
     * Returns a list of all event definitions
     *
     * @return
     */
    public ResponseItem getEventDefinitions() {
        logger.info("EventService.getEventDefinitions");
        try {
            List<Eventdefinition> definitions = eventdefinitionRepository.findAll();
            if (definitions != null) {
                return new ResponseItem(definitions.size(), definitions);
            } else {
                return new ResponseItem(0, new ArrayList<Eventdefinition>());
            }
        } catch (Exception e) {
            logger.error("Could not get event definitions.", e);
            return null;
        }
    }


    /**
     *
     * Insert an action associated with a specific event
     *
     * @param eventId
     * @param appId
     * @param badgeAward
     * @param pointsAward
     * @return
     */
	public boolean insertEventAction(int eventId, int appId, int badgeAward, int pointsAward) {
		logger.info("EventService.insertEventAction: " +eventId +","+appId +","+badgeAward+"," +pointsAward);
		Eventaction action = new Eventaction();
		EventactionPK key = new EventactionPK();

        try {
            key.setApplicationid(appId);
            key.setEventid(eventId);
            action.setId(key);
            action.setBadge(badgeService.getBadge(badgeAward));
            action.setPointsaward(pointsAward);
            eventactionRepository.save(action);
		    return true;
        } catch (Exception e) {
            logger.error("Could not insert event action.", e);
            return false;
        }
	}



    /**
     * Returns a list of all event actions
     *
     * @return
     */
    public List<Eventaction> getEventActions() {
        logger.info("EventService.getEventActions");
        try {
            return eventactionRepository.findAll();
        } catch (Exception e) {
            logger.error("Could not get event actions.", e);
            return null;
        }
    }



    /**
     * Returns a list of all event actions for a specific application
     *
     * @return
     */
    public List<Eventaction> getApplicationEventActions(int appId) {
        logger.info("EventService.getApplicationEventActions: " +appId);
        try {
            return eventactionRepository.findByApplication_Id(appId);
        } catch (Exception e) {
            logger.error("Could not get event actions.", e);
            return null;
        }
    }



    /**
     * Returns external events
     *
     * @return
     */
    public ResponseItem getExternalEvents(int page, int pagezize, String field, String dir, Externaleventhistory event) {
        logger.info("EventService.getExternalEvents");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Externaleventhistory> resultsPage = externaleventhistoryRepository.findAll(
                    new ExternalEventSpecification().createSpecFromSearchForm(event),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Externaleventhistory>());
            }
        } catch (Exception e) {
            logger.error("Could not load external events.", e);
            return null;
        }
    }

    /**
	 * Register an external event
	 * 
	 * @param event
	 * @return
	 */
	public Externaleventhistory registerEvent(Externaleventhistory event) {
		logger.info("EventService.registerEvent: " +event.getEventdefinition().getName() +"," +event.getApplication().getId()
                +"," +event.getUser() != null ? event.getUser().getUsername() : "N/A");

        try {
            /*
             * Load event definition
             */
            Eventdefinition eventdefinition = event.getEventdefinition().getId() > 0 ?
                    eventdefinitionRepository.findOne(event.getEventdefinition().getId()) :
                    eventdefinitionRepository.findByName(event.getEventdefinition().getName());
            event.setEventdefinition(eventdefinition);
            if (eventdefinition == null) {
                throw new Exception("Non-existing event type");
            }
            /*
             * Check if user exists
             */
            User user = userService.getUserProfile(event.getUser().getUsername(),event.getApplication().getId());
            if (user == null) {
                /*
                 * Allow null user only for REGISTER_USER event
                 */
                if (EventEnum.REGISTER_USER.name().equals(eventdefinition.getName())) {
                    if (event.getTextvalue1() == null && event.getUser() != null && event.getUser().getUsername() != null) {
                        event.setTextvalue1(event.getUser().getUsername());
                    }
                } else {
                    throw new Exception("Non-existing user");
                }
            }

            /*
             * For redeem reward events generate a unique identifier
             * and store it in textValue1 field
             */
            if (EventEnum.REDEEM_POINTS.name().equals(eventdefinition.getName())) {
                String identifier = IdentifierUtil.getRewardIdentifier(event.getApplication().getId(), user.getId());
                event.setTextvalue1(identifier);
            }

            event.setEventdate(new Date());
            event.setUser(user);
            /*
             * Save event to DB
             */
            event = externaleventhistoryRepository.save(event);
            /*
             * Add event to JMS queue
             */
            logger.info("Pushing event to queue...");
            eventPush.pushEventToQueue(event);
            return event;
        } catch (Exception e) {
            logger.error("Could not register event.", e);
        }
        return null;
	}


    /**
     * Returns internal events
     *
     * @return
     */
    public ResponseItem getInternalEvents(int page, int pagezize, String field, String dir, Eventhistory event) {
        logger.info("EventService.getInternalEvents");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Eventhistory> resultsPage = eventhistoryRepository.findAll(
                    new InternalEventSpecification().createSpecFromSearchForm(event),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Eventhistory>());
            }
        } catch (Exception e) {
            logger.error("Could not load external events.", e);
            return null;
        }
    }
}
