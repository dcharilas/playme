package com.playMe.core.service.impl;

import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.repository.*;
import com.playMe.core.db.repository.specification.RewardSpecification;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * Service that handles all actions related to Reward entity
 *
 * @author Dimitris Charilas
 */
@Service("rewardService")
public class RewardServiceImpl implements RewardService {
	
	private final Logger logger = LoggerFactory.getLogger(RewardServiceImpl.class);


    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private UserrewardsRepository userrewardsRepository;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserService userService;

    @Autowired
    private PointService pointService;

    @Autowired
    private UserMessageService userMessageService;


	/**
	 * Returns a user's rewards
	 * 
	 * @param userid
	 * @param appId
	 * @return
	 */
	public List<Userreward> getUserRewards(long userid, int appId) {
		logger.info("RewardService.getUserRewards: " +userid +"," +appId);
        try {
            return addMessages(userrewardsRepository.findByUser_IdAndApplicationId(userid, appId));
        } catch (Exception e) {
            logger.error("Could not get rewards.", e);
            return null;
        }
	}


    /**
     * Returns a user's reward
     *
     * @param identifier
     * @return
     */
    public Userreward getUserReward(String identifier) {
        logger.info("RewardService.getUserReward: " +identifier);
        try {
            return addMessages(userrewardsRepository.findByIdentifier(identifier));
        } catch (Exception e) {
            logger.error("Could not get rewards.", e);
            return null;
        }
    }
	
	
	/**
	 * Returns the rewards available for a specific application
	 * 
	 * @param appId
	 * @return
	 */
	public List<Reward> getApplicationRewards(int appId) {
		logger.info("RewardService.getApplicationRewards: "  +appId);
        try {
		    return addMessages2(rewardRepository.findByApplicationId(appId));
        } catch (Exception e) {
            logger.error("Could not get rewards.", e);
            return null;
        }
	}


    /**
     * Returns the active rewards for a specific application
     *
     * @param appId
     * @return
     */
    public List<Reward> getApplicationActiveRewards(int appId) {
        logger.info("RewardService.getApplicationActiveRewards: "  +appId);
        try {
            return addMessages2(rewardRepository.findApplicationActiveRewards(appId));
        } catch (Exception e) {
            logger.error("Could not get rewards.", e);
            return null;
        }
    }


    /**
     * Returns all rewards with the specified criteria
     *
     * @return
     */
    public ResponseItem getRewards(int page, int pagezize, String field, String dir, Reward reward) {
        logger.info("RewardService.getRewards");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Reward> resultsPage = rewardRepository.findAll(
                    new RewardSpecification().createSpecFromSearchForm(reward),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), addMessages2(resultsPage.getContent()));
            } else {
                return new ResponseItem(0, new ArrayList<Reward>());
            }
        } catch (Exception e) {
            logger.error("Could not load rewards.", e);
            return null;
        }
    }
	
	
	/**
	 * Returns a specific reward
	 * 
	 * @param rewardId
	 * @return
	 */
	public Reward getReward(int rewardId) {
		logger.info("RewardService.getReward: " +rewardId);
        try {
            Reward reward = rewardRepository.findOne(rewardId);
            return reward.getActive() ? addMessages2(reward) : null;
        } catch (Exception e) {
            logger.error("Could not get reward.", e);
            return null;
        }
	}
	

    /**
     * Updates reward information
     *
     * @param reward
     * @return
     */
    public Reward saveRewardInfo(Reward reward) {
        logger.info("RewardService.saveRewardInfo: " +reward.getId() +"," +reward.getName());
        try {
            return rewardRepository.save(reward);
        } catch (Exception e) {
            logger.error("Could not save reward info.", e);
            return null;
        }
    }


    /**
     * Deletes a reward
     *
     * @param rewardId
     */
    public boolean deleteReward(int rewardId) {
        logger.info("RewardService.deleteReward: " +rewardId);
        try {
            rewardRepository.delete(rewardId);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete reward.", e);
            return false;
        }
    }


    /**
     * Assigns a reward to a user, by removing points
     *
     * @param appId
     * @param userId
     * @param reward
     * @return
     */
    public User redeemReward(int appId, long userId, Reward reward, Eventhistory event, String identifier) {
        logger.info("RewardService.redeemReward: " +appId +"," +userId +"," +reward.getId());

        try {
            User user = userService.getUserProfile(userId,appId);
            /*
             * Check that user has enough points
             */
            if (user.getRedeemablepoints() < reward.getPointsreq()) {
                throw new Exception("Not enough points");
            }
            /*
             * Associate reward with user
             */
            Userreward userreward = new Userreward();
            Application application = new Application();
            application.setId(appId);
            userreward.setApplication(application);
            userreward.setReward(reward);
            userreward.setUser(user);
            userreward.setRedeemdate(new Date());
            userreward.setRedeemevent(event);
            userreward.setIdentifier(identifier);
            userrewardsRepository.save(userreward);
            /*
             * Remove the points
             */
            user = pointService.redeemPoints(reward.getPointsreq(), userId, appId, event);
            if (user == null) {
                throw new Exception("Could not remove points.");
            }

            /*
             * Create the proper user message
             */
            List<String> values = Arrays.asList(""+reward.getPointsreq(),reward.getName());
            userMessageService.createNewUserMessage(user.getId(), application.getId(), values, UserMessageCodeEnum.REDEEM_POINTS);

            return user;
        } catch (Exception e) {
            logger.error("Could not redeem reward", e);
            return null;
        }
    }


    /**
     * Add messages to rewards
     *
     * @param rewards
     * @return
     */
    private List<Userreward> addMessages(List<Userreward> rewards) {
        for (Userreward reward : rewards) {
            reward.getReward().setDescriptions(configurationService.getDescriptions(reward.getReward().getDescriptionid()));
        }
        return rewards;
    }

    /**
     * Add messages to reward
     *
     * @param reward
     * @return
     */
    private Reward addMessages2(Reward reward) {
        reward.setDescriptions(configurationService.getDescriptions(reward.getDescriptionid()));
        return reward;
    }

    /**
     * Add messages to rewards
     *
     * @param rewards
     * @return
     */
    private List<Reward> addMessages2(List<Reward> rewards) {
        for (Reward reward : rewards) {
            reward.setDescriptions(configurationService.getDescriptions(reward.getDescriptionid()));
        }
        return rewards;
    }

    /**
     * Add messages to a reward
     *
     * @param reward
     * @return
     */
    private Userreward addMessages(Userreward reward) {
        reward.getReward().setDescriptions(configurationService.getDescriptions(reward.getReward().getDescriptionid()));
        return reward;
    }
}
