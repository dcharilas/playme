package com.playMe.core.service;

import java.util.List;


import com.playMe.core.db.entity.Applicationlevel;
import com.playMe.core.db.entity.Level;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.Userlevelprogression;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that handles all actions related to Level entity
 *
 * @author Dimitris Charilas
 */
public interface LevelService {

    @Transactional
	Level getUserLevel(long userId, int appId);

    @Transactional
	List<Applicationlevel> getApplicationLevels(int appId);

    @Transactional
    Applicationlevel toggleLevel(int appId, int levelid, boolean active);

    @Transactional
    ResponseItem getLevels(int page, int pagezize, String field, String dir, Applicationlevel level);

    @Transactional
	User increaseUserLevel(long userId, int appId);

    @Transactional
    ResponseItem getUserLevelProgression(long userId, int appId);

    @Transactional
    Applicationlevel saveLevelInfo(Applicationlevel level);

    @Transactional
    boolean deleteLevel(int levelId, int applId);

    @Transactional
    public User checkUserLevelIncrease(User user);

    @Transactional
    int getLevelSequence(int levelId, int applId);

}
