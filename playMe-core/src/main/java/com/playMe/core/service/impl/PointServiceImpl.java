package com.playMe.core.service.impl;

import com.playMe.core.constant.Constants;
import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.entity.Eventhistory;
import com.playMe.core.db.repository.UserRepository;
import com.playMe.core.db.repository.UserpointshistoryRepository;
import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.Userpointshistory;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.PointService;
import com.playMe.core.service.UserMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service that handles all actions related to points handling
 *
 * @author Dimitris Charilas
 */
@Service("pointService")
public class PointServiceImpl implements PointService {
	
	private final Logger logger = LoggerFactory.getLogger(PointServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserpointshistoryRepository userpointshistoryRepository;

    @Autowired
    private UserMessageService userMessageService;
	
	
	/**
	 * Assigns extra points to user
	 * 
	 * @param awardPoints
	 * @param userId
	 * @param appId
     * @param event
	 * @return
	 */
     public User awardUserWithPoints(int awardPoints, long userId, int appId, Eventhistory event) {
		logger.info("PointService.awardUserWithPoints :" +userId +","+appId +","+awardPoints);

        try {
            User user = userRepository.findByIdAndApplication_Id(userId,appId);
            if (user != null) {
               /*
                * Assign the points
                */
                user.setPoints(user.getPoints() + awardPoints);
                user.setRedeemablepoints(user.getRedeemablepoints() + awardPoints);
                user = userRepository.save(user);
                /*
                 * Update history of user points
                 */
                Userpointshistory hist = new Userpointshistory();
                Application application = new Application();
                application.setId(appId);
                hist.setApplication(application);
                hist.setUser(user);
                hist.setEventhistory(event);
                hist.setPoints(awardPoints);
                hist.setUpdatedate(new Date());
                userpointshistoryRepository.save(hist);

               /*
                * Create the proper user message
                */
                List<String> values = Arrays.asList(""+awardPoints);
                userMessageService.createNewUserMessage(user.getId(), application.getId(), values, UserMessageCodeEnum.AWARD_POINTS);
            } else {
                logger.warn("User was not found!");
            }
            return user;
        } catch (Exception e) {
            logger.error("Could not assign points.", e);
            return null;
        }
	}

	
	/**
	 * Returns a list of all changes in user's points
	 * 
	 * @param userId
	 * @param appId
	 * @return
	 */
	public ResponseItem getUserPointsHistory(long userId, int appId) {
		logger.info("PointService.getUserPointsHistory :" +userId +","+appId);
        try {
            /*
             * Sort by descending date. Fetch only 10 results.
             */
            Sort.Order pointsOrder = new Sort.Order(Sort.Direction.DESC, "updatedate");
            Sort sort = new Sort(pointsOrder);
            //TODO this returns only first 10 results
            Pageable paging = new PageRequest(0, Constants.defaultPageSize, sort);

		    List<Userpointshistory> results = userpointshistoryRepository.findByUser_IdAndApplication_Id(userId, appId, paging);
            if (results != null) {
                return new ResponseItem(results.size(), results);
            } else {
                return new ResponseItem(0, new ArrayList<Userpointshistory>());
            }
        } catch (Exception e) {
            logger.error("Could not load user points history.", e);
            return null;
        }
	}


    /**
     * Redeems points
     *
     * @param redeemPoints
     * @param userId
     * @param appId
     * @param event
     * @return
     */
    public User redeemPoints(int redeemPoints, long userId, int appId, Eventhistory event) {
        logger.info("PointService.redeemPoints :" +userId +","+appId +","+redeemPoints);

        try {
            User user = userRepository.findByIdAndApplication_Id(userId,appId);
            if (user != null) {
               /*
                * First check that enough points actually exist
                */
                if (user.getRedeemablepoints() < redeemPoints) {
                    throw new Exception("Not enough redeemable points");
                }
               /*
                * Remove the points
                */
                user.setRedeemablepoints(user.getRedeemablepoints() - redeemPoints);
                user = userRepository.save(user);
                /*
                 * Update history of user points
                 */
                Userpointshistory hist = new Userpointshistory();
                Application application = new Application();
                application.setId(appId);
                hist.setApplication(application);
                hist.setUser(user);
                hist.setEventhistory(event);
                hist.setPoints(-redeemPoints);
                hist.setUpdatedate(new Date());

                userpointshistoryRepository.save(hist);
            } else {
                logger.warn("User was not found!");
            }
            return user;
        } catch (Exception e) {
            logger.error("Could not redeem points.", e);
            return null;
        }
    }
	
}
