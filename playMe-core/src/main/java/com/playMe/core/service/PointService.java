package com.playMe.core.service;

import java.util.List;

import com.playMe.core.db.entity.Eventhistory;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.Userpointshistory;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that handles all actions related to points handling
 *
 * @author Dimitris Charilas
 */
public interface PointService {

    @Transactional
	User awardUserWithPoints(int awardPoints, long userId, int appId, Eventhistory event);

    @Transactional
    ResponseItem getUserPointsHistory(long userId, int appId);

    @Transactional
    User redeemPoints(int redeemPoints, long userId, int appId, Eventhistory event);
	
}
