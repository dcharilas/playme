package com.playMe.core.service;

import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service that handles all actions related to Badge entity
 *
 * @author Dimitris Charilas
 */
public interface RewardService {

    @Transactional
    List<Userreward> getUserRewards(long userid, int appId);

    @Transactional
    List<Reward> getApplicationRewards(int appId);

    @Transactional
    List<Reward> getApplicationActiveRewards(int appId);

    @Transactional
    ResponseItem getRewards(int page, int pagezize, String field, String dir, Reward reward);

    @Transactional
    Reward getReward(int rewardId);

    @Transactional
    Reward saveRewardInfo(Reward reward);

    @Transactional
    boolean deleteReward(int rewardId);

    @Transactional
    User redeemReward(int appId, long userId, Reward reward, Eventhistory event, String identifier);

    @Transactional
    Userreward getUserReward(String identifier);

}
