package com.playMe.core.service.impl;

import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.keys.ApplicationbadgePK;
import com.playMe.core.db.keys.UserbadgePK;
import com.playMe.core.db.repository.ApplicationbadgesRepository;
import com.playMe.core.db.repository.BadgeRepository;
import com.playMe.core.db.repository.UserbadgesRepository;
import com.playMe.core.db.repository.specification.ApplicationBadgeSpecification;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.BadgeService;
import com.playMe.core.service.ConfigurationService;
import com.playMe.core.service.UserMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * Service that handles all actions related to Badge entity
 *
 * @author Dimitris Charilas
 */
@Service("badgeService")
public class BadgeServiceImpl implements BadgeService {
	
	private final Logger logger = LoggerFactory.getLogger(BadgeServiceImpl.class);


    @Autowired
    private BadgeRepository badgeRepository;

    @Autowired
    private ApplicationbadgesRepository applicationbadgesRepository;

    @Autowired
    private UserbadgesRepository userbadgesRepository;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserMessageService userMessageService;


	/**
	 * Returns a user's badges
	 * 
	 * @param userid
	 * @param appId
	 * @return
	 */
	public List<Badge> getUserBadges(long userid, int appId) {
		logger.info("BadgeService.getUserBadges: " +userid +"," +appId);
        try {
            return addMessages2(userbadgesRepository.findUserBadges(userid, appId));
        } catch (Exception e) {
            logger.error("Could not get badges.", e);
            return null;
        }
	}
	
	
	/**
	 * Returns the badges available for a specific application
	 * 
	 * @param appId
	 * @return
	 */
	public List<Applicationbadge> getApplicationBadges(int appId, boolean onlyActive) {
		logger.info("BadgeService.getApplicationBadges: "  +appId);
        try {
		    return onlyActive ? applicationbadgesRepository.findApplicationBadges(appId) :
                    applicationbadgesRepository.findApplicationBadges(appId);
        } catch (Exception e) {
            logger.error("Could not get badges.", e);
            return null;
        }
	}




    /**
     * Returns the active badges for a specific application
     *
     * @param appId
     * @return
     */
    public List<Badge> getApplicationActiveBadges(int appId) {
        logger.info("BadgeService.getApplicationActiveBadges: "  +appId);
        try {
            return badgeRepository.findApplicationActiveBadges(appId);
        } catch (Exception e) {
            logger.error("Could not get badges.", e);
            return null;
        }
    }


    /**
     * Returns all badges with the specified criteria
     *
     * @return
     */
    public ResponseItem getBadges(int page, int pagezize, String field, String dir, Applicationbadge badge) {
        logger.info("BadgeService.getBadges");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Applicationbadge> resultsPage = applicationbadgesRepository.findAll(
                    new ApplicationBadgeSpecification().createSpecFromSearchForm(badge),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), addMessages(resultsPage.getContent()));
            } else {
                return new ResponseItem(0, new ArrayList<Applicationbadge>());
            }
        } catch (Exception e) {
            logger.error("Could not load badges.", e);
            return null;
        }
    }
	
	
	/**
	 * Returns a specific badge
	 * 
	 * @param badgeId
	 * @return
	 */
	public Badge getBadge(int badgeId) {
		logger.info("BadgeService.getBadge: " +badgeId);
        try {
		    return badgeRepository.findOne(badgeId);
        } catch (Exception e) {
            logger.error("Could not get badge.", e);
            return null;
        }
	}
	
	
	/**
	 * Assigns a badge to a user
	 * 
	 * @param userid
	 * @param appId
	 * @param badgeid
	 * @return
	 */
	public List<Badge> awardUserWithBadge (long userid, int appId, int badgeid) {
		logger.info("BadgeService.awardUserWithBadge: " +userid +"," +appId +"," +badgeid);
        try {
            /*
             * Load list of user badges
             */
            List<Badge> badges = getUserBadges(userid, appId);
            /*
             * Verify that the badge is active
             */
            Applicationbadge applicationbadge = applicationbadgesRepository.findByApplication_IdAndBadge_Id(appId,badgeid);
            if (applicationbadge == null || !applicationbadge.getActive()) {
                logger.info("Badge cannot be assigned.");
                return null;
            }
            if (awardUserWithBadge(userid, appId, badgeid, badges)) {
                // return list of all user badges
                return getUserBadges(userid,appId);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("Could not award badge.", e);
            return null;
        }
	}



    /**
     * Assigns a badge to a user
     *
     * @param userid
     * @param appId
     * @param badgeid
     * @return
     */
    private boolean awardUserWithBadge (long userid, int appId, int badgeid, List<Badge> badges) {
        logger.info("BadgeService.awardUserWithBadge: " +userid +"," +appId +"," +badgeid);
        try {
            Badge badge = getBadge(badgeid);
            if (badge == null) {
                return false;
            }
            /*
             * Verify that the user does not already have the badge
             */
            for (Badge existingBadge : badges) {
                if (existingBadge.getId() == badgeid) {
                    logger.info("User already has this badge!");
                    return true;
                }
            }

            Userbadge userbadge = new Userbadge();
            UserbadgePK key = new UserbadgePK();
            key.setBadgeid(badgeid);
            key.setUserid(userid);
            key.setApplicationid(appId);
            userbadge.setId(key);
            userbadge.setUnlockdate(new Date());
            userbadgesRepository.save(userbadge);

            /*
             * Create the proper user message
             */
            List<String> values = Arrays.asList(badge.getName());
            userMessageService.createNewUserMessage(userid, appId, values, UserMessageCodeEnum.AWARD_BADGE);

            return true;
        } catch (Exception e) {
            logger.error("Could not award badge.", e);
            return false;
        }
    }


    /**
     * This is called each time user profile is updated. Its role is to determine whether
     * user should be awarded with badge(s) because the profile completeness percentage
     * has reached a specific level
     *
     * @param user
     * @return
     */
    public boolean checkIfProfileCompletnessBadgeShouldBeAwarded(User user, int percentage) {
        /*
         * First check if the application has this type of badges
         */
        List<Applicationbadge> allBadges = getApplicationBadges(user.getApplication().getId(), true);
        List<Applicationbadge> badges = new ArrayList();
        for (Applicationbadge badge : allBadges) {
            if (badge.getAwardaftercompleteness() != null && badge.getAwardaftercompleteness() > 0) {
                badges.add(badge);
            }
        }
       /*
        * If yes, then for each one that has a valid percentage, call the function
        * to award it to the user
        */
        if (badges.size() > 0) {
           /*
            * Load list of user badges
            */
            List<Badge> userBadges = getUserBadges(user.getId(), user.getApplication().getId());
            for (Applicationbadge badge : badges) {
                if (badge.getAwardaftercompleteness() <= percentage) {
                    awardUserWithBadge(user.getId(), user.getApplication().getId(), badge.getBadge().getId(), userBadges);
                }
            }
        }

        return true;
    }


    /**
     * This is called by the related task in order to get the badges that
     * should be awarded for consecutive logins for a specific
     * application
     *
     * @param applId
     * @return
     */
    public List<Applicationbadge> getApplicationBadgesForConsequtiveLogins(int applId) {
        /*
         * First check if the application has this type of badges
         */
        List<Applicationbadge> allBadges = getApplicationBadges(applId, true);
        List<Applicationbadge> badges = new ArrayList();
        for (Applicationbadge badge : allBadges) {
            if (badge.getAwardafterlogins() != null && badge.getAwardafterlogins() > 0) {
                badges.add(badge);
            }
        }
        return badges;
    }



    /**
     * Activates or deactivates a badge for an application
     *
     * @param appId
     * @param badgeid
     * @param active
     * @return
     */
    public Applicationbadge toggleBadge(int appId, int badgeid, boolean active) {
        logger.info("BadgeService.activateBadge: " +appId +"," +badgeid +"," +active);
        try {
            applicationbadgesRepository.toggleApplicationBadge(appId,badgeid,active);
            return applicationbadgesRepository.findByApplication_IdAndBadge_Id(appId, badgeid);
        } catch (Exception e) {
            logger.error("Could not activate/deactivate badge.", e);
            return null;
        }
    }


    /**
     * Updates badge information
     *
     * @param badge
     * @return
     */
    public Applicationbadge	saveBadgeInfo(Applicationbadge badge) {
        logger.info("BadgeService.saveBadgeInfo: " +badge.getApplication().getId() +"," +badge.getBadge().getName());
        try {
           /*
            * Check if this is a new badge
            */
            if (badge.getBadge().getId() != 0) {
               /*
                * This is an existing badge, so construct the id again and save it
                */
                ApplicationbadgePK key = new ApplicationbadgePK();
                key.setBadgeid(badge.getBadge().getId());
                key.setApplicationid(badge.getApplication().getId());
                badge.setId(key);
                /*
                 * Check if the badge exists already but is assigned to another application.
                 * If this is the case, then delete the existing Applicationbadge record
                 */
                List<Applicationbadge> existingRecords = applicationbadgesRepository.findByBadge_Id(badge.getBadge().getId());
                applicationbadgesRepository.delete(existingRecords);
                /*
                 * Finally save the new record
                 */
                badgeRepository.save(badge.getBadge());
                applicationbadgesRepository.save(badge);
                return badge;
            }
            else {
               /*
                * Check if another badge with the same name exists already
                * for the same application
                */
                List<Badge> savedBadges = badgeRepository.findApplicationBadge(badge.getApplication().getId(), badge.getBadge().getName());
                if (savedBadges != null && savedBadges.size() > 0) {
                    throw new Exception("Another badge with the same name is registered to this application.");
                }

               /*
                * This is a new badge, so save it first
                */
                Badge newBadgeItem = badgeRepository.save(badge.getBadge());
                badge.setBadge(newBadgeItem);
               /*
                * Now set the application and the id
                */
                Application application = applicationService.getApplication(badge.getApplication().getId());
                badge.setApplication(application);
                ApplicationbadgePK key = new ApplicationbadgePK();
                key.setBadgeid(badge.getBadge().getId());
                key.setApplicationid(badge.getApplication().getId());
                badge.setId(key);
               /*
                * Register the badge to the selected application
                */
                return applicationbadgesRepository.save(badge);
            }
        } catch (Exception e) {
            logger.error("Could not save badge info.", e);
            return null;
        }
    }


    /**
     * Deletes a badge
     *
     * @param badgeId
     * @param applId
     */
    public boolean deleteBadge(int badgeId, int applId) {
        logger.info("BadgeService.deleteBadge: " +badgeId +"," +applId);
        try {
            Applicationbadge applicationbadge = applicationbadgesRepository.findByApplication_IdAndBadge_Id(applId,badgeId);
            applicationbadgesRepository.delete(applicationbadge);
            badgeRepository.delete(applicationbadge.getBadge());
            return true;
        } catch (Exception e) {
            logger.error("Could not delete badge.", e);
            return false;
        }
    }


    /**
     * Returns true if a badge is active for a specific application
     *
     * @param badgeId
     * @param applId
     * @return
     */
    public boolean checkIfBadgeIsActive(int badgeId, int applId) {
        Applicationbadge applicationbadge = applicationbadgesRepository.findByApplication_IdAndBadge_Id(applId,badgeId);
        return applicationbadge != null ? applicationbadge.getActive() : false;
    }


    /**
     * Add messages to badges
     *
     * @param badges
     * @return
     */
    private List<Applicationbadge> addMessages(List<Applicationbadge> badges) {
        for (Applicationbadge badge : badges) {
            badge.getBadge().setDescriptions(configurationService.getDescriptions(badge.getBadge().getDescriptionid()));
        }
        return badges;
    }


    /**
     * Add messages to badges
     *
     * @param badges
     * @return
     */
    private List<Badge> addMessages2(List<Badge> badges) {
        for (Badge badge : badges) {
            badge.setDescriptions(configurationService.getDescriptions(badge.getDescriptionid()));
        }
        return badges;
    }
}
