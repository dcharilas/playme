package com.playMe.core.service;

import java.util.List;

import com.playMe.core.db.entity.Applicationbadge;
import com.playMe.core.db.entity.Badge;
import com.playMe.core.db.entity.User;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service that handles all actions related to Badge entity
 *
 * @author Dimitris Charilas
 */
public interface BadgeService {

    @Transactional
	List<Badge> getUserBadges(long userid, int appId);

    @Transactional
	List<Applicationbadge> getApplicationBadges(int appId, boolean onlyActive);

    @Transactional
    ResponseItem getBadges(int page, int pagezize, String field, String dir, Applicationbadge badge);

    @Transactional
	Badge getBadge(int badgeId);

    @Transactional
	List<Badge> awardUserWithBadge (long userid, int appId, int badgeid);

    @Transactional
    Applicationbadge toggleBadge(int appId, int badgeid, boolean active);

    @Transactional
    Applicationbadge saveBadgeInfo(Applicationbadge badge);

    @Transactional
    boolean deleteBadge(int badgeId, int applId);

    @Transactional
    boolean checkIfBadgeIsActive(int badgeId, int applId);

    @Transactional
    List<Badge> getApplicationActiveBadges(int appId);

    @Transactional
    boolean checkIfProfileCompletnessBadgeShouldBeAwarded(User user, int percentage);

    @Transactional
    List<Applicationbadge> getApplicationBadgesForConsequtiveLogins(int applId);
}
