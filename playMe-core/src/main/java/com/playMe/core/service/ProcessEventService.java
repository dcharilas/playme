package com.playMe.core.service;

import com.playMe.core.db.entity.Eventhistory;
import com.playMe.core.dto.Event;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service that performs processing of external events and converts them
 * to internal ones
 *
 * @author Dimitris Charilas
 */
public interface ProcessEventService {

    @Transactional
    List<Eventhistory> processEvent(Event externalEvent);
	
}
