package com.playMe.core.service;

import java.util.List;

import com.playMe.core.db.entity.User;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that handles all actions related to leaderboards
 *
 * @author Dimitris Charilas
 */
public interface LeaderboardService {
	
    @Transactional
    ResponseItem getLeaderBoard(int applicationId, int page, int pagezize);
}
