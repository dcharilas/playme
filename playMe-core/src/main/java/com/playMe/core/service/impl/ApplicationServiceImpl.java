package com.playMe.core.service.impl;


import com.playMe.core.db.repository.ApplicationRepository;
import com.playMe.core.db.entity.Application;
import com.playMe.core.db.repository.specification.ApplicationSpecification;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Service that handles all actions related to Application entity
 * 
 * @author Dimitris Charilas
 *
 */
@Service("applicationService")
public class ApplicationServiceImpl implements ApplicationService {
	
	private final Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);


    @Autowired
    private ApplicationRepository applicationRepository;



    /**
     * Returns all applications
     *
     * @return
     */
    public ResponseItem getApplications(int page, int pagezize, String field, String dir, Application application) {
        logger.info("ApplicationService.getApplications");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Application> resultsPage = applicationRepository.findAll(
                    new ApplicationSpecification().createSpecFromSearchForm(application),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Application>());
            }
        } catch (Exception e) {
            logger.error("Could not load applications.", e);
            return null;
        }
    }


    /**
     * Returns all applications
     *
     * @return
     */
    public List<Application> getAllApplications() {
        logger.info("ApplicationService.getAllApplications");
        try {
             return applicationRepository.findAll();
        } catch (Exception e) {
            logger.error("Could not load applications.", e);
            return null;
        }
    }


	/**
     * Returns the application
     *
     * @param id
     * @return
     */
    public Application getApplication(int id) {
        logger.info("ApplicationService.getApplication: " +id);
        try {
            return applicationRepository.findOne(id);
        } catch (Exception e) {
            logger.error("Could not load application.", e);
            return null;
        }
    }

	
	/**
	 * Adds a new application
	 * 
	 * @param name
	 * @return
	 */
	public Application insertApplication(String name) {
		logger.info("ApplicationService.insertApplication: " +name);

        Application app = new Application();
        app.setName(name);

		try {
            /*
             * Check if application with the same name already exists
             */
            List<Application> applications = applicationRepository.findByName(name);
            if (applications != null && applications.size() > 0) {
                throw new Exception("Application already exists!");
            }
			return applicationRepository.save(app);
		} catch (Exception e) {
            logger.error("Could not insert application.", e);
			return null;
		}
	}
	
	
	/**
	 * Deletes an application
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteApplication(int id) {
		logger.info("ApplicationService.deleteApplication: " +id);
        try {
            applicationRepository.delete(id);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete application.", e);
            return false;
        }
	}

}
