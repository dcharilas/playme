package com.playMe.core.service.impl;

import com.playMe.core.db.entity.Application;
import com.playMe.core.db.repository.UserRepository;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.repository.specification.UserSpecification;
import com.playMe.core.model.LeaderboardItem;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.LeaderboardService;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Service that handles all requests related to User entity for leaderboards
 *
 * @author Dimitris Charilas
 */
@Service("leaderboardService")
public class LeaderboardServiceImpl implements LeaderboardService {
	
	private final Logger logger = LoggerFactory.getLogger(LeaderboardServiceImpl.class);
	
	@Autowired
    UserRepository userRepository;

    @Autowired
    private Mapper dozerBeanMapper;

    /**
     * Returns the leaderboard
     *
     * @param applicationId
     * @param page
     * @param pagezize
     * @return
     */
	public ResponseItem getLeaderBoard(int applicationId, int page, int pagezize) {
		logger.info("LeaderboardService.getLeaderBoard: " +applicationId);
        try {
            /*
             * Sort by descending points
             */
            Sort.Order pointsOrder = new Sort.Order(Sort.Direction.DESC, "points");
            Sort sort = new Sort(pointsOrder);
            //TODO currently only 1 page is returned
            Pageable paging = new PageRequest(0, pagezize, sort);
            /*
             * Get users of specific application
             */
            User user = new User();
            Application application = new Application();
            application.setId(applicationId);
            user.setApplication(application);

            Page<User> resultsPage = userRepository.findAll(
                    new UserSpecification().createSpecFromSearchForm(user),paging);

           /*
            * For security reasons do not return the whole user object,
            * but only the fields that will be displayed
            */
            List<LeaderboardItem> items = new LinkedList<>();
            for (User retrievedUser : resultsPage.getContent()) {
                items.add(dozerBeanMapper.map(retrievedUser, LeaderboardItem.class));
            }

            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), items);
            } else {
                return new ResponseItem(0, new ArrayList<LeaderboardItem>());
            }
        } catch (Exception e) {
            logger.error("Could not load leaderboard.", e);
            return null;
        }
	}
	

}
