package com.playMe.core.service.impl;

import com.playMe.core.constant.ProfileFieldEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.repository.ApplicationlevelsRepository;
import com.playMe.core.db.repository.UserRepository;
import com.playMe.core.db.repository.UserloginRepository;
import com.playMe.core.db.repository.UserprofileweightsRepository;
import com.playMe.core.db.repository.specification.UserSpecification;
import com.playMe.core.exception.DuplicateEntryException;
import com.playMe.core.helper.ProfileWeightHelper;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.BadgeService;
import com.playMe.core.service.UserService;
import com.playMe.core.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * Service that handles all actions related to User entity
 *
 * @author Dimitris Charilas
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private static List<Userprofileweights> profileWeights;

    private static int sumOfWeights = 0;
    private static int readBatchSize = 50;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserloginRepository userloginRepository;

    @Autowired
    private UserprofileweightsRepository userprofileweightsRepository;

    @Autowired
    private ApplicationlevelsRepository applicationlevelsRepository;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private BadgeService badgeService;


    /**
     * Sets the values of profileWeights and sumOfWeights
     */
    public void setProfileMappings() {
        /*
         * Load once the mappings needed to calculate the user profile percentage
         */
        profileWeights = userprofileweightsRepository.findAll();
        /*
         * Calculate the sum of all weights in order to normalize later
         */
        sumOfWeights = 0;
        for (Userprofileweights mapping : profileWeights) {
            sumOfWeights += mapping.getWeight();
        }
    }



    /**
     * Inserts a new user
     *
     * @param applicationId
     * @param username
     * @return
     */
    public User	insertNewUser(int applicationId, String username) {
        logger.info("UserService.insertNewUser: " +applicationId +" , "+username);
        try {
            /*
             * Check first if user already exists
             */
            User user = userRepository.findByUsernameAndApplication_Id(username, applicationId);
            if (user != null) {
                // user already exists
                throw new DuplicateEntryException();
            }

            user = new User();
            user.setUsername(username);
            Application app = applicationService.getApplication(applicationId);
            user.setApplication(app);
            user.setRegDate(new Date());
           /*
            * Set level to the 1st available one
            */
            Applicationlevel level = applicationlevelsRepository.findByApplication_IdAndSequence(user.getApplication().getId(), 1);
            if (level != null) {
                user.setLevel(level.getLevel());
            }
            user = userRepository.save(user);
            return user;
        } catch (Exception e) {
            logger.error("Could not insert user.", e);
            return null;
        }
    }

    /**
     * Updates user information
     *
     * @param user
     * @return
     */
    public User	saveUserInfo(User user) {
        logger.info("UserService.saveUserInfo: " +user.getUsername());

        try {
            /*
             * First check if user already exists
             */
            User savedUser = getUserProfile(user.getUsername(), user.getApplication().getId());
            if (savedUser != null) {
                /*
                 * Check if id exists. If not then this is a new user and we can't add
                 * another one with the same username!
                 */
                if (user.getId() > 0) {
                    /*
                     * Re-calculate age
                     */
                    setUserAge(user);
                    /*
                     * Check if badge should be awarded because of profile completeness
                     */
                    badgeService.checkIfProfileCompletnessBadgeShouldBeAwarded(user, calculateProfileCompleteness(user));
                    /*
                     * Update user
                     */
                    return userRepository.save(user);
                } else {
                    throw new Exception("Another user with the same username already exists.");
                }
            } else {
                /*
                 * This is a new user.
                 * Set level to the 1st available one
                 */
                Applicationlevel level = applicationlevelsRepository.findByApplication_IdAndSequence(user.getApplication().getId(), 1);
                if (level != null) {
                    user.setLevel(level.getLevel());
                }
                user.setRegDate(new Date());
                /*
                 * Calculate age
                 */
                setUserAge(user);
                /*
                 * Check if badge should be awarded because of profile completeness
                 */
                badgeService.checkIfProfileCompletnessBadgeShouldBeAwarded(user, calculateProfileCompleteness(user));

                return userRepository.save(user);
            }
        } catch (Exception e) {
            logger.error("Could not save user info.", e);
            return null;
        }
    }


    /**
     * This is used to update an existing user
     *
     * @param user
     * @return
     */
    public User	updateSpecificUserInfo(User user) {
        logger.info("UserService.updateSpecificUserInfo: " +user.getUsername());

        try {
            /*
             * First verify that user already exists
             */
            User savedUser = getUserProfile(user.getUsername(), user.getApplication().getId());
            if (savedUser != null) {
                /*
                 * Update only specific fields of user object
                 */
                savedUser.setPhone(user.getPhone());
                savedUser.setAddress(user.getAddress());
                savedUser.setFirstname(user.getFirstname());
                savedUser.setLastname(user.getLastname());
                savedUser.setAvatar(user.getAvatar());
                savedUser.setGender(user.getGender());
                savedUser.setEmail(user.getEmail());
                savedUser.setBirthdate(user.getBirthdate());
                savedUser.setPostcode(user.getPostcode());
                /*
                 * Re-calculate age
                 */
                setUserAge(savedUser);
                /*
                 * Check if badge should be awarded because of profile completeness
                 */
                badgeService.checkIfProfileCompletnessBadgeShouldBeAwarded(user, calculateProfileCompleteness(user));
                /*
                 * Update user
                 */
                return userRepository.save(savedUser);

            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not save user info.", e);
            return null;
        }
    }


    /**
     * Returns the % user profile percentage
     *
     * @param userId
     * @param applicationId
     * @return
     */
    public int getProfileCompletenessPercentage(long userId, int applicationId) {
        logger.info("UserService.getProfileCompletenessPercentage: " +userId +" , "+applicationId);
        try {
            User user = userRepository.findByIdAndApplication_Id(userId, applicationId);
            if (user != null) {
                return calculateProfileCompleteness(user);
            }
        } catch (Exception e) {
            logger.error("Could not calculate profile completeness percentage.", e);
            return 0;
        }
        return 0;
    }


    /**
     * Returns the % user profile percentage
     *
     * @param user
     * @return
     */
    public int getProfileCompletenessPercentage(User user) {
        logger.info("UserService.getProfileCompletenessPercentage :" +user.getId() +","+user.getApplication());
        try {
            if (user != null) {
                return calculateProfileCompleteness(user);
            }
        } catch (Exception e) {
            logger.error("Could not calculate profile completeness percentage.", e);
            return 0;
        }
        return 0;
    }


    /**
     * Loads user information
     *
     * @param userId
     * @param applicationId
     * @return
     */
    public User getUserProfile(long userId, int applicationId) {
        logger.info("UserService.getUserProfile: " +userId +" , "+applicationId);
        try {
            User user = userRepository.findByIdAndApplication_Id(userId,applicationId);
            if (user != null) {
                user.setCompleteness(calculateProfileCompleteness(user));
            }
            return user;
        } catch (Exception e) {
            logger.error("Could not load user profile.", e);
            return null;
        }
    }


    /**
     * Loads user information
     *
     * @param applicationId
     * @return
     */
    public List<User> getApplicationUsers(int applicationId) {
        logger.info("UserService.getUserProfile: " +applicationId);
        try {
            //dummy values
            //TODO
            int page = 0;
            int numberOfUsers = 1000;
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, numberOfUsers);
            return userRepository.findByApplication_Id(applicationId, paging);
        } catch (Exception e) {
            logger.error("Could not load user profile.", e);
            return null;
        }
    }


    /**
     * Returns all users
     *
     * @return
     */
    public ResponseItem getUsers(int page, int pagezize, String field, String dir, User user) {
        logger.info("UserService.getUsers");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<User> resultsPage = userRepository.findAll(
                    new UserSpecification().createSpecFromSearchForm(user),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), addCompletenessPerc(resultsPage.getContent()));
            } else {
                return new ResponseItem(0, new ArrayList<User>());
            }
        } catch (Exception e) {
            logger.error("Could not load users.", e);
            return null;
        }
    }


    /**
     * Loads user information
     *
     * @param username
     * @param applicationId
     * @return
     */
    public User getUserProfile(String username, int applicationId) {
        logger.info("UserService.getUserProfile: " +username +" , "+applicationId);
        try {
            User user = userRepository.findByUsernameAndApplication_Id(username, applicationId);
            if (user != null) {
                user.setCompleteness(calculateProfileCompleteness(user));
            }
            return user;
        } catch (Exception e) {
            logger.error("Could not load user profile.", e);
            return null;
        }
    }


    /**
     * Loads user profile. If it exists, a record is created in UserLogin table
     *
     * @param username
     * @param applicationId
     * @return
     */
    public User loginUser(String username, int applicationId) {
        logger.info("UserService.loginUser: " +username +" , "+applicationId);
        try {
            User user = getUserProfile(username, applicationId);
            if (user != null) {
                Userlogin loginInfo = new Userlogin();
                loginInfo.setUser(user);
                loginInfo.setApplication(user.getApplication());
                loginInfo.setDate(new Date());
                userloginRepository.save(loginInfo);
            }
            return user;
        } catch (Exception e) {
            logger.error("Could not load user profile.", e);
            return null;
        }
    }


    /**
     *  This function estimates the % completeness percentage of a user profile, based on
     *  the mappings stored in UserProfileWeights
     *
     * @param user
     * @return
     */
    private int calculateProfileCompleteness(User user) {
        int profilePoints = 0;
        /*
         * If profile mappings are not loaded yet, load them
         */
        if (profileWeights == null || profileWeights.size() == 0) {
            setProfileMappings();
        }

        /*
         * Check which fields are filled, find their weight and then normalize
         */
        if (!StringUtils.isEmpty(user.getAddress())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.ADDRESS.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getAvatar())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.AVATAR.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getBirthdate())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.BIRTHDATE.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getEmail())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.EMAIL.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getFirstname())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.FIRSTNAME.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getGender())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.GENDER.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getLastname())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.LASTNAME.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getPhone())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.PHONE.value(),profileWeights);
        }
        if (!StringUtils.isEmpty(user.getPostcode())) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.POSTCODE.value(),profileWeights);
        }
        /*
         * In case of social profile, if at least one is provide then it counts as filled field
         */
        if (user.getSocialintegrations() != null && !user.getSocialintegrations().isEmpty()) {
            profilePoints += ProfileWeightHelper.getFieldWeight(ProfileFieldEnum.SOCIALPROFILE.value(),profileWeights);
        }
        /*
         * Now normalize to 100
         */
        logger.debug("profilePoints " +profilePoints);
        logger.debug("(100*profilePoints)/sumOfWeights) " +(100*profilePoints)/sumOfWeights);
        logger.debug("Math.round((100*profilePoints)/sumOfWeights) " +Math.round((100*profilePoints)/sumOfWeights));
        return Math.round((100*profilePoints)/sumOfWeights);

    }


    /**
     * Add completeness percentage to user
     *
     * @param users
     * @return
     */
    private List<User> addCompletenessPerc(List<User> users) {
        for (User user : users) {
            user.setCompleteness(calculateProfileCompleteness(user));
        }
        return users;
    }


    /**
     * This method is used by a task. Its role is to
     * update the age of all current users according to their birth dates
     */
    public int updateUserAge() {
        /*
         * Read users in batches
         */
        int totalUsers = 0;
        Sort.Order pointsOrder = new Sort.Order(Sort.Direction.ASC, "id");
        Sort sort = new Sort(pointsOrder);
        /*
         * First iteration in order to get total number of pages
         */
        Pageable paging = new PageRequest(0, readBatchSize, sort);
        Page<User> resultsPage = userRepository.findAll(paging);
        logger.info("Reading page " +paging.getPageNumber() +" with size " + resultsPage.getContent().size());
        recalculateAgeForUserBatch(resultsPage.getContent());
        totalUsers += resultsPage.getContent().size();
        /*
         * Continue until all users are updated
         */
        while (paging.getPageNumber() < resultsPage.getTotalPages()) {
            logger.info("Reading page " +paging.getPageNumber() +" with size " + resultsPage.getContent().size());
            paging = new PageRequest(paging.getPageNumber()+1, readBatchSize, sort);
            resultsPage = userRepository.findAll(paging);
            recalculateAgeForUserBatch(resultsPage.getContent());
            totalUsers += resultsPage.getContent().size();
        }
        return totalUsers;
    }



    /**
     * This method is used by a task. Its role is to
     * browse UserLogin table and check whether all users
     * have performed consecutive logins
     */
    public List<Long> checkConsecutiveLogins(int applId, int consecutiveDays) {
        List<Long> users = new LinkedList<>();
        /*
         * Read users in batches
         */
        Sort.Order pointsOrder = new Sort.Order(Sort.Direction.ASC, "id");
        Sort sort = new Sort(pointsOrder);
        /*
         * First iteration in order to get total number of pages
         */
        Pageable paging = new PageRequest(0, readBatchSize, sort);
        Page<User> resultsPage = userRepository.findAll(paging);
        logger.info("Reading page " +paging.getPageNumber() +" with size " + resultsPage.getContent().size());
        checkUserConsecutiveLogin(resultsPage.getContent(), users, consecutiveDays);
        /*
         * Continue until all users are updated
         */
        while (paging.getPageNumber() < resultsPage.getTotalPages()) {
            logger.info("Reading page " +paging.getPageNumber() +" with size " + resultsPage.getContent().size());
            paging = new PageRequest(paging.getPageNumber()+1, readBatchSize, sort);
            resultsPage = userRepository.findAll(paging);
            checkUserConsecutiveLogin(resultsPage.getContent(), users, consecutiveDays);
        }
        return users;
    }


    /**
     * Updates the age in a given set of users and saves in DB
     *
     * @param users
     */
    private void recalculateAgeForUserBatch(List<User> users) {
        if (users != null && users.size() > 0) {
            for (User user : users) {
                setUserAge(user);
            }
            userRepository.save(users);
        }
    }


    /**
     * Updates the age in a given set of users and saves in DB
     *
     * @param users
     */
    private void checkUserConsecutiveLogin(List<User> users, List<Long> verifiedUsers, int consecutiveDays) {
        if (users != null && users.size() > 0) {
            for (User user : users) {
                int check = userloginRepository.checkConsecutiveLogins(user.getApplication().getId(), user.getId(), consecutiveDays);
                if (check > 0) {
                    verifiedUsers.add(user.getId());
                }
            }
        }
    }


    /**
     * Sets the age in user object
     *
     * @param user
     */
    private void setUserAge(User user) {
        if (user.getBirthdate() != null) {
            user.setAge(DateUtil.getAge(user.getBirthdate()));
        }
    }
}
