package com.playMe.core.service;

import java.util.List;

import com.playMe.core.db.entity.*;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service that handles all actions related to entities associated with events
 *
 * @author Dimitris Charilas
 */
public interface EventService {

    @Transactional
	boolean insertEventDefinition(String name);

    @Transactional
    ResponseItem getEventDefinitions();

    @Transactional
    boolean insertEventAction(int eventId, int appId, int badgeAward, int pointsAward);

    @Transactional
    List<Eventaction> getEventActions();

    @Transactional
    ResponseItem getExternalEvents(int page, int pagezize, String field, String dir, Externaleventhistory event);

    @Transactional
    List<Eventaction> getApplicationEventActions(int appId);

    @Transactional
    Externaleventhistory registerEvent(Externaleventhistory event);

    @Transactional
    ResponseItem getInternalEvents(int page, int pagezize, String field, String dir, Eventhistory event);
	
}
