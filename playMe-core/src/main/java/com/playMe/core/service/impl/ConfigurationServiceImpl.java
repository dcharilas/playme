package com.playMe.core.service.impl;

import com.playMe.core.db.entity.*;
import com.playMe.core.db.keys.EventactionPK;
import com.playMe.core.db.repository.*;
import com.playMe.core.db.repository.specification.*;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ConfigurationService;
import com.playMe.core.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that handles all actions related to configuration entities
 *
 * @author Dimitris Charilas
 */
@Service("configurationService")
public class ConfigurationServiceImpl implements ConfigurationService {


    private final Logger logger = LoggerFactory.getLogger(ConfigurationServiceImpl.class);


    @Autowired
    private DisplaytextRepository displaytextRepository;

    @Autowired
    private EventdefinitionRepository eventdefinitionRepository;

    @Autowired
    private EventactionRepository eventactionRepository;

    @Autowired
    private UserprofileweightsRepository userprofileweightsRepository;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private UsermessagecodesRepository usermessagecodesRepository;

    @Autowired
    private UserService userService;



    /**
     * Returns a message in all locales
     *
     * @param messageId
     * @return
     */
	public List<Displaytext> getDescriptions(int messageId) {
        logger.debug("ConfigurationService.getDescriptions: " +messageId);
        try {
            return displaytextRepository.findById_Messageid(messageId);
        } catch (Exception e) {
            logger.error("Could not load messages.", e);
            return null;
        }
    }


    /**
     * Returns a message in specific locale
     *
     * @param messageId
     * @param locale
     * @return
     */
    public Displaytext getDescription(int messageId, String locale) {
        logger.info("ConfigurationService.getDescription: " +messageId +"," +locale);
        try {
            return displaytextRepository.findById_MessageidAndId_Locale(messageId, locale);
        } catch (Exception e) {
            logger.error("Could not load message.", e);
            return null;
        }
    }


    /**
     * Returns all event definitions
     *
     * @param page
     * @param pagezize
     * @param field
     * @param dir
     * @return
     */
    public ResponseItem getEventDefinitions(int page, int pagezize, String field, String dir, Eventdefinition definition) {
        logger.info("ConfigurationService.getEventDefinitions");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Eventdefinition> resultsPage = eventdefinitionRepository.findAll(
                    new EventDefinitionSpecification().createSpecFromSearchForm(definition), paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Eventdefinition>());
            }
        } catch (Exception e) {
            logger.error("Could not load event definitions.", e);
            return null;
        }
    }


    /**
     * Returns all event actions with the specified criteria
     *
     * @return
     */
    public ResponseItem getEventActions(int page, int pagezize, String field, String dir, Eventaction action) {
        logger.info("ConfigurationService.getEventActions");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Eventaction> resultsPage = eventactionRepository.findAll(
                    new EventActionSpecification().createSpecFromSearchForm(action),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Eventaction>());
            }
        } catch (Exception e) {
            logger.error("Could not load event actions.", e);
            return null;
        }
    }


    /**
     * Returns all display texts with the specified criteria
     *
     * @return
     */
    public ResponseItem getDisplayTexts(int page, int pagezize, String field, String dir, Displaytext text) {
        logger.info("ConfigurationService.getDisplayTexts");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Displaytext> resultsPage = displaytextRepository.findAll(
                    new DisplayTextSpecification().createSpecFromSearchForm(text),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Displaytext>());
            }
        } catch (Exception e) {
            logger.error("Could not load display texts.", e);
            return null;
        }
    }


    /**
     * Returns all user messages with the specified criteria
     *
     * @return
     */
    public ResponseItem getUserMessages(int page, int pagezize, String field, String dir, Usermessagecodes text) {
        logger.info("ConfigurationService.getUserMessages");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Usermessagecodes> resultsPage = usermessagecodesRepository.findAll(
                    new UserMessageCodeSpecification().createSpecFromSearchForm(text),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Usermessagecodes>());
            }
        } catch (Exception e) {
            logger.error("Could not load display texts.", e);
            return null;
        }
    }


    /**
     * Returns all settings with the specified criteria
     *
     * @return
     */
    public ResponseItem getConfiguration(int page, int pagezize, String field, String dir, Configuration configuration) {
        logger.info("ConfigurationService.getConfiguration");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Configuration> resultsPage = configurationRepository.findAll(
                    new ConfigurationSpecification().createSpecFromSearchForm(configuration),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Configuration>());
            }
        } catch (Exception e) {
            logger.error("Could not load settings.", e);
            return null;
        }
    }


    /**
     * Returns all profile weights with the specified criteria
     *
     * @return
     */
    public ResponseItem getProfileWeights(int page, int pagezize, String field, String dir, Userprofileweights weight) {
        logger.info("ConfigurationService.getProfileWeights");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Userprofileweights> resultsPage = userprofileweightsRepository.findAll(paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), resultsPage.getContent());
            } else {
                return new ResponseItem(0, new ArrayList<Userprofileweights>());
            }
        } catch (Exception e) {
            logger.error("Could not load profile weights.", e);
            return null;
        }
    }


    /**
     * Adds a new event definition
     *
     * @param name
     * @return
     */
    public Eventdefinition insertEventDefinition(String name) {
        logger.info("ConfigurationService.insertEventDefinition: " +name);

        Eventdefinition def = new Eventdefinition();
        def.setName(name);

        try {
            return eventdefinitionRepository.save(def);
        } catch (Exception e) {
            logger.error("Could not insert event definition.", e);
            return null;
        }
    }


    /**
     * Adds a new setting
     *
     * @param configuration
     * @return
     */
    public Configuration insertConfiguration(Configuration configuration) {
        logger.info("ConfigurationService.insertConfiguration");
        try {
            return configurationRepository.save(configuration);
        } catch (Exception e) {
            logger.error("Could not insert setting.", e);
            return null;
        }
    }


    /**
     * Adds a new profile weight
     *
     * @param weight
     * @return
     */
    public Userprofileweights insertProfileWeight(Userprofileweights weight) {
        logger.info("ConfigurationService.insertProfileWeight: " + weight.getFieldname());
        try {
            userprofileweightsRepository.save(weight);
            /*
             * After saving we need to reload the percentages!
             */
            userService.setProfileMappings();
            return weight;
        } catch (Exception e) {
            logger.error("Could not insert profile weight.", e);
            return null;
        }
    }


    /**
     * Adds a new event action
     *
     * @param action
     * @return
     */
    public Eventaction insertEventAction(Eventaction action) {
        logger.info("ConfigurationService.insertEventAction");

        try {
            /*
             * Construct the id first
             */
            EventactionPK key = new EventactionPK();
            key.setApplicationid(action.getApplication().getId());
            key.setEventid(action.getEventdefinition().getId());
            action.setId(key);
            return eventactionRepository.save(action);
        } catch (Exception e) {
            logger.error("Could not insert event action.", e);
            return null;
        }
    }


    /**
     * Adds a new message
     *
     * @param text
     * @return
     */
    public Displaytext insertMessage(Displaytext text) {
        logger.info("ConfigurationService.insertMessage: " + text.getValue()); 
        try {
            if (!(text.getId().getMessageid() > 0)) {
               /*
                * Get the next available id
                */
                int messageId = displaytextRepository.getNextMessageId();
                text.getId().setMessageid(messageId);
            }
            return displaytextRepository.save(text);
        } catch (Exception e) {
            logger.error("Could not insert message.", e);
            return null;
        }
    }


    /**
     * Adds multiple messages with the same id
     *
     * @param texts
     * @return
     */
    public List<Displaytext> insertMessages(List<Displaytext> texts) {
        logger.info("ConfigurationService.insertMessages: " + texts.size());
        try {
            if (!(texts.get(0).getId().getMessageid() > 0)) {
               /*
                * Get the next available id
                */
                int messageId = displaytextRepository.getNextMessageId();
                for (Displaytext text : texts) {
                    text.getId().setMessageid(messageId);
                }
            }
            return displaytextRepository.save(texts);
        } catch (Exception e) {
            logger.error("Could not insert messages.", e);
            return null;
        }
    }


    /**
     * Adds a new user message
     *
     * @param text
     * @return
     */
    public Usermessagecodes insertUserMessage(Usermessagecodes text) {
        logger.info("ConfigurationService.insertMessage: " + text.getMessage());
        try {
            return usermessagecodesRepository.save(text);
        } catch (Exception e) {
            logger.error("Could not insert user message.", e);
            return null;
        }
    }


    /**
     * Deletes an event definition
     *
     * @param id
     * @return
     */
    public boolean deleteEventDefinition(int id) {
        logger.info("ConfigurationService.deleteEventDefinition: " +id);
        try {
            eventdefinitionRepository.delete(id);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete event definition.", e);
            return false;
        }
    }


    /**
     * Deletes an event action
     *
     * @param applId
     * @param eventId
     * @return
     */
    public boolean deleteEventAction(int applId, int eventId) {
        logger.info("ConfigurationService.deleteEventAction: " +applId +"," +eventId);
        try {
            Eventaction action = eventactionRepository.findByEventdefinition_IdAndApplication_Id(eventId,applId);
            eventactionRepository.delete(action);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete event action.", e);
            return false;
        }
    }


    /**
     * Deletes a display text
     *
     * @param messageId
     * @param locale
     * @return
     */
    public boolean deleteDisplayText(int messageId, String locale) {
        logger.info("ConfigurationService.deleteDisplayText: " +messageId +"," +locale);
        try {
            Displaytext text = displaytextRepository.findById_MessageidAndId_Locale(messageId,locale);
            displaytextRepository.delete(text);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete display text.", e);
            return false;
        }
    }


    /**
     * Deletes a setting
     *
     * @param id
     * @return
     */
    public boolean deleteConfiguration(int id) {
        logger.info("ConfigurationService.deleteConfiguration: " +id);
        try {
            configurationRepository.delete(id);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete configuration", e);
            return false;
        }
    }


    /**
     * Deletes a profile weight
     *
     * @param id
     * @return
     */
    public boolean deleteProfileWeight(String id) {
        logger.info("ConfigurationService.deleteProfileWeight: " +id);
        try {
            userprofileweightsRepository.delete(id);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete profile weight.", e);
            return false;
        }
    }


    /**
     * Deletes a user message
     *
     * @param messageCode
     * @param applId
     * @param locale
     * @return
     */
    public boolean deleteUserMessage(String messageCode, int applId, String locale) {
        logger.info("ConfigurationService.deleteUserMessage: " +messageCode +"," +applId +"," +locale);
        try {
            Usermessagecodes text = usermessagecodesRepository.findById_ApplicationidAndId_MsgcodeAndId_Locale(applId,messageCode,locale);
            usermessagecodesRepository.delete(text);
            return true;
        } catch (Exception e) {
            logger.error("Could not delete user message.", e);
            return false;
        }
    }



    /**
     * Returns a user message in all locales for a specific code and application
     *
     * @param messageCode
     * @param applId
     * @return
     */
    public List<Usermessagecodes> getUserMessagesPerCode(String messageCode, int applId) {
        logger.info("ConfigurationService.getUserMessagesPerCode: " +messageCode +"," +applId);
        try {
            return usermessagecodesRepository.findById_ApplicationidAndId_Msgcode(applId,messageCode);
        } catch (Exception e) {
            logger.error("Could not load user messages.", e);
            return null;
        }
    }


}
