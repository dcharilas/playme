package com.playMe.core.service.impl;

import com.playMe.core.constant.StatisticsEnum;
import com.playMe.core.db.repository.*;
import com.playMe.core.model.StatisticsDateItem;
import com.playMe.core.model.StatisticsItem;
import com.playMe.core.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Service that handles all actions related to generation and organisation of statistics
 *
 * @author Dimitris Charilas
 */
@Service("statisticsService")
public class StatisticsServiceImpl implements StatisticsService {
	
	private final Logger logger = LoggerFactory.getLogger(StatisticsServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserpointshistoryRepository userpointshistoryRepository;

    @Autowired
    private ExternaleventhistoryRepository externaleventhistoryRepository;

    @Autowired
    private EventhistoryRepository eventhistoryRepository;

    @Autowired
    private UserrewardsRepository userrewardsRepository;


    /**
     * Wrapper for getStatistics without application id
     *
     * @param stat
     * @return
     */
    public List<StatisticsItem> getStatistics(StatisticsEnum stat) {
        return getStatistics(stat,0);
    }

    /**
     * Return statistics
     *
     * @return
     */
    public List<StatisticsItem> getStatistics(StatisticsEnum stat, int applId) {
        logger.info("StatisticsService.getStatistics: " +stat.value());

        List<StatisticsItem> items = new LinkedList<StatisticsItem>();
        StatisticsItem item;
        List<Object[]> dbItems = new LinkedList();

        try {
            /*
             * Call the proper statistics method
             */
            if (StatisticsEnum.USER_GENDER.equals(stat)) {
                if (applId > 0) {
                    dbItems = userRepository.countUsersPerGender(applId);
                } else {
                    dbItems = userRepository.countUsersPerGender();
                }
            } else if (StatisticsEnum.USER_AGE.equals(stat)) {
                if (applId > 0) {
                    dbItems = userRepository.countUsersPerAgeGroup(applId);
                } else {
                    dbItems = userRepository.countUsersPerAgeGroup();
                }
            } else if (StatisticsEnum.APPLICATION_USERS.equals(stat)) {
                dbItems = userRepository.countUsersPerApplication();
            } else if (StatisticsEnum.USERS_LEVEL_SEQUENCE.equals(stat)) {
                dbItems = userRepository.countUsersPerLevelSequence();
            } else if (StatisticsEnum.REDEEMED_REWARDS.equals(stat)) {
                dbItems = userrewardsRepository.countRedeemedRewardsPerApplication();
            } else if (StatisticsEnum.REDEEMED_REWARD_PER_TYPE.equals(stat)) {
                if (applId > 0) {
                    dbItems = userrewardsRepository.countRedeemedRewardsPerType(applId);
                }
            }

            for (Object[] dbItem : dbItems) {
                item = new StatisticsItem();
                item.setCode(String.valueOf(dbItem[0]));
                item.setValue(Integer.valueOf(dbItem[1].toString()));
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            logger.error("Could not get statistics.", e);
        }
        return null;
    }


    /**
     * Wrapper for getDateStatistics without application id and user id
     *
     * @param stat
     * @param days
     * @return
     */
    public List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days) {
        return getDateStatistics(stat,days,0,0);
    }

    /**
     * Wrapper for getDateStatistics without user id
     *
     * @param stat
     * @param days
     * @return
     */
    public List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId) {
        return getDateStatistics(stat,days,applId,0);
    }

    /**
     * Return statistics including date
     *
     * @return
     */
    public List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId, long userId) {
        logger.info("StatisticsService.getDateStatistics: " +stat.value());

        List<StatisticsDateItem> items = new LinkedList<StatisticsDateItem>();
        StatisticsDateItem item;
        List<Object[]> dbItems = new LinkedList();
        /*
         * Subtract days from today
         */
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -days);

        try {
            /*
             * Call the proper statistics method
             */
            if (StatisticsEnum.EXTERNAL_EVENTS_PER_DAY.equals(stat)) {
                dbItems = externaleventhistoryRepository.getExternalEventsPerDay(calendar.getTime(),Calendar.getInstance().getTime());
            } else if (StatisticsEnum.INTERNAL_EVENTS_PER_DAY.equals(stat)) {
                dbItems = eventhistoryRepository.getEventsPerDay(days);
            } else if (StatisticsEnum.APPLICATION_EXTERNAL_EVENTS_PER_DAY.equals(stat)) {
                dbItems = externaleventhistoryRepository.getApplicationEventsPerDay(calendar.getTime(),Calendar.getInstance().getTime(),applId);
            } else if (StatisticsEnum.APPLICATION_INTERNAL_EVENTS_PER_DAY.equals(stat)) {
                dbItems = eventhistoryRepository.getApplicationEventsPerDay(days,applId);
            } else if (StatisticsEnum.APPLICATION_USERS_PER_DAY.equals(stat)) {
                dbItems = userRepository.countUsersPerDayForAplication(days,applId);
            } else if (StatisticsEnum.TOTAL_POINTS_PER_DAY.equals(stat)) {
                dbItems = userpointshistoryRepository.getPointsHistory(applId,userId);
            } else if (StatisticsEnum.REDEEMABLE_POINTS_PER_DAY.equals(stat)) {
                dbItems = userpointshistoryRepository.getRedeemablePointsHistory(applId,userId);
            } else if (StatisticsEnum.REDEEMED_POINTS_PER_DAY.equals(stat)) {
                dbItems = userrewardsRepository.countRedeemedPointsPerDay(days,applId);
            }

            for (Object[] dbItem : dbItems) {
                item = new StatisticsDateItem();
                item.setDate(Date.valueOf(dbItem[0].toString()));
                item.setCode(String.valueOf(dbItem[1]));
                /*
                 * Do not convert to int directly, otherwise we might get exception
                 * (Integer.valueOf("4.0") fails). So convert to double first and then
                 * get the int value
                 */
                item.setValue(Double.valueOf(dbItem[2].toString()).intValue());
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            logger.error("Could not get statistics.", e);
        }
        return null;
    }
}
