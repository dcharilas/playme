package com.playMe.core.service;

import com.playMe.core.db.entity.User;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service that handles all actions related to User entity
 *
 * @author Dimitris Charilas
 */
public interface UserService  {

    @Transactional
    User insertNewUser(int applicationId, String username);

    @Transactional
    User saveUserInfo(User user);

    @Transactional
    int getProfileCompletenessPercentage(long userId, int applicationId);

    @Transactional
    int getProfileCompletenessPercentage(User user);

    @Transactional
    User getUserProfile(long userId, int applicationId);

    @Transactional
    User getUserProfile(String username, int applicationId);

    @Transactional
    User loginUser(String username, int applicationId);

    @Transactional
    List<User> getApplicationUsers(int applicationId);

    @Transactional
    ResponseItem getUsers(int page, int pagezize, String field, String dir, User user);

    @Transactional
    void setProfileMappings();

    @Transactional
    User updateSpecificUserInfo(User user);


    //----------------------
    //------ FOR TASKS -----
    //----------------------

    @Transactional
    int updateUserAge();

    @Transactional
    List<Long> checkConsecutiveLogins(int applId, int consecutiveDays);
}
