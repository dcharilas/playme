package com.playMe.core.service;

import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.entity.Application;
import com.playMe.core.db.entity.User;
import com.playMe.core.db.entity.Usermessages;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service that handles all actions related to user messages
 *
 * @author Dimitris Charilas
 */
public interface UserMessageService {

    @Transactional
    List<String> getAllUserMessageCodes();
	
    @Transactional
    Usermessages addNewUserMessage(Usermessages usermessages);

    @Transactional
    Usermessages createNewUserMessage(User user, Application application, List<String> values, UserMessageCodeEnum msgCode);

    @Transactional
    Usermessages createNewUserMessage(long userId, int applId, List<String> values, UserMessageCodeEnum msgCode);

    @Transactional
    ResponseItem getUnreadUserMessages(int applId, long userId, int messagesNumber);

    @Transactional
    int moveUserMessages();
}
