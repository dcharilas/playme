package com.playMe.core.service;


import com.playMe.core.db.entity.Application;
import com.playMe.core.model.ResponseItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service that handles all actions related to Application entity
 * 
 * @author Dimitris Charilas
 *
 */
public interface ApplicationService {

    @Transactional
	Application getApplication(int id);

    @Transactional
    List<Application> getAllApplications();

    @Transactional
    ResponseItem getApplications(int page, int pagezize, String field, String dir, Application application);

    @Transactional
	Application insertApplication(String name);

    @Transactional
	boolean deleteApplication(int id);

}
