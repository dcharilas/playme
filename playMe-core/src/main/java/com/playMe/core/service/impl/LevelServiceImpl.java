package com.playMe.core.service.impl;

import com.playMe.core.constant.Constants;
import com.playMe.core.constant.UserMessageCodeEnum;
import com.playMe.core.db.repository.ApplicationlevelsRepository;
import com.playMe.core.db.repository.LevelRepository;
import com.playMe.core.db.repository.UserRepository;
import com.playMe.core.db.repository.UserlevelprogressionRepository;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.keys.ApplicationlevelPK;
import com.playMe.core.db.keys.UserlevelprogressionPK;
import com.playMe.core.db.repository.specification.ApplicationLevelSpecification;
import com.playMe.core.model.ResponseItem;
import com.playMe.core.service.ApplicationService;
import com.playMe.core.service.ConfigurationService;
import com.playMe.core.service.LevelService;
import com.playMe.core.service.UserMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service that handles all actions related to Level entity
 *
 * @author Dimitris Charilas
 */
@Service("levelService")
public class LevelServiceImpl implements LevelService {
	
	private final Logger logger = LoggerFactory.getLogger(LevelServiceImpl.class);


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ApplicationlevelsRepository applicationlevelsRepository;

    @Autowired
    private UserlevelprogressionRepository userlevelprogressionRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserMessageService userMessageService;


	/**
	 * Returns the user's level
	 * 
	 * @param userId
	 * @param appId
	 * @return
	 */
	public Level getUserLevel(long userId, int appId) {
		logger.info("LevelService.getUserLevel :" +userId +","+appId);
		User user = userRepository.findByIdAndApplication_Id(userId,appId);
        if (user.getLevel() != null) {
            return addMessages(user.getLevel());
        }
		return null;
	}
	
	
	/**
	 * Returns the available levels of the application
	 * 
	 * @param appId
	 * @return
	 */
	public List<Applicationlevel> getApplicationLevels(int appId) {
		logger.info("LevelService.getApplicationLevels :" +appId);
        return applicationlevelsRepository.findByApplication_Id(appId);
	}


    /**
     * Activates or deactivates a badge for an application
     *
     * @param appId
     * @param levelid
     * @param active
     * @return
     */
    public Applicationlevel toggleLevel(int appId, int levelid, boolean active) {
        logger.info("BadgeService.activateBadge: " +appId +"," +levelid +"," +active);
        try {
            applicationlevelsRepository.toggleApplicationLevel(appId,levelid,active);
            return applicationlevelsRepository.findByApplication_IdAndLevel_Id(appId, levelid);
        } catch (Exception e) {
            logger.error("Could not activate/deactivate badge.", e);
            return null;
        }
    }
	
	
	/**
	 * Moves user to the next level
	 * 
	 * @param userId
	 * @param appId
	 * @return
	 */
	public User increaseUserLevel(long userId, int appId) {
		logger.info("LevelService.increaseUserLevel :" +userId +","+appId);
		Applicationlevel newAppLevel;

        try {
            User user = userRepository.findByIdAndApplication_Id(userId, appId);
            int previousLevelId = user.getLevel() != null ? user.getLevel().getId() : 0;
            int previousLevelSequence = previousLevelId == 0 ? 0 :
                    applicationlevelsRepository.findApplicationLevelSequence(appId,previousLevelId);
            /*
             * Retrieve application levels sorted by sequence
             */
            List<Applicationlevel> levels = applicationlevelsRepository.findApplicationLevelsSequence(appId);

            for (Applicationlevel l : levels) {
                /*
                 * Find current level. In case user is in level 0, find the first one
                 * Only active levels are considered
                 */
                if (l.getActive() && ((previousLevelSequence == 0 && l.getSequence() == 1) ||
                        (user.getLevel() != null && l.getSequence() == previousLevelSequence+1))) {
                    /*
                     * Found current level, get next one (we assume levels are sorted)
                     */
                    if(levels.size() > levels.indexOf(l)) {
                        newAppLevel = levels.get(levels.indexOf(l));
                       /*
                        * Update the user level
                        */
                        user.setLevel(newAppLevel.getLevel());
                        user = userRepository.save(user);
                       /*
                        * Now save level progression history
                        */
                        Userlevelprogression prog = new Userlevelprogression();
                        prog.setApplication(newAppLevel.getApplication());
                        prog.setLevel(newAppLevel.getLevel());
                        prog.setUnlockdate(new Date());
                        UserlevelprogressionPK key = new UserlevelprogressionPK();
                        key.setLevelid(newAppLevel.getLevel().getId());
                        key.setApplicationid(appId);
                        key.setUserid(user.getId());
                        prog.setId(key);
                        userlevelprogressionRepository.save(prog);

                        /*
                         * Create the proper user message
                         */
                        List<String> values = Arrays.asList(newAppLevel.getLevel().getName());
                        userMessageService.createNewUserMessage(user.getId(), newAppLevel.getApplication().getId(), values, UserMessageCodeEnum.AWARD_LEVEL);

                        logger.info("LevelService.getUserLevel : Updated user level from " +previousLevelSequence +" to " +newAppLevel.getSequence());
                        return user;
                    }
                    break;
                }
            }
            logger.info("LevelService.getUserLevel : Level was not updated");
            return userRepository.findByIdAndApplication_Id(userId, appId);
        } catch (Exception e) {
            logger.error("Could not increase user level.", e);
            return null;
        }
	}


    /**
     * Checks if the user has enough points in order to proceed
     * to the next level. If so, then the level is increased.
     *
     * @return
     */
    public User checkUserLevelIncrease(User user) {
        logger.info("LevelService.checkIfUserLevelIncrease :" +user.getId() +","+user.getApplication().getId());
        Applicationlevel newAppLevel;

        int previousLevelId = user.getLevel() != null ? user.getLevel().getId() : 0;
        int previousLevelSequence = previousLevelId == 0 ? 0 :
                applicationlevelsRepository.findApplicationLevelSequence(user.getApplication().getId(),previousLevelId);
       /*
        * Retrieve application levels sorted by sequence
        */
        List<Applicationlevel> levels = applicationlevelsRepository.findApplicationLevelsSequence(user.getApplication().getId());

        for (Applicationlevel l : levels) {
           /*
            * Find current level. In case user is in level 0, find the first one
            * Only active levels are considered
            */
            if (l.getActive() && (previousLevelSequence == 0 && l.getSequence() == 1) ||
                    (user.getLevel() != null && l.getSequence() == previousLevelSequence+1)) {
               /*
                * Found current level, get next one (we assume levels are sorted)
                */
                if(levels.size() > levels.indexOf(l)) {
                    newAppLevel = levels.get(levels.indexOf(l));
                   /*
                    * Check whether user has enough points to proceed to the
                    * next level
                    */
                    if (newAppLevel.getPointsrequired() <= user.getPoints()) {
                       /*
                        * Update the user level
                        */
                        user.setLevel(newAppLevel.getLevel());
                        user = userRepository.save(user);
                       /*
                        * Now save level progression history
                        */
                        Userlevelprogression prog = new Userlevelprogression();
                        prog.setApplication(user.getApplication());
                        prog.setLevel(newAppLevel.getLevel());
                        prog.setUnlockdate(new Date());
                        UserlevelprogressionPK key = new UserlevelprogressionPK();
                        key.setLevelid(newAppLevel.getLevel().getId());
                        key.setApplicationid(user.getApplication().getId());
                        key.setUserid(user.getId());
                        prog.setId(key);
                        userlevelprogressionRepository.save(prog);

                        /*
                         * Create the proper user message
                         */
                        List<String> values = Arrays.asList(newAppLevel.getLevel().getName());
                        userMessageService.createNewUserMessage(user.getId(), newAppLevel.getApplication().getId(), values, UserMessageCodeEnum.AWARD_LEVEL);

                        logger.info("LevelService.getUserLevel : Updated user level from " +previousLevelSequence +" to " +newAppLevel.getSequence());
                    }
                }
            }
        }
        return user;
    }


    /**
     * Returns all levels with the specified criteria
     *
     * @return
     */
    public ResponseItem getLevels(int page, int pagezize, String field, String dir, Applicationlevel level) {
        logger.info("LevelService.getLevels");
        try {
            Sort.Order pointsOrder = new Sort.Order("asc".equalsIgnoreCase(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field);
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(page == 0 ? page : page - 1, pagezize, sort);
            Page<Applicationlevel> resultsPage = applicationlevelsRepository.findAll(
                    new ApplicationLevelSpecification().createSpecFromSearchForm(level),paging);
            if (resultsPage != null) {
                return new ResponseItem(resultsPage.getTotalElements(), addMessages(resultsPage.getContent()));
            } else {
                return new ResponseItem(0, new ArrayList<Applicationlevel>());
            }
        } catch (Exception e) {
            logger.error("Could not load levels.", e);
            return null;
        }
    }


	/**
	 * Gets the user's history regarding levels
	 * 
	 * @param userId
	 * @param appId
	 * @return
	 */
	public ResponseItem getUserLevelProgression(long userId, int appId) {
		logger.info("LevelService.getUserLevelProgression :" +userId +","+appId);
        try {
            /*
             * Sort by descending date. Fetch only 10 results.
             */
            Sort.Order pointsOrder = new Sort.Order(Sort.Direction.DESC, "unlockdate");
            Sort sort = new Sort(pointsOrder);
            Pageable paging = new PageRequest(0, Constants.defaultPageSize, sort);

		    List<Userlevelprogression> results = userlevelprogressionRepository.findByUser_IdAndApplication_Id(userId, appId, paging);
            if (results != null) {
                return new ResponseItem(results.size(), results);
            } else {
                return new ResponseItem(0, new ArrayList<Userlevelprogression>());
            }
        } catch (Exception e) {
            logger.error("Could not load user level progression.", e);
            return null;
        }
	}


    /**
     * Updates level information
     *
     * @param level
     * @return
     */
    public Applicationlevel saveLevelInfo(Applicationlevel level) {
        logger.info("LevelService.saveLevelInfo: " + level.getApplication().getId() + "," + level.getLevel().getName());
        try {
           /*
            * Check if this is a new level
            */
            if (level.getLevel().getId() != 0) {
               /*
                * This is an existing level, so construct the id again and save it
                */
                ApplicationlevelPK key = new ApplicationlevelPK();
                key.setLevelid(level.getLevel().getId());
                key.setApplicationid(level.getApplication().getId());
                level.setId(key);
                /*
                 * Check if the level exists already but is assigned to another application.
                 * If this is the case, then delete the existing Applicationlevel record
                 */
                List<Applicationlevel> existingRecords = applicationlevelsRepository.findByLevel_Id(level.getLevel().getId());
                applicationlevelsRepository.delete(existingRecords);
                /*
                 * Finally save the new record
                 */
                levelRepository.save(level.getLevel());
                applicationlevelsRepository.save(level);
                return level;
            } else {
               /*
                * Check if another level with the same name exists already
                * for the same application
                */
                List<Level> savedLevels = levelRepository.findApplicationLevel(level.getApplication().getId(), level.getLevel().getName());
                if (savedLevels != null && savedLevels.size() > 0) {
                    throw new Exception("Another level with the same name is registered to this application.");
                }
               /*
                * This is a new level, so save it first
                */
                Level newLevelItem = levelRepository.save(level.getLevel());
                level.setLevel(newLevelItem);
               /*
                * Now set the application and the id
                */
                Application application = applicationService.getApplication(level.getApplication().getId());
                level.setApplication(application);
                ApplicationlevelPK key = new ApplicationlevelPK();
                key.setLevelid(level.getLevel().getId());
                key.setApplicationid(level.getApplication().getId());
                level.setId(key);
               /*
                * Register the level to the selected application
                */
                return applicationlevelsRepository.save(level);
            }
        } catch (Exception e) {
            logger.error("Could not save level info.", e);
            return null;
        }
    }


    /**
     * Deletes a level
     *
     * @param levelId
     * @param applId
     */
    public boolean deleteLevel(int levelId, int applId) {
        logger.info("LevelService.deleteLevel: " +levelId +"," +applId);
        try {
            Applicationlevel applicationlevel = applicationlevelsRepository.findByApplication_IdAndLevel_Id(applId,levelId);
            applicationlevelsRepository.delete(applicationlevel);
            levelRepository.delete(applicationlevel.getLevel());
            return true;
        } catch (Exception e) {
            logger.error("Could not delete level.", e);
            return false;
        }
    }


    /**
     * Add messages to levels
     *
     * @param levels
     * @return
     */
    private List<Applicationlevel> addMessages(List<Applicationlevel> levels) {
        for (Applicationlevel level : levels) {
            level.getLevel().setDescriptions(configurationService.getDescriptions(level.getLevel().getDescriptionid()));
        }
        return levels;
    }

    /**
     * Add messages to level
     *
     * @param level
     * @return
     */
    private Level addMessages(Level level) {
        level.setDescriptions(configurationService.getDescriptions(level.getDescriptionid()));
        return level;
    }


    /**
     * Returns the sequence of a specific level in an application
     *
     * @param levelId
     * @param applId
     * @return
     */
    public int getLevelSequence(int levelId, int applId) {
        Applicationlevel level = applicationlevelsRepository.findByApplication_IdAndLevel_Id(applId,levelId);
        return (level != null) ? level.getSequence() : 0;
    }
}
