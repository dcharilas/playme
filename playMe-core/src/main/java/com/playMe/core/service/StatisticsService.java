package com.playMe.core.service;

import com.playMe.core.constant.StatisticsEnum;
import com.playMe.core.model.StatisticsDateItem;
import com.playMe.core.model.StatisticsItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service that handles all actions related to generation and organisation of statistics
 *
 * @author Dimitris Charilas
 */
public interface StatisticsService {

    @Transactional
    List<StatisticsItem> getStatistics(StatisticsEnum stat);

    @Transactional
    List<StatisticsItem> getStatistics(StatisticsEnum stat, int applId);

    @Transactional
    List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days);

    @Transactional
    List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId);

    @Transactional
    List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId, long userId);
	
}
