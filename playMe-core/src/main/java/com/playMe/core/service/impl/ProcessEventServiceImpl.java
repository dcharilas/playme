package com.playMe.core.service.impl;

import com.playMe.core.constant.ActionEnum;
import com.playMe.core.constant.EventEnum;
import com.playMe.core.db.entity.*;
import com.playMe.core.db.repository.EventactionRepository;
import com.playMe.core.db.repository.EventdefinitionRepository;
import com.playMe.core.db.repository.EventhistoryRepository;
import com.playMe.core.db.repository.ExternaleventhistoryRepository;
import com.playMe.core.dto.Event;
import com.playMe.core.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * Service that handles processing of external events
 *
 * @author Dimitris Charilas
 */
@Service("processEventService")
public class ProcessEventServiceImpl implements ProcessEventService {

	private final Logger logger = LoggerFactory.getLogger(ProcessEventServiceImpl.class);

    @Autowired
    private EventdefinitionRepository eventdefinitionRepository;

    @Autowired
    private EventactionRepository eventactionRepository;

    @Autowired
    private ExternaleventhistoryRepository externaleventhistoryRepository;

    @Autowired
    private EventhistoryRepository eventhistoryRepository;

    @Autowired
    private PointService pointService;

    @Autowired
    private BadgeService badgeService;

    @Autowired
    private UserService userService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private RewardService rewardService;

    @PersistenceContext
    private EntityManager entityManager;



    /**
     * This is a mapper class. Its role is to translate external events into internal ones.
     * Based on the type of the event (see EventDefinition), a proper
     * EventAction has to be triggered. When this is completed, a new internal event
     * will be created.
     *
     * @param externalEvent
     * @return
     */
    public List<Eventhistory> processEvent(Event externalEvent) {
        logger.info("ProcessEventService.processEvent: " +externalEvent.getId());

        List<Eventhistory> triggeredEvents = new LinkedList<Eventhistory>();
        Application application = new Application();
        application.setId(externalEvent.getApplicationId());

        /*
         * Load event definition
         */
        Eventdefinition eventdefinition = eventdefinitionRepository.findOne(externalEvent.getEventId());
        String eventName = eventdefinition != null ? eventdefinition.getName() : null;

        /*
         * REGISTER_NEW_USER event
         */
        if (EventEnum.REGISTER_USER.name().equals(eventName)) {
            /*
             * Insert new user before proceeding to actions. Configuration is expected as follows
             * Text Value 1 -> username
             * Text Value 2 -> firstname
             * Text Value 3 -> lastname
             * Text Value 4 -> email
             */
            User user = new User();
            user.setUsername(externalEvent.getTextvalue1());
            user.setFirstname(externalEvent.getTextvalue2());
            user.setLastname(externalEvent.getTextvalue3());
            user.setEmail(externalEvent.getTextvalue4());
            user.setApplication(application);

            user = userService.saveUserInfo(user);
            if (user != null) {
                externalEvent.setUserId(user.getId());
            } else {
                return null;
            }
        }

        /*
         * REDEEM_POINTS event
         * Numeric Value 1 -> reward id
         */
        if (EventEnum.REDEEM_POINTS.name().equals(eventName)) {
           /*
            * Load the reward
            */
            Reward reward = rewardService.getReward(externalEvent.getNumericvalue1());
           /*
            *  Create the event record
            */
            Eventhistory event = getNewInternalEvent(externalEvent);
            event.setPointsawarded(-reward.getPointsreq());
            event.setEventdefinition(eventdefinition);
            event = saveEvent(event,ActionEnum.REDEEM_POINTS);
            triggeredEvents.add(event);
            /*
             * Remove the points and associate reward with user
             */
            rewardService.redeemReward(externalEvent.getApplicationId(),externalEvent.getUserId(),reward,event, externalEvent.getTextvalue1());
        }

        /*
         * Load event action
         */
        Eventaction action = eventactionRepository.findByEventdefinition_IdAndApplication_Id(externalEvent.getEventId(), externalEvent.getApplicationId());
        if (action != null) {
            entityManager.detach(action);
        }

        /*
         * USER_ACTION_ASSIGN_POINTS event
         */
        if (EventEnum.USER_ACTION_ASSIGN_POINTS.name().equals(eventName)) {
            /*
             * Points are specified so override default configuration
             */
            if (externalEvent.getNumericvalue1() > 0) {
                if (action == null) {
                    action = new Eventaction();
                    action.setEventdefinition(eventdefinition);
                    action.setApplication(application);
                }
                action.setPointsaward(externalEvent.getNumericvalue1());
            }
        }

        /*
         * USER_ACTION_ASSIGN_BADGE event
         */
        if (EventEnum.USER_ACTION_ASSIGN_BADGE.name().equals(eventName)) {
            /*
             * Badge id is specified so override default configuration
             */
            if (externalEvent.getNumericvalue1() > 0) {
                if (action == null) {
                    action = new Eventaction();
                    action.setEventdefinition(eventdefinition);
                    action.setApplication(application);
                }
                action.getBadge().setId(externalEvent.getNumericvalue1());
            }
        }


        if (action != null) {
            /*
             * After finding the required action perform the mapping
             */
            if (action.getPointsaward() > 0) {
               /*
                *  Create the event record
                */
                Eventhistory event = getNewInternalEvent(externalEvent);
                event.setPointsawarded(action.getPointsaward());
                event.setEventdefinition(eventdefinition);
                event = saveEvent(event,ActionEnum.AWARD_POINTS);
                triggeredEvents.add(event);
                /*
                 *  User must be awarded with points
                 */
                User user = pointService.awardUserWithPoints(action.getPointsaward(), externalEvent.getUserId(), externalEvent.getApplicationId(), event);
                /*
                 * Every time user is awarded with points check if level should
                 * be increased
                 */
                user = levelService.checkUserLevelIncrease(user);
                /*
                 * Check if badge should be awarded. It is possible that a badge should be
                 * awarded when user exceeds a number of points or reaches a specific
                 * level sequence.
                 */
                checkIfBadgeShouldBeAwarded(externalEvent, triggeredEvents, user, eventdefinition);

            }
            if (action.getBadge() != null) {
                /*
                 * Get the badge
                 */
                Badge badge = badgeService.getBadge(action.getBadge().getId());
                if (badgeService.checkIfBadgeIsActive(action.getBadge().getId(), action.getApplication().getId())) {
                   /*
                    *  Create the event record
                    */
                    Eventhistory event = getNewInternalEvent(externalEvent);
                    event.setBadge(badge);
                    event.setEventdefinition(eventdefinition);
                    event = saveEvent(event,ActionEnum.AWARD_BADGE);
                    triggeredEvents.add(event);
                    /*
                     *  User must be awarded with badge
                     */
                    badgeService.awardUserWithBadge(externalEvent.getUserId(), externalEvent.getApplicationId(), action.getBadge().getId());
                }
            }
        }

        logger.info("Generated " +triggeredEvents.size() + " internal events.");
        return triggeredEvents;
    }


    /**
     * Used to check if badge should be awarded after initial actions are completed
     *
     * @param externalEvent
     * @param triggeredEvents
     * @param user
     */
    private void checkIfBadgeShouldBeAwarded(Event externalEvent, List<Eventhistory> triggeredEvents, User user, Eventdefinition eventdefinition) {
        List<Applicationbadge> badges = badgeService.getApplicationBadges(externalEvent.getApplicationId(),true);
        List<Badge> userBadges = badgeService.getUserBadges(externalEvent.getUserId(), externalEvent.getApplicationId());
        int levelSequence = user.getLevel() != null ? levelService.getLevelSequence(user.getLevel().getId(), externalEvent.getApplicationId()) : 0;

        for (Applicationbadge badge : badges) {
            /*
             * Badge must be active and user must not have it already
             */
            if (badge.getActive() && !userBadges.contains(badge.getBadge())) {
                if ((badge.getAwardafterpoints() != null && badge.getAwardafterpoints() > 0
                        && user.getPoints() >= badge.getAwardafterpoints())
                    || (badge.getAwardafterlevelseq() != null && badge.getAwardafterlevelseq() > 0
                        && levelSequence > 0 && levelSequence > badge.getAwardafterlevelseq())) {
                   /*
                    *  Create the event record
                    */
                    Eventhistory event = getNewInternalEvent(externalEvent);
                    event.setBadge(badge.getBadge());
                    event.setEventdefinition(eventdefinition);
                    event = saveEvent(event,ActionEnum.AWARD_BADGE);
                    triggeredEvents.add(event);
                    /*
                     *  User must be awarded with badge
                     */
                    badgeService.awardUserWithBadge(externalEvent.getUserId(), externalEvent.getApplicationId(), badge.getBadge().getId());
                }
            }
        }

    }


    /**
     * Instantiates a new event
     *
     * @param externalEvent
     */
    private Eventhistory getNewInternalEvent(Event externalEvent) {
        Eventhistory event = new Eventhistory();
        event.setEventdate(new Date());

        Application app = new Application();
        app.setId(externalEvent.getApplicationId());
        event.setApplication(app);

        Externaleventhistory associatedEvent = externaleventhistoryRepository.findOne(externalEvent.getId());
        event.setExternaleventhistory(associatedEvent);

        User user = new User();
        user.setId(externalEvent.getUserId());
        event.setUser(user);

        return event;
    }


    /**
     * Saves an internal event
     *
     * @param event
     */
    private Eventhistory saveEvent(Eventhistory event, ActionEnum type) {
        logger.info("Generating internal event " +type.name() +" for external event " +event.getEventdefinition().getName());
        event = eventhistoryRepository.save(event);
        logger.info("Saved event successfully! Internal event id is " +event.getId());
        /*
         * Add a small delay so that the event is properly
         * saved before processing continues
         */
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return event;
    }
	
}
