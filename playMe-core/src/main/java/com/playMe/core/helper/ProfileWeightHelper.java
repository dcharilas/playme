package com.playMe.core.helper;

import com.playMe.core.db.entity.Userprofileweights;

import java.util.List;

/**
 * Class containing methods that parse through Userprofileweights collection
 *
 * @author Charilas
 */
public class ProfileWeightHelper {

    /**
     * Returns the weight for a specific field of user profile
     *
     * @param fieldname
     * @param profileWeights
     * @return
     */
    public static int getFieldWeight(String fieldname, List<Userprofileweights> profileWeights) {
        for (Userprofileweights field: profileWeights) {
            if (fieldname.equals(field.getFieldname())) {
                return field.getWeight();
            }
        }
        return 0;
    }

}
