package com.playMe.core.constant;

/**
 * List of supported events
 * 
 * @author Charilas
 *
 */
public enum UserMessageCodeEnum {

    AWARD_POINTS,
    AWARD_BADGE,
    AWARD_LEVEL,
    REDEEM_POINTS;


    public String value() {
        return name();
    }

    public static UserMessageCodeEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

