package com.playMe.core.constant;

/**
 * List of fields in user profile
 * 
 * @author Charilas
 *
 */
public enum ProfileFieldEnum {

    ADDRESS,
    AVATAR,
    BIRTHDATE,
    EMAIL,
    FIRSTNAME,
    GENDER,
    LASTNAME,
    PHONE,
    POSTCODE,
    SOCIALPROFILE;

    public String value() {
        return name();
    }

    public static ProfileFieldEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

