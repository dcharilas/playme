package com.playMe.core.constant;

/**
 * List of supported events
 * 
 * @author Charilas
 *
 */
public enum TaskRequirementEnum {

	POINTS_REQUIRED,
	INTERACTIONS_REQUIRED,
	SOCIAL_LOGINS_REQUIRED,
	LIKES_REQUIRED,
	SHARES_REQUIRED,
	PROFILE_COMPLETENESS_REQUIRED,
	CONSEQUTIVE_DAYS_REQUIRED,
	CONSEQUTIVE_WEEKS_REQUIRED;

    public String value() {
        return name();
    }

    public static TaskRequirementEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

