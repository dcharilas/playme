package com.playMe.core.constant;

/**
 * List of supported internal events
 * 
 * @author Charilas
 *
 */
public enum ActionEnum {

    AWARD_POINTS,
    AWARD_LEVEL,
	AWARD_BADGE,
    REDEEM_POINTS;

    public String value() {
        return name();
    }

    public static ActionEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

