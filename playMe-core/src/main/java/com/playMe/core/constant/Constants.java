package com.playMe.core.constant;

public class Constants {

    /* Grid default configuration */
    public static final int defaultStartPage = 1;
    public static final int defaultPageSize = 10;

}
