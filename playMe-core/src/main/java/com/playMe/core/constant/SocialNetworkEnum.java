package com.playMe.core.constant;

/**
 * List of supported events
 * 
 * @author Charilas
 *
 */
public enum SocialNetworkEnum {

	FACEBOOK,
	TWITTER,
	GOOGLE_PLUS;

    public String value() {
        return name();
    }

    public static SocialNetworkEnum fromValue(String v) {
        return valueOf(v);
    }
    
    
}

