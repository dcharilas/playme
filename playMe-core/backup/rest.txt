Users
---------
http://localhost:8080/playMe/services/user/get/25
http://localhost:8080/playMe/services/user/get/25/40
http://localhost:8080/playMe/services/user/get/25/username/jim10
http://localhost:8080/playMe/services/user/insert/25/kolofido


Applications
------------
http://localhost:8080/playMe/services/application/insert/test
http://localhost:8080/playMe/services/application/get
http://localhost:8080/playMe/services/application/get/25


Points
--------
http://localhost:8080/playMe/services/user/points/update/25/40/5
http://localhost:8080/playMe/services/user/points/get/25/40


Levels
---------
http://localhost:8080/playMe/services/user/level/get/25/40
http://localhost:8080/playMe/services/application/levels/insert/25/level3/3/3/0/test
http://localhost:8080/playMe/services/application/levels/get/25
http://localhost:8080/playMe/services/user/level/update/25/40
http://localhost:8080/playMe/services/application/levels/update/25/13/0


Badges
----------
http://localhost:8080/playMe/services/application/badges/insert/25/badge1/1/test/test
http://localhost:8080/playMe/services/application/badges/get/25
http://localhost:8080/playMe/services/application/badges/update/25
http://localhost:8080/playMe/services/user/badges/update/25/40/1
http://localhost:8080/playMe/services/user/badges/get/25/40


Leaderboard
-------------
http://localhost:8080/playMe/services/leaderboard/get/25/1


Events
---------
http://localhost:8080/playMe/services/event/definition/insert/testevent
http://localhost:8080/playMe/services/event/definition/get
http://localhost:8080/playMe/services/event/action/insert/25/2/0/5
http://localhost:8080/playMe/services/event/action/get
http://localhost:8080/playMe/services/event/action/get/25
http://localhost:8080/playMe/services/event/insert/25/3/40/1/2/test1/test2/mycomment