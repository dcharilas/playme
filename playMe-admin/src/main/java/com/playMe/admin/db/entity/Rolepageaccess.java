package com.playMe.admin.db.entity;

import com.playMe.admin.db.key.RolepageaccessPK;

import javax.persistence.*;

/**
 * @author Dimitris Charilas
 */
@Entity
@Table(name="rolepageaccess", schema="", catalog="playmeadmin")
@IdClass(RolepageaccessPK.class)
public class Rolepageaccess {
    private int roleid;
    private String pageid;

    @Id
    @Column(name = "roleid")
    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Id
    @Column(name = "pageid")
    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rolepageaccess that = (Rolepageaccess) o;

        if (roleid != that.roleid) return false;
        if (pageid != null ? !pageid.equals(that.pageid) : that.pageid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid;
        result = 31 * result + (pageid != null ? pageid.hashCode() : 0);
        return result;
    }
}
