package com.playMe.admin.db.repository;

import com.playMe.admin.db.entity.Rolepageaccess;
import com.playMe.admin.db.key.RolepageaccessPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface RolepageaccessRepository extends JpaRepository<Rolepageaccess,RolepageaccessPK> {

    List<Rolepageaccess> findByRoleid(@Param("roleid") int roleid);

}
