package com.playMe.admin.db.entity;

import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the adminuser database table.
 *
 */
@Entity
@Table(name="adminuser", schema="", catalog="playmeadmin")
public class Adminuser implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique=true, nullable=false)
	private Integer id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="roleid")
	private Role role;

    @Column(length=30)
	private String username;

    @Column(length=30)
	private String password;

    @Column(length=100)
	private String firstname;

    @Column(length=100)
	private String lastname;

    @Transient
    private List<Integer> userapplicationaccess;

	public Adminuser() {
	}

	public Adminuser(Role role, String username, String password) {
		this.role = role;
		this.username = username;
		this.password = password;
	}

	public Adminuser(Role role, String username, String password,
			String firstname, String lastname) {
		this.role = role;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

    public List<Integer> getUserapplicationaccess() {
        return userapplicationaccess;
    }

    public void setUserapplicationaccess(List<Integer> userapplicationaccess) {
        this.userapplicationaccess = userapplicationaccess;
    }
}
