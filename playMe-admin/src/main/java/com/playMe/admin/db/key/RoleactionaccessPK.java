package com.playMe.admin.db.key;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class RoleactionaccessPK implements Serializable {
    private int roleid;
    private String mapping;
    private String action;

    @Column(name = "roleid")
    @Id
    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Column(name = "mapping")
    @Id
    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    @Column(name = "action")
    @Id
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleactionaccessPK that = (RoleactionaccessPK) o;

        if (roleid != that.roleid) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (mapping != null ? !mapping.equals(that.mapping) : that.mapping != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid;
        result = 31 * result + (mapping != null ? mapping.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        return result;
    }
}
