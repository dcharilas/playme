package com.playMe.admin.db.repository;

import com.playMe.admin.db.entity.Adminuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface AdminuserRepository extends JpaRepository<Adminuser,Integer> {

    Adminuser findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

}
