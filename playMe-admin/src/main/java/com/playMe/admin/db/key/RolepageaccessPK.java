package com.playMe.admin.db.key;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class RolepageaccessPK implements Serializable {
    private int roleid;
    private String pageid;

    @Column(name = "roleid")
    @Id
    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Column(name = "pageid")
    @Id
    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolepageaccessPK that = (RolepageaccessPK) o;

        if (roleid != that.roleid) return false;
        if (pageid != null ? !pageid.equals(that.pageid) : that.pageid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid;
        result = 31 * result + (pageid != null ? pageid.hashCode() : 0);
        return result;
    }
}
