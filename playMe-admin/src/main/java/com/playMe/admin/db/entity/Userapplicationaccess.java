package com.playMe.admin.db.entity;

import com.playMe.admin.db.key.UserapplicationaccessPK;

import javax.persistence.*;

/**
 * @author Dimitris Charilas
 */
@Entity
@Table(name="userapplicationaccess", schema="", catalog="playmeadmin")
@IdClass(UserapplicationaccessPK.class)
public class Userapplicationaccess {
    private int userid;
    private int applicationid;

    @Id
    @Column(name = "userid")
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Id
    @Column(name = "applicationid")
    public int getApplicationid() {
        return applicationid;
    }

    public void setApplicationid(int applicationid) {
        this.applicationid = applicationid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Userapplicationaccess that = (Userapplicationaccess) o;

        if (applicationid != that.applicationid) return false;
        if (userid != that.userid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid;
        result = 31 * result + applicationid;
        return result;
    }
}
