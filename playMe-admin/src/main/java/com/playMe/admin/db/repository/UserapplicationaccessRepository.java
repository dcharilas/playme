package com.playMe.admin.db.repository;

import com.playMe.admin.db.entity.Userapplicationaccess;
import com.playMe.admin.db.key.UserapplicationaccessPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface UserapplicationaccessRepository extends JpaRepository<Userapplicationaccess,UserapplicationaccessPK> {

    List<Userapplicationaccess> findByUserid(@Param("userid") int userid);

}
