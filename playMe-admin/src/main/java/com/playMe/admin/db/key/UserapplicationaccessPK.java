package com.playMe.admin.db.key;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class UserapplicationaccessPK implements Serializable {
    private int userid;
    private int applicationid;

    @Column(name = "userid")
    @Id
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Column(name = "applicationid")
    @Id
    public int getApplicationid() {
        return applicationid;
    }

    public void setApplicationid(int applicationid) {
        this.applicationid = applicationid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserapplicationaccessPK that = (UserapplicationaccessPK) o;

        if (applicationid != that.applicationid) return false;
        if (userid != that.userid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid;
        result = 31 * result + applicationid;
        return result;
    }
}
