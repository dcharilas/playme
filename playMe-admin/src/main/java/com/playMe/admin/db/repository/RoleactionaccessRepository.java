package com.playMe.admin.db.repository;

import com.playMe.admin.db.entity.Roleactionaccess;
import com.playMe.admin.db.key.RoleactionaccessPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
@Repository
public interface RoleactionaccessRepository extends JpaRepository<Roleactionaccess,RoleactionaccessPK> {

    List<Roleactionaccess> findByRoleid(@Param("roleid") int roleid);

}
