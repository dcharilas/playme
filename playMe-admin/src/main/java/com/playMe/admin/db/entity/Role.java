package com.playMe.admin.db.entity;


import org.codehaus.jackson.annotate.JsonIgnore;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the role database table.
 *
 */
@Entity
@Table(name="role", schema="", catalog="playmeadmin")
public class Role implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(unique=true, nullable=false)
	private Integer id;

    @Column(length=30)
	private String role;

    @JsonIgnore
    @OneToMany(mappedBy="role", fetch=FetchType.EAGER)
    private List<Adminuser> adminusers;

    @Transient
    private List<String> allowedPages;

    @Transient
    private List<Roleactionaccess> roleactionaccesses;


	public Role() {
	}

	public Role(String role) {
		this.role = role;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

    public List<Adminuser> getAdminusers() {
        return adminusers;
    }

    public void setAdminusers(List<Adminuser> adminusers) {
        this.adminusers = adminusers;
    }

    public List<String> getAllowedPages() {
        return allowedPages;
    }

    public void setAllowedPages(List<String> allowedPages) {
        this.allowedPages = allowedPages;
    }

    public List<Roleactionaccess> getRoleactionaccesses() {
        return roleactionaccesses;
    }

    public void setRoleactionaccesses(List<Roleactionaccess> roleactionaccesses) {
        this.roleactionaccesses = roleactionaccesses;
    }
}
