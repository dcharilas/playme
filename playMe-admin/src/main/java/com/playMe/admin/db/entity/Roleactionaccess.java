package com.playMe.admin.db.entity;

import com.playMe.admin.db.key.RoleactionaccessPK;

import javax.persistence.*;

/**
 * @author Dimitris Charilas
 */
@Entity
@Table(name="roleactionaccess", schema="", catalog="playmeadmin")
@IdClass(RoleactionaccessPK.class)
public class Roleactionaccess {
    private int roleid;
    private String mapping;
    private String action;

    @Id
    @Column(name = "roleid")
    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Id
    @Column(name = "mapping")
    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    @Id
    @Column(name = "action")
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Roleactionaccess that = (Roleactionaccess) o;

        if (roleid != that.roleid) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (mapping != null ? !mapping.equals(that.mapping) : that.mapping != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid;
        result = 31 * result + (mapping != null ? mapping.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        return result;
    }
}
