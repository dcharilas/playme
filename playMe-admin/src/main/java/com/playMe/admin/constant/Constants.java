package com.playMe.admin.constant;

public class Constants {

    /* Model attributes */
    public static final String propertiesAttrName = "properties";

    /* Session attributes */
    public static final String userAttrName = "user";
    public static final String userMessageCodesAttrName = "userMessageCodes";

    /* Grid default configuration */
    public static final int defaultStartPage = 1;
    public static final int defaultPageSize = 10;

    // TODO move this to enumeration
    public static final String adminRole = "ADMIN";


}
