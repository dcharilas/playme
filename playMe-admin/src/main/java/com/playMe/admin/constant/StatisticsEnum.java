package com.playMe.admin.constant;

/**
 * List of statistics
 *
 * @author Dimitris Charilas
 */
public enum StatisticsEnum {

    /*
     * User statistics
     */
    USER_GENDER,
    USER_AGE,
    APPLICATION_USERS,
    USERS_LEVEL_SEQUENCE,
    /*
     * Event statistics
     */
    EXTERNAL_EVENTS_PER_DAY,
    INTERNAL_EVENTS_PER_DAY,
    /*
     * Application statistics
     */
    APPLICATION_USERS_PER_DAY,
    APPLICATION_EXTERNAL_EVENTS_PER_DAY,
    APPLICATION_INTERNAL_EVENTS_PER_DAY,
    /*
     * Reward statistics
     */
    REDEEMED_REWARDS,
    REDEEMED_REWARD_PER_TYPE,
    REDEEMED_POINTS_PER_DAY;

    public String value() {
        return name();
    }

    public static StatisticsEnum fromValue(String v) {
        return valueOf(v);
    }
}
