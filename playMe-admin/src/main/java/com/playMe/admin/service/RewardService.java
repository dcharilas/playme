package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationBadgeCore;
import com.playMe.admin.model.core.RewardCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface RewardService {

    ResponseItem getRewards(int page, int pagezize, SearchOption<RewardCore> searchOption);

    boolean deleteRewards(List<Integer> ids);

    RewardCore addReward(RewardCore reward);
}
