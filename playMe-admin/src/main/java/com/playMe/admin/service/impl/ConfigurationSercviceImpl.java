package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.*;
import com.playMe.admin.service.ConfigurationService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Service that handles all requests related to system configuration
 *
 * @author Dimitris Charilas
 */
@Service("configurationService")
public class ConfigurationSercviceImpl implements ConfigurationService {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationSercviceImpl.class);

    private final static String messageGetURL = "/services/configuration/message/get";
    private final static String messageDeleteURL = "/services/configuration/message/delete";
    private final static String messageAddURL = "/services/configuration/message/insert";
    private final static String messageAddBulkURL = "/services/configuration/message/insert/bulk";

    private final static String actionsGetURL = "/services/configuration/eventaction/get";
    private final static String actionsDeleteURL = "/services/configuration/eventaction/delete";
    private final static String actionsAddURL = "/services/configuration/eventaction/insert";

    private final static String definitionsGetURL = "/services/configuration/eventdef/get";
    private final static String definitionsDeleteURL = "/services/configuration/eventdef/delete";
    private final static String definitionsAddURL = "/services/configuration/eventdef/insert";

    private final static String profileWeightsGetURL = "/services/configuration/profileweight/get";
    private final static String profileWeightsDeleteURL = "/services/configuration/profileweight/delete";
    private final static String profileWeightsAddURL = "/services/configuration/profileweight/insert";

    private final static String settingsGetURL = "/services/configuration/setting/get";
    private final static String settingsDeleteURL = "/services/configuration/setting/delete";
    private final static String settingsAddURL = "/services/configuration/setting/insert";

    private final static String userMessageCodesGetURL = "/services/configuration/usermessage/get/codes";
    private final static String userMessageGetURL = "/services/configuration/usermessage/get";
    private final static String userMessageDeleteURL = "/services/configuration/usermessage/delete";
    private final static String userMessageAddURL = "/services/configuration/usermessage/insert";


    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;

    public ConfigurationSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve message codes
     *
     * @return
     */
    public List<String> getMessageCodes() {
        logger.info("getMessageCodes()");

        try {
            String targetURL = coreURL + userMessageCodesGetURL;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                String[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(String[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user message codes.", e);
            return new ArrayList<String>();
        }
    }


    /**
     * Calls core REST service to retrieve a message
     *
     * @param id
     * @param locale
     * @return
     */
    public DisplayTextCore getMessage(int id, String locale) {
        logger.info("getMessage(): (" +id +"," +locale +")");

        try {
            String targetURL = coreURL + messageGetURL +"/" +id +"/" +locale;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                DisplayTextCore responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(DisplayTextCore.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve message.", e);
            return new DisplayTextCore();
        }
    }


    /**
     * Calls core REST service to retrieve all event definitions
     *
     * @return
     */
    public ResponseItem getEventDefinitions(int page, int pagesize, SearchOption<EventDefinitionCore> searchOption) {
        logger.info("getEventDefinitions()");

        try {
            String targetURL = coreURL + definitionsGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve event definitions.", e);
            return new ResponseItem(0, new ArrayList<EventDefinitionCore>());
        }
    }


    /**
     * Calls core REST service to retrieve event actions
     *
     * @return
     */
    public ResponseItem getEventActions(int page, int pagesize, SearchOption<EventActionCore> searchOption) {
        logger.info("getEventActions()");

        try {
            String targetURL = coreURL + actionsGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve event actions.", e);
            return new ResponseItem(0, new ArrayList<EventActionCore>());
        }
    }


    /**
     * Calls core REST service to retrieve profile weights
     *
     * @return
     */
    public ResponseItem getProfileWeights(int page, int pagesize, SearchOption<ProfileWeightsCore> searchOption) {
        logger.info("getProfileWeights()");
        /*
         * Fix the sorting column, since no column 'id' exists
         */
        if ("id".equalsIgnoreCase(searchOption.getSortField())) {
            searchOption.setSortField("fieldname");
        }

        try {
            String targetURL = coreURL + profileWeightsGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve profile weights.", e);
            return new ResponseItem(0, new ArrayList<ProfileWeightsCore>());
        }
    }


    /**
     * Calls core REST service to retrieve settings
     *
     * @return
     */
    public ResponseItem getSettings(int page, int pagesize, SearchOption<ConfigurationCore> searchOption) {
        logger.info("getProfileWeights()");

        try {
            String targetURL = coreURL + settingsGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve settings.", e);
            return new ResponseItem(0, new ArrayList<ConfigurationCore>());
        }
    }



    /**
     * Calls core REST service to retrieve display texts
     *
     * @return
     */
    public ResponseItem getMessages(int page, int pagesize, SearchOption<DisplayTextCore> searchOption) {
        logger.info("getMessages()");

        try {
            String targetURL = coreURL + messageGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve messages.", e);
            return new ResponseItem(0, new ArrayList<DisplayTextCore>());
        }
    }


    /**
     * Calls core REST service to retrieve user messages
     *
     * @return
     */
    public ResponseItem getUserMessages(int page, int pagesize, SearchOption<UserMessageCodesCore> searchOption) {
        logger.info("getUserMessages()");

        try {
            String targetURL = coreURL + userMessageGetURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve user messages.", e);
            return new ResponseItem(0, new ArrayList<UserMessageCodesCore>());
        }
    }


    /**
     * Calls core REST service to delete event definitions
     *
     * @param ids
     * @return
     */
    public boolean deleteEventDefinitions(List<Integer> ids) {
        logger.info("deleteEventDefinitions(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (Integer id : ids) {
                targetURL = coreURL + definitionsDeleteURL +"/" +id;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete event definitions.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add an event definition
     *
     * @param definition
     * @return
     */
    public EventDefinitionCore addEventDefinition(EventDefinitionCore definition) {
        logger.info("addEventDefinition(): (" +definition.getName() +")");

        String targetURL = "";
        EventDefinitionCore responseItem = null;

        try {
            targetURL = coreURL + definitionsAddURL + "/" + definition.getName();
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, null);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(EventDefinitionCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add event definition.", e);
        }

        return responseItem;
    }


    /**
     * Calls core REST service to add a profile weight
     *
     * @param weight
     * @return
     */
    public ProfileWeightsCore addProfileWeight(ProfileWeightsCore weight) {
        logger.info("addProfileWeight(): (" +weight.getFieldname() +")");

        String targetURL = "";
        ProfileWeightsCore responseItem = null;

        try {
            targetURL = coreURL + profileWeightsAddURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, weight);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ProfileWeightsCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add profile weight.", e);
        }

        return responseItem;
    }



    /**
     * Calls core REST service to add a setting
     *
     * @param config
     * @return
     */
    public ConfigurationCore addSetting(ConfigurationCore config) {
        logger.info("addSetting(): (" +config.getApplication().getId() +"," +config.getProperty() +")");

        String targetURL = "";
        ConfigurationCore responseItem = null;

        try {
            targetURL = coreURL + settingsAddURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, config);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ConfigurationCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add setting.", e);
        }

        return responseItem;
    }


    /**
     * Calls core REST service to add an event action
     *
     * @param action
     * @return
     */
    public EventActionCore addEventAction(EventActionCore action) {
        logger.info("addEventDefinition(): (" +action.getApplication().getName() +"," +action.getEventdefinition().getName() +")");

        String targetURL = "";
        EventActionCore responseItem = null;

        try {
            targetURL = coreURL + actionsAddURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, action);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(EventActionCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add event action.", e);
        }

        return responseItem;
    }



    /**
     * Calls core REST service to delete event actions
     *
     * @param ids
     * @return
     */
    public boolean deleteEventActions(List<String> ids) {
        logger.info("deleteEventActions(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                /*
                 * Split the id in two parts. The first one is the application id
                 * and the second one the event id
                 */
                String applId = id.substring(0,id.indexOf("$"));
                String eventId = id.substring(id.indexOf("$")+1);

                targetURL = coreURL + actionsDeleteURL +"/" +applId +"/" +eventId;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete event definitions.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add a message
     *
     * @param message
     * @return
     */
    public DisplayTextCore addMessage(DisplayTextCore message) {
        logger.info("addMessage(): (" +message.getId().getMessageid() +","
                + message.getId().getLocale() +"," +message.getValue() +")");

        String targetURL = "";
        DisplayTextCore responseItem = null;
        try {
            targetURL = coreURL + messageAddURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, message);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(DisplayTextCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add message.", e);
        }

        return responseItem;
    }


    /**
     * Calls core REST service to add a message
     *
     * @param messages
     * @return
     */
    public List<DisplayTextCore> addMessages(List<DisplayTextCore> messages) {
        logger.info("addMessages(): (" +messages.size());

        String targetURL = "";
        DisplayTextCore[] responseItem = null;
        try {
            targetURL = coreURL + messageAddBulkURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, messages);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(DisplayTextCore[].class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add messages.", e);
        }

        return Arrays.asList(responseItem);
    }


    /**
     * Calls core REST service to add a user message
     *
     * @param message
     * @return
     */
    public UserMessageCodesCore addUserMessage(UserMessageCodesCore message) {
        logger.info("addUserMessage(): (" +message.getId().getMsgcode() +","
                + message.getId().getLocale() +"," +message.getMessage() +")");

        String targetURL = "";
        UserMessageCodesCore responseItem = null;
        try {
            targetURL = coreURL + userMessageAddURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, message);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserMessageCodesCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add user message.", e);
        }

        return responseItem;
    }


    /**
     * Calls core REST service to delete messages
     *
     * @param ids
     * @return
     */
    public boolean deleteMessages(List<String> ids) {
        logger.info("deleteMessages(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                /*
                 * Split the id in two parts. The first one is the message id
                 * and the second one the locale
                 */
                String messageId = id.substring(0,id.indexOf("$"));
                String locale = id.substring(id.indexOf("$")+1);

                targetURL = coreURL + messageDeleteURL +"/" +messageId +"/" +locale;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete messages.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to delete user messages
     *
     * @param ids
     * @return
     */
    public boolean deleteUserMessages(List<String> ids) {
        logger.info("deleteUserMessages(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                /*
                 * Split the id in three parts. The first one is the message code
                 * the second one the application id and the third the locale
                 */
                String messageId = id.substring(0,id.indexOf("$"));
                String applicationId = id.substring(id.indexOf("$")+1,id.lastIndexOf("$"));
                String locale = id.substring(id.lastIndexOf("$")+1);

                targetURL = coreURL + userMessageDeleteURL +"/" +messageId +"/" +applicationId +"/" +locale;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete user messages.", e);
        }

        return result;
    }


    /**
     * Calls core REST service to delete profile weights
     *
     * @param ids
     * @return
     */
    public boolean deleteProfileWeights(List<String> ids) {
        logger.info("deleteProfileWeights(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                targetURL = coreURL + profileWeightsDeleteURL +"/" +id;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete profile weights.", e);
        }

        return result;
    }


    /**
     * Calls core REST service to delete settings
     *
     * @param ids
     * @return
     */
    public boolean deleteSettings(List<Integer> ids) {
        logger.info("deleteSettings(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (Integer id : ids) {
                targetURL = coreURL + settingsDeleteURL +"/" +id;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete settings.", e);
        }

        return result;
    }
}
