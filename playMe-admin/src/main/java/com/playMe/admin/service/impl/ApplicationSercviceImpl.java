package com.playMe.admin.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Service that handles all requests related to applications
 *
 * @author Dimitris Charilas
 */
@Service("applicationService")
public class ApplicationSercviceImpl implements ApplicationService {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationSercviceImpl.class);

    private final static String searchURL = "/services/application/get";
    private final static String deleteURL = "/services/application/delete";
    private final static String addURL = "/services/application/insert";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    public ApplicationSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve applications
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getApplications( int page, int pagesize, SearchOption<ApplicationCore> searchOption, Adminuser adminuser) {
        logger.info("getApplications(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreURL + searchURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem<ApplicationCore> responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                /*
                 * Filter applications according to user rights
                 */
                filterApplications(responseItem,adminuser);
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve applications.", e);
            return new ResponseItem(0, new ArrayList<ApplicationCore>());
        }
    }


    /**
     * Calls core REST service to delete applications
     *
     * @param ids
     * @return
     */
    public boolean deleteApplications(List<Integer> ids) {
        logger.info("deleteApplications(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (Integer id : ids) {
                targetURL = coreURL + deleteURL +"/" +id;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete application.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add an application
     *
     * @param application
     * @return
     */
    public ApplicationCore addApplication(ApplicationCore application) {
        logger.info("addApplication(): (" +application.getName() +")");

        String targetURL = "";
        ApplicationCore appResponse = null;

        try {
            targetURL = coreURL + addURL + "/" + application.getName();
            logger.info("Target url: " + targetURL);
            /*
             * Get the service response
             */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, null);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                appResponse = objectMapper.readValue(response.getBody(),
                    objectMapper.getTypeFactory().constructType(ApplicationCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add application.", e);
        }

        return appResponse;
    }


    /**
     * Filters applications in order to allow only the ones user is allowed to view
     */
    private void filterApplications(ResponseItem<ApplicationCore> responseItem, Adminuser adminuser) {
        if (adminuser.getUserapplicationaccess() == null || adminuser.getUserapplicationaccess().isEmpty() ||
                responseItem.getItems() == null || responseItem.getItems().isEmpty()) {
            return;
        }
        List<ApplicationCore> items = objectMapper.convertValue(responseItem.getItems(),
                new TypeReference<List<ApplicationCore>>() { });

        /*
         * Get list of allowed applications for the specific user
         */
        Iterator<ApplicationCore> iterator = items.iterator();
        while (iterator.hasNext()) {
            ApplicationCore application = iterator.next();
            /*
             * Check if user is allowed to view application.
             * If not, remove it from list
             */
            if (!adminuser.getUserapplicationaccess().contains(application.getId())) {
                iterator.remove();
            }
        }
        /*
         * Finally update the number of total elements
         */
        responseItem.setItems(items);
        responseItem.setTotal(responseItem.getItems().size());
    }

}
