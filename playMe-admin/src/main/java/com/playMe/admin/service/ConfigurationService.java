package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.*;

import java.util.List;


/**
 * @author Dimitris Charilas
 */
public interface ConfigurationService {

    List<String> getMessageCodes();

    DisplayTextCore getMessage(int id, String locale);

    ResponseItem getEventDefinitions(int page, int pagesize, SearchOption<EventDefinitionCore> searchOption);

    ResponseItem getEventActions(int page, int pagesize, SearchOption<EventActionCore> searchOption);

    ResponseItem getProfileWeights(int page, int pagesize, SearchOption<ProfileWeightsCore> searchOption);

    ResponseItem getMessages(int page, int pagesize, SearchOption<DisplayTextCore> searchOption);

    ResponseItem getSettings(int page, int pagesize, SearchOption<ConfigurationCore> searchOption);

    ResponseItem getUserMessages(int page, int pagesize, SearchOption<UserMessageCodesCore> searchOption);



    EventDefinitionCore addEventDefinition(EventDefinitionCore definition);

    EventActionCore addEventAction(EventActionCore action);

    DisplayTextCore addMessage(DisplayTextCore message);

    List<DisplayTextCore> addMessages(List<DisplayTextCore> messages);

    ProfileWeightsCore addProfileWeight(ProfileWeightsCore weight);

    ConfigurationCore addSetting(ConfigurationCore config);

    UserMessageCodesCore addUserMessage(UserMessageCodesCore message);



    boolean deleteEventDefinitions(List<Integer> ids);

    boolean deleteEventActions(List<String> ids);

    boolean deleteMessages(List<String> ids);

    boolean deleteProfileWeights(List<String> ids);

    boolean deleteSettings(List<Integer> ids);

    boolean deleteUserMessages(List<String> ids);

}
