package com.playMe.admin.service;

import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface ApplicationService {

    ResponseItem getApplications(int page, int pagezize, SearchOption<ApplicationCore> searchOption, Adminuser adminuser);

    boolean deleteApplications(List<Integer> ids);

    ApplicationCore addApplication(ApplicationCore application);

}
