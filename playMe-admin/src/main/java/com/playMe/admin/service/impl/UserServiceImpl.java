package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.BadgeCore;
import com.playMe.admin.model.core.UserCore;
import com.playMe.admin.service.UserService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Service that handles all requests related to users
 *
 * @author Dimitris Charilas
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final static String searchURL = "/services/user/get";
    private final static String addURL = "/services/user/insert";
    private final static String userBadgesURL = "/services/user/badges/get";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    public UserServiceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve applications
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getUsers(int page, int pagesize, SearchOption<UserCore> searchOption) {
        logger.info("getUsers(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreURL + searchURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve users.", e);
            return new ResponseItem(0, new ArrayList<UserCore>());
        }
    }


    /**
     * Calls core REST service to add a user
     *
     * @param user
     * @return
     */
    public UserCore addUser(UserCore user) {
        logger.info("addUser(): (" +user.getUsername() +")");

        String targetURL = "";
        UserCore userResponse = null;

        try {
            targetURL = coreURL + addURL;
            logger.info("Target url: " + targetURL);
               /*
                * Get the service response
                */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, user);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                userResponse = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(UserCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add user.", e);
        }

        return userResponse;
    }



    /**
     * Calls core REST service to retrieve user badges
     *
     * @param appId
     * @param userId
     * @return
     */
    public List<BadgeCore> getUserBadges(int appId, long userId) {
        logger.info("getUsers(): (" +appId +"," +userId +")");

        try {
            String targetURL = coreURL + userBadgesURL +"/" +appId +"/" +userId;
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                BadgeCore[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(BadgeCore[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve users.", e);
            return new ArrayList<BadgeCore>();
        }
    }
}
