package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.model.core.ApplicationLevelCore;
import com.playMe.admin.service.LevelService;
import com.playMe.admin.util.RestUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that handles all requests related to levels
 *
 * @author Dimitris Charilas
 */
@Service("levelService")
public class LevelSercviceImpl implements LevelService {

    private static final Logger logger = LoggerFactory.getLogger(LevelSercviceImpl.class);

    private final static String searchURL = "/services/level/get";
    private final static String deleteURL = "/services/level/delete";
    private final static String addURL = "/services/level/insert";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    public LevelSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve levels
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getLevels(int page, int pagesize, SearchOption<ApplicationLevelCore> searchOption) {
        logger.info("getLevels(): (" +page +"," +pagesize +")");

        /*
         * No id exists here, so update the name of the sort field
         */
        if (!StringUtils.isEmpty(searchOption.getSortField()) && "id".equalsIgnoreCase(searchOption.getSortField())) {
            searchOption.setSortField("level.id");
        }

        try {
            String targetURL = coreURL + searchURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve levels.", e);
            return new ResponseItem(0, new ArrayList<ApplicationCore>());
        }
    }


    /**
     * Calls core REST service to delete levels
     *
     * @param ids
     * @return
     */
    public boolean deleteLevels(List<String> ids) {
        logger.info("deleteLevels(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                /*
                 * Split the id in two parts. The first one is the message id
                 * and the second one the locale
                 */
                String levelId = id.substring(0,id.indexOf("$"));
                String appId = id.substring(id.indexOf("$")+1);

                targetURL = coreURL + deleteURL +"/" +levelId +"/" +appId;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete level.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add a level
     *
     * @param level
     * @return
     */
    public ApplicationLevelCore addLevel(ApplicationLevelCore level) {
        logger.info("addLevel(): (" +level.getLevel().getName() +")");

        String targetURL = "";
        ApplicationLevelCore levelResponse = null;

        try {
            targetURL = coreURL + addURL;
            logger.info("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, level);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                levelResponse = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ApplicationLevelCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add level.", e);
        }

        return levelResponse;
    }

}
