package com.playMe.admin.service;

import com.playMe.admin.db.entity.Adminuser;

/**
 * Service used for admin user login
 *
 * @author Dimitris Charilas
 */
public interface LoginService {

    Adminuser authenticateUser(String username, String password);
}
