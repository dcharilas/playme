package com.playMe.admin.service;


import com.playMe.admin.constant.StatisticsEnum;
import com.playMe.admin.model.StatisticsItem;
import com.playMe.admin.model.core.StatisticsDateItem;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface StatisticsService {

    List<StatisticsItem> getStatistics(StatisticsEnum stat);

    List<StatisticsItem> getStatistics(StatisticsEnum stat, int applId);

    List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days);

    List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId);
}
