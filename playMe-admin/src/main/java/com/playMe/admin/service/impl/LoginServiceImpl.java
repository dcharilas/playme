package com.playMe.admin.service.impl;

import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.db.entity.Roleactionaccess;
import com.playMe.admin.db.entity.Rolepageaccess;
import com.playMe.admin.db.entity.Userapplicationaccess;
import com.playMe.admin.db.repository.AdminuserRepository;
import com.playMe.admin.db.repository.RoleactionaccessRepository;
import com.playMe.admin.db.repository.RolepageaccessRepository;
import com.playMe.admin.db.repository.UserapplicationaccessRepository;
import com.playMe.admin.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


/**
 * Service used for admin user login
 *
 * @author Dimitris Charilas
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private AdminuserRepository adminuserRepository;

    @Autowired
    private UserapplicationaccessRepository userapplicationaccessRepository;

    @Autowired
    private RolepageaccessRepository rolepageaccessRepository;

    @Autowired
    private RoleactionaccessRepository roleactionaccessRepository;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    /**
     * Authenticate admin user credentials in DB
     *
     * @param username
     * @param password
     * @return
     */
    public Adminuser authenticateUser(String username, String password) {
        logger.info("authenticateUser(): (" +username +"," +password +")");

        try {
            Adminuser adminuser = adminuserRepository.findByUsernameAndPassword(username,password);
            if (adminuser != null) {
                logger.info("Found user with id " +adminuser.getId() +" and role " +adminuser.getRole().getRole());
                /*
                 * Set role to null to avoid lazy loading issues
                 */
                adminuser.getRole().setAdminusers(null);
                /*
                 * Add allowed pages to role
                 */
                List<Rolepageaccess> pages = rolepageaccessRepository.findByRoleid(adminuser.getRole().getId());
                List<String> pageIds = new LinkedList<>();
                for (Rolepageaccess access : pages) {
                    pageIds.add(access.getPageid());
                }
                adminuser.getRole().setAllowedPages(pageIds);
                /*
                 * Add allowed actions to role
                 */
                List<Roleactionaccess> actions = roleactionaccessRepository.findByRoleid(adminuser.getRole().getId());
                adminuser.getRole().setRoleactionaccesses(actions);
                /*
                 * Load allowed applications
                 */
                List<Userapplicationaccess> rights = userapplicationaccessRepository.findByUserid(adminuser.getId());
                List<Integer> applicationIds = new LinkedList<>();
                for (Userapplicationaccess access : rights) {
                    applicationIds.add(access.getApplicationid());
                }
                adminuser.setUserapplicationaccess(applicationIds);
            }
            return adminuser;
        } catch (Exception e) {
            logger.error("Could not find user.", e);
            return null;
        }
    }
}
