package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.BadgeCore;
import com.playMe.admin.model.core.UserCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface UserService {

    ResponseItem getUsers(int page, int pagezize, SearchOption<UserCore> searchOption);

    UserCore addUser(UserCore application);

    List<BadgeCore> getUserBadges(int appId, long userId);
}
