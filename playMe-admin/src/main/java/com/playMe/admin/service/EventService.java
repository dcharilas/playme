package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ExternalEventCore;
import com.playMe.admin.model.core.InternalEventCore;

/**
 * @author Dimitris Charilas
 */
public interface EventService {

    ResponseItem getExternalEvents(int page, int pagezize, SearchOption<ExternalEventCore> searchOption);

    ResponseItem getInternalEvents(int page, int pagesize, SearchOption<InternalEventCore> searchOption);

    ExternalEventCore addExternalEvent(ExternalEventCore event);
}
