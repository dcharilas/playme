package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationLevelCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface LevelService {

    ResponseItem getLevels(int page, int pagezize, SearchOption<ApplicationLevelCore> searchOption);

    boolean deleteLevels(List<String> ids);

    ApplicationLevelCore addLevel(ApplicationLevelCore level);
}
