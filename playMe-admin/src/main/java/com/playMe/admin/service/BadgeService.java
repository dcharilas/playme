package com.playMe.admin.service;

import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationBadgeCore;

import java.util.List;

/**
 * @author Dimitris Charilas
 */
public interface BadgeService {

    ResponseItem getBadges(int page, int pagezize, SearchOption<ApplicationBadgeCore> searchOption);

    boolean deleteBadges(List<String> ids);

    ApplicationBadgeCore addBadge(ApplicationBadgeCore badge);
}
