package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.constant.StatisticsEnum;
import com.playMe.admin.model.StatisticsItem;
import com.playMe.admin.model.core.StatisticsDateItem;
import com.playMe.admin.service.StatisticsService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Service that handles all requests related to statistics
 *
 * @author Dimitris Charilas
 */
@Service("statisticsService")
public class StatisticsSercviceImpl implements StatisticsService {

    private static final Logger logger = LoggerFactory.getLogger(StatisticsSercviceImpl.class);
    /*
     * User statistics
     */
    private final static String userGenderURL = "/services/statistics/get/user/gender";
    private final static String userAgeURL = "/services/statistics/get/user/age";
    private final static String applicationUsersURL = "/services/statistics/get/application/users";
    private final static String userLevelSequenceURL = "/services/statistics/get/user/level/sequence";
    /*
     * Event statistics
     */
    private final static String externalEventPerDayURL = "/services/statistics/get/externalevent/day";
    private final static String eventPerDayURL = "/services/statistics/get/event/day";
    /*
     * Application statistics
     */
    private final static String applExternalEventPerDayURL = "/services/statistics/get/externalevent/day";
    private final static String applEventPerDayURL = "/services/statistics/get/event/day";
    private final static String applUsersPerDayURL = "/services/statistics/get/application/users/day";
    /*
     * Reward statistics
     */
    private final static String rewardsURL = "/services/statistics/get/reward/application";
    private final static String rewardTypesURL = "/services/statistics/get/reward/type";
    private final static String redeemedPointsPerDayURL = "/services/statistics/get/reward/points/day";


    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    public StatisticsSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Return statistics
     *
     * @param stat
     * @return
     */
    public List<StatisticsItem> getStatistics(StatisticsEnum stat) {
        logger.info("getStatistics():" +stat.value());

        String targetURL = coreURL;

        try {
           /*
            * Call the proper statistics method
            */
            if (StatisticsEnum.USER_GENDER.equals(stat)) {
                targetURL += userGenderURL;
            } else if (StatisticsEnum.APPLICATION_USERS.equals(stat)) {
                targetURL += applicationUsersURL;
            } else if (StatisticsEnum.USERS_LEVEL_SEQUENCE.equals(stat)) {
                targetURL += userLevelSequenceURL;
            } else if (StatisticsEnum.USER_AGE.equals(stat)) {
                targetURL += userAgeURL;
            } else if (StatisticsEnum.REDEEMED_REWARDS.equals(stat)) {
                targetURL += rewardsURL;
            }

            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                StatisticsItem[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(StatisticsItem[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve statistics.", e);
            return null;
        }
    }


    /**
     * Return statistics including application
     *
     * @param stat
     * @param applId
     * @return
     */
    public List<StatisticsItem> getStatistics(StatisticsEnum stat, int applId) {
        logger.info("getStatistics():" +stat.value());

        String targetURL = coreURL;

        try {
           /*
            * Call the proper statistics method
            */
            if (StatisticsEnum.USER_GENDER.equals(stat)) {
                targetURL += userGenderURL +"/" +applId;
            } else if (StatisticsEnum.USER_AGE.equals(stat)) {
                targetURL += userAgeURL +"/" +applId;
            } else if (StatisticsEnum.REDEEMED_REWARD_PER_TYPE.equals(stat)) {
                targetURL += rewardTypesURL +"/" +applId;
            }

            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                StatisticsItem[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(StatisticsItem[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve statistics.", e);
            return null;
        }
    }


    /**
     * Return statistics including date
     *
     * @param stat
     * @param days
     * @return
     */
    public List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days) {
        logger.info("getDateStatistics():" +stat.value());

        String targetURL = coreURL;

        try {
           /*
            * Call the proper statistics method
            */
            if (StatisticsEnum.EXTERNAL_EVENTS_PER_DAY.equals(stat)) {
                targetURL += externalEventPerDayURL +"/" +days;
            } else if (StatisticsEnum.INTERNAL_EVENTS_PER_DAY.equals(stat)) {
                targetURL += eventPerDayURL +"/" +days;
            }

            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                StatisticsDateItem[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(StatisticsDateItem[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve statistics.", e);
            return null;
        }
    }


    /**
     * Return statistics including date and application
     *
     * @param stat
     * @param days
     * @param applId
     * @return
     */
    public List<StatisticsDateItem> getDateStatistics(StatisticsEnum stat, int days, int applId) {
        logger.info("getDateStatistics():" +stat.value());

        String targetURL = coreURL;

        try {
           /*
            * Call the proper statistics method
            */
            if (StatisticsEnum.APPLICATION_EXTERNAL_EVENTS_PER_DAY.equals(stat)) {
                targetURL += applExternalEventPerDayURL +"/" +days +"/" +applId;
            } else if (StatisticsEnum.APPLICATION_INTERNAL_EVENTS_PER_DAY.equals(stat)) {
                targetURL += applEventPerDayURL +"/" +days +"/" +applId;
            } else if (StatisticsEnum.APPLICATION_USERS_PER_DAY.equals(stat)) {
                targetURL += applUsersPerDayURL +"/" +days +"/" +applId;
            } else if (StatisticsEnum.REDEEMED_POINTS_PER_DAY.equals(stat)) {
                targetURL += redeemedPointsPerDayURL +"/" +days +"/" +applId;
            }

            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.GET, null);
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                StatisticsDateItem[] responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(StatisticsDateItem[].class));
                return Arrays.asList(responseItem);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve statistics.", e);
            return null;
        }
    }

}
