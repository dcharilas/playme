package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ExternalEventCore;
import com.playMe.admin.model.core.InternalEventCore;
import com.playMe.admin.service.EventService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Service that handles all requests related to events
 *
 * @author Dimitris Charilas
 */
@Service("eventService")
public class EventSercviceImpl implements EventService {

    private static final Logger logger = LoggerFactory.getLogger(EventSercviceImpl.class);

    private final static String searchExternalURL = "/services/event/external/get";
    private final static String addExternalURL = "/services/event/external/insert";

    private final static String searchInternalURL = "/services/event/internal/get";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;

    public EventSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve external events
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getExternalEvents(int page, int pagesize, SearchOption<ExternalEventCore> searchOption) {
        logger.info("getExternalEvents(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreURL + searchExternalURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve events.", e);
            return new ResponseItem(0, new ArrayList<ExternalEventCore>());
        }
    }


    /**
     * Calls core REST service to add an event
     *
     * @param event
     * @return
     */
    public ExternalEventCore addExternalEvent(ExternalEventCore event) {
        logger.info("addExternalEvent()");

        String targetURL = "";
        ExternalEventCore eventResponse = null;

        try {
            targetURL = coreURL + addExternalURL;
            logger.info("Target url: " + targetURL);
               /*
                * Get the service response
                */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, event);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                eventResponse = objectMapper.readValue(response.getBody(),
                     objectMapper.getTypeFactory().constructType(ExternalEventCore.class));

                logger.info("event " +event);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not add event.", e);
        }

        return eventResponse;
    }


    /**
     * Calls core REST service to retrieve internal events
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getInternalEvents(int page, int pagesize, SearchOption<InternalEventCore> searchOption) {
        logger.info("getInternalEvents(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreURL + searchInternalURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve internal events.", e);
            return new ResponseItem(0, new ArrayList<InternalEventCore>());
        }
    }
}
