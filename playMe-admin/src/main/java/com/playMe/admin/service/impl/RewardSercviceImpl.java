package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.RewardCore;
import com.playMe.admin.service.RewardService;
import com.playMe.admin.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that handles all requests related to rewards
 *
 * @author Dimitris Charilas
 */
@Service("rewardService")
public class RewardSercviceImpl implements RewardService {

    private static final Logger logger = LoggerFactory.getLogger(RewardSercviceImpl.class);

    private final static String searchURL = "/services/reward/get";
    private final static String deleteURL = "/services/reward/delete";
    private final static String addURL = "/services/reward/insert";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    public RewardSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve rewards
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getRewards(int page, int pagesize, SearchOption<RewardCore> searchOption) {
        logger.info("getRewards(): (" +page +"," +pagesize +")");

        try {
            String targetURL = coreURL + searchURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve rewards.", e);
            return new ResponseItem(0, new ArrayList<RewardCore>());
        }
    }


    /**
     * Calls core REST service to delete rewards
     *
     * @param ids
     * @return
     */
    public boolean deleteRewards(List<Integer> ids) {
        logger.info("deleteBadges(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (Integer id : ids) {
                targetURL = coreURL + deleteURL +"/" +id;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete rewards.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add a rewards
     *
     * @param reward
     * @return
     */
    public RewardCore addReward(RewardCore reward) {
        logger.info("addReward(): (" +reward.getName() +"," +reward.getApplication().getId() +")");

        String targetURL = "";
        RewardCore rewardResponse = null;

        try {
            targetURL = coreURL + addURL;
            logger.info("Target url: " + targetURL);
            /*
             * Get the service response
             */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, reward);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                rewardResponse = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(RewardCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add reward.", e);
        }

        return rewardResponse;
    }

}
