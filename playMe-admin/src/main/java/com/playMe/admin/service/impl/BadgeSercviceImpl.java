package com.playMe.admin.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationBadgeCore;
import com.playMe.admin.service.BadgeService;
import com.playMe.admin.util.RestUtil;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service that handles all requests related to badges
 *
 * @author Dimitris Charilas
 */
@Service("badgeService")
public class BadgeSercviceImpl implements BadgeService {

    private static final Logger logger = LoggerFactory.getLogger(BadgeSercviceImpl.class);

    private final static String searchURL = "/services/badge/get";
    private final static String deleteURL = "/services/badge/delete";
    private final static String addURL = "/services/badge/insert";

    private ObjectMapper objectMapper;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;
    

    public BadgeSercviceImpl() {
        objectMapper = new ObjectMapper();
    }


    /**
     * Calls core REST service to retrieve badges
     *
     * @param page
     * @param pagesize
     * @return
     */
    public ResponseItem getBadges(int page, int pagesize, SearchOption<ApplicationBadgeCore> searchOption) {
        logger.info("getBadges(): (" +page +"," +pagesize +")");

        /*
         * No id exists here, so update the name of the sort field
         */
        if (!StringUtils.isEmpty(searchOption.getSortField()) && "id".equalsIgnoreCase(searchOption.getSortField())) {
            searchOption.setSortField("badge.id");
        }

        try {
            String targetURL = coreURL + searchURL +"/paging/" +page +"/" +pagesize +"/sorting/" +searchOption.getSortField() +"/" +searchOption.getSortDir();
            logger.debug("Target url: " + targetURL);
           /*
            * Get the service response
            */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.POST, searchOption.getForm());
            if (response.getStatusCode() == HttpStatus.OK) {
                /*
                 * Parse the response
                 */
                ResponseItem responseItem = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ResponseItem.class));
                return responseItem;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error("Could not retrieve badges.", e);
            return new ResponseItem(0, new ArrayList<ApplicationBadgeCore>());
        }
    }


    /**
     * Calls core REST service to delete badges
     *
     * @param ids
     * @return
     */
    public boolean deleteBadges(List<String> ids) {
        logger.info("deleteBadges(): (" +ids +")");

        String targetURL = "";
        Boolean result = false;

        try {
            for (String id : ids) {
                /*
                 * Split the id in two parts. The first one is the message id
                 * and the second one the locale
                 */
                String badgeId = id.substring(0,id.indexOf("$"));
                String appId = id.substring(id.indexOf("$")+1);

                targetURL = coreURL + deleteURL +"/" +badgeId +"/" +appId;
                logger.info("Target url: " +targetURL);
               /*
                * Get the service response
                */
                ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.DELETE, null);
                if (response.getStatusCode() == HttpStatus.OK) {
                    /*
                     * Parse the response
                     */
                    result = objectMapper.readValue(response.getBody(),
                            objectMapper.getTypeFactory().constructType(Boolean.class));
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error("Could not delete badge.", e);
        }

        return result;
    }



    /**
     * Calls core REST service to add a badge
     *
     * @param badge
     * @return
     */
    public ApplicationBadgeCore addBadge(ApplicationBadgeCore badge) {
        logger.info("addBadge(): (" +badge.getBadge().getName() +")");

        String targetURL = "";
        ApplicationBadgeCore badgeResponse = null;

        try {
            targetURL = coreURL + addURL;
            logger.info("Target url: " + targetURL);
            /*
             * Get the service response
             */
            ResponseEntity<byte[]> response = RestUtil.callRestUrl(targetURL, HttpMethod.PUT, badge);
            if (response.getStatusCode() == HttpStatus.OK) {
               /*
                * Parse the response
                */
                badgeResponse = objectMapper.readValue(response.getBody(),
                        objectMapper.getTypeFactory().constructType(ApplicationBadgeCore.class));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("Could not add badge.", e);
        }

        return badgeResponse;
    }

}
