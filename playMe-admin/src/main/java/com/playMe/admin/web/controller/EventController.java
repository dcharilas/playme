package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.EventDefinitionCore;
import com.playMe.admin.model.core.ExternalEventCore;
import com.playMe.admin.model.core.InternalEventCore;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.ConfigurationService;
import com.playMe.admin.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Handles requests for the statistics page.
 */
@Controller
public class EventController {

	private static final Logger logger = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private EventService eventService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private ConfigurationService configurationService;


    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    @ModelAttribute("listOfEventTypes")
    public List<EventDefinitionCore> listOfEventTypes() {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<EventDefinitionCore> responseItem = configurationService.getEventDefinitions(1,1000,searchOption);
        List<EventDefinitionCore> listOfEventTypes = responseItem.getItems();
        return listOfEventTypes;
    }


    /**
     * Get method for events page
     */
    @RequestMapping(value = "/event", method = RequestMethod.GET)
    public String getEventsPage(Model model) {
        logger.info("getEventsPage()");
        return "pages/events";
    }


    /**
     * Get method for actions page
     */
    @RequestMapping(value = "/action", method = RequestMethod.GET)
    public String getActionsPage(Model model) {
        logger.info("getActionsPage()");
        return "pages/actions";
    }


    /**
     * Returns a list of external events
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/event/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getExternalEvents(@RequestBody SearchOption<ExternalEventCore> searchOption){
        logger.info("getExternalEvents(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((ExternalEventCore) searchOption.getForm()).toString());
        }
        return eventService.getExternalEvents(page, pagesize, searchOption);
    }


    /**
     * Add a new event
     *
     * @param event
     * @return
     */
    @RequestMapping(value = "/event/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addExternalEvent(@RequestBody ExternalEventCore event){
        logger.info("addExternalEvent():");

        ExternalEventCore response = eventService.addExternalEvent(event);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns a list of external events
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/action/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getInternalEvents(@RequestBody SearchOption<InternalEventCore> searchOption){
        logger.info("getInternalEvents(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((InternalEventCore) searchOption.getForm()).toString());
        }
        return eventService.getInternalEvents(page, pagesize, searchOption);
    }


}
