package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/application")
public class ApplicationController  {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @Autowired
    private ApplicationService applicationService;



    /**
     * Get method for application page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getApplicationPage(Model model) {
        logger.info("getApplicationPage()");
        return "pages/applications";
    }

    private Adminuser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        return user;
    }


    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    @ResponseBody
    public List<ApplicationCore> listOfApplications(HttpServletRequest request) {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<ApplicationCore> responseItem = applicationService.getApplications(1,1000,searchOption, getUser(request));
        List<ApplicationCore> listOfApplications = responseItem.getItems();
        return listOfApplications;
    }

    /**
     * Returns a list of applications
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getApplications(@RequestBody SearchOption<ApplicationCore> searchOption, HttpServletRequest request){
        logger.info("getApplications(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((ApplicationCore) searchOption.getForm()).toString());
        }
        return applicationService.getApplications(page,pagesize,searchOption, getUser(request));
    }


    /**
     * Deletes a list of applications
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteApplications(@RequestBody List<Integer> idList){
        logger.info("deleteApplications():");
        boolean response = applicationService.deleteApplications(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Add a new application
     *
     * @param application
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addApplication(@RequestBody ApplicationCore application){
        logger.info("addApplication():");
        ApplicationCore response = applicationService.addApplication(application);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
