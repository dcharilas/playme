package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.*;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.BadgeService;
import com.playMe.admin.service.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the system configuration page
 */
@Controller
@RequestMapping(value = "/configuration")
public class ConfigurationController  {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationController.class);

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private BadgeService badgeService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    @ModelAttribute("listOfApplications")
    public List<ApplicationCore> listOfApplications(HttpServletRequest request) {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<ApplicationCore> responseItem = applicationService.getApplications(1,1000,searchOption, getUser(request));
        List<ApplicationCore> listOfApplications = responseItem.getItems();
        return listOfApplications;
    }

    private Adminuser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        return user;
    }

    @ModelAttribute("listOfEventTypes")
    public List<EventDefinitionCore> listOfEventTypes() {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<EventDefinitionCore> responseItem = configurationService.getEventDefinitions(1,1000,searchOption);
        List<EventDefinitionCore> listOfEventTypes = responseItem.getItems();
        return listOfEventTypes;
    }

    @ModelAttribute("listOfBadges")
    public List<ApplicationBadgeCore> listOfBadges() {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("badge.name");
        searchOption.setSortDir("asc");
        ResponseItem<ApplicationBadgeCore> responseItem = badgeService.getBadges(1, 1000, searchOption);
        List<ApplicationBadgeCore> listOfBadges = responseItem.getItems();
        return listOfBadges;
    }

    @ModelAttribute("listOfUserMessageCodes")
    public List<String> listOfUserMessageCodes(HttpServletRequest request) {
        HttpSession session = request.getSession();
        List<String> codes = (List<String>) session.getAttribute(Constants.userMessageCodesAttrName);
        /*
         * If codes are not loaded yet, load them
         */
        if (codes == null || codes.size() == 0) {
            codes = configurationService.getMessageCodes();
            session.setAttribute(Constants.userMessageCodesAttrName, codes);
        }
        return codes;
    }


    /**
     * Get method for configuration page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getConfigurationPage(Model model) {
        logger.info("getConfigurationPage()");
        return "pages/configuration";
    }

    /**
     * Get method for settings page
     */
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String getSettingsPage(Model model) {
        logger.info("getSettingsPage()");
        return "pages/config/settings";
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public String getMessagesPage(Model model) {
        logger.info("getMessagesPage()");
        return "pages/config/messages";
    }

    @RequestMapping(value = "/eventDef", method = RequestMethod.GET)
    public String getEventDefinitionsPage(Model model) {
        logger.info("getEventDefinitionsPage()");
        return "pages/config/eventDef";
    }

    @RequestMapping(value = "/eventActions", method = RequestMethod.GET)
    public String getEventActionsPage(Model model) {
        logger.info("getEventActionsPage()");
        return "pages/config/eventActions";
    }

    @RequestMapping(value = "/profileWeights", method = RequestMethod.GET)
    public String getProfileWeightsPage(Model model) {
        logger.info("getProfileWeightsPage()");
        return "pages/config/profileWeights";
    }

    @RequestMapping(value = "/usermessages", method = RequestMethod.GET)
    public String getUserMessagesPage(Model model) {
        logger.info("getMessagesPage()");
        return "pages/config/usermessages";
    }



    /**
     * Returns a list of messages
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/messages/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getMessages(@RequestBody SearchOption<DisplayTextCore> searchOption){
        logger.info("getMessages(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");

        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
                logger.info("Search Criteria: " + ((DisplayTextCore) searchOption.getForm()).toString());
        }
        return configurationService.getMessages(page, pagesize, searchOption);
    }


    /**
     * Returns a list of user messages
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/usermessages/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getUserMessages(@RequestBody SearchOption<UserMessageCodesCore> searchOption){
        logger.info("getUserMessages(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");

        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " + ((UserMessageCodesCore) searchOption.getForm()).toString());
        }
        return configurationService.getUserMessages(page, pagesize, searchOption);
    }


    /**
     * Returns a list of event definitions
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/eventDef/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getEventDefinitions(@RequestBody SearchOption<EventDefinitionCore> searchOption){
        logger.info("getEventDefinitions(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " + ((EventDefinitionCore) searchOption.getForm()).toString());
        }
        return configurationService.getEventDefinitions(page, pagesize, searchOption);
    }


    /**
     * Returns a list of profile weights
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/profileWeights/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getProfileWeights(@RequestBody SearchOption<ProfileWeightsCore> searchOption){
        logger.info("getProfileWeights(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " + ((ProfileWeightsCore) searchOption.getForm()).toString());
        }
        return configurationService.getProfileWeights(page, pagesize, searchOption);
    }


    /**
     * Deletes a list of event definitions
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/eventDef/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteEventDefinitions(@RequestBody List<Integer> idList){
        logger.info("deleteEventDefinitions():");
        boolean response = configurationService.deleteEventDefinitions(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of profile weights
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/profileWeights/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteProfileWeights(@RequestBody List<String> idList){
        logger.info("deleteProfileWeights():");
        boolean response = configurationService.deleteProfileWeights(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    /**
     * Add a new event definition
     *
     * @param event
     * @return
     */
    @RequestMapping(value = "/eventDef/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addEventDefinition(@RequestBody EventDefinitionCore event){
        logger.info("addEventDefinition():");
        EventDefinitionCore response = configurationService.addEventDefinition(event);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Add a new profile weight
     *
     * @param weight
     * @return
     */
    @RequestMapping(value = "/profileWeights/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addProfileWeight(@RequestBody ProfileWeightsCore weight){
        logger.info("addProfileWeight():");
        ProfileWeightsCore response = configurationService.addProfileWeight(weight);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a list of event actions
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/eventActions/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getEventActions(@RequestBody SearchOption<EventActionCore> searchOption){
        logger.info("getEventActions(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " + ((EventActionCore) searchOption.getForm()).toString());
        }
        return configurationService.getEventActions(page, pagesize, searchOption);
    }


    /**
     * Add a new event action
     *
     * @param action
     * @return
     */
    @RequestMapping(value = "/eventActions/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addEventAction(@RequestBody EventActionCore action){
        logger.info("addEventAction():");
        EventActionCore response = configurationService.addEventAction(action);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of event actions
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/eventActions/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteEventActions(@RequestBody List<String> idList){
        logger.info("deleteEventActions():");
        boolean response = configurationService.deleteEventActions(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of event actions
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/messages/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteMessages(@RequestBody List<String> idList){
        logger.info("deleteMessages():");
        boolean response = configurationService.deleteMessages(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Add a new message
     *
     * @param text
     * @return
     */
    @RequestMapping(value = "/messages/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addMessage(@RequestBody DisplayTextCore text){
        logger.info("addMessage():");
        DisplayTextCore response = configurationService.addMessage(text);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Add multiple messages
     *
     * @param texts
     * @return
     */
    @RequestMapping(value = "/messages/add/bulk", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addMessages(@RequestBody List<DisplayTextCore> texts){
        logger.info("addMessages():");

        List<DisplayTextCore> response = configurationService.addMessages(texts);
        if (response == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }



    /**
     * Deletes a list of user messages
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/usermessages/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteUserMessages(@RequestBody List<String> idList){
        logger.info("deleteUserMessages():");
        boolean response = configurationService.deleteUserMessages(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Add a new user message
     *
     * @param text
     * @return
     */
    @RequestMapping(value = "/usermessages/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addUserMessage(@RequestBody UserMessageCodesCore text){
        logger.info("addMessage():");
        UserMessageCodesCore response = configurationService.addUserMessage(text);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns a list of settings
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/settings/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getSettings(@RequestBody SearchOption<ConfigurationCore> searchOption){
        logger.info("getSettings(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " + ((ConfigurationCore) searchOption.getForm()).toString());
        }
        return configurationService.getSettings(page, pagesize, searchOption);
    }


    /**
     * Add a new setting
     *
     * @param config
     * @return
     */
    @RequestMapping(value = "/settings/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addSetting(@RequestBody ConfigurationCore config){
        logger.info("addSetting()");
        ConfigurationCore response = configurationService.addSetting(config);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of settings
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/settings/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteSettings(@RequestBody List<Integer> idList){
        logger.info("deleteEventActions():");
        boolean response = configurationService.deleteSettings(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
