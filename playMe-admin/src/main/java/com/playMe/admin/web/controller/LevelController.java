package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationLevelCore;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.LevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Handles requests for the levels page.
 */
@Controller
@RequestMapping(value = "/level")
public class LevelController {

	private static final Logger logger = LoggerFactory.getLogger(LevelController.class);

    @Autowired
    private LevelService levelService;

    @Autowired
    private ApplicationService applicationService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    /**
     * Get method for levels page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getLevelsPage(Model model) {
        logger.info("getLevelsPage()");
        return "pages/levels";
    }


    /**
     * Returns a list of levels
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getLevels(@RequestBody SearchOption<ApplicationLevelCore> searchOption){
        logger.info("getLevels(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((ApplicationLevelCore) searchOption.getForm()).toString());
        }
        return levelService.getLevels(page, pagesize, searchOption);
    }


    /**
     * Add a new level
     *
     * @param level
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addLevel(@RequestBody ApplicationLevelCore level){
        logger.info("addLevel():");
        ApplicationLevelCore levelResponse = levelService.addLevel(level);
        if (levelResponse != null) {
            return new ResponseEntity(levelResponse, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of levels
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteLevels(@RequestBody List<String> idList){
        logger.info("deleteLevels():");
        boolean response = levelService.deleteLevels(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
