package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.LoginInfo;
import com.playMe.admin.service.LoginService;
import com.playMe.admin.web.util.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Handles requests for the login page.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @Autowired
    private ApplicationUtil applicationUtil;

	/**
	 * Get method for login page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getLogin(Model model) {
		logger.info("getLogin()");
		return "login";
	}


    /**
     * Get application version
     */
    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getVersion() {
        String version = applicationUtil.getVersionFromManifest();
        logger.info("getVersion(): " +version);
        return new ResponseEntity(version, HttpStatus.OK);
    }

    /**
     * Login for existing users
     *
     * @param loginInfo
     * @return
     */
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity loginUser(@RequestBody LoginInfo loginInfo, HttpServletRequest request) {

        logger.info("loginUser(): " +loginInfo.getUsername() +"," +loginInfo.getPassword());

        if (loginInfo.getUsername() != null && loginInfo.getPassword() != null) {
            /*
             * Perform the search
             */
            Adminuser adminuser = loginService.authenticateUser(loginInfo.getUsername(),loginInfo.getPassword());
            if (adminuser != null) {
                logger.info("Logged in user with username " +adminuser.getUsername()  +" and password " +adminuser.getPassword());
                /*
                 * Save user in session
                 */
                HttpSession session = request.getSession();
                session.setAttribute(Constants.userAttrName, adminuser);

                return new ResponseEntity(adminuser, HttpStatus.OK);
            } else {
                logger.info("User could not be logged in");
                return new ResponseEntity(null, HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Remove user from session
     *
     * @param request
     */
    @RequestMapping(value = "/invalidate", method = RequestMethod.GET)
    public void logoutUser(HttpServletRequest request, HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        logger.info("logoutUser(): " +user.getUsername());

        session.setAttribute(Constants.userAttrName, null);
        response.sendRedirect("login");
    }

}
