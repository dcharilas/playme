package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.model.core.BadgeCore;
import com.playMe.admin.model.core.UserCore;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the users page.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController  {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationService applicationService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    @ModelAttribute("listOfApplications")
    public List<ApplicationCore> listOfApplications(HttpServletRequest request) {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<ApplicationCore> responseItem = applicationService.getApplications(1,1000,searchOption, getUser(request));
        List<ApplicationCore> listOfApplications = responseItem.getItems();
        return listOfApplications;
    }

    private Adminuser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        return user;
    }

    /**
     * Get method for users page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getUsersPage(Model model) {
        logger.info("getUserPage()");
        return "pages/users";
    }

    /**
     * Returns a list of users
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getUsers(@RequestBody SearchOption<UserCore> searchOption){
        logger.info("getUsers(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((UserCore) searchOption.getForm()).toString());
        }
        return userService.getUsers(page, pagesize, searchOption);
    }


    /**
     * Add a new user
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addUser(@RequestBody UserCore user){
        logger.info("addUser():");
        UserCore response = userService.addUser(user);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns a list of badges for the specified user
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/badges/search", method = RequestMethod.POST)
    @ResponseBody
    public List<BadgeCore> getUserBadges(@RequestBody UserCore user){
        logger.info("getUserBadges(): (" +user.getApplication().getId() +"," +user.getId() +")");
        return userService.getUserBadges(user.getApplication().getId(), user.getId());
    }

}
