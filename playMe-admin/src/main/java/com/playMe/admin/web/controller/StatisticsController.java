package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.constant.StatisticsEnum;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.StatisticsItem;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.model.core.StatisticsDateItem;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the statistics page.
 */
@Controller
@RequestMapping(value = "/statistics")
public class StatisticsController  {

	private static final Logger logger = LoggerFactory.getLogger(StatisticsController.class);

    @Autowired
    private StatisticsService statisticsService;

    @Autowired
    private ApplicationService applicationService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    @ModelAttribute("listOfApplications")
    public List<ApplicationCore> listOfApplications(HttpServletRequest request) {
        SearchOption searchOption= new SearchOption();
        searchOption.setSortField("name");
        searchOption.setSortDir("asc");
        ResponseItem<ApplicationCore> responseItem = applicationService.getApplications(1,1000,searchOption, getUser(request));
        List<ApplicationCore> listOfApplications = responseItem.getItems();
        return listOfApplications;
    }

    private Adminuser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        return user;
    }


    /**
     * Get method for statistics page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getStatisticsPage(Model model) {
        logger.info("getStatisticsPage()");
        return "pages/statistics";
    }


    /**
     * Get method for user statistics page
     */
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUserStatisticsPage(Model model) {
        logger.info("getUserStatisticsPage()");
        return "pages/statistics/userStatistics";
    }

    /**
     * Get method for event statistics page
     */
    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public String getEventStatisticsPage(Model model) {
        logger.info("getEventStatisticsPage()");
        return "pages/statistics/eventStatistics";
    }

    /**
     * Get method for application statistics page
     */
    @RequestMapping(value = "/applications", method = RequestMethod.GET)
    public String getApplicationStatisticsPage(Model model) {
        logger.info("getApplicationStatisticsPage()");
        return "pages/statistics/applicationStatistics";
    }

    /**
     * Get method for reward statistics page
     */
    @RequestMapping(value = "/rewards", method = RequestMethod.GET)
    public String getRewardStatisticsPage(Model model) {
        logger.info("getRewardStatisticsPage()");
        return "pages/statistics/rewardStatistics";
    }


    /**
     * Returns gender statistics
     *
     * @return
     */
    @RequestMapping(value = "/user/gender", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getUserGenderStatistics(){
        return statisticsService.getStatistics(StatisticsEnum.USER_GENDER);
    }

    /**
     * Returns gender statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/user/gender", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getUserGenderStatistics(@RequestParam int applId){
        return statisticsService.getStatistics(StatisticsEnum.USER_GENDER,applId);
    }


    /**
     * Returns age statistics
     *
     * @return
     */
    @RequestMapping(value = "/user/age", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getUserAgeStatistics(){
        return statisticsService.getStatistics(StatisticsEnum.USER_AGE);
    }

    /**
     * Returns age statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/user/age", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getUserAgeStatistics(@RequestParam int applId){
        return statisticsService.getStatistics(StatisticsEnum.USER_AGE,applId);
    }


    /**
     * Returns application users statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/users", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getApplicationUsersStatistics(){
        return statisticsService.getStatistics(StatisticsEnum.APPLICATION_USERS);
    }


    /**
     * Returns users per level sequence statistics
     *
     * @return
     */
    @RequestMapping(value = "/user/level/sequence", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getUserLevelSequenceStatistics(){
        return statisticsService.getStatistics(StatisticsEnum.USERS_LEVEL_SEQUENCE);
    }


    /**
     * Returns external events per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/externalevent/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getExternalEventsPerDayStatistics(@RequestParam int days){
        return statisticsService.getDateStatistics(StatisticsEnum.EXTERNAL_EVENTS_PER_DAY, days);
    }


    /**
     * Returns events per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/event/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getEventsPerDayStatistics(@RequestParam int days){
        return statisticsService.getDateStatistics(StatisticsEnum.INTERNAL_EVENTS_PER_DAY, days);
    }

    /**
     * Returns application external events per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/externalevent/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getApplicationExternalEventsPerDayStatistics(@RequestParam int days, @RequestParam int applId){
        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_EXTERNAL_EVENTS_PER_DAY, days, applId);
    }


    /**
     * Returns application events per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/event/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getApplicationEventsPerDayStatistics(@RequestParam int days, @RequestParam int applId){
        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_INTERNAL_EVENTS_PER_DAY, days, applId);
    }


    /**
     * Returns application users per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/application/user/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getApplicationUsersPerDayStatistics(@RequestParam int days, @RequestParam int applId){
        return statisticsService.getDateStatistics(StatisticsEnum.APPLICATION_USERS_PER_DAY, days, applId);
    }


    /**
     * Returns redeemed reward statistics
     *
     * @return
     */
    @RequestMapping(value = "/reward/application", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getRewardsPerApplicationtatistics(){
        return statisticsService.getStatistics(StatisticsEnum.REDEEMED_REWARDS);
    }

    /**
     * Returns redeemed reward for application statistics
     *
     * @return
     */
    @RequestMapping(value = "/reward/type", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsItem> getRewardsPerTypetatistics(@RequestParam int applId){
        return statisticsService.getStatistics(StatisticsEnum.REDEEMED_REWARD_PER_TYPE,applId);
    }

    /**
     * Returns redeemed points per day statistics
     *
     * @return
     */
    @RequestMapping(value = "/reward/points/day", method = RequestMethod.GET)
    @ResponseBody
    public List<StatisticsDateItem> getRedeemedPointsPerDayStatistics(@RequestParam int days, @RequestParam int applId){
        return statisticsService.getDateStatistics(StatisticsEnum.REDEEMED_POINTS_PER_DAY, days, applId);
    }

}
