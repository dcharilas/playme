package com.playMe.admin.web.util;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Created by Mimitatsis on 20/1/2016.
 */
@Component
public class ApplicationUtil {

    private static String version = null;

    private final static String versionAttributeName = "Admin-Version";

    /**
     * Returns the version out of the resource MANIFEST.MF.
     */
    public String getVersionFromManifest() {

        if (version != null) {
            return version;
        }

        String source = getClass().getResource(
                "/" + this.getClass().getName().replace('.', '/') + ".class")
                .toExternalForm();
        String jar = source.substring(0, source.lastIndexOf("WEB-INF"))
                + "/META-INF/MANIFEST.MF";
        try {
            URL jarUrl = new URL(jar);
            Manifest manifest = new Manifest(jarUrl.openStream());
            Attributes attributes = manifest.getMainAttributes();

            if (attributes.getValue(versionAttributeName) != null) {
                version = attributes.getValue(versionAttributeName);
            } else {
                version = "N/A";
            }
        } catch (IOException e) {
            version = "N/A";
        }
        return version;
    }
}
