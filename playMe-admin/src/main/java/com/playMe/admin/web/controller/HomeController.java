package com.playMe.admin.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.playMe.admin.model.WebProperties;
import com.playMe.admin.constant.Constants;
import com.playMe.admin.util.JsonUtil;

/**
 * Handles requests for the home page.
 */
@Controller
@RequestMapping(value = "/home")
public class HomeController  {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


	/**
	 * Get method for home page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getHome(ModelAndView model) {
		logger.info("getHome()");
        /*
         * Fill properties
         */
        WebProperties props = new WebProperties();
        props.setCoreURL(coreURL);
        model.addObject(Constants.propertiesAttrName, JsonUtil.toJacksonJSON(props));
        model.setViewName("home");
		return model;
	}


}
