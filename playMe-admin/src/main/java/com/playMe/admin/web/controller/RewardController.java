package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.RewardCore;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.RewardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Handles requests for the rewards page.
 */
@Controller
@RequestMapping(value = "/reward")
public class RewardController {

	private static final Logger logger = LoggerFactory.getLogger(RewardController.class);

    @Autowired
    private RewardService rewardService;

    @Autowired
    private ApplicationService applicationService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    /**
     * Get method for rewards page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getRewardsPage(Model model) {
        logger.info("getRewardsPage()");
        return "pages/rewards";
    }


    /**
     * Returns a list of rewards
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getRewards(@RequestBody SearchOption<RewardCore> searchOption){
        logger.info("getRewards(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((RewardCore) searchOption.getForm()).toString());
        }
        return rewardService.getRewards(page, pagesize, searchOption);
    }


    /**
     * Add a new reward
     *
     * @param reward
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addReward(@RequestBody RewardCore reward){
        logger.info("addReward():");
        RewardCore response = rewardService.addReward(reward);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of rewards
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteRewards(@RequestBody List<Integer> idList){
        logger.info("deleteRewards():");
        boolean response = rewardService.deleteRewards(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
