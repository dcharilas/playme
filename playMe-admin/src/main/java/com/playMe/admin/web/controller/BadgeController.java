package com.playMe.admin.web.controller;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.model.ResponseItem;
import com.playMe.admin.model.SearchOption;
import com.playMe.admin.model.core.ApplicationBadgeCore;
import com.playMe.admin.model.core.ApplicationCore;
import com.playMe.admin.service.ApplicationService;
import com.playMe.admin.service.BadgeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Handles requests for the badges page.
 */
@Controller
@RequestMapping(value = "/badge")
public class BadgeController {

	private static final Logger logger = LoggerFactory.getLogger(BadgeController.class);

    @Autowired
    private BadgeService badgeService;

    @Autowired
    private ApplicationService applicationService;

    @Value("#{appProperties['core.url.context']}")
    private String coreURL;


    /**
     * Get method for badges page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getBadgesPage(Model model) {
        logger.info("getBadgesPage()");
        return "pages/badges";
    }


    /**
     * Returns a list of badges
     *
     * @param searchOption
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseItem getBadges(@RequestBody SearchOption<ApplicationBadgeCore> searchOption){
        logger.info("getBadges(): (" +searchOption.getPage() +"," +searchOption.getPagesize() +")");
        int page = searchOption != null ? searchOption.getPage() : Constants.defaultStartPage;
        int pagesize = searchOption != null ? searchOption.getPagesize() : Constants.defaultPageSize;
        if (searchOption != null && searchOption.getForm() != null) {
            logger.info("Search Criteria: " +((ApplicationBadgeCore) searchOption.getForm()).toString());
        }
        return badgeService.getBadges(page, pagesize, searchOption);
    }


    /**
     * Add a new badge
     *
     * @param badge
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addBadge(@RequestBody ApplicationBadgeCore badge){
        logger.info("addBadge():");
        ApplicationBadgeCore response = badgeService.addBadge(badge);
        if (response != null) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Deletes a list of badges
     *
     * @param idList
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteBadges(@RequestBody List<String> idList){
        logger.info("deleteBadges():");
        boolean response = badgeService.deleteBadges(idList);
        if (response) {
            return new ResponseEntity(response, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
