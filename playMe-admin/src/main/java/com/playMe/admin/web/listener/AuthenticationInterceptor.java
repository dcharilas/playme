package com.playMe.admin.web.listener;

import com.playMe.admin.constant.Constants;
import com.playMe.admin.db.entity.Adminuser;
import com.playMe.admin.db.entity.Roleactionaccess;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Interceptor for user authentication purposes
 *
 * @author Charilas
 *
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {

        logger.debug("AuthenticationInterceptor: Pre-handle");

        String path = request.getRequestURI().replace(request.getContextPath(),"");
        /*
         * Allow always login page
         */
        if (path.startsWith("/login")) {
            return true;
        } else {
            /*
             * Check if user exists in session
             */
            Adminuser user = getUser(request);
            if (user == null) {
                if (path.endsWith("/home.action")) {
                    response.sendRedirect(request.getContextPath() +"/login.action");
                }
                return false;
            } else {
                /*
                 * Admin user is allowed to do anything
                 */
                if (Constants.adminRole.equalsIgnoreCase(user.getRole().getRole())){
                    return true;
                }
                /*
                 * Check if this action is allowed for the current (non-admin) user
                 */
                List<Roleactionaccess> actions = user.getRole().getRoleactionaccesses();
                String mapping = path.substring(1).replace(".action","");
                String actionPath;
                for (Roleactionaccess action : actions) {
                    actionPath = StringUtils.isEmpty(action.getAction()) ? action.getMapping() :
                            (action.getMapping() +"/" +action.getAction());
                    if (actionPath.equals(mapping)) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest request,
           HttpServletResponse response, Object handler,
           ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request,
          HttpServletResponse response, Object handler, Exception ex) {


    }


    private Adminuser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Adminuser user = (Adminuser) session.getAttribute(Constants.userAttrName);
        return user;
    }
}