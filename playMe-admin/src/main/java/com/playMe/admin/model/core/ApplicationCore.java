package com.playMe.admin.model.core;


import java.io.Serializable;

/**
 * Class representing the Application object retrieved from core module
 *
 */
public class ApplicationCore implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "ApplicationCore [id=" + id + ", name=" + name +"]";
    }
}