package com.playMe.admin.model.core;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class ApplicationBadgeCore implements Serializable {

    private Boolean active;

    private ApplicationCore application;

    private BadgeCore badge;

    private Integer awardafterpoints;

    private Integer awardafterlevelseq;

    private Integer awardafterlogins;

    private Integer awardaftercompleteness;


    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public BadgeCore getBadge() {
        return badge;
    }

    public void setBadge(BadgeCore badge) {
        this.badge = badge;
    }

    public Integer getAwardafterlevelseq() {
        return awardafterlevelseq;
    }

    public void setAwardafterlevelseq(Integer awardafterlevelseq) {
        this.awardafterlevelseq = awardafterlevelseq;
    }

    public Integer getAwardafterpoints() {
        return awardafterpoints;
    }

    public void setAwardafterpoints(Integer awardafterpoints) {
        this.awardafterpoints = awardafterpoints;
    }

    public Integer getAwardafterlogins() {
        return awardafterlogins;
    }

    public void setAwardafterlogins(Integer awardafterlogins) {
        this.awardafterlogins = awardafterlogins;
    }

    public Integer getAwardaftercompleteness() {
        return awardaftercompleteness;
    }

    public void setAwardaftercompleteness(Integer awardaftercompleteness) {
        this.awardaftercompleteness = awardaftercompleteness;
    }

    @Override
    public String toString() {
        return "ApplicationBadgeCore [active=" + active +", "
                + (application != null ? application.toString() : "")
                + (badge != null ? badge.toString() : "")
                + ", awardafterpoints=" + awardafterpoints
                + ", awardafterlevelseq=" + awardafterlevelseq
                + ", awardafterlogins=" + awardafterlogins
                + ", awardaftercompleteness=" + awardaftercompleteness
                + "]";
    }
}
