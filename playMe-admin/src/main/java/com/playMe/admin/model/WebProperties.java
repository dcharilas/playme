package com.playMe.admin.model;

/**
 * @author Dimitris Charilas
 */
public class WebProperties {

    private static final long serialVersionUID = 1L;

    private String coreURL;

    public String getCoreURL() {
        return coreURL;
    }

    public void setCoreURL(String coreURL) {
        this.coreURL = coreURL;
    }
}
