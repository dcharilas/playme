package com.playMe.admin.model.core;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class ConfigurationCore implements Serializable {

    private int id;
    private ApplicationCore application;
    private String property;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
