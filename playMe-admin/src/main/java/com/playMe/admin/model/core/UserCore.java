package com.playMe.admin.model.core;

import java.io.Serializable;
import java.util.Date;


/**
 * Class representing the User object retrieved from core module
 *
 */
public class UserCore implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private String address;

	private int age;

	private byte[] avatar;

	private Date birthdate;

	private String email;

	private String firstname;

	private String gender;

	private String lastname;

	private String phone;

	private int points;

    private int redeemablepoints;

	private String postcode;

	private Date regDate;

	private String username;

    private LevelCore level;

    private ApplicationCore application;

    private int completeness;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRedeemablepoints() {
        return redeemablepoints;
    }

    public void setRedeemablepoints(int redeemablepoints) {
        this.redeemablepoints = redeemablepoints;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LevelCore getLevel() {
        return level;
    }

    public void setLevel(LevelCore level) {
        this.level = level;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public int getCompleteness() {
        return completeness;
    }

    public void setCompleteness(int completeness) {
        this.completeness = completeness;
    }

    @Override
    public String toString() {
        return "UserCore [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname
                + ", username=" + username + ", points=" + points + ", redeemablepoints=" + redeemablepoints
                + ", address=" + address + ", age=" + age + ", birthdate=" + birthdate
                + ", email="+ email + ", gender="+ gender   + ", phone="+ phone  + ", postcode=" + postcode
                + ", regDate=" + regDate + "," + ", completeness=" + completeness + ","
                + (application != null ? application.toString() : "") +", "
                + (level != null ? level.toString() : "")
                 + "]";
    }
}

