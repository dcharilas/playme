package com.playMe.admin.model.core;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class DisplayTextCore implements Serializable {

    private TextKey id;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TextKey getId() {
        return id;
    }

    public void setId(TextKey id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DisplayTextCore [messageId=" + (id!= null ? id.getMessageid() : "") + ", locale=" + (id != null ? id.getLocale() : "") + ", value=" + value+ "]";
    }


    public class TextKey {

        private int messageid;
        private String locale;

        public int getMessageid() {
            return messageid;
        }

        public void setMessageid(int messageid) {
            this.messageid = messageid;
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }
    }
}
