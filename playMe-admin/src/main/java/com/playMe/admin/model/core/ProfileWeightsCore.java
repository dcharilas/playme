package com.playMe.admin.model.core;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class ProfileWeightsCore implements Serializable {

    private String fieldname;
    private int weight;

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    @Override
    public String toString() {
        return "ProfileWeightsCore [fieldname=" + fieldname + ", weight=" + weight +"]";
    }

}
