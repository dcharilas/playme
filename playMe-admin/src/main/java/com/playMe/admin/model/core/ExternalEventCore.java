package com.playMe.admin.model.core;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class ExternalEventCore implements Serializable {

    private long id;

    private String comments;

    private Date eventdate;

    private int numericvalue1;

    private int numericvalue2;

    private int numericvalue3;

    private int numericvalue4;

    private String textvalue1;

    private String textvalue2;

    private String textvalue3;

    private String textvalue4;

    private ApplicationCore application;

    private UserCore user;

    private EventDefinitionCore eventdefinition;

    /*
     * Custom fields to allow date searching
     */
    private Date searchDateStart;

    private Date searchDateEnd;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getEventdate() {
        return eventdate;
    }

    public void setEventdate(Date eventdate) {
        this.eventdate = eventdate;
    }

    public int getNumericvalue1() {
        return numericvalue1;
    }

    public void setNumericvalue1(int numericvalue1) {
        this.numericvalue1 = numericvalue1;
    }

    public int getNumericvalue2() {
        return numericvalue2;
    }

    public void setNumericvalue2(int numericvalue2) {
        this.numericvalue2 = numericvalue2;
    }

    public String getTextvalue1() {
        return textvalue1;
    }

    public void setTextvalue1(String textvalue1) {
        this.textvalue1 = textvalue1;
    }

    public String getTextvalue2() {
        return textvalue2;
    }

    public void setTextvalue2(String textvalue2) {
        this.textvalue2 = textvalue2;
    }

    public int getNumericvalue3() {
        return numericvalue3;
    }

    public void setNumericvalue3(int numericvalue3) {
        this.numericvalue3 = numericvalue3;
    }

    public int getNumericvalue4() {
        return numericvalue4;
    }

    public void setNumericvalue4(int numericvalue4) {
        this.numericvalue4 = numericvalue4;
    }

    public String getTextvalue3() {
        return textvalue3;
    }

    public void setTextvalue3(String textvalue3) {
        this.textvalue3 = textvalue3;
    }

    public String getTextvalue4() {
        return textvalue4;
    }

    public void setTextvalue4(String textvalue4) {
        this.textvalue4 = textvalue4;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public UserCore getUser() {
        return user;
    }

    public void setUser(UserCore user) {
        this.user = user;
    }

    public EventDefinitionCore getEventdefinition() {
        return eventdefinition;
    }

    public void setEventdefinition(EventDefinitionCore eventdefinition) {
        this.eventdefinition = eventdefinition;
    }

    public Date getSearchDateStart() {
        return searchDateStart;
    }

    @JsonFormat(pattern="dd/MM/yyyy")
    public void setSearchDateStart(Date searchDateStart) {
        this.searchDateStart = searchDateStart;
    }

    public Date getSearchDateEnd() {
        return searchDateEnd;
    }

    @JsonFormat(pattern="dd/MM/yyyy")
    public void setSearchDateEnd(Date searchDateEnd) {
        this.searchDateEnd = searchDateEnd;
    }

    @Override
    public String toString() {
        return "ExternalEventCore [id=" + id + ", comments=" + comments + ", eventdate=" + eventdate
                + ", numericvalue1" + numericvalue1 + ", numericvalue2=" + numericvalue2
                + ", numericvalue3" + numericvalue3 + ", numericvalue4=" + numericvalue4
                + ", textvalue1=" + textvalue1 + ", textvalue2=" + textvalue2 +", "
                + ", textvalue3=" + textvalue3 + ", textvalue4=" + textvalue4 +", "
                + (application != null ? application.toString() : "") +", "
                + (user != null ? user.toString() : "") +", "
                + (eventdefinition != null ? eventdefinition.toString() : "")
                + "]";
    }
}
