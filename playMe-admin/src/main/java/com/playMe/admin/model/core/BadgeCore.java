package com.playMe.admin.model.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
public class BadgeCore implements Serializable {

    private int id;
    private int descriptionid;
    private byte[] activeimage;
    private byte[] inactiveimage;
    private String name;
    private List<DisplayTextCore> descriptions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDescriptionid() {
        return descriptionid;
    }

    public void setDescriptionid(int descriptionid) {
        this.descriptionid = descriptionid;
    }

    public byte[] getActiveimage() {
        return activeimage;
    }

    public void setActiveimage(byte[] activeimage) {
        this.activeimage = activeimage;
    }

    public byte[] getInactiveimage() {
        return inactiveimage;
    }

    public void setInactiveimage(byte[] inactiveimage) {
        this.inactiveimage = inactiveimage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DisplayTextCore> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DisplayTextCore> descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        return "BadgeCore [id=" + id + ", name=" + name + ", descriptionid=" + descriptionid+ "]";
    }
}
