package com.playMe.admin.model.core;

import java.io.Serializable;

/**
 * @author Dimitris Charilas
 */
public class EventActionCore implements Serializable {

    private int pointsaward;

    private ApplicationCore application;

    private BadgeCore badge;

    private EventDefinitionCore eventdefinition;


    public int getPointsaward() {
        return pointsaward;
    }

    public void setPointsaward(int pointsaward) {
        this.pointsaward = pointsaward;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public BadgeCore getBadge() {
        return badge;
    }

    public void setBadge(BadgeCore badge) {
        this.badge = badge;
    }

    public EventDefinitionCore getEventdefinition() {
        return eventdefinition;
    }

    public void setEventdefinition(EventDefinitionCore eventdefinition) {
        this.eventdefinition = eventdefinition;
    }

    @Override
    public String toString() {
        return "EventActionCore [pointsaward=" + pointsaward +", "
                + (application != null ? application.toString() : "") +", "
                + (badge != null ? badge.toString() : "") +", "
                + (eventdefinition != null ? eventdefinition.toString() : "")
                + "]";
    }
}
