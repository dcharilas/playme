package com.playMe.admin.model.core;


/**
 * @author Dimitris Charilas
 */
public class UserMessageCodesCore {

    private TextKey id;
    private ApplicationCore application;
    private String message;



    public class TextKey {

        private int applicationid;
        private String msgcode;
        private String locale;

        public int getApplicationid() {
            return applicationid;
        }

        public void setApplicationid(int applicationid) {
            this.applicationid = applicationid;
        }

        public String getMsgcode() {
            return msgcode;
        }

        public void setMsgcode(String msgcode) {
            this.msgcode = msgcode;
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }
    }


    public TextKey getId() {
        return id;
    }

    public void setId(TextKey id) {
        this.id = id;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "UserMessageCodesCore [getMsgcode=" + (id!= null ? id.getMsgcode() : "") + ", applicationid=" + (id != null ? id.getApplicationid() : "")
                + ", locale=" + (id != null ? id.getLocale() : "") + ", message=" + message+ "]";
    }
}
