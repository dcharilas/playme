package com.playMe.admin.model.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
public class ApplicationLevelCore implements Serializable {

    private Boolean active;

    private int pointsrequired;

    private int sequence;

    private ApplicationCore application;

    private LevelCore level;

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getPointsrequired() {
        return pointsrequired;
    }

    public void setPointsrequired(int pointsrequired) {
        this.pointsrequired = pointsrequired;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public LevelCore getLevel() {
        return level;
    }

    public void setLevel(LevelCore level) {
        this.level = level;
    }


    @Override
    public String toString() {
        return "ApplicationLevelCore [active=" + active + ", pointsrequired=" + pointsrequired
                + ", sequence=" + sequence +", "
                + (application != null ? application.toString() : "") +", "
                + (level != null ? level.toString() : "")
                + "]";
    }
}
