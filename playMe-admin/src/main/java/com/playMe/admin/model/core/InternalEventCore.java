package com.playMe.admin.model.core;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dimitris Charilas
 */
public class InternalEventCore implements Serializable {

    private long id;

    private BadgeCore badge;

    private Date eventdate;

    private LevelCore level;

    private int pointsawarded;

    private ApplicationCore application;

    private EventDefinitionCore eventdefinition;

    private ExternalEventCore externaleventhistory;

    private UserCore user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getEventdate() {
        return eventdate;
    }

    public void setEventdate(Date eventdate) {
        this.eventdate = eventdate;
    }

    public int getPointsawarded() {
        return pointsawarded;
    }

    public void setPointsawarded(int pointsawarded) {
        this.pointsawarded = pointsawarded;
    }

    public ApplicationCore getApplication() {
        return application;
    }

    public void setApplication(ApplicationCore application) {
        this.application = application;
    }

    public BadgeCore getBadge() {
        return badge;
    }

    public void setBadge(BadgeCore badge) {
        this.badge = badge;
    }

    public LevelCore getLevel() {
        return level;
    }

    public void setLevel(LevelCore level) {
        this.level = level;
    }

    public EventDefinitionCore getEventdefinition() {
        return eventdefinition;
    }

    public void setEventdefinition(EventDefinitionCore eventdefinition) {
        this.eventdefinition = eventdefinition;
    }

    public ExternalEventCore getExternaleventhistory() {
        return externaleventhistory;
    }

    public void setExternaleventhistory(ExternalEventCore externaleventhistory) {
        this.externaleventhistory = externaleventhistory;
    }

    public UserCore getUser() {
        return user;
    }

    public void setUser(UserCore user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ExternalEventCore [id=" + id + ", eventdate=" + eventdate + ", pointsawarded=" + pointsawarded +","
                + (application != null ? application.toString() : "") +", "
                + (user != null ? user.toString() : "") +", "
                + (badge != null ? badge.toString() : "") +", "
                + (level != null ? level.toString() : "") +", "
                + (eventdefinition != null ? eventdefinition.toString() : "")
                + (externaleventhistory != null ? externaleventhistory.toString() : "")
                + "]";
    }
}
