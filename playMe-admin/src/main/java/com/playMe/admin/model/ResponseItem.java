package com.playMe.admin.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dimitris Charilas
 */
public class ResponseItem<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private long total;
    private String responseMsg;

    private List<T> items;


    public ResponseItem(){}

    public ResponseItem(long total, List<T> items) {
        this.total = total;
        this.items = items;
    }


    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }
}
