myApp.controller('RewardStatisticsCtrl', ['$scope', '$http', '$translate',
    function($scope, $http, $translate) {

    $scope.redeemedRewardsURL = contextPath + '/statistics/reward/application.action';

    $scope.redeemedRewardsTitle = $translate.instant('graphMsg.appRedRewards');

    $scope.redeemedRewards = $translate.instant('graphMsg.userNum');
    $scope.applications = $translate.instant('graphMsg.applications');
    $scope.rewardNum = $translate.instant('graphMsg.rewardNum');


    initializeGraph($scope);

    /**
     * Application user data
     */
    $http.get($scope.redeemedRewardsURL)
        .success(function (data) {
            $scope.redeemedRewards = $scope.transformDataForBarChart(data);
        });


}]);
