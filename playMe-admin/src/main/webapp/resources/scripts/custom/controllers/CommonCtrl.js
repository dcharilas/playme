/**
* The Sidebar Controller
*/
myApp.controller('SidebarCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    $rootScope.navLinks = [
        {text: "navigationMsg.applications", href: "#applications", cls: "Selected", icon:"glyphicon glyphicon-globe", pageId:"page_app"},
        {text: "navigationMsg.users", href: "#users", cls: "", icon:"glyphicon glyphicon-user", pageId:"page_user"},
        {text: "navigationMsg.levels", href: "#levels", cls: "", icon:"glyphicon glyphicon-road", pageId:"page_level"},
        {text: "navigationMsg.badges", href: "#badges", cls: "", icon:"glyphicon glyphicon-star", pageId:"page_badge"},
        {text: "navigationMsg.rewards", href: "#rewards", cls: "", icon:"glyphicon glyphicon-gift", pageId:"page_gift"},
        {text: "navigationMsg.events", href: "#events", cls: "", icon:"glyphicon glyphicon-bell", pageId:"page_event"},
        {text: "navigationMsg.actions", href: "#actions", cls: "", icon:"glyphicon glyphicon-flash", pageId:"page_action"},
        {text: "navigationMsg.statistics", href:"#statistics", cls: "", icon:"glyphicon glyphicon-eye-open", pageId:"page_stat", parentRef : "statistics"},
        {text: "navigationMsg.sysconfig", href: "#configuration", cls: "", icon:"glyphicon glyphicon-wrench", pageId:"page_config", parentRef : "configuration"}
    ];
    $scope.navLinks = $rootScope.navLinks;

    $rootScope.statisticsLinks = [
        {text: "navigationMsg.users", href: "#userStatistics", cls: "", childRef : "users", pageId:"page_stat_user"},
        {text: "navigationMsg.events", href: "#eventStatistics", cls: "", childRef : "events", pageId:"page_stat_event"},
        {text: "navigationMsg.applications", href: "#applicationStatistics", cls: "", childRef : "applications", pageId:"page_stat_app"},
        {text: "navigationMsg.rewards", href: "#rewardStatistics", cls: "", childRef : "rewards", pageId:"page_stat_reward"}
    ];
    $rootScope.configLinks = [
        {text: "navigationMsg.settings", href: "#settings", cls: "", childRef : "settings", pageId:"page_config_set"},
        {text: "navigationMsg.messages", href: "#messages", cls: "", childRef : "messages", pageId:"page_config_text"},
        {text: "navigationMsg.usermessages", href: "#usermessages", cls: "", childRef : "usermessages", pageId:"page_config_usermsg"},
        {text: "navigationMsg.eventDef", href: "#eventDef", cls: "", childRef : "eventDef", pageId:"page_config_event"},
        {text: "navigationMsg.eventActions", href: "#eventActions", cls: "", childRef : "eventActions", pageId:"page_config_action"},
        {text: "navigationMsg.profileWeights", href: "#profileWeights", cls: "", childRef : "profileWeights", pageId:"page_config_weight"}
    ]


    /**
     * Marks selected option from sidebar
     */
    $scope.$on('$routeChangeStart', function (event, currRoute, prevRoute) {
        if (currRoute != null && currRoute.templateUrl != null){
            var currentPath = currRoute.templateUrl.replace(".action","");
            /*
             * Sidebar menu
             */
            angular.forEach($scope.navLinks, function(index) {
                //clear all classes from the items
                if (index.href.indexOf(currentPath) != -1) {
                    //add to the selected item the "Selected" class
                    index.cls = "Selected";
                } else {
                    index.cls = "";
                    // check children as well
                    if (index.subItems) {
                        var href = '';
                        angular.forEach(index.subItems, function(child) {
                            href = index.parentRef + "/" +child.childRef;
                            if (href.indexOf(currentPath) != -1) {
                                //add to the selected item the "Selected" class
                                child.cls = "Selected";
                            } else {
                                child.cls = "";
                            }
                        });
                    }
                }
            });
        }
    });

    /**
     * Listener for sidebar menu
     *
     * @param item
     */
    $scope.showChilds = function(item){
        /*
         * Close all items
         */
        angular.forEach($scope.navLinks , function(nav) {
            nav.active = false;
        });
        if (item.subItems) {
            item.active = !item.active;
        }
    };

    /**
     * Determine whether user is allowed to view this page
     *
     * @param pageId
     */
    $scope.showPage = function(pageId) {
        var role = $rootScope.globals.currentUser.roleObj;
        if (!exists(role)) {
            return false;
        }
        if (role.role == 'ADMIN') {
            return true;
        } else {
            return role.allowedPages.contains(pageId);
        }
    }
    $rootScope.showPage = $scope.showPage;

}]);

/**
* The Header Controller
*/
myApp.controller('HeaderCtrl', ['$scope', '$translate', 'LoginService', '$rootScope', '$http',
    function($scope, $translate, LoginService, $rootScope, $http) {

    var loginURL = contextPath +'/login.action';
    var logoutURL = contextPath +'/login/invalidate.action';

    $scope.constants = {
  		logout_btn : "Log out",
  		loginText : "You are logged in as:"
  	};

    $scope.languages = [
	    {name:'en', value:'en'},
	    {name:'fr', value:'fr'}
  	];
  	$scope.language = $scope.languages[0];

    /**
     * Display user information
     */
    if (exists($rootScope.globals) && exists($rootScope.globals.currentUser)) {
        $scope.user = {
            username : $rootScope.globals.currentUser.name,
            profile : $rootScope.globals.currentUser.role
        };
    }

  	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
	};

    /**
     * Logout link handler
     */
    $scope.logout = function() {
        LoginService.ClearCredentials();
        /*
         * Invalidate the session and redirect to login page
         */
        $http.get(logoutURL)
            .success(function (data) {
            })
            .error(function(){
                location.href = loginURL;
            })
    }

}]);

/**
* The Footer Controller
*/
myApp.controller('FooterCtrl', ['$scope', '$http',
    function ($scope, $http) {

        var versionURL = contextPath + '/login/version.action';

        $scope.constants = {
            aboutLink: "About"
        };
        //TODO
        $scope.aboutlink = {url: "GEN_About.html"};

        /*
         * Display application version
         */
        $http.get(versionURL)
            .success(function (response) {
                $scope.version = response;
            })
    }]);