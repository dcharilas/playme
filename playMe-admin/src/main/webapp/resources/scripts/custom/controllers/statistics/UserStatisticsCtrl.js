myApp.controller('UserStatisticsCtrl', ['$scope', '$http', '$translate',
    function($scope, $http, $translate) {

    $scope.userGenderURL = contextPath + '/statistics/user/gender.action';
    $scope.userAgeURL = contextPath + '/statistics/user/age.action';
    $scope.applicationUsersURL = contextPath +"/statistics/application/users.action";
    $scope.userLevelSequenceURL = contextPath +"/statistics/user/level/sequence.action";

    $scope.genderUsersTitle = $translate.instant('graphMsg.genderUsers');
    $scope.appUsersTitle = $translate.instant('graphMsg.appUsers');
    $scope.levelSeqUsersTitle = $translate.instant('graphMsg.levelSeqUsers');
    $scope.ageUsersTitle = $translate.instant('graphMsg.ageUsers');


    $scope.userNum = $translate.instant('graphMsg.userNum');
    $scope.applications = $translate.instant('graphMsg.applications');
    $scope.levelSeq = $translate.instant('graphMsg.levelSeq');


    initializeGraph($scope);

    /**
     * User gender data
     */
    $http.get($scope.userGenderURL)
        .success(function (data) {
            /*
             * Fix labels
             */
            if (exists(data)) {
                for (var i=0; i<data.length; i++) {
                    if (data[i].code == 'M') {
                        data[i].code = "Male";
                    } else if (data[i].code == 'F') {
                        data[i].code = "Female";
                    } else {
                        data[i].code = "N/A";
                    }
                }
            }
            $scope.genderData = $scope.transformDataForDonutChart(data);;
        });

    /**
     * Application user data
     */
    $http.get($scope.applicationUsersURL)
        .success(function (data) {
            $scope.applicationUserData = $scope.transformDataForBarChart(data);
        });

    /**
     * User level sequence data
     */
    $http.get($scope.userLevelSequenceURL)
        .success(function (data) {
            $scope.userLevelSequenceData = $scope.transformDataForBarChart(data);
        });

    /**
      * User age data
      */
     $http.get($scope.userAgeURL)
         .success(function (data) {
            $scope.userAgeData = $scope.transformDataForDonutChart(data);
         });
}]);
