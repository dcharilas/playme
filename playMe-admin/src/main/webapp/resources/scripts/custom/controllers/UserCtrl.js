myApp.controller('UserCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/user/search.action';
        $scope.addURL = contextPath + '/user/add.action';
        $scope.badgesURL = contextPath + '/user/badges/search.action';
        $scope.modalController = 'UserModalCtrl';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);

        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Fix date format
             */
            $scope.modalForm.regDate = getDateTimeDisplay($filter, $scope.modalForm.regDate);
            $scope.modalForm.birthdate = getDateDisplay($filter, $scope.modalForm.birthdate);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputApplNameModal').attr("disabled", true);
                $('#inputUsernameModal').attr("disabled", true);
                $('#inputRegDateModal').children().attr("disabled", true);
                /*
                 * Set the application in combobox
                 */
                $('#inputApplNameModal').val($scope.modalForm.application.id);
                /*
                 * Set the avatar
                 */
                if (exists($scope.modalForm.avatar)) {
                    $('#photo-id').attr('src', "data:image/jpeg;base64,"+$scope.modalForm.avatar);
                }
            }, 100);
        }


        /**
         * Retrieves and displays badges for the specific user
         *
         * @param item
         */
        $scope.showUserBadges = function(item) {
            $http.post($scope.badgesURL, item)
                .success(function (data) {
                    if (exists(data)) {
                        $scope.badges = data;
                        $modal.open({
                            animation: true,
                            templateUrl: 'myModalContent2.html',
                            controller: $scope.modalController,
                            scope: $scope,
                            size: 'lg',
                            resolve: {
                                items: function () {
                                    //return $scope.items;
                                }
                            }
                        });
                    } else {
                        bootbox.alert($translate.instant('notificationMsg.noBadges'));
                    }
                })
                .error(function (data) {
                    bootbox.alert($translate.instant('notificationMsg.noBadges'));
                });
        }
    }]);



/**
 * Controller for modal instance
 */
myApp.controller('UserModalCtrl', function ($scope, $http, $modalInstance) {

    /**
     * Configure the datepicker fields
     */
    $('.modal-content').on('shown', function() {
        $('#inputBirthDateModal').datepicker({
            format: 'dd/mm/yyyy'
        });
    });

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();
        /*
         * Fix dates
         */
        var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
        if (exists($scope.modalForm.regDate)) {
            cloneForm.regDate = getDateTimeFromString($scope.modalForm.regDate);
        }
        if (exists($scope.modalForm.birthdate)) {
            cloneForm.birthdate = getDateFromString($scope.modalForm.birthdate);
        }
        /*
         * Format uploaded image
         */
        if (exists($scope.modalForm.avatar)) {
            cloneForm.avatar = fixImageForUpload($scope.modalForm.avatar);
        }

            $http.post($scope.addURL, cloneForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.uploadHandler = function ($this) {
        //$("#upload-file-info").html($(this).val());
        $('#photo-id').attr('ng-src',$scope.modalForm.avatar);
    };

});