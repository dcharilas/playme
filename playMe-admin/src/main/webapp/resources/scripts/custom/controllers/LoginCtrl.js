myApp.controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$cookieStore', 'LoginService',
    function ($scope, $rootScope, $location, $cookieStore, LoginService) {

        var homeURL = contextPath +'/home.action';
        /**
         *  Reset user information
         */
        LoginService.ClearCredentials();

        /**
         * Login button handler
         */
        $scope.login = function () {
            $scope.dataLoading = true;
            /*
             * Call login service
             */
            LoginService.Login($scope.username, $scope.password, function (response) {
                if (response.success) {
                    LoginService.SetCredentials($scope.username, $scope.password, response);
                    /*
                     * Redirect to home page
                     */
                    location.href = homeURL;
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };

}]);
