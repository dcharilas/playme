myApp.controller('SettingsCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/configuration/settings/search.action';
        $scope.deleteURL = contextPath + '/configuration/settings/delete.action';
        $scope.addURL = contextPath + '/configuration/settings/add.action';
        $scope.modalController = 'SettingsModalCtrl';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);

        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputApplNameModal').attr("disabled", true);
            }, 100);
        }
    }]);


myApp.controller('MessagesCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/configuration/messages/search.action';
        $scope.deleteURL = contextPath + '/configuration/messages/delete.action';
        $scope.addURL = contextPath + '/configuration/messages/add.action';
        $scope.modalController = 'MessagesModalCtrl';

        $scope.searchForm = {
            value: '',
            id: {
                locale: ''
            }
        }

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);


        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputMessageIdModal').attr("disabled", true);
                /*
                 * Set the locale in combobox
                 */
                $('#inputLocaleModal').val($scope.modalForm.locale);
            }, 100);
        }
    }]);



myApp.controller('UserMessagesCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/configuration/usermessages/search.action';
        $scope.deleteURL = contextPath + '/configuration/usermessages/delete.action';
        $scope.addURL = contextPath + '/configuration/usermessages/add.action';
        $scope.modalController = 'UserMessagesModalCtrl';

        $scope.searchForm = {
            message: '',
            id: {
                locale: '',
                msgcode: '',
                applicationid: ''
            }
        }

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);


        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputApplNameModal').attr("disabled", true);
                /*
                 * Set the application in combox
                 */
                $('#inputApplNameModal').val($scope.modalForm.application.id);
                /*
                 * Set the locale in combobox
                 */
                $('#inputLocaleModal').val($scope.modalForm.locale);
            }, 100);
        }
    }]);



myApp.controller('EventActionsCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/configuration/eventActions/search.action';
        $scope.deleteURL = contextPath + '/configuration/eventActions/delete.action';
        $scope.addURL = contextPath + '/configuration/eventActions/add.action';
        $scope.modalController = 'EventActionsModalCtrl';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);


        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputApplNameModal').attr("disabled", true);
                /*
                 * Set the application in combox
                 */
                $('#inputApplNameModal').val($scope.modalForm.application.id);
                /*
                 * Set the event in combox
                 */
                $('#inputEventNameModal').val($scope.modalForm.eventdefinition.id);
                $scope.filterBadges();
                /*
                 * Set the badge in combox
                 */
                if (exists($scope.modalForm.badge)) {
                    $('#inputBadgeModal').val($scope.modalForm.badge.id);
                }
            }, 100);
        }

        /**
         * Filter available badges according to selected application
         */
        $scope.filterBadges = function() {
            /*
             * Filter badges according to application
             */
            var selectedApplId = $scope.modalForm.application.id;
            for (var i=0; i<document.getElementById("inputBadgeModal").options.length; i++) {
                if (document.getElementById("inputBadgeModal").options[i].title == '') {
                    document.getElementById("inputBadgeModal").options[i].hidden = false;
                } else {
                    document.getElementById("inputBadgeModal").options[i].hidden = (document.getElementById("inputBadgeModal").options[i].title != selectedApplId);
                }
            }
            /*
             * Reset the value
             */
            document.getElementById("inputBadgeModal").value = '';
        }
    }]);


myApp.controller('EventDefCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate',
    function($scope, $http, $filter, ngTableParams, $translate) {

        $scope.searchURL = contextPath + '/configuration/eventDef/search.action';
        $scope.deleteURL = contextPath + '/configuration/eventDef/delete.action';
        $scope.addURL = contextPath + '/configuration/eventDef/add.action';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);
    }]);


myApp.controller('ProfileWeightsCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/configuration/profileWeights/search.action';
        $scope.deleteURL = contextPath + '/configuration/profileWeights/delete.action';
        $scope.addURL = contextPath + '/configuration/profileWeights/add.action';
        $scope.modalController = 'ProfileWeightsModalCtrl';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);


        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                $('#inputFieldNameModal').attr("disabled", true);
            }, 100);
        }
    }]);


/**
 * Controller for modal instance
 */
myApp.controller('EventActionsModalCtrl', function ($scope, $http, $modalInstance) {

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();
        /*
         * Remove attributes that do not exist in model first
         */
        var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
        delete cloneForm.id;
        if (exists($scope.modalForm.badge)) {
            delete cloneForm.badge.descriptions;
            delete cloneForm.badge.activeimage;
            delete cloneForm.badge.inactiveimage;
            if (!exists($scope.modalForm.badge.id)) {
                delete cloneForm.badge;
            }
        }

        $http.post($scope.addURL, cloneForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


/**
 * Controller for modal instance
 */
myApp.controller('MessagesModalCtrl', function ($scope, $http, $modalInstance) {


    /**
     * Set default message id to 0
     */
    if (!exists($scope.modalForm.messageid)) {
        $scope.modalForm.messageid = 0;
    }

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();
        /*
         * Remove attributes that do not exist in model first
         */
        var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
        cloneForm.id = {};
        if (exists($scope.modalForm.locale)) {
            cloneForm.id.locale = $scope.modalForm.locale;
        }
        delete cloneForm.locale;
        if (exists($scope.modalForm.messageid)) {
            cloneForm.id.messageid = $scope.modalForm.messageid;
        }
        delete cloneForm.messageid;
        $http.post($scope.addURL, cloneForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});



/**
 * Controller for modal instance
 */
myApp.controller('UserMessagesModalCtrl', function ($scope, $http, $modalInstance) {

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();
        /*
         * Remove attributes that do not exist in model first
         */
        var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
        cloneForm.id = {};
        if (exists($scope.modalForm.locale)) {
            cloneForm.id.locale = $scope.modalForm.locale;
        }
        delete cloneForm.locale;
        if (exists($scope.modalForm.msgcode)) {
            cloneForm.id.msgcode = $scope.modalForm.msgcode;
        }
        delete cloneForm.msgcode;
        if (exists($scope.modalForm.application) && exists($scope.modalForm.application.id)) {
            cloneForm.id.applicationid = $scope.modalForm.application.id;
        }
        delete cloneForm.applicationid;

        $http.post($scope.addURL, cloneForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});



/**
 * Controller for modal instance
 */
myApp.controller('ProfileWeightsModalCtrl', function ($scope, $http, $modalInstance) {

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();
        /*
         * Remove attributes that do not exist in model first
         */
        var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
        delete cloneForm.id;

        $http.post($scope.addURL, cloneForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});



/**
 * Controller for modal instance
 */
myApp.controller('SettingsModalCtrl', function ($scope, $http, $modalInstance) {

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();

        $http.post($scope.addURL, $scope.modalForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
