myApp.controller('StatisticsCtrl',['$scope','$rootScope','TabService',
    function($scope, $rootScope, TabService){

        $scope.items = $rootScope.statisticsLinks;
        $scope.showPage = $rootScope.showPage;
        $scope.prefix = "statistics";

        /**
         * Add listener to tab items
         */
        TabService.initialize($scope, $rootScope);
}]);


myApp.controller('ConfigurationCtrl',['$scope','$rootScope','TabService',
    function($scope, $rootScope, TabService){

        $scope.items = $rootScope.configLinks;
        $scope.showPage = $rootScope.showPage;
        $scope.prefix = "configuration";

        /**
         * Add listener to tab items
         */
        TabService.initialize($scope, $rootScope);
    }]);