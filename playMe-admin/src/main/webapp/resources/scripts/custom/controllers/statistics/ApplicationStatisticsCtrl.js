myApp.controller('ApplicationStatisticsCtrl', ['$scope', '$http', '$translate', '$rootScope',
    function($scope, $http, $translate, $rootScope) {

    $scope.eventsPerDayURL = contextPath +"/statistics/application/externalevent/day.action";
    $scope.actionsPerDayURL = contextPath +"/statistics/application/event/day.action";
    $scope.usersPerDayURL = contextPath +"/statistics/application/user/day.action";
    $scope.userGenderURL = contextPath + '/statistics/application/user/gender.action';
    $scope.userAgeURL = contextPath + '/statistics/application/user/age.action';
    $scope.rewardTypeURL = contextPath + '/statistics/reward/type.action';
    $scope.redeemedPointsURL = contextPath + '/statistics/reward/points/day.action';

    $scope.eventsPerDayTitle = $translate.instant('graphMsg.eventsPerDay');
    $scope.actionsPerDayTitle = $translate.instant('graphMsg.actionsPerDay');
    $scope.usersPerDayTitle = $translate.instant('graphMsg.regUsersPerDay');
    $scope.genderUsersTitle = $translate.instant('graphMsg.genderUsers');
    $scope.ageUsersTitle = $translate.instant('graphMsg.ageUsers');
    $scope.rewardTypeTitle = $translate.instant('graphMsg.appTypeRedRewards');
    $scope.redeemedPointsTitle = $translate.instant('graphMsg.redeemedPointsPerDay');

    $scope.defaultDays = 30;


    initializeGraph($scope);


        $scope.filterApplications = function() {
            /*
             * Filter badges according to application
             */
            var allowedAppdIs = $rootScope.globals.currentUser.allowedPages;
            for (var i=0; i<document.getElementById("inputApplName").options.length; i++) {
                document.getElementById("inputApplName").options[i].hidden = (allowedAppdIs.contains(document.getElementById("inputApplName").options[i].title));
            }
            /*
             * Reset the value
             */
            document.getElementById("inputApplName").value = '';
        }


        $scope.loadGraphs = function (days) {
            if (!exists(days)) {
                days = $scope.defaultDays;
            }
            /**
             * External events per day and type
             */
            $http.get($scope.eventsPerDayURL, { params: {days: days, applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.eventsPerDayData = $scope.transformDataForLineChart(data);
                });

            /**
             * Actions per day and type
             */
            $http.get($scope.actionsPerDayURL, { params: {days: days, applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.actionsPerDayData = $scope.transformDataForLineChart(data);
                });


            /**
             * Users per day
             */
            $http.get($scope.usersPerDayURL, { params: {days: days, applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.usersPerDayData = $scope.transformDataForLineChart(data);
                });


            /**
             * User gender data
             */
            $http.get($scope.userGenderURL, { params: {applId: $scope.applicationId}})
                .success(function (data) {
                    /*
                     * Fix labels
                     */
                    if (exists(data)) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].code == 'M') {
                                data[i].code = "Male";
                            } else if (data[i].code == 'F') {
                                data[i].code = "Female";
                            } else {
                                data[i].code = "N/A";
                            }
                        }
                    }
                    $scope.genderData = $scope.transformDataForDonutChart(data);
                    ;
                });


            /**
             * User age data
             */
            $http.get($scope.userAgeURL, { params: {applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.userAgeData = $scope.transformDataForDonutChart(data);
                });


            /**
             * Redeemed rewards data
             */
            $http.get($scope.rewardTypeURL, { params: {applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.rewardTypesData = $scope.transformDataForDonutChart(data);
                });


            /**
             * Redeemed points per day
             */
            $http.get($scope.redeemedPointsURL, { params: {days: days, applId: $scope.applicationId}})
                .success(function (data) {
                    $scope.redeemedPointsData = $scope.transformDataForLineChart(data);
                });
        }


    }]);
