myApp.controller('LevelCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/level/search.action';
        $scope.deleteURL = contextPath + '/level/delete.action';
        $scope.addURL = contextPath + '/level/add.action';
        $scope.modalController = 'LevelModalCtrl';

        $scope.applicationsListURL = contextPath + '/application/listAll.action';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);

        /**
         * Get list of applications
         */
        $http.get($scope.applicationsListURL)
            .success(function (data) {
                $scope.listOfApplications = data;
            });

        /**
         * Open the modal and fill the fields
         */
        $scope.openEditModal = function(item) {
            $scope.modalForm = clone(item);
            /*
             * Convert boolean to string
             */
            if ($scope.modalForm.active != null) {
                $scope.modalForm.active = $scope.modalForm.active.toString();
            }
            /*
             * Open modal
             */
            $scope.open(false);
            /*
             * Disable fields that should not be changed
             */
            $timeout(function() {
                /*
                 * Set the sequence in combobox
                 */
                $('#inputSequenceModal').val($scope.modalForm.sequence);
                /*
                 * Set the image
                 */
                if (exists($scope.modalForm.level) && exists($scope.modalForm.level.image)) {
                    $('#photo-id').attr('src', "data:image/jpeg;base64,"+$scope.modalForm.level.image);
                }
            }, 100);
        }
}]);


/**
 * Controller for modal instance
 */
myApp.controller('LevelModalCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$modalInstance', 'ConfigService',
    function ($scope, $http, $filter, ngTableParams, $modalInstance, ConfigService) {

    $scope.addMessagesURL = contextPath + '/configuration/messages/add/bulk.action';


   /**
     * Draw grid with descriptions. If a description is missing then an
     * empty row will appear in order to add it.
     */
    $scope.handleDescriptions = function () {
        if (exists($scope.modalForm.level) && exists($scope.modalForm.level.descriptions)) {
            $scope.descriptions = $scope.modalForm.level.descriptions;
            for (var i=0; i<$scope.descriptions.length; i++) {
                $scope.descriptions[i].locale = $scope.descriptions[i].id.locale;
                $scope.descriptions[i].messageid = $scope.descriptions[i].id.messageid;
            }
            if ($scope.descriptions.length < languages.length) {
                var foundLanguage;
                for (var i=0; i<languages.length; i++) {
                    foundLanguage = false;
                    for (var j=0; j<$scope.descriptions.length; j++) {
                        if (languages[i] == $scope.descriptions[j].locale) {
                            foundLanguage = true;
                        }
                    }
                    if (!foundLanguage) {
                        $scope.descriptions.push({"messageid":"","locale":languages[i],"value":""});
                    }
                }
            }
        } else {
            $scope.descriptions = [];
            for (var i=0; i<languages.length; i++) {
                $scope.descriptions.push({"messageid":"","locale":languages[i],"value":""});
            }
        }
    }

    $scope.handleDescriptions();
    initializeEditableGrid($scope, $http, $filter, ngTableParams);

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();

        /*
         * First save descriptions
         */
        var myPromise = ConfigService.addDescriptions($scope.descriptions, $scope.modalForm);
        // wait until the promise return resolve or eject
        //"then" has 2 functions (resolveFunction, rejectFunction)
        myPromise.then(function(resolve){
            /*
             * Format uploaded image
             */
            var cloneForm = jQuery.extend(true, {}, $scope.modalForm);
            if (exists($scope.modalForm.level.image)) {
                cloneForm.level.image = fixImageForUpload($scope.modalForm.level.image);
            }
            /*
             * Remove attributes that do not exist in model first
             */
            delete cloneForm.id;
            if (exists(cloneForm.level)) {
                delete cloneForm.level.descriptions;
            }

            $http.post($scope.addURL, cloneForm)
                .success(function (data) {
                    $("#modalSuccessAlert").show();
                    $scope.handleDescriptions();
                    // refresh grid
                    $scope.tableParams.reload();
                })
                .error(function (data) {
                    $scope.handleDescriptions();
                    $("#modalErrorAlert").show();
                });
        }, function(reject){
            return;
        });

    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);