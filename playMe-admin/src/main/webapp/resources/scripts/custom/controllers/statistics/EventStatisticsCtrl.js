myApp.controller('EventStatisticsCtrl', ['$scope', '$http', '$translate',
    function($scope, $http, $translate) {

    $scope.eventsPerDayURL = contextPath +"/statistics/externalevent/day.action";
    $scope.actionsPerDayURL = contextPath +"/statistics/event/day.action";

    $scope.eventsPerDayTitle = $translate.instant('graphMsg.eventsPerDay');
    $scope.actionsPerDayTitle = $translate.instant('graphMsg.actionsPerDay');

    $scope.eventNum = $translate.instant('graphMsg.eventNum');
    $scope.actionNum = $translate.instant('graphMsg.actionNum');

    $scope.defaultDays = 30;

    initializeGraph($scope);


    $scope.loadGraphs = function(days) {
        /**
         * External events per day and type
         */
        $http.get($scope.eventsPerDayURL, { params: {days: days}})
            .success(function (data) {
                $scope.eventsPerDayData = $scope.transformDataForLineChart(data);
            });

        /**
         * Actions per day and type
         */
        $http.get($scope.actionsPerDayURL, { params: {days: days}})
            .success(function (data) {
                $scope.actionsPerDayData = $scope.transformDataForLineChart(data);
            });
    }

   /**
     * Retrieve the data and construct the graphs
     */
    $scope.loadGraphs($scope.defaultDays);


}]);
