myApp.controller('EventCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/event/search.action';
        $scope.addURL = contextPath + '/event/add.action';
        $scope.modalController = 'EventModalCtrl';
        $scope.modalController2 = 'EventModalCtrl2';

        $scope.applicationsListURL = contextPath + '/application/listAll.action';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);

        initializeSecondModal($scope, $modal);

        /**
         * Get list of applications
         */
        $http.get($scope.applicationsListURL)
            .success(function (data) {
                $scope.listOfApplications = data;
            });


        /**
         * Open the modal
         */
        $scope.openViewModal = function(item) {
            $scope.selecteEventId = item.id;
            /*
             * Open modal
             */
            $scope.openSecond();
        }

        /**
         * Configure the datepicker fields
         */
        angular.element(document).ready(function () {
            $('#startDatePicker').datepicker({
                format: 'dd/mm/yyyy'
            });

            $('#endDatePicker').datepicker({
                format: 'dd/mm/yyyy'
            });
        });

}]);


/**
 * Controller for modal instance
 */
myApp.controller('EventModalCtrl', function ($scope, $http, $modalInstance) {

    $scope.ok = function () {
        /*
         * Hide alerts
         */
        $("#modalSuccessAlert").hide();
        $("#modalErrorAlert").hide();

        $http.post($scope.addURL, $scope.modalForm)
            .success(function (data) {
                $("#modalSuccessAlert").show();
                // refresh grid
                $scope.tableParams.reload();
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
            });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});


/**
 * Controller for modal instance
 */
myApp.controller('EventModalCtrl2', function ($scope, $http, $modalInstance, ngTableParams) {

    $scope.searchActionsURL = contextPath + '/action/search.action';

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.tableParams = new ngTableParams({
        page: defaultGridPage,  // show first page
        total: 1,   // if total < count hide pagination
        count: defaultGridPageSize,  // count per page
        sorting: false
    }, {
        counts: [],
        getData: function ($defer, params) {
            /*
             * Load Grid data
             */
            var dataObj = {
                "page": params.$params.page,
                "pagesize": params.$params.count,
                "sortField": 'id',
                "sortDir": 'asc'
            };
            /*
             * Add event id to form
             */
            dataObj.form = {};
            dataObj.form.externaleventhistory = {};
            dataObj.form.externaleventhistory.id = $scope.$parent.selecteEventId;

            $http.post($scope.searchActionsURL, dataObj)
                .success(function (data) {
                    // update table params
                    params.total(data.total);
                    // set new data
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data.items, params.orderBy()) : data.items;

                    $scope.data = orderedData;
                    $defer.resolve(orderedData);
                });
        }
    });

});