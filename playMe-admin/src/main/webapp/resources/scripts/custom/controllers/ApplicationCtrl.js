myApp.controller('ApplicationCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate',
    function($scope, $http, $filter, ngTableParams, $translate) {

        $scope.searchURL = contextPath + '/application/search.action';
        $scope.deleteURL = contextPath + '/application/delete.action';
        $scope.addURL = contextPath + '/application/add.action';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);
}]);
