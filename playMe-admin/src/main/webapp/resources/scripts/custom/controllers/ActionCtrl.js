myApp.controller('ActionCtrl', ['$scope', '$http', '$filter', 'ngTableParams', '$translate', '$modal', '$timeout',
    function($scope, $http, $filter, ngTableParams, $translate, $modal, $timeout) {

        $scope.searchURL = contextPath + '/action/search.action';
        $scope.modalController = 'ActionModalCtrl';

        $scope.applicationsListURL = contextPath + '/application/listAll.action';

        initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate);

        initializeModal($scope, $modal);

        /**
         * Get list of applications
         */
        $http.get($scope.applicationsListURL)
            .success(function (data) {
                $scope.listOfApplications = data;
            });

        /**
         * Open the modal and fill the fields
         */
        $scope.openViewModal = function(item) {
            $scope.event = clone(item);
            /*
             * Open modal
             */
            $scope.open(false);
        }

        /**
         * Configure the datepicker fields
         */
        angular.element(document).ready(function () {
            $('#startDatePicker').datepicker({
                format: 'dd/mm/yyyy'
            });

            $('#endDatePicker').datepicker({
                format: 'dd/mm/yyyy'
            });
        });

}]);



/**
 * Controller for modal instance
 */
myApp.controller('ActionModalCtrl', function ($scope, $http, $modalInstance, ngTableParams) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.tableParams = new ngTableParams({
        page: defaultGridPage,  // show first page
        total: 1,   // if total < count hide pagination
        count: defaultGridPageSize,  // count per page
        sorting: false
    }, {
        counts: []
    });

});