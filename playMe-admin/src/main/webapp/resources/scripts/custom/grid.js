var defaultGridPage = 1;
var defaultGridPageSize = 10;


/* -----------------------------------------
 * Grid initialisation and button handlers
 * -----------------------------------------*/

function initializeFormAndGrid($scope, $http, $filter, ngTableParams, $translate) {
    /*
     * Function to hide alerts section
     */
    $scope.hideAlerts = function() {
        // hide alerts
        if ($("#successAlert")) {
            $("#successAlert").hide();
        }
        if ($("#errorAlert")) {
            $("#errorAlert").hide();
        }
    }

    $scope.showSuccess = function() {
        $scope.hideAlerts();
        if ($("#successAlert")) {
            $("#successAlert").show();
        }
    }

    $scope.showError = function() {
        $scope.hideAlerts();
        if ($("#errorAlert")) {
            $("#errorAlert").show();
        }
    }

    /*
     * Grid configuration
     */
    $scope.tableParams = new ngTableParams({
        page: defaultGridPage,            // show first page
        count: defaultGridPageSize,       // count per page
        sorting: {
            id: 'asc'     // initial sorting
        }
    }, {
        total: 0, // length of data
        getData: function ($defer, params) {
            // hide alerts
            $scope.hideAlerts();

            /*
             * Load Grid data
             */
            var sortInfo = JSON.stringify(params.$params.sorting);
            var dataObj = {
                "page": params.$params.page,
                "pagesize": params.$params.count,
                "sortField": sortInfo.substring(2,sortInfo.indexOf(":")-1),
                "sortDir": sortInfo.substring(sortInfo.indexOf(":")+2,sortInfo.length-2)
            };
            /*
             * Add search form content
             */
            if (!isEmpty($scope.searchForm)) {
                dataObj.form = $scope.searchForm;
            }
            $http.post($scope.searchURL, dataObj)
                .success(function (data) {
                    // update table params
                    params.total(data.total);
                    // set new data
                    // use build-in angular filter
                    var filters = params.filter();
                    var tempDateFilter;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data.items, params.orderBy()) : data.items;

                    /*
                     * Apply filters if needed
                     */
                    if(filters) {
                        orderedData = $filter('filter')(orderedData, filters);
                        filters.Date = tempDateFilter;
                    }

                    $scope.data = orderedData;
                    $defer.resolve(orderedData);
                    //$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                    // set checkboxes
                    if (document.getElementById("select_all")) {
                        setGridCheckboxes($scope);
                    }
                });
        }
    });


    /**
     * Handler for search button
     */
    $scope.searchHandler = function(){
        // reset grid params first
        $scope.tableParams.$params.page = defaultGridPage;
        $scope.tableParams.$params.pagesize = defaultGridPageSize;
        $scope.tableParams.reload();
    }


    /**
     * Handler for add button
     */
    $scope.addHandler = function(){
        /*
         * Call the service
         */
        $http.post($scope.addURL, $scope.searchForm)
            .success(function (data) {
                // refresh grid
                $scope.tableParams.reload();
                $scope.showSuccess();
            })
            .error(function (data) {
                $scope.showError();
            });
    }

    $scope.resetHandler = function () {
        $scope.searchForm = {};
        $scope.form.$setPristine();
        // hide alerts
        $scope.hideAlerts();
        // reset grid params first
        $scope.tableParams.$params.page = defaultGridPage;
        $scope.tableParams.$params.pagesize = defaultGridPageSize;
        // reset grid as well
        $scope.tableParams.reload();
    };


    /**
     * Delete button handler
     */
    $scope.deleteHandler = function(){
        // hide alerts
        $scope.hideAlerts();
        /*
         * Call the service
         */
        $http.post($scope.deleteURL, $scope.checkboxes.checkedItems)
            .success(function (data) {
                // refresh grid
                $scope.tableParams.reload();
                $scope.showSuccess();
            })
            .error(function (data) {
                $scope.showError();
            });
    }
}


/* -----------------------------------------
 * Editable grid initialisation (used to display messages)
 * -----------------------------------------*/

 function initializeEditableGrid($scope, $http, $filter, ngTableParams) {
     /*
      * Grid configuration
      */
     $scope.editableTableParams = new ngTableParams({
         page: defaultGridPage,  // show first page
         total: 1,   // if total < count hide pagination
         count: defaultGridPageSize,  // count per page
         sorting: false
     }, {
         counts: [],
         getData: function($defer, params) {
             $defer.resolve($scope.descriptions.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
     });

}



/* ---------------------------
 * For checkboxes in grids
 * --------------------------*/

 function setGridCheckboxes($scope) {
    $scope.checkboxes = { 'checked': false, items: {}, 'checkedItems':[] };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function(value) {
        angular.forEach($scope.data, function(item) {
            /*
             * Select the proper id
             */
            if (angular.isDefined(item.id)) {
                // messages
                if (angular.isDefined(item.id.messageid) && angular.isDefined(item.id.locale)) {
                    item.messageid = item.id.messageid;
                    item.locale = item.id.locale;
                    item.id = item.id.messageid +'$' +item.id.locale;
                // actions
                } else if (angular.isDefined(item.id.applicationid) && angular.isDefined(item.id.eventid)) {
                    item.id = item.id.applicationid +'$' +item.id.eventid;
                // user messages
                } else if (angular.isDefined(item.id.msgcode) && angular.isDefined(item.id.locale) && angular.isDefined(item.id.applicationid)) {
                    item.msgcode = item.id.msgcode;
                    item.locale = item.id.locale;
                    item.applicationid = item.id.applicationid;
                    item.id = item.id.msgcode +'$' +item.id.applicationid +'$' +item.id.locale;
                }

                $scope.checkboxes.items[item.id] = value;
            } else {
                // profile weights
                if (angular.isDefined(item.fieldname)){
                    item.id = item.fieldname;
                // badges
                } else if (exists(item.badge) && angular.isDefined(item.badge.id)) {
                    item.id = item.badge.id +'$' +item.application.id;
                // levels
                } else if (exists(item.level) && angular.isDefined(item.level.id)) {
                    item.id = item.level.id +'$' +item.application.id;
                }
                // actions
                else if (exists(item.application) && exists(item.eventdefinition)) {
                    item.id = item.application.id +'$' +item.eventdefinition.id;
                }
                $scope.checkboxes.items[item.id] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function(values) {
        if (!$scope.data) {
            return;
        }
        $scope.checkboxes.checkedItems = [];
        var checked = 0, unchecked = 0,
        total = $scope.data.length;
        angular.forEach($scope.data, function(item) {
            checked   +=  ($scope.checkboxes.items[item.id]) || 0;
            unchecked += (!$scope.checkboxes.items[item.id]) || 0;
            // store checked item id
            if (($scope.checkboxes.items[item.id] || 0) && !$scope.checkboxes.checkedItems.contains(item.id)) {
                $scope.checkboxes.checkedItems.push(item.id);
            }
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }
        // grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);
}