/* -----------------------------------------
 * Graph generic functions
 * -----------------------------------------*/

function initializeGraph($scope) {

    /* ---------------------------  */
    /* --- Pie chart functions ---  */
    /* ---------------------------  */

    /**
     * Draws a donut chart
     *
     * @param divId
     * @param data
     * @param title
     */
    $scope.drawDonutChart = function(divId,data,title) {
        $('#' +divId).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                backgroundColor: "#F2EBC5"
            },
            title: {
                text: title
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: title,
                colorByPoint: true,
                data: data
            }]
        });
    }


    /**
     * Removes a graph
     *
     * @param divId
     */
    $scope.drawEmptyDonutChart = function(divId,title) {
        var data2 = [{
            "key" : "A key",
            "values" : [[]]
        }];
        $scope.drawDonutChart(divId,data2,title);
    }


    /**
     * Transforms data from StatisticItem to the format that is expected from donut chart
     *
     * @param data
     * @returns {*}
     */
    $scope.transformDataForDonutChart = function(data) {
        if (exists(data)) {
            var transformedData = [];
            for (var i=0; i<data.length; i++) {
                transformedData.push({name:data[i].code, y:data[i].value});
            }
        }
        return transformedData;
    }


    /* ---------------------------  */
    /* --- Bar chart functions ---  */
    /* ---------------------------  */

    /**
     * Draws a bar chart
     *
     * @param divId
     * @param data
     * @param title
     * @param xLabel
     * @param yLabel
     */
    $scope.drawBarChart = function(divId,data,title,subtitle,xLabel,yLabel) {
        $('#' +divId).highcharts({
            chart: {
                type: 'column',
                backgroundColor: "#F2EBC5"
            },
            title: {
                text: title
            },
            subtitle: {
                text: subtitle
            },
            xAxis: {
                type: 'category',
                title: {
                    text: xLabel
                }
            },
            yAxis: {
                title: {
                    text: yLabel
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
            },
            series: [{
                name: title,
                colorByPoint: true,
                data: data
            }]
        });
    }


    /**
     * Transforms data from StatisticItem to the format that is expected from nvd bar chart
     *
     * @param data
     * @returns {*}
     */
    $scope.transformDataForBarChart = function(data) {
        if (exists(data)) {
            var transformedData = [];
            for (var i=0; i<data.length; i++) {
                transformedData.push([data[i].code, data[i].value]);
            }
        }
        return transformedData;
    }


    /* ---------------------------  */
    /* --- Line chart functions ---  */
    /* ---------------------------  */

    /**
     * Draws a line chart
     *
     * @param divId
     * @param data
     * @param title
     * @param subtitle
     * @param xLabel
     * @param yLabel
     */
    $scope.drawLinehart = function(divId,data,title,subtitle,xLabel,yLabel) {
        $('#' +divId).highcharts({
            chart: {
                type: 'spline',
                backgroundColor: "#F2EBC5"
            },
            title: {
                text: title
            },
            subtitle: {
                text: subtitle
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: xLabel
                }
            },
            yAxis: {
                title: {
                    text: yLabel
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: data
        });
    }


    /**
     * Transforms data from StatisticDateItem to the format that is expected from line chart
     *
     * @param data
     * @returns {*}
     */
    $scope.transformDataForLineChart = function(data) {
        if (exists(data)) {
            var codes = [];
            var transformedData = [];
            var categorizedData = [];

            for (var i=0; i<data.length; i++) {
                if (categorizedData[data[i].code] == null) {
                    categorizedData[data[i].code] = [];
                    codes.push(data[i].code);
                }
                categorizedData[data[i].code].push([data[i].date, data[i].value]);
            }
            for (var i=0; i<codes.length; i++) {
                transformedData.push({data: categorizedData[codes[i]], name:codes[i]});
            }
        }
        return transformedData;
    }


    /**
     * Removes a graph
     *
     * @param divId
     */
    $scope.drawEmptyLineChart = function(divId,title,subtitle,xLabel,yLabel) {
        var data2 = [{
            "key" : "A key",
            "values" : [[]]
        }];
        $scope.drawLinehart(divId,data2,title,subtitle,xLabel,yLabel);
    }
}

