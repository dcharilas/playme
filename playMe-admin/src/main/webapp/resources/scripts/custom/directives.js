myApp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);


myApp.directive('fieldValidation', function ($compile) {
    return {
        restrict: 'E',
        transclude: true,
        scope: true,
        compile: function($scope, element, attrs) {
            var htmlText =
                '<div ng-messages="' +attrs.field +'.$error" role="alert">'+
                    '<div ng-messages-include="error-messages"></div>'+
                    '</div>';
            element.replaceWith($compile(htmlText)($scope));
        }
    }
});


myApp.directive('tabMenu', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: { modelValue: '=ngModel' },  // modelValue for $watch
        link: function (scope, element, attr, ngModel) {

            // Links collection
            var links = element.find('a');

            // Add click listeners
            links.on('click', function (e) {
                e.preventDefault();
                ngModel.$setViewValue(angular.element(this).attr('href'));
                scope.$apply();
            })

            // State handling (set active) on model change
            scope.$watch('modelValue', function () {
                for (var i = 0, l = links.length; i < l; ++i) {
                    var link = angular.element(links[i]);
                    link.attr('href') === scope.modelValue ?
                        link.addClass('active') : link.removeClass('active')
                }
            })
        }
    }
}]);


myApp.directive('firstTabLoad', ['$timeout', function($timeout) {
    return {
        link: function (scope, elem, attrs, ctrl) {
           var loadFirstTab = function() {
                var link = $('ul[data-tab-menu] li:not([class="ng-hide"]) a')[0].href;
                if (exists(link)) {
                    location.href = link;
                }
           }
            $timeout(loadFirstTab, 100);
        }
    };
}]);