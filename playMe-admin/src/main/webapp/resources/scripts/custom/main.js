var languages = ["en","fr"];

/**
 * Import module dependencies
 * @type {*}
 */
var myApp = angular.module('playMe',
	['ngRoute', 'pascalprecht.translate', 'ngTable', 'ui.bootstrap', 'ngMessages', 'ngCookies']
);

myApp.config(['$translateProvider', function($translateProvider) {
  $translateProvider.useStaticFilesLoader({
    prefix: 'resources/messages/locale_',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage('en');
}]);


/**
* The main controller
* 
*/
myApp.controller('MainCtrl', ['$scope', function($scope) {
  $scope.initialSidebarHeight = 580;

  $scope.$on('$routeChangeSuccess', function (event, currRoute, prevRoute) {
		
	});

  $scope.$watch(function(){return angular.element('#Main').height(); }, function(newValue, oldValue) {
      var $sidebar = angular.element('#Nav');
      var $Main = angular.element('#Main');

      if (newValue >= $scope.initialSidebarHeight){
        $sidebar.css("min-height", newValue);
      }else {
        $Main.css("min-height", $scope.initialSidebarHeight);
      }
    });

}]);


myApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
        when('/applications', {
            templateUrl: 'application.action',
            controller: 'ApplicationCtrl'
        }).
        when('/users', {
            templateUrl: 'user.action',
            controller: 'UserCtrl'
        }).
        when('/levels', {
            templateUrl: 'level.action',
            controller: 'LevelCtrl'
        }).
        when('/badges', {
            templateUrl: 'badge.action',
            controller: 'BadgeCtrl'
        }).
        when('/rewards', {
            templateUrl: 'reward.action',
            controller: 'RewardCtrl'
        }).
        when('/events', {
            templateUrl: 'event.action',
            controller: 'EventCtrl'
        }).
        when('/actions', {
            templateUrl: 'action.action',
            controller: 'ActionCtrl'
        }).
        when('/statistics', {
            templateUrl: 'statistics.action',
            controller: 'StatisticsCtrl'
        }).
        when('/userStatistics', {
            templateUrl: 'statistics/users.action',
            controller: 'UserStatisticsCtrl'
        }).
        when('/eventStatistics', {
            templateUrl: 'statistics/events.action',
            controller: 'EventStatisticsCtrl'
        }).
        when('/applicationStatistics', {
            templateUrl: 'statistics/applications.action',
            controller: 'ApplicationStatisticsCtrl'
        }).
        when('/rewardStatistics', {
            templateUrl: 'statistics/rewards.action',
            controller: 'RewardStatisticsCtrl'
        }).
        when('/configuration', {
            templateUrl: 'configuration.action',
            controller: 'ConfigurationCtrl'
        }).
        when('/settings', {
            templateUrl: 'configuration/settings.action',
            controller: 'SettingsCtrl'
        }).
        when('/messages', {
            templateUrl: 'configuration/messages.action',
            controller: 'MessagesCtrl'
        }).
        when('/usermessages', {
            templateUrl: 'configuration/usermessages.action',
            controller: 'UserMessagesCtrl'
        }).
        when('/eventDef', {
            templateUrl: 'configuration/eventDef.action',
            controller: 'EventDefCtrl'
        }).
        when('/eventActions', {
            templateUrl: 'configuration/eventActions.action',
            controller: 'EventActionsCtrl'
        }).
        when('/profileWeights', {
            templateUrl: 'configuration/profileWeights.action',
            controller: 'ProfileWeightsCtrl'
        }).
        otherwise({
            redirectTo: '/statistics'
        });
  }]);


/**
 * Handle user login info in cookie
 */
myApp.run(['$rootScope', '$location', '$cookieStore', '$http', '$document', 'LoginService',
    function ($rootScope, $location, $cookieStore, $http, $document, LoginService) {

        var loginURL = contextPath +'/login.action';
        var d = new Date();
        var n = d.getTime();  //n in ms
        var minutesToLogout = 30*60*1000 //set end time to 30 min from now

        $rootScope.idleEndTime = n+minutesToLogout;

        /**
         * Keep user logged in after page refresh
         */
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        /**
         * Redirect to login page if not logged in
         */
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            if (!$location.absUrl().endsWith("login.action") && !$rootScope.globals.currentUser) {
                location.href = contextPath + '/login.action';
            }
        });

        function logout() {
            LoginService.ClearCredentials();
            /*
             * Redirect to login page
             */
            location.href = loginURL;
        }

        /**
         * Check if user is idle too much time. If yes then logout.
         */
        function checkAndResetIdle() {
            var d = new Date();
            var n = d.getTime();  //n in ms

            if (n>$rootScope.idleEndTime) {
                $document.find('body').off('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart'); //un-monitor events
                logout();
            } else {
                $rootScope.idleEndTime = n+minutesToLogout;
            }
        }
        $document.find('body').on('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart', checkAndResetIdle); //monitor events
    }]);


/* ------------------
 * Prototype overrides
 * ----------------- */

Array.prototype.contains = function(elem) {
    for (var i in this) {
        if (this[i] == elem) return true;
    }
    return false;
}

Array.prototype.clone = function() {
    return this.slice(0);
};

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


/* ------------------
 * Utility functions
 * ----------------- */

function trim(value) {
    value = value.replace(/^\s+/,'');
    value = value.replace(/\s+$/,'');
    return value;
}

function exists(val){
    return (val!=null && val!="" && val!=undefined);
}


 function clone(elem){
    return jQuery.extend(true, {}, elem);
}

function isEmpty(obj) {
    return !exists(obj) || Object.keys(obj).length === 0;
}


function fixImageForUpload(data) {
    return data.replace(/data:image\/jpeg;base64,/g, '')
               .replace(/data:image\/png;base64,/g, '')
               .replace(/data:image\/jpg;base64,/g, '');
}


/* ------------------------
 * Date utility functions
 * ------------------------ */

function getDateTimeDisplay($filter, date) {
    return $filter('date')(date, 'dd/MM/yyyy hh:mm:ss');
}

function getDateDisplay($filter, date) {
    return $filter('date')(date, 'dd/MM/yyyy');
}

function getDateTimeFromString(datestring) {
    //TODO this will not work with current format
    return new Date(datestring);
}

function getDateFromString(datestring) {
    var parts = datestring.split("/");
    return new Date(Number(parts[2]), Number(parts[1]) - 1, Number(parts[0]));
}