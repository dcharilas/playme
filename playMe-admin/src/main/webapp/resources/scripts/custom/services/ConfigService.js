myApp.service('ConfigService', function($http, $q) {

    var addMessagesURL = contextPath + '/configuration/messages/add/bulk.action';

    /**
     * Perform the call to save messages first
     */
    this.addDescriptions = function (descriptions, modalForm) {
        var data = descriptions.clone();
        var deferred = $q.defer();

        /*
         * Remove attributes that do not exist in model first
         */
        for (var i=0; i<data.length; i++) {
            delete data[i].$edit;
            data[i].id = {};
            if (exists(data[i].locale)) {
                data[i].id.locale = data[i].locale;
            }
            delete data[i].locale;
            if (exists(data[i].messageid)) {
                data[i].id.messageid = data[i].messageid;
            }
            delete data[i].messageid;
        }
        /*
         * Save all messages
         */
        $http.post(addMessagesURL, data)
            .success(function (data) {
                /*
                 * Get the id that was assigned to the new messages and
                 * assign it to the badge
                 */
                if (exists(data) && exists(data[0] && exists(data[0].id))) {
                    if (exists(modalForm.badge)) {
                        modalForm.badge.descriptionid = data[0].id.messageid;
                    } else if (exists(modalForm.level)) {
                        modalForm.level.descriptionid = data[0].id.messageid;
                    } else {
                        modalForm.descriptionid = data[0].id.messageid;
                    }
                }
                /*
                 * Resolve the promise
                 */
                deferred.resolve('SUCCESS');
            })
            .error(function (data) {
                $("#modalErrorAlert").show();
                /*
                 * Reject the promise
                 */
                deferred.reject('ERROR');
            });

        //return the promise
        return deferred.promise;
    }

});