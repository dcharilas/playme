myApp.service('TabService', function () {

        /**
         * Marks selected option from tab menu
         */
        this.initialize = function($scope, $rootScope) {
            $scope.$on('$routeChangeStart', function (event, currRoute) {
                if (currRoute != null && currRoute.templateUrl != null) {
                    var currentPath = currRoute.templateUrl.replace(".action", "");
                    /*
                     * Mark tab as selected
                     */
                    angular.forEach($scope.items, function (index) {
                        var href = $scope.prefix + "/" + index.childRef;
                        //clear all classes from the items
                        if (href.indexOf(currentPath) != -1 && currentPath != $scope.prefix) {
                            //add to the selected item the "Selected" class
                            index.cls = "Selected";
                            /*
                             * Mark parent as selected as well
                             */
                            angular.forEach($rootScope.navLinks, function (index) {
                                //clear all classes from the items
                                if (index.href.indexOf($scope.prefix) != -1) {
                                    //add to the selected item the "Selected" class
                                    index.cls = "Selected";
                                } else {
                                    index.cls = "";
                                }
                            });
                        } else {
                            index.cls = "";
                        }
                    });
                }
            });
        }

});