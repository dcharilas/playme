/* -----------------------------------------
 * Modal initialisation
 * -----------------------------------------*/

function initializeModal($scope, $modal) {
    $scope.modalForm = {};

    /**
     * Handler for modal
     *
     * @param size
     */
    $scope.open = function (clean) {
        $scope.isNew = clean;
        if (clean) {
            $scope.modalForm = {};
        }
        $modal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: $scope.modalController,
            scope: $scope,
            size: 'lg',
            resolve: {
                items: function () {
                    //return $scope.items;
                }
            }
        });
        /*
         * Datepickers do not work by default in modal, so we need
         * to initialize them
         */
        setTimeout(function() {
            $('.dateContainer div input').each(function() {
                /*
                 * For birthdate field apply year restrictions
                 */
                if ($(this)[0].name.toLowerCase().indexOf("birth") > -1) {
                    $(this).datepicker({
                        format: 'dd/mm/yyyy',
                        startDate: '01/01/1940',
                        endDate: '31/12/2001'
                    });
                } else {
                    $(this).datepicker({
                        format: 'dd/mm/yyyy'
                    });
                }
            });
        }, 100);
    };
}



function initializeSecondModal($scope, $modal) {
    $scope.modalForm = {};

    /**
     * In case we need a second modal as well
     */
    $scope.openSecond = function () {
        $modal.open({
            animation: true,
            templateUrl: 'myModalContent2.html',
            controller: $scope.modalController2,
            scope: $scope,
            size: 'lg',
            resolve: {
                items: function () {
                    //return $scope.items;
                }
            }
        });
    };
}

