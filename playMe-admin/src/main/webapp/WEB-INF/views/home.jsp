<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<jsp:include page="templates/head.jsp"></jsp:include>

<script>
    var PROPERTIES = ${properties};
    var contextPath = "${pageContext.request.contextPath}";
</script>

<body ng-app="playMe" ng-controller="MainCtrl">

<div id="Container1">
    <div id="Toolbar" ng-controller="HeaderCtrl">
        <jsp:include page="templates/common/header.tlp.html"></jsp:include>
    </div>

    <div id="Container2">
        <div id="Banner">
            <div id="Identity">
                <a href="#"><img src="<spring:url value="/resources/images/logo.jpg" />" alt="levelUp" /></a>
            </div><!-- Identity -->
        </div><!-- Banner -->
        <div id="Container3">
            <div id="Nav" ng-controller="SidebarCtrl">
                <ul>
                    <li ng-repeat="item in navLinks" class="{{item.cls}}" ng-click="showChilds(item)" ng-show="showPage(item.pageId)">
                        <a href="#"><span class="{{item.icon}}"></span></a>
                        <a href="{{item.href}}" translate>{{item.text}}</a>
                        <ul ng-show="item.active" class="subItem">
                            <li ng-repeat="subItem in item.subItems" class="{{subItem.cls}}" ng-show="showPage(subItem.pageId)">
                                <a href="{{subItem.href}}" translate>{{subItem.text}}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- Nav -->
            <div ng-view id="Main">

            </div> <!-- Main -->
        </div><!-- Container3 -->
    </div><!-- Container2 -->
</div><!-- Container1 -->

<div id="Footer" ng-controller="FooterCtrl">
    <jsp:include page="templates/common/footer.tlp.html"></jsp:include>
</div><!-- Footer -->


</body>
</html>