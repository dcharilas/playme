<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include tabular menu -->
<div ng-controller="ConfigurationCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>

<!-- Include form validation messages -->
<jsp:include page="../frags/formMessages.jsp"></jsp:include>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>

<div id="gridArea">
    <button id="deleteBtn" ng-click="deleteHandler()" class="btn btn-danger submit-button">
        <span class="glyphicon glyphicon-minus-sign"></span>{{ 'formMsg.delete' | translate}}
    </button>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="setting in $data">
            <td width="30" style="text-align: left" header="'ng-table/headers/checkbox.html'">
                <input type="checkbox" ng-model="checkboxes.items[setting.id]" />
            </td>
            <td data-title="'Id'" sortable="'setting.id'">{{setting.id}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{setting.application.name}}</td>
            <td data-title="'Setting name'" sortable="'setting.property'">{{setting.property}}</td>
            <td data-title="'Value'" sortable="'setting.value'">{{setting.value}}</td>
            <td data-title="'Action'"><a ng-click="openEditModal(setting)"><img src="resources/images/edit-icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>


<script type="text/ng-template" id="ng-table/headers/checkbox.html">
    <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
</script>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configAction' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>

        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplNameModal">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputApplNameModal" id="inputApplNameModal" ng-model="modalForm.application.id" placeholder="" required>
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputApplNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-2'>
                <div class="form-group">
                    <label for="inputPropertyModal">{{ 'formMsg.settingName' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputPropertyModal" id="inputPropertyModal" ng-model="modalForm.property" placeholder="" ng-maxlength="100" required/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputPropertyModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-2'>
                <div class="form-group">
                    <label for="inputValueModal">{{ 'formMsg.settingValue' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputValueModal" id="inputValueModal" ng-model="modalForm.value" placeholder="" ng-maxlength="30" required/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputValueModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>