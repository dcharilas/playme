<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include tabular menu -->
<div ng-controller="ConfigurationCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>

<!-- Include form validation messages -->
<jsp:include page="../frags/formMessages.jsp"></jsp:include>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputMsgCode">{{ 'formMsg.msgCode' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputMsgCode" ng-model="searchForm.id.msgcode" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfUserMessageCodes}">
                            <option value="${item}">${item}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-md-1'>
                <div class="form-group">
                    <label for="inputLocale">{{ 'formMsg.locale' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputLocale" ng-model="searchForm.id.locale" placeholder="">
                        <option value=""> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="en">EN</option>
                        <option value="fr">FR</option>
                    </select>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>

<div id="gridArea">
    <button id="deleteBtn" ng-click="deleteHandler()" class="btn btn-danger submit-button">
        <span class="glyphicon glyphicon-minus-sign"></span>{{ 'formMsg.delete' | translate}}
    </button>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="message in $data">
            <td width="30" style="text-align: left" header="'ng-table/headers/checkbox.html'">
                <input type="checkbox" ng-model="checkboxes.items[message.id]" />
            </td>
            <td data-title="'Message Code'" sortable="'id.msgcode'">{{message.msgcode}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{message.application.name}}</td>
            <td data-title="'Locale'" sortable="'id.locale'">{{message.locale}}</td>
            <td data-title="'Value'" sortable="'message'">{{message.message}}</td>
            <td data-title="'Action'"><a ng-click="openEditModal(message)"><img src="resources/images/edit-icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>


<script type="text/ng-template" id="ng-table/headers/checkbox.html">
    <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
</script>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configMessage' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>

        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplNameModal">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputApplNameModal" id="inputApplNameModal" ng-model="modalForm.application.id" placeholder="" required>
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputApplNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputMsgCodeModal">{{ 'formMsg.msgCode' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputMsgCodeModal" ng-model="modalForm.msgcode" placeholder="" required>
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfUserMessageCodes}">
                            <option value="${item}">${item}</option>
                        </c:forEach>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputMsgCodeModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputLocaleModal">{{ 'formMsg.locale' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputLocaleModal" id="inputLocaleModal" ng-model="modalForm.locale" placeholder="" required>
                        <option value=""> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="en">EN</option>
                        <option value="fr">FR</option>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputLocaleModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-10'>
                    <div class="form-group">
                        <label for="inputValueModal">{{ 'formMsg.msgContent' | translate}}</label>
                        <textarea class="form-control bigtext" name="inputValueModal" id="inputValueModal" ng-model="modalForm.message" placeholder="" required ng-maxlength="1000" />
                        <!-- Form validation message -->
                        <div ng-messages="form.inputValueModal.$error" role="alert">
                            <div ng-messages-include="error-messages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>
