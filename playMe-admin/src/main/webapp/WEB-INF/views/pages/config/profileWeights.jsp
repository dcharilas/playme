<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include tabular menu -->
<div ng-controller="ConfigurationCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>

<!-- Include form validation messages -->
<jsp:include page="../frags/formMessages.jsp"></jsp:include>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div id="buttonGroup">
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
        </div>
    </form>
</div>

<div id="gridArea">
    <button id="deleteBtn" ng-click="deleteHandler()" class="btn btn-danger submit-button">
        <span class="glyphicon glyphicon-minus-sign"></span>{{ 'formMsg.delete' | translate}}
    </button>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="weight in $data">
            <td width="30" style="text-align: left" header="'ng-table/headers/checkbox.html'">
                <input type="checkbox" ng-model="checkboxes.items[weight.fieldname]" />
            </td>
            <td data-title="'Field Name'" sortable="'fieldname'">{{weight.fieldname}}</td>
            <td data-title="'Weight'" sortable="'weight'">{{weight.weight}}</td>
            <td data-title="'Action'"><a ng-click="openEditModal(weight)"><img src="resources/images/edit-icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>


<script type="text/ng-template" id="ng-table/headers/checkbox.html">
    <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
</script>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configProfileWeight' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>

        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputFieldNameModal">{{ 'formMsg.fieldName' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputFieldNameModal" id="inputFieldNameModal" ng-model="modalForm.fieldname" placeholder="" required ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputFieldNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputWeightModal">{{ 'formMsg.weight' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputWeightModal" id="inputWeightModal" ng-model="modalForm.weight" placeholder="" required ng-maxlength="11"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputWeightModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>