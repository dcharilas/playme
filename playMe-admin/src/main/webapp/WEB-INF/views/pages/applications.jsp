<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include form validation messages -->
<jsp:include page="../pages/frags/formMessages.jsp"></jsp:include>

<!-- Alerts section -->
<div id="successAlert" class="alert alert-success" style="display:none;">
    <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
</div>
<div id="errorAlert" class="alert alert-danger alert-error" style="display:none;">
    <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
</div>

<div id="myForm">
    <form name="form" ng-submit="form.$valid && addHandler()" novalidate class="formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputName">{{ 'formMsg.applName' | translate}}</label>
                    <input class="form-control" name="inputName" id="inputName" ng-model="searchForm.name" placeholder="" required ng-maxlength="50">
                    <!-- Form validation message -->
                    <div ng-messages="form.inputName.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="submit" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>


<div id="gridArea">
    <button id="deleteBtn" ng-click="deleteHandler()" class="btn btn-danger submit-button">
        <span class="glyphicon glyphicon-minus-sign"></span>{{ 'formMsg.delete' | translate}}
    </button>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="appl in $data">
            <td width="30" style="text-align: left" header="'ng-table/headers/checkbox.html'"><input type="checkbox" ng-model="checkboxes.items[appl.id]"/></td>
            <td data-title="'Id'" sortable="'id'">{{appl.id}}</td>
            <td data-title="'Name'" sortable="'name'">{{appl.name}}</td>
        </tr>
    </table>
</div>



<script type="text/ng-template" id="ng-table/headers/checkbox.html">
    <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
</script>