<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include form validation messages -->
<jsp:include page="../pages/frags/formMessages.jsp"></jsp:include>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder=""
                            ng-options="option.id as option.name for option in listOfApplications">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputName">{{ 'formMsg.rewardName' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputName" ng-model="searchForm.name" placeholder="" />
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputActive">{{ 'formMsg.active' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputActive" ng-model="searchForm.active" placeholder="">
                        <option value="" style="display:none" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="true" selected="selected">true</option>
                        <option value="false">false</option>
                    </select>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>


<div id="gridArea">
    <button id="deleteBtn" ng-click="deleteHandler()" class="btn btn-danger submit-button">
        <span class="glyphicon glyphicon-minus-sign"></span>{{ 'formMsg.delete' | translate}}
    </button>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="reward in $data">
            <td width="30" style="text-align: left" header="'ng-table/headers/checkbox.html'">
                <input type="checkbox" ng-model="checkboxes.items[reward.id]" />
            </td>
            <td data-title="'Id'" sortable="'id'">{{reward.id}}</td>
            <td data-title="'Name'" sortable="'name'">{{reward.name}}</td>
            <td data-title="'Description Id'" sortable="'descriptionid'">{{reward.descriptionid}}</td>
            <td data-title="'Active'" sortable="'active'">{{reward.active}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{reward.application.name}}</td>
            <td data-title="'Points Required'" sortable="'pointsreq'">{{reward.pointsreq}}</td>
            <td data-title="'Action'"><a ng-click="openEditModal(reward)"><img src="resources/images/edit-icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>


<script type="text/ng-template" id="ng-table/headers/checkbox.html">
    <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
</script>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configReward' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>


        <div class="avatarDiv">
            <div align="center">
                <!-- If image does not exist show default one -->
                <img class="avatar" ng-src="{{modalForm.inactiveimage && modalForm.inactiveimage || 'resources/images/reward-inactive.jpg'}}" id="photo-id"/>
            </div>
            <div id="browseBtn" style="position:relative;">
                <a class='btn btn-primary' href='javascript:;'>
                    {{ 'formMsg.uploadInactiveImg' | translate}}
                    <input id="fileUploadButton" type="file" name="file_source" size="40" fileread="modalForm.inactiveimage"
                           onchange='$("#upload-file-info-2").html($(this).val());' />
                </a>
            </div>
        </div>

        <div class="avatarDiv">
            <div align="center">
                <!-- If image does not exist show default one -->
                <img class="avatar" ng-src="{{modalForm.activeimage && modalForm.activeimage || 'resources/images/reward.jpg'}}" id="photo-id-2"/>
            </div>
            <div id="browseBtn" style="position:relative;">
                <a class='btn btn-primary' href='javascript:;'>
                    {{ 'formMsg.uploadActiveImg' | translate}}
                    <input id="fileUploadButton" type="file" name="file_source" size="40" fileread="modalForm.activeimage"
                           onchange='$("#upload-file-info").html($(this).val());' />
                </a>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplNameModal">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputApplNameModal" id="inputApplNameModal" ng-model="modalForm.application.id" placeholder="" required
                            ng-options="option.id as option.name for option in listOfApplications">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputApplNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputActiveModal">{{ 'formMsg.active' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputActiveModal" id="inputActiveModal" ng-model="modalForm.active" placeholder="" required>
                        <option value="" style="display:none" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="true" selected="selected">true</option>
                        <option value="false">false</option>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputActiveModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputNameModal">{{ 'formMsg.rewardName' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputNameModal" id="inputNameModal" ng-model="modalForm.name" placeholder="" required ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputPointsModal">{{ 'formMsg.pointsReq' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputPointsModal" id="inputPointsModal" ng-model="modalForm.pointsreq" required placeholder="" ng-min="0"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputPointsModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <!-- Show table with descriptions -->
            <table ng-table="editableTableParams" class="table marginTop">
                <tr ng-repeat="message in $data">
                    <td data-title="'Id'" width="50">{{message.messageid}}</td>
                    <td data-title="'Locale'" width="80">{{message.locale}}</td>
                    <td data-title="'Reward Description'">
                        <span ng-if="!message.$edit">{{message.value}}</span>
                        <div ng-if="message.$edit"><input class="form-control" ng-model="message.value"/></div>
                    </td>
                    <td data-title="'Action'" width="80">
                        <a ng-if="!message.$edit" href="" class="btn btn-default btn-xs" ng-click="message.$edit = true">Edit</a>
                        <a ng-if="message.$edit" href="" class="btn btn-primary btn-xs" ng-click="message.$edit = false">Save</a>
                    </td>
                </tr>
            </table>
        </div>

        <input type="hidden" id="inputRewardIdModal" ng-model="modalForm.id" value="${modalForm.id}" />
        <input type="hidden" id="inputDescriptionIdModal" ng-model="modalForm.descriptionid" value="${modalForm.descriptionid}" />

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>