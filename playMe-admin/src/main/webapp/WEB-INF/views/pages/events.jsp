<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include form validation messages -->
<jsp:include page="../pages/frags/formMessages.jsp"></jsp:include>

<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder=""
                            ng-options="option.id as option.name for option in listOfApplications">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.eventType' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputEventName" ng-model="searchForm.eventdefinition.id" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfEventTypes}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputUsername">{{ 'formMsg.username' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputUsername" ng-model="searchForm.user.username" placeholder="" />
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.startDate' | translate}}</label>
                    <div class="dateContainer">
                        <div class="input-group input-append date" id="startDatePicker">
                            <input type="text" class="form-control" name="startDate" ng-model="searchForm.searchDateStart"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.endDate' | translate}}</label>
                    <div class="dateContainer">
                        <div class="input-group input-append date" id="endDatePicker">
                            <input type="text" class="form-control" name="endDate" ng-model="searchForm.searchDateEnd"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>

<div id="gridArea">
    <div id="deleteBtn"></div>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="event in $data">
            <td data-title="'Id'" sortable="'id'">{{event.id}}</td>
            <td data-title="'Name'" sortable="'eventdefinition.name'">{{event.eventdefinition.name}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{event.application.name}}</td>
            <td data-title="'Username'" sortable="'user.username'">{{event.user.username}}</td>
            <td data-title="'Date'" sortable="'eventdate'" filter="{ 'Date': 'text' }">{{event.eventdate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
            <td data-title="'Number 1'" sortable="'numericvalue1'">{{event.numericvalue1}}</td>
            <td data-title="'Number 2'" sortable="numericvalue2">{{event.numericvalue2}}</td>
            <td data-title="'Text 1'" sortable="'textvalue1'">{{event.textvalue1}}</td>
            <td data-title="'Text 2'" sortable="'textvalue2'">{{event.textvalue2}}</td>
            <td data-title="'Comments'" sortable="'comments'">{{event.comments}}</td>
            <td data-title="'Action'"><a ng-click="openViewModal(event)"><img src="resources/images/magnifying_glass_icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configEvent' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>

        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplNameModal">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputApplNameModal" id="inputApplNameModal" ng-model="modalForm.application.id" placeholder="" required
                            ng-options="option.id as option.name for option in listOfApplications">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputApplNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventNameModal">{{ 'formMsg.eventType' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputEventNameModal" id="inputEventNameModal" ng-model="modalForm.eventdefinition.id" placeholder="" required>
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                            <c:forEach var="item" items="${listOfEventTypes}">
                                <option value="${item.id}">${item.name}</option>
                            </c:forEach>
                     </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputEventNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputUsernameModal">{{ 'formMsg.username' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputUsernameModal" id="inputUsernameModal" ng-model="modalForm.user.username" placeholder="" ng-maxlength="30" required/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputUsernameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputNumber1Modal">{{ 'formMsg.numberValue1' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputNumber1Modal" id="inputNumber1Modal" ng-model="modalForm.numericvalue1" placeholder="" ng-maxlength="11"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputNumber1Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputNumber2Modal">{{ 'formMsg.numberValue2' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputNumber2Modal" id="inputNumber2Modal" ng-model="modalForm.numberValue2" placeholder="" ng-maxlength="11"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputNumber2Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputNumber3Modal">{{ 'formMsg.numberValue3' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputNumber3Modal" id="inputNumber3Modal" ng-model="modalForm.numericvalue3" placeholder="" ng-maxlength="11"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputNumber1Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputNumber4Modal">{{ 'formMsg.numberValue4' | translate}}</label>
                    <input type="number" class="form-control multi-column-form" name="inputNumber4Modal" id="inputNumber4Modal" ng-model="modalForm.numberValue4" placeholder="" ng-maxlength="11"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputNumber2Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputText1Modal">{{ 'formMsg.textValue1' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputText1Modal" id="inputText1Modal" ng-model="modalForm.textvalue1" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputText1Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputText2Modal">{{ 'formMsg.textValue2' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputText2Modal" id="inputText2Modal" ng-model="modalForm.textvalue2" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputText2Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputText3Modal">{{ 'formMsg.textValue3' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputText3Modal" id="inputText3Modal" ng-model="modalForm.textvalue3" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputText1Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputText4Modal">{{ 'formMsg.textValue4' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputText4Modal" id="inputText4Modal" ng-model="modalForm.textvalue4" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputText2Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-10'>
                <div class="form-group">
                    <label for="inputCommentsModal">{{ 'formMsg.comments' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputCommentsModal" id="inputCommentsModal" ng-model="modalForm.comments" placeholder="" ng-maxlength="200"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputText2Modal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="inputEventIdModal" ng-model="modalForm.id" value="${modalForm.id}" />

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>



<!-- Popup form -->
<script type="text/ng-template" id="myModalContent2.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.triggerActions' | translate}}</h3>
    </div>

    <table ng-table="tableParams" class="table">
        <tr ng-repeat="event in $data">
            <td data-title="'Id'">{{event.id}}</td>
            <td data-title="'Event Name'">{{event.eventdefinition.name}}</td>
            <td data-title="'Application'">{{event.application.name}}</td>
            <td data-title="'Username'">{{event.user.username}}</td>
            <td data-title="'Date'" filter="{ 'Date': 'text' }">{{event.eventdate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
            <td data-title="'Badge Award'" >{{event.badge.name}}</td>
            <td data-title="'Level Award'" >{{event.level.name}}</td>
            <td data-title="'Points Award'" >{{event.pointsawarded}}</td>
        </tr>
    </table>

    <div class="modal-footer">
        <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
    </div>
</script>