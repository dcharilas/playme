<div ng-controller="ConfigurationCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)" first-tab-load>
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>