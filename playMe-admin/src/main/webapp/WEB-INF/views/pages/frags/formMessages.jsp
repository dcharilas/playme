
<script type="text/ng-template" id="error-messages">
    <div ng-message="required" class="error-messages">This field is required</div>
    <div ng-message="minlength" class="error-messages">This field is too short</div>
    <div ng-message="maxlength" class="error-messages">This field is too long</div>
    <div ng-message="email" class="error-messages">This needs to be a valid email</div>
    <div ng-message="number" class="error-messages">Only numbers are allowed in this field</div>
    <div ng-message="min" class="error-messages">This number needs to be greater than 0</div>
</script>