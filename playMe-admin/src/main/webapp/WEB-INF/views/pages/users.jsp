<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include form validation messages -->
<jsp:include page="../pages/frags/formMessages.jsp"></jsp:include>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputUsername">{{ 'formMsg.username' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputUsername" ng-model="searchForm.username" placeholder="">
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputLastname">{{ 'formMsg.lastname' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputLastname" ng-model="searchForm.lastname" placeholder="">
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputFirstname">{{ 'formMsg.firstname' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputFirstname" ng-model="searchForm.firstname" placeholder="">
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputGender">{{ 'formMsg.gender' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputGender" ng-model="searchForm.gender" placeholder="">
                        <option value="" style="display:none" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="M" selected="selected">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" type="button" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="addNewBtn" type="button" ng-click="open(true)" class="btn btn-primary submit-button">
                <span class="glyphicon glyphicon-plus-sign"></span>{{ 'formMsg.addNew' | translate}}
            </button>
            <button id="clearBtn" type="button" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>


<div id="gridArea">
    <div id="deleteBtn"></div>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="user in $data">
            <td data-title="'Id'" sortable="'id'">{{user.id}}</td>
            <td data-title="'Username'" sortable="'username'">{{user.username}}</td>
            <td data-title="'Firstname'" sortable="'firstname'">{{user.firstname}}</td>
            <td data-title="'Lastname'" sortable="'lastname'">{{user.lastname}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{user.application.name}}</td>
            <td data-title="'Points'" sortable="'points'">{{user.points}}</td>
            <td data-title="'Redeem. Points'" sortable="'redeemablepoints'">{{user.redeemablepoints}}</td>
            <td data-title="'Level'" sortable="'level.name'">{{user.level.name}}</td>
            <td data-title="'Gender'" sortable="'gender'">{{user.gender}}</td>
            <td data-title="'Age'" sortable="'age'">{{user.age}}</td>
            <td data-title="'Address'" sortable="'address'">{{user.address}}</td>
            <td data-title="'Phone'" sortable="'phone'">{{user.phone}}</td>
            <td data-title="'E-mail'" sortable="'email'">{{user.email}}</td>
            <td data-title="'Reg. Date'" sortable="'regDate'" filter="{ 'Date': 'text' }">{{user.regDate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
            <td data-title="'Prof. Compl. %'">{{user.completeness}}</td>
            <td data-title="'Action'">
                <a ng-click="openEditModal(user)"><img src="resources/images/edit-icon.png" class="iconLink"/></a>
                <a ng-click="showUserBadges(user)"><img src="resources/images/Star-Icon.png" class="iconLink"/></a>
            </td>
        </tr>
    </table>
</div>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.configUser' | translate}}</h3>
    </div>
    <form name="form" class="form-inline" ng-submit="form.$valid && ok()" novalidate>

        <!-- Alerts section -->
        <div id="modalSuccessAlert" class="alert alert-success" style="display:none;">
            <strong>Success!</strong> {{ 'notificationMsg.dataSaved' | translate}}
        </div>
        <div id="modalErrorAlert" class="alert alert-danger alert-error" style="display:none;">
            <strong>Error!</strong> {{ 'notificationMsg.dataNotSaved' | translate}}
        </div>

        <div class="avatarDiv">
            <div align="center">
                <!-- If image does not exist show default one -->
                <img class="avatar" ng-src="{{modalForm.avatar && modalForm.avatar || 'resources/images/user.png'}}" id="photo-id"/>
            </div>
            <!--            <div ng-show="{{showAvatar}}">
                            <img class="avatar" ng-src="data:image/jpeg;base64,{{modalForm.avatar}}" id="photo-id"/>
                        </div>-->
            <div id="browseBtn" style="position:relative;">
                <a class='btn btn-primary buttonWidth' href='javascript:;'>
                    {{ 'formMsg.uploadAvatar' | translate}}
                    <input id="fileUploadButton" type="file" name="file_source" size="40" fileread="modalForm.avatar"
                           onchange="angular.element(this).scope().uploadHandler(this)" />
                </a>
            </div>
        </div>

        <div class='formRow'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplNameModal">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" name="inputApplNameModal" id="inputApplNameModal" ng-model="modalForm.application.id" placeholder="" required>
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfApplications}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputApplNameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputUsernameModal">{{ 'formMsg.username' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputUsernameModal" id="inputUsernameModal" ng-model="modalForm.username" placeholder="" ng-maxlength="30" required/>
                    <!-- Form validation message -->
                    <!--<field-validation field="form.inputUsernameModal" />-->
                    <div ng-messages="form.inputUsernameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputLastnameModal">{{ 'formMsg.lastname' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputLastnameModal" id="inputLastnameModal" ng-model="modalForm.lastname" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputLastnameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='formRow'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputFirstnameModal">{{ 'formMsg.firstname' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputFirstnameModal" id="inputFirstnameModal" ng-model="modalForm.firstname" placeholder="" ng-maxlength="100"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputFirstnameModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputGenderModal">{{ 'formMsg.gender' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputGenderModal" ng-model="modalForm.gender" placeholder="">
                        <option value="" style="display:none" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <option value="M" selected="selected">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputPhoneModal">{{ 'formMsg.phone' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputPhoneModal" id="inputPhoneModal" ng-model="modalForm.phone" placeholder="" ng-minlength="7" ng-maxlength="15"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputPhoneModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='formRow'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputAddressModal">{{ 'formMsg.address' | translate}}</label>
                    <input class="form-control multi-column-form" name="inputAddressModal" id="inputAddressModal" ng-model="modalForm.address" placeholder="" ng-maxlength="150"/>
                    <!-- Form validation message -->
                    <div ng-messages="form.inputAddressModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputPostcodeModal">{{ 'formMsg.postcode' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputPostcodeModal" ng-model="modalForm.postcode" placeholder="" />
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEmailModal">{{ 'formMsg.email' | translate}}</label>
                    <input class="form-control multi-column-form" type="email" name="inputEmailModal" id="inputEmailModal" ng-model="modalForm.email" placeholder="" />
                    <!-- Form validation message -->
                    <div ng-messages="form.inputEmailModal.$error" role="alert">
                        <div ng-messages-include="error-messages"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='formRow'>
            <!-- Show only for existing users -->
            <div ng-show="{{!isNew}}">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="inputRegDateModal">{{ 'formMsg.regDate' | translate}}</label>
                        <div class="dateContainer">
                            <div class="input-group input-append date" id="inputRegDateModal">
                                <input type="text" class="form-control" name="inputRegDateModal" ng-model="modalForm.regDate"/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-4'>
                <div class="form-group">
                    <label for="inputBirthDateModal">{{ 'formMsg.birthDate' | translate}}</label>
                    <div class="dateContainer">
                        <div class="input-group input-append date" id="inputBirthDateModal">
                            <input type="text" class="form-control" name="inputBirthDateModal" ng-model="modalForm.birthdate"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary submit-button" value="{{ 'formMsg.save' | translate}}" />
            <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>
    </form>
</script>


<!-- Popup form -->
<script type="text/ng-template" id="myModalContent2.html">
    <div class="modal-content modal-content-collection">

        <div class="modal-header">
            <h3 class="modal-title">{{ 'modalMsg.userBadges' | translate}}</h3>
        </div>

        <div ng-repeat="badge in badges" class="col-md-3">
            <div>
                <img class="avatar badgeCollection hvr-bounce-in playful-border"
                     ng-src="data:image/jpeg;base64,{{badge.activeimage}}"/>
                <figcaption class="fig-caption">{{badge.name}}</figcaption>
            </div>
        </div>

        <div class="modal-footer modal-footer-collection">
            <button class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
        </div>

    </div>
</script>