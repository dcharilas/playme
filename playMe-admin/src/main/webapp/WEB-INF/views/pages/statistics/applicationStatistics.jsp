<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include tabular menu -->
<div ng-controller="StatisticsCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>


<div id="myForm">
    <form name="form" novalidate class="formStyle radio-form">
        <!-- Application select -->
        <div class='col-md-3'>
            <div class="form-group">
                <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                <select class="form-control multi-column-form" id="inputApplName" ng-model="applicationId" ng-change="loadGraphs()">
                    <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    <c:forEach var="item" items="${listOfApplications}">
                        <option value="${item.id}">${item.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <!-- Number of days select -->
        <div class="radio-selection" align="right">
            <input type="radio" id="numberofdays-30" name="numberofdays" value="30" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-30">30 days</label>
            <input type="radio" id="numberofdays-60" name="numberofdays" value="60" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-60">60 days</label>
            <input type="radio" id="numberofdays-90" name="numberofdays" value="90" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-90">90 days</label>
        </div>
    </form>
</div>

<div id="gridArea">
    <div class="row">
        <div id="eventsPerDayChart" class="col-md-6"></div>
        <div id="actionsPerDayChart" class="col-md-6"></div>
    </div>
    <div class="row">
        <div id="usersPerDayChart" class="col-md-6"></div>
        <div id="userGenderChart" class="col-md-2"></div>
        <div id="userAgeChart" class="col-md-2"></div>
    </div>
    <div class="row">
        <div id="rewardsPerTypeChart" class="col-md-2"></div>
        <div id="redeemedPointsPerDayChart" class="col-md-6"></div>
    </div>
</div>


<script>
    var scope = $('#Main').scope();

    /**
     * Draw line chart for events
     */
    scope.$watch("eventsPerDayData", function(n, o){
        var data = $('#Main').scope().eventsPerDayData;
        var divId = 'eventsPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.eventsPerDayTitle,null,null,null)
            return;
        }
        scope.drawLinehart(divId,data,scope.eventsPerDayTitle,null,null,null);
    });


    /**
     * Draw line chart for actions
     */
    scope.$watch("actionsPerDayData", function(n, o){
        var data = $('#Main').scope().actionsPerDayData;
        var divId = 'actionsPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.actionsPerDayTitle,null,null,null);
            return;
        }
        return scope.drawLinehart(divId,data,scope.actionsPerDayTitle,null,null,null);
    });


    /**
     * Draw line chart for users
     */
    scope.$watch("usersPerDayData", function(n, o){
        var data = $('#Main').scope().usersPerDayData;
        var divId = 'usersPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.usersPerDayTitle,null,null,null);
            return;
        }
        return scope.drawLinehart(divId,data,scope.usersPerDayTitle,null,null,null);
    });


    /**
     * Draw donut chart for user gender
     */
    scope.$watch("genderData", function(n, o){
        var data = $('#Main').scope().genderData;
        if (!exists(data)) {
            scope.drawEmptyDonutChart('userGenderChart',scope.genderUsersTitle);
            return;
        }
        return scope.drawDonutChart('userGenderChart',data,scope.genderUsersTitle);
    });


    /**
     * Draw donut chart for user age
     */
    scope.$watch("userAgeData", function(n, o){
        var data = $('#Main').scope().userAgeData;
        if (!exists(data)) {
            scope.drawEmptyDonutChart('userAgeChart',scope.ageUsersTitle);
            return;
        }
        return scope.drawDonutChart('userAgeChart',data,scope.ageUsersTitle);
    });


    /**
     * Draw donut chart for redeemed reward types
     */
    scope.$watch("rewardTypesData", function(n, o){
        var data = $('#Main').scope().rewardTypesData;
        if (!exists(data)) {
            scope.drawEmptyDonutChart('rewardsPerTypeChart',scope.rewardTypeTitle);
            return;
        }
        return scope.drawDonutChart('rewardsPerTypeChart',data,scope.rewardTypeTitle);
    });


    /**
     * Draw line chart for redeemed points
     */
    scope.$watch("redeemedPointsData", function(n, o){
        var data = $('#Main').scope().redeemedPointsData;
        var divId = 'redeemedPointsPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.redeemedPointsTitle,null,null,null);
            return;
        }
        return scope.drawLinehart(divId,data,scope.redeemedPointsTitle,null,null,null);
    });


</script>