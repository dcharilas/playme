<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!-- Include tabular menu -->
<div ng-controller="StatisticsCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>


<div id="gridArea">
    <div class="row">
        <div id="rewardPerAppChart" class="col-md-5"></div>
    </div>
</div>


<script>
    var scope = $('#Main').scope();

    /**
     * Draw bar chart for redeemed rewards
     */
    scope.$watch("redeemedRewards", function(n, o){
        var data = $('#Main').scope().redeemedRewards;
        if (!exists(data)) {
            return;
        }
        return scope.drawBarChart('rewardPerAppChart',data,scope.redeemedRewardsTitle,null,scope.applications,scope.rewardNum);
    });



</script>