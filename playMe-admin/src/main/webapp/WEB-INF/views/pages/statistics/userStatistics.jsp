<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!-- Include tabular menu -->
<div ng-controller="StatisticsCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>


<div id="gridArea">
    <div class="row">
        <div id="appUserChart" class="col-md-5"></div>
        <div id="userLevelSeqChart" class="col-md-5"></div>
        <div id="userGenderChart" class="col-md-2"></div>
        <div id="userAgeChart" class="col-md-2"></div>
    </div>
</div>


<script>
    var scope = $('#Main').scope();

    /**
     * Draw bar chart for application users
     */
    scope.$watch("applicationUserData", function(n, o){
        var data = $('#Main').scope().applicationUserData;
        if (!exists(data)) {
            return;
        }
        return scope.drawBarChart('appUserChart',data,scope.appUsersTitle,null,scope.applications,scope.userNum);
    });

    /**
     * Draw bar chart for users distribution per level sequence
     */
    scope.$watch("userLevelSequenceData", function(n, o){
        var data = $('#Main').scope().userLevelSequenceData;
        if (!exists(data)) {
            return;
        }
        return scope.drawBarChart('userLevelSeqChart',data,scope.levelSeqUsersTitle,null,scope.levelSeq,scope.userNum);
    });

    /**
     * Draw donut chart for user gender
     */
    scope.$watch("genderData", function(n, o){
        var data = $('#Main').scope().genderData;
        if (!exists(data)) {
            return;
        }
        return scope.drawDonutChart('userGenderChart',data,scope.genderUsersTitle);
    });


    /**
     * Draw donut chart for user age
     */
    scope.$watch("userAgeData", function(n, o){
        var data = $('#Main').scope().userAgeData;
        if (!exists(data)) {
            return;
        }
        return scope.drawDonutChart('userAgeChart',data,scope.ageUsersTitle);
    });

</script>