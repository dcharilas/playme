<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- Include tabular menu -->
<div ng-controller="StatisticsCtrl">
    <ul data-tab-menu data-ng-model="ui.tabview" class="list-inline">
        <li ng-repeat="item in items" class="{{item.cls}}" ng-show="showPage(item.pageId)">
            <a href="{{item.href}}" translate>{{item.text}}</a>
        </li>
    </ul>
</div>


<div id="myForm">
    <form name="form" novalidate class="formStyle">
        <!-- Number of days select -->
        <div class="radio-selection radio-selection-solo">
            <input type="radio" id="numberofdays-30" name="numberofdays" value="30" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-30">30 days</label>
            <input type="radio" id="numberofdays-60" name="numberofdays" value="60" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-60">60 days</label>
            <input type="radio" id="numberofdays-90" name="numberofdays" value="90" ng-model="defaultDays" ng-change="loadGraphs(defaultDays)">
            <label class="radio-inline radio-label" for="numberofdays-90">90 days</label>
        </div>
    </form>
</div>

<div id="gridArea">
    <div class="row">
        <div id="eventsPerDayChart" class="col-md-6"></div>
        <div id="actionsPerDayChart" class="col-md-6"></div>
    </div>
</div>


<script>
    var scope = $('#Main').scope();

    /**
     * Draw line chart for events
     */
    scope.$watch("eventsPerDayData", function(n, o){
        var data = $('#Main').scope().eventsPerDayData;
        var divId = 'eventsPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.eventsPerDayTitle,null,null,null);
            return;
        }
        scope.drawLinehart(divId,data,scope.eventsPerDayTitle,null,null,scope.eventNum);
    });


    /**
     * Draw line chart for actions
     */
    scope.$watch("actionsPerDayData", function(n, o){
        var data = $('#Main').scope().actionsPerDayData;
        var divId = 'actionsPerDayChart';
        if (!exists(data)) {
            scope.drawEmptyLineChart(divId,scope.actionsPerDayTitle,null,null,null);
            return;
        }
        return scope.drawLinehart(divId,data,scope.actionsPerDayTitle,null,null,scope.actionNum);
    });


</script>