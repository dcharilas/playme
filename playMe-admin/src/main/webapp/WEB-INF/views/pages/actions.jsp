<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div id="myForm">
    <form name="form" class="form-inline formStyle">
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputApplName">{{ 'formMsg.applName' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputApplName" ng-model="searchForm.application.id" placeholder=""
                            ng-options="option.id as option.name for option in listOfApplications">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.eventType' | translate}}</label>
                    <select class="form-control multi-column-form" id="inputEventName" ng-model="searchForm.eventdefinition.id" placeholder="">
                        <option value="" selected="selected"> - {{ 'formMsg.select' | translate}} -</option>
                        <c:forEach var="item" items="${listOfEventTypes}">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputUsername">{{ 'formMsg.username' | translate}}</label>
                    <input class="form-control multi-column-form" id="inputUsername" ng-model="searchForm.user.username" placeholder="" />
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.startDate' | translate}}</label>
                    <div class="dateContainer">
                        <div class="input-group input-append date" id="startDatePicker">
                            <input type="text" class="form-control" name="startDate" ng-model="searchForm.searchDateStart"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                    <label for="inputEventName">{{ 'formMsg.endDate' | translate}}</label>
                    <div class="dateContainer">
                        <div class="input-group input-append date" id="endDatePicker">
                            <input type="text" class="form-control" name="endDate" ng-model="searchForm.searchDateEnd"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="buttonGroup">
            <button id="searchBtn" ng-click="searchHandler()" class="btn btn-primary submit-button">{{ 'formMsg.search' | translate}}</button>
            <button id="clearBtn" ng-click="resetHandler()" class="btn btn-primary submit-button">{{ 'formMsg.clear' | translate}}</button>
        </div>
    </form>
</div>

<div id="gridArea">
    <div id="deleteBtn"></div>
    <table ng-table="tableParams" class="table">
        <tr ng-repeat="event in $data">
            <td data-title="'Id'" sortable="'id'">{{event.id}}</td>
            <td data-title="'Event Name'" sortable="'eventdefinition.name'">{{event.eventdefinition.name}}</td>
            <td data-title="'Application'" sortable="'application.name'">{{event.application.name}}</td>
            <td data-title="'Username'" sortable="'user.username'">{{event.user.username}}</td>
            <td data-title="'Date'" sortable="'eventdate'" filter="{ 'Date': 'text' }">{{event.eventdate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
            <td data-title="'Badge Award'" sortable="'badge.name'">{{event.badge.name}}</td>
            <td data-title="'Level Award'" sortable="'level.name'">{{event.level.name}}</td>
            <td data-title="'Points Award'" sortable="'user.username'">{{event.pointsawarded}}</td>
            <td data-title="'Action'"><a ng-click="openViewModal(event.externaleventhistory)"><img src="resources/images/magnifying_glass_icon.png" class="iconLink"/></a></td>
        </tr>
    </table>
</div>



<!-- Popup form -->
<script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
        <h3 class="modal-title">{{ 'modalMsg.triggerEvent' | translate}}</h3>
    </div>

    <table ng-table="tableParams" class="table">
        <tr>
            <td data-title="'Id'">{{event.id}}</td>
            <td data-title="'Name'">{{event.eventdefinition.name}}</td>
            <td data-title="'Application'">{{event.application.name}}</td>
            <td data-title="'Username'">{{event.user.username}}</td>
            <td data-title="'Date'" filter="{ 'Date': 'text' }">{{event.eventdate | date:'dd/MM/yyyy hh:mm:ss'}}</td>
            <td data-title="'Number 1'">{{event.numericvalue1}}</td>
            <td data-title="'Number 2'">{{event.numericvalue2}}</td>
            <td data-title="'Text 1'">{{event.textvalue1}}</td>
            <td data-title="'Text 2'">{{event.textvalue2}}</td>
            <td data-title="'Comments'">{{event.comments}}</td>
        </tr>
    </table>

    <div class="modal-footer">
        <button type="button" class="btn btn-warning" ng-click="cancel()">{{ 'formMsg.close' | translate}}</button>
    </div>
</script>