<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<jsp:include page="templates/head.jsp"></jsp:include>

<script>
    var contextPath = "${pageContext.request.contextPath}";
</script>

<body ng-app="playMe" ng-controller="MainCtrl">

<div id="Container1">
    <div id="Toolbar" class="loginToolbar" ng-controller="HeaderCtrl">
        <!-- In toolbar show only language change -->
        <div id="tLang">
            <form method="get" action="">
                <div>
                    <select id="LanguageSelection" ng-model="language" ng-change="changeLanguage(language.value)" ng-options="c.name for c in languages"></select>
                </div>
            </form>
        </div><!-- tLang -->
    </div>

    <div id="loginContainer" ng-controller="LoginCtrl">

        <div class="col-md-3 col-md-offset-4 loginForm">
            <h2 class="loginTitle">{{ 'loginMsg.login' | translate}}</h2>
            <div ng-show="error" class="alert alert-danger">{{error}}</div>
            <form name="form" ng-submit="login()" role="form">
                <div class="form-group" ng-class="{ 'has-error': form.username.$dirty && form.username.$error.required }">
                    <label for="username">{{ 'loginMsg.username' | translate}}</label>
                    <input type="text" name="username" id="username" class="form-control" ng-model="username" required />
                    <span ng-show="form.username.$dirty && form.username.$error.required" class="help-block">Username is required</span>
                </div>
                <div class="form-group" ng-class="{ 'has-error': form.password.$dirty && form.password.$error.required }">
                    <label for="password">{{ 'loginMsg.password' | translate}}</label>
                    <input type="password" name="password" id="password" class="form-control" ng-model="password" required />
                    <span ng-show="form.password.$dirty && form.password.$error.required" class="help-block">Password is required</span>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-actions loginButton">
                    <button type="submit" <%--ng-disabled="form.$invalid || dataLoading"--%> class="btn btn-primary">{{ 'loginMsg.login' | translate}}</button>
                    <img ng-if="dataLoading" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                </div>
            </form>
        </div>

    </div><!-- loginContainer -->
</div><!-- Container1 -->

<div id="Footer" ng-controller="FooterCtrl">
    <jsp:include page="templates/common/footer.tlp.html"></jsp:include>
</div><!-- Footer -->


</body>
</html>

tUser
tOptions