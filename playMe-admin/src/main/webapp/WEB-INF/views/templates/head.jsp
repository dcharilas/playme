<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
    <meta charset="utf-8">
    <title>levelUP Admin</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- CSS -->
    <link data-require="ng-table@*" data-semver="0.3.0" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ng-table/0.3.3/ng-table.min.css" />
    <link data-require="bootstrap-css@*" data-semver="3.3.4" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">

    <link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/screen.css" />" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/theme.css" />" media="screen, projection">

    <!-- Jquery libraries -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

    <!-- Angular libraries -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.1/angular.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-route.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bower-angular-translate/2.6.1/angular-translate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bower-angular-translate-loader-static-files/2.6.1/angular-translate-loader-static-files.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ng-table/0.3.3/ng-table.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-messages.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-cookies.min.js"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.0/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <!-- Graphs (Highcharts) -->
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/highcharts-more.js"></script>

    <!-- Custom scripts -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/main.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/directives.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/grid.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/modal.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/graph.js" />"></script>

    <!-- Services -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/services/ConfigService.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/services/LoginService.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/services/TabService.js" />"></script>

    <!-- Controllers -->
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/LoginCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/CommonCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/ApplicationCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/UserCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/LevelCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/BadgeCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/RewardCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/EventCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/ActionCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/SystemConfigCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/TabMenuCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/statistics/UserStatisticsCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/statistics/RewardStatisticsCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/statistics/EventStatisticsCtrl.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/resources/scripts/custom/controllers/statistics/ApplicationStatisticsCtrl.js" />"></script>

</head>