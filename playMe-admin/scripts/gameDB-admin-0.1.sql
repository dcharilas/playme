use playmeadmin;

DROP TABLE IF EXISTS `RolePageAccess`;
DROP TABLE IF EXISTS `RoleActionAccess`;
DROP TABLE IF EXISTS `UserApplicationAccess`;
DROP TABLE IF EXISTS `AdminUser`;
DROP TABLE IF EXISTS `Role`;

CREATE TABLE `Role` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `role` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `RolePageAccess` (
  `roleid` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `pageid` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`roleid`,`pageid`),
  FOREIGN KEY (`roleid`) REFERENCES `Role` (`id`)
);

CREATE TABLE `RoleActionAccess` (
  `roleid` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `mapping` VARCHAR(20) NOT NULL,
  `action` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`roleid`,`mapping`,`action`),
  FOREIGN KEY (`roleid`) REFERENCES `Role` (`id`)
);


CREATE TABLE `AdminUser` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `username` VARCHAR(30) NOT NULL,
  `password` VARCHAR(30) NOT NULL,
  `roleid` INT NOT NULL,
  `firstname` VARCHAR(100) NULL DEFAULT NULL,
  `lastname` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`roleid`) REFERENCES `Role` (`id`)
);


CREATE TABLE `UserApplicationAccess` (
  `userid` INT NOT NULL,
  `applicationid` INT NOT NULL,
  PRIMARY KEY (`userid`, `applicationid`),
  FOREIGN KEY (`userid`) REFERENCES `AdminUser` (`id`)
);


INSERT INTO `Role` VALUES (1,'ADMIN');
INSERT INTO `Role` VALUES (2,'ANALYST');

INSERT INTO `AdminUser` VALUES (1,'admin','password',1,'Jim','Slothius');
INSERT INTO `AdminUser` VALUES (2,'analyst','password',2,'Nick','S');
INSERT INTO `UserApplicationAccess` VALUES (2,25);
INSERT INTO `UserApplicationAccess` VALUES (2,50);
INSERT INTO `RolePageAccess` VALUES (2,'page_stat');
INSERT INTO `RolePageAccess` VALUES (2,'page_stat_app');

INSERT INTO `RoleActionAccess` VALUES (2,'home','');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','applications');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','application/externalevent/day');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','application/event/day');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','application/user/day');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','application/user/gender');
INSERT INTO `RoleActionAccess` VALUES (2,'statistics','application/user/age');