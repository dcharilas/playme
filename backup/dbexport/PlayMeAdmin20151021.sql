create schema playmeadmin;
use playmeadmin;

-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: playmeadmin
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adminuser`
--

DROP TABLE IF EXISTS `adminuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `roleid` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleid` (`roleid`),
  CONSTRAINT `adminuser_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adminuser`
--

LOCK TABLES `adminuser` WRITE;
/*!40000 ALTER TABLE `adminuser` DISABLE KEYS */;
INSERT INTO `adminuser` VALUES (1,'admin','password',1,'Jim','Slothius'),(2,'analyst','password',2,'Nick','S');
/*!40000 ALTER TABLE `adminuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN'),(2,'ANALYST');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roleactionaccess`
--

DROP TABLE IF EXISTS `roleactionaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roleactionaccess` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `mapping` varchar(20) NOT NULL,
  `action` varchar(50) NOT NULL,
  PRIMARY KEY (`roleid`,`mapping`,`action`),
  CONSTRAINT `roleactionaccess_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roleactionaccess`
--

LOCK TABLES `roleactionaccess` WRITE;
/*!40000 ALTER TABLE `roleactionaccess` DISABLE KEYS */;
INSERT INTO `roleactionaccess` VALUES (2,'home',''),(2,'statistics',''),(2,'statistics','application/event/day'),(2,'statistics','application/externalevent/day'),(2,'statistics','application/user/age'),(2,'statistics','application/user/day'),(2,'statistics','application/user/gender'),(2,'statistics','applications');
/*!40000 ALTER TABLE `roleactionaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolepageaccess`
--

DROP TABLE IF EXISTS `rolepageaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolepageaccess` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` varchar(30) NOT NULL,
  PRIMARY KEY (`roleid`,`pageid`),
  CONSTRAINT `rolepageaccess_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolepageaccess`
--

LOCK TABLES `rolepageaccess` WRITE;
/*!40000 ALTER TABLE `rolepageaccess` DISABLE KEYS */;
INSERT INTO `rolepageaccess` VALUES (2,'page_stat'),(2,'page_stat_app');
/*!40000 ALTER TABLE `rolepageaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userapplicationaccess`
--

DROP TABLE IF EXISTS `userapplicationaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userapplicationaccess` (
  `userid` int(11) NOT NULL,
  `applicationid` int(11) NOT NULL,
  PRIMARY KEY (`userid`,`applicationid`),
  CONSTRAINT `userapplicationaccess_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `adminuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userapplicationaccess`
--

LOCK TABLES `userapplicationaccess` WRITE;
/*!40000 ALTER TABLE `userapplicationaccess` DISABLE KEYS */;
INSERT INTO `userapplicationaccess` VALUES (2,25),(2,50);
/*!40000 ALTER TABLE `userapplicationaccess` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-21 15:00:35
